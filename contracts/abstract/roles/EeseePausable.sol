// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/security/Pausable.sol";
import "./EeseeRoleHandler.sol";

abstract contract EeseePausable is Pausable, EeseeRoleHandler {
    ///@dev Pauser role in {accessManager}.
    bytes32 public constant PAUSER_ROLE = keccak256("PAUSER_ROLE");

    modifier onlyPauser(){
        _validateRole(PAUSER_ROLE, _msgSender());
        _;
    }

    // ============ Admin Methods ============

    /**
    * @dev Called by the PAUSER_ROLE to pause, triggers stopped state.
    */
    function pause() onlyPauser external virtual {
        _pause();
    }

    /**
     * @dev Called by the PAUSER_ROLE to unpause, returns to normal state.
     */
    function unpause() onlyPauser external virtual {
        _unpause();
    }
}