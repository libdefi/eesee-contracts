// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/utils/Context.sol";
import "../../interfaces/IEeseeAccessManager.sol";

abstract contract EeseeRoleHandler is Context {
    ///@dev Access manager for Eesee contract ecosystem.
    IEeseeAccessManager public immutable accessManager;

    error CallerNotAuthorized();
    error InvalidAccessManager();

    constructor(IEeseeAccessManager _accessManager) {
        if(address(_accessManager) == address(0)) revert InvalidAccessManager();
        accessManager = _accessManager;
    }

    modifier onlyRole(bytes32 role){
        _validateRole(role, _msgSender());
        _;
    }

    function _hasRole(bytes32 role, address _addr) internal view virtual returns (bool) {
        return accessManager.hasRole(role, _addr);
    }

    function _validateRole(bytes32 role, address _addr) internal view {
        if(!_hasRole(role, _addr)) revert CallerNotAuthorized();
    }
}