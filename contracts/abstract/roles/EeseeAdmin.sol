// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "./EeseeRoleHandler.sol";

abstract contract EeseeAdmin is EeseeRoleHandler {
    ///@dev Admin role in {accessManager}.
    bytes32 public immutable ADMIN_ROLE;

    constructor() {
        ADMIN_ROLE = accessManager.ADMIN_ROLE();
    }

    modifier onlyAdmin(){
        _validateRole(ADMIN_ROLE, _msgSender());
        _;
    }
}