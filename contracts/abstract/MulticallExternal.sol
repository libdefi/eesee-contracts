// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../types/Call.sol";

abstract contract MulticallExternal {
    /// @dev Blacklist for functions.
    mapping(address => mapping(bytes4 => bool)) internal blacklist;

    event AddToBlacklist(address indexed target, bytes4 indexed functionSelector);
    event RemoveFromBlacklist(address indexed target, bytes4 indexed functionSelector);
    event MulticallFailed(bytes err);

    error MulticallReverted(bytes err);
    error FunctionBlacklisted(address target, bytes4 functionSignature);

    /**
     * @dev Call any contract's function.
     * @param calls - Stuct describing the call.
     */
    function multicall(Call[] calldata calls) external {
        for (uint256 i; i < calls.length;) {
            bytes4 functionSelector = bytes4(calls[i].callData[:4]);
            if(blacklist[calls[i].target][functionSelector]) revert FunctionBlacklisted(calls[i].target, functionSelector);

            (bool success, bytes memory err) = calls[i].target.call(calls[i].callData);
            if (!success) revert MulticallReverted(err);
            unchecked { ++i; }
        }
    }

    function _tryMulticall(Call[] memory calls) internal {
        if(calls.length == 0) return;
        try this.multicall(calls) {} catch (bytes memory err) {
            emit MulticallFailed(err);
        }
    }

    function _addToBlacklist(address target, bytes4 functionSignature) internal {
        if(!blacklist[target][functionSignature]){
            blacklist[target][functionSignature] = true;
            emit AddToBlacklist(target, functionSignature);
        }
    }

    function _removeFromBlacklist(address target, bytes4 functionSignature) internal {
        if(blacklist[target][functionSignature]){
            blacklist[target][functionSignature] = false;
            emit RemoveFromBlacklist(target, functionSignature);
        }
    }
}