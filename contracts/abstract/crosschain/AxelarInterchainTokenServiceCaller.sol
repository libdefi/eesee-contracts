// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import { IInterchainTokenService } from '@axelar-network/interchain-token-service/contracts/interfaces/IInterchainTokenService.sol';
import { InterchainTokenExecutable } from '@axelar-network/interchain-token-service/contracts/executable/InterchainTokenExecutable.sol';
import { AddressBytes } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/libs/AddressBytes.sol';
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "../../libraries/SafeApproveMax.sol";
import "../../types/AddressWithChain.sol";
import "../../errors/EeseeErrors.sol";
import "../../interfaces/ICallerBase.sol";

abstract contract AxelarInterchainTokenServiceCaller is InterchainTokenExecutable, ICallerBase {
    using SafeERC20 for IERC20;
    using SafeApproveMax for IERC20;
    using AddressBytes for address;
    using AddressBytes for bytes;

    bytes32 constant public AXELAR_INTERCHAIN_TOKEN_SERVICE_CALLER_IDENTIFIER = keccak256("AXELAR_INTERCHAIN_TOKEN_SERVICE_CALLER");

    error InvalidInterchainTokenService();

    constructor(address _interchainTokenService) InterchainTokenExecutable(_interchainTokenService) {
        if(interchainTokenService == address(0)) revert InvalidInterchainTokenService();
    }

    // ============ Internal Write Functions ============

    function _sendAxelarMessageWithInterchainToken(
        address useTokensFrom,
        AddressWithChain memory destination,
        bytes32 tokenId,
        uint256 tokenAmount,
        bytes memory payload,
        uint256 gas
    ) internal {
        if(tokenAmount == 0) revert InvalidAmount();
        IERC20 token = IERC20(IInterchainTokenService(interchainTokenService).validTokenAddress(tokenId));
        if(address(token) == address(0)) revert InvalidToken();
        _transferTokens(token, useTokensFrom, tokenAmount);
        token.safeApproveMax(tokenAmount, interchainTokenService);

        IInterchainTokenService(interchainTokenService).callContractWithInterchainToken{value: gas}(
            tokenId,
            abi.decode(destination.chainSelector, (string)),
            destination._address.toBytes(),
            tokenAmount,
            payload,
            gas
        );

        emit CrosschainSend(AXELAR_INTERCHAIN_TOKEN_SERVICE_CALLER_IDENTIFIER, destination, abi.encode(token, tokenAmount, gas));
    }

    function _executeWithInterchainToken(
        bytes32 commandId,
        string calldata sourceChain,
        bytes calldata sourceAddress,
        bytes calldata payload,
        bytes32 tokenId,
        address token,
        uint256 amount
    ) internal override {
        AddressWithChain memory source = AddressWithChain({
            chainSelector: abi.encode(sourceChain),
            _address: sourceAddress.toAddress()
        });
        IERC20 _token = IERC20(token);
        _executeWithInterchainToken(commandId, source, payload, tokenId, _token, amount);

        emit CrosschainReceive(AXELAR_INTERCHAIN_TOKEN_SERVICE_CALLER_IDENTIFIER, source, abi.encode(_token, amount));
    }

    function _executeWithInterchainToken(
        bytes32 commandId,
        AddressWithChain memory source,
        bytes calldata payload,
        bytes32 tokenId,
        IERC20 token,
        uint256 amount
    ) internal virtual;

    function _transferTokens(IERC20 token, address useTokensFrom, uint256 tokenAmount) internal virtual {
        if(useTokensFrom != address(this)) token.safeTransferFrom(useTokensFrom, address(this), tokenAmount);
    }
}
