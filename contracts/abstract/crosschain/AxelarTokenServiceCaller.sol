// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import { IAxelarGateway } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/interfaces/IAxelarGateway.sol';
import { StringToAddress } from "@axelar-network/axelar-gmp-sdk-solidity/contracts/libs/AddressString.sol";
import { IAxelarGasService } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/interfaces/IAxelarGasService.sol';
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "../../libraries/SafeApproveMax.sol";
import "../../types/AddressWithChain.sol";
import "../../errors/EeseeErrors.sol";
import "../../interfaces/ICallerBase.sol";
import "./AxelarGatewayBase.sol";

abstract contract AxelarTokenServiceCaller is AxelarGatewayBase, ICallerBase {
    using SafeERC20 for IERC20;
    using SafeApproveMax for IERC20;
    using Strings for address;
    using StringToAddress for string;

    bytes32 constant public AXELAR_TOKEN_SERVICE_CALLER_IDENTIFIER = keccak256("AXELAR_TOKEN_SERVICE_CALLER");

    // ============ Internal Write Functions ============

    function _sendAxelarMessageWithToken(
        address useTokensFrom,
        AddressWithChain memory destination,
        string memory tokenSymbol,
        uint256 tokenAmount,
        bytes memory payload,
        uint256 gas
    ) internal {
        if(tokenAmount == 0) revert InvalidAmount();
        IERC20 token = IERC20(gateway.tokenAddresses(tokenSymbol));
        if(address(token) == address(0)) revert InvalidToken();
        _transferTokens(token, useTokensFrom, tokenAmount);
        token.safeApproveMax(tokenAmount, address(gateway));

        string memory destinationString = destination._address.toHexString();
        string memory chainSelector = abi.decode(destination.chainSelector, (string));

        gasService.payNativeGasForContractCallWithToken{value: gas}(
            address(this),
            chainSelector,
            destinationString,
            payload,
            tokenSymbol,
            tokenAmount,
            tx.origin
        );
        gateway.callContractWithToken(
            chainSelector,
            destinationString, 
            payload, 
            tokenSymbol, 
            tokenAmount
        );

        emit CrosschainSend(AXELAR_TOKEN_SERVICE_CALLER_IDENTIFIER, destination, abi.encode(token, tokenAmount, gas));
    }

    function executeWithToken(
        bytes32 commandId,
        string calldata sourceChain,
        string calldata sourceAddress,
        bytes calldata payload,
        string calldata tokenSymbol,
        uint256 amount
    ) external {
        bytes32 payloadHash = keccak256(payload);

        if (
            !gateway.validateContractCallAndMint(
                commandId,
                sourceChain,
                sourceAddress,
                payloadHash,
                tokenSymbol,
                amount
            )
        ) revert NotApprovedByGateway();

        AddressWithChain memory source = AddressWithChain({
            chainSelector: abi.encode(sourceChain),
            _address: sourceAddress.toAddress()
        });
        IERC20 token = IERC20(gateway.tokenAddresses(tokenSymbol));
        _executeWithToken(source, payload, token, amount);

        emit CrosschainReceive(AXELAR_TOKEN_SERVICE_CALLER_IDENTIFIER, source, abi.encode(token, amount));
    }

    function _executeWithToken(
        AddressWithChain memory source,
        bytes calldata payload,
        IERC20 token,
        uint256 amount
    ) internal virtual;

    function _transferTokens(IERC20 token, address useTokensFrom, uint256 tokenAmount) internal virtual {
        if(useTokensFrom != address(this)) token.safeTransferFrom(useTokensFrom, address(this), tokenAmount);
    }
}
