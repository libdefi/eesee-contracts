// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import { IAxelarGateway } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/interfaces/IAxelarGateway.sol';
import { StringToAddress } from "@axelar-network/axelar-gmp-sdk-solidity/contracts/libs/AddressString.sol";
import { IAxelarGasService } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/interfaces/IAxelarGasService.sol';
import "@openzeppelin/contracts/utils/Strings.sol";
import "../../types/AddressWithChain.sol";
import "../../interfaces/ICallerBase.sol";
import "./AxelarGatewayBase.sol";

abstract contract AxelarCaller is AxelarGatewayBase, ICallerBase {
    using Strings for address;
    using StringToAddress for string;

    bytes32 constant public AXELAR_CALLER_IDENTIFIER = keccak256("AXELAR_CALLER");

    // ============ Internal Write Functions ============

    function _sendAxelarMessage(
        AddressWithChain memory destination,
        bytes memory payload,
        uint256 gas
    ) internal {
        string memory destinationString = destination._address.toHexString();
        string memory chainSelector = abi.decode(destination.chainSelector, (string));

        gasService.payNativeGasForContractCall{value: gas}(
            address(this),
            chainSelector,
            destinationString,
            payload,
            tx.origin
        );
        gateway.callContract(
            chainSelector, 
            destinationString, 
            payload
        );

        emit CrosschainSend(AXELAR_CALLER_IDENTIFIER, destination, abi.encode(gas));
    }

    function execute(
        bytes32 commandId,
        string calldata sourceChain,
        string calldata sourceAddress,
        bytes calldata payload
    ) external {
        bytes32 payloadHash = keccak256(payload);
        if (!gateway.validateContractCall(commandId, sourceChain, sourceAddress, payloadHash))
            revert NotApprovedByGateway();

        AddressWithChain memory source = AddressWithChain({
            chainSelector: abi.encode(sourceChain),
            _address: sourceAddress.toAddress()
        });
        _execute(source, payload);

        emit CrosschainReceive(AXELAR_CALLER_IDENTIFIER, source, "");
    }

    function _execute(
        AddressWithChain memory source,
        bytes calldata payload
    ) internal virtual;
}
