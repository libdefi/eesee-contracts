// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import {IAny2EVMMessageReceiver} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IAny2EVMMessageReceiver.sol";
import {Client} from "@chainlink/contracts-ccip/src/v0.8/ccip/libraries/Client.sol";
import {IRouterClient} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IRouterClient.sol";
import "@openzeppelin/contracts/utils/introspection/ERC165.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "../../types/AddressWithChain.sol";
import "../../libraries/SafeApproveMax.sol";
import "../../errors/EeseeErrors.sol";
import "../../interfaces/ICallerBase.sol";

abstract contract CCIPCaller is ICallerBase, IAny2EVMMessageReceiver, ERC165 {
    using SafeERC20 for IERC20;
    using SafeApproveMax for IERC20;

    IRouterClient public immutable CCIPRouter;
    bytes32 constant public CCIP_CALLER_IDENTIFIER = keccak256("CCIP_CALLER");

    error InvalidRouter();

    modifier onlyRouter() {
        if(msg.sender != address(CCIPRouter)) revert InvalidRouter();
        _;
    }

    constructor(IRouterClient _CCIPRouter) {
        if(address(_CCIPRouter) == address(0)) revert InvalidRouter();
        CCIPRouter = _CCIPRouter;
    }

    // ============ External Write Functions ============

    /// @inheritdoc IAny2EVMMessageReceiver
    function ccipReceive(Client.Any2EVMMessage calldata message) external virtual override onlyRouter {
        AddressWithChain memory source = AddressWithChain({
            chainSelector: abi.encode(message.sourceChainSelector),
            _address: abi.decode(message.sender, (address))
        });
        _ccipReceive(
            source, 
            message.data,
            message.destTokenAmounts
        );

        emit CrosschainReceive(CCIP_CALLER_IDENTIFIER, source, abi.encode(message.messageId, message.destTokenAmounts));
    }

    // ============ Public View Functions ============

    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC165) returns (bool) {
        return interfaceId == type(IAny2EVMMessageReceiver).interfaceId || super.supportsInterface(interfaceId);
    }

    // ============ Internal Write Functions ============

    function _sendCCIPMessage(
        address useTokensFrom,
        AddressWithChain memory destination,
        bytes memory data,
        Client.EVMTokenAmount[] memory tokenAmounts,
        uint256 value,
        uint256 gasLimit
    ) internal returns (bytes32 messageId, uint256 fee) {
        for(uint256 i; i < tokenAmounts.length;){
            _transferTokens(IERC20(tokenAmounts[i].token), useTokensFrom, tokenAmounts[i].amount);
            IERC20(tokenAmounts[i].token).safeApproveMax(tokenAmounts[i].amount, address(CCIPRouter));
            unchecked{ ++i; }
        }

        Client.EVM2AnyMessage memory evm2AnyMessage = Client.EVM2AnyMessage({
            receiver: abi.encode(destination._address),
            data: data,
            tokenAmounts: tokenAmounts,
            extraArgs: Client._argsToBytes(Client.EVMExtraArgsV1({gasLimit: gasLimit})),
            feeToken: address(0)
        });

        uint64 chainSelector = abi.decode(destination.chainSelector, (uint64));
        fee = CCIPRouter.getFee(chainSelector, evm2AnyMessage);
        if(value > fee){
            // tx.origin is always an EOA, so unsuccessful transfers are impossible.
            unchecked { tx.origin.call{value: value - fee}(""); }
        } else if (value != fee) revert InsufficientValue();

        messageId = CCIPRouter.ccipSend{value: fee}(chainSelector, evm2AnyMessage);
        
        emit CrosschainSend(CCIP_CALLER_IDENTIFIER, destination, abi.encode(messageId, tokenAmounts, fee));
    }

    function _ccipReceive(
        AddressWithChain memory source, 
        bytes memory data, 
        Client.EVMTokenAmount[] memory destTokenAmounts
    ) internal virtual;

    function _transferTokens(IERC20 token, address useTokensFrom, uint256 tokenAmount) internal virtual {
        if(useTokensFrom != address(this)) token.safeTransferFrom(useTokensFrom, address(this), tokenAmount);
    }
}