// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import { IAxelarGateway } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/interfaces/IAxelarGateway.sol';
import { IAxelarGasService } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/interfaces/IAxelarGasService.sol';

abstract contract AxelarGatewayBase {
    /// @dev Axelar gateway.
    IAxelarGateway public immutable gateway;
    /// @dev Gas service for Axelar.
    IAxelarGasService immutable public gasService;

    error InvalidGasService();
    error InvalidGateway();
    error NotApprovedByGateway();

    constructor(IAxelarGateway _gateway, IAxelarGasService _gasService) {
        if(address(_gateway) == address(0)) revert InvalidGateway();
        if(address(_gasService) == address(0)) revert InvalidGasService();

        gateway = _gateway;
        gasService = _gasService;
    }
}
