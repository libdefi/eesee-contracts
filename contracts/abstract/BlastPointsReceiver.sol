
// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

interface IBlastPoints {
	function configurePointsOperator(address operator) external;
}

abstract contract BlastPointsReceiver {
    error InvalidPointsOperator();
	constructor(IBlastPoints _blastPoints, address _pointsOperator) {
        if(address(_blastPoints) != address(0)){
            if(_pointsOperator == address(0)) revert InvalidPointsOperator();
		    _blastPoints.configurePointsOperator(_pointsOperator);
        } else if(_pointsOperator != address(0)) revert InvalidPointsOperator();
	}
}
