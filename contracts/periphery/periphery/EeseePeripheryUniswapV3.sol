// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "./EeseePeripheryBase.sol";
import "../abstract/EeseeUniswapV3Base.sol";

contract EeseePeripheryUniswapV3 is EeseePeripheryBase, EeseeUniswapV3Base {
    constructor(
        address _uniswapRouterV3, 
        IEeseeAccessManager _accessManager,
        address _trustedForwarder
    ) EeseeUniswapV3Base(_uniswapRouterV3, _accessManager, _trustedForwarder){}
}