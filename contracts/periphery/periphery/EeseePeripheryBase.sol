// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../../libraries/SafeApproveMax.sol";
import "../abstract/EeseeExecuteWithSwapBase.sol";
import "../../interfaces/IEeseePeriphery.sol";

abstract contract EeseePeripheryBase is IEeseePeriphery, EeseeExecuteWithSwapBase {
    using SafeApproveMax for IERC20;
    using SafeERC20 for IERC20;

    error ExecutionFailed(bytes err);

    // =============== External Write Functions ===============

    /**
     * @dev Collects tokens from sender, swaps them for dstToken and calls {target} contract with {targetCalldata}.
     * @param swapData - Data for swap.
     * @param target - Target to call with {targetCalldata}.
     * @param targetCalldata - Data to call {target} with.
    
     * @return srcToken - Source token used in swap.
     * @return srcSpent - Source token amount spent in swap.
     * @return srcDust - Source token dust returned to msg.sender.
     * @return dstToken - Destination token returned from swap.
     * @return dstAmount - Destination token amount spent or recieved in {target} call.
     * @return dstDust - Destination token dust returned to msg.sender.
     * @return returnData - Data returned from {target} call.
     */
    function executeWithSwap(
        bytes calldata swapData,
        address target,
        bytes calldata targetCalldata
    ) external payable returns(
        IERC20 srcToken,
        uint256 srcSpent,
        uint256 srcDust,
        IERC20 dstToken,
        uint256 dstAmount,
        uint256 dstDust,
        bytes memory returnData
    ){
        return _executeWithSwap(
            swapData, 
            abi.encode(target, targetCalldata)
        );
    }

    // =============== Internal Write Functions ===============

    function _execute(bytes memory _data, IERC20 dstToken, uint256 dstAvailable) internal override returns(bytes memory returnData) {
        (address target, bytes memory targetCalldata) = abi.decode(_data, (address, bytes));

        uint256 value;
        if(_isETH(dstToken)) {
            value = dstAvailable;
        } else {
            dstToken.safeApproveMax(dstAvailable, target);
        }

        bool success;
        (success, returnData) = target.call{value:value}(targetCalldata);
        if(!success) revert ExecutionFailed(returnData);
    }

    function _ETHTransferValidation(IERC20 /*token*/, uint256 amount) internal override returns(uint256){
        if(msg.value != amount) revert InvalidMsgValue();
        return amount;
    }

    function _ERC20TransferValidation(IERC20 token, uint256 amount) internal override returns(uint256){
        token.safeTransferFrom(_msgSender(), address(this), amount);
        return amount;
    }

    // =============== Internal View Functions ===============

    function _refundRecipient() internal view override returns(address){
        return _msgSender();
    }
}