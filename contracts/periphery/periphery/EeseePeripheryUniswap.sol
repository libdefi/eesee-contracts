// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "./EeseePeripheryBase.sol";
import "../abstract/EeseeUniswapBase.sol";

contract EeseePeripheryUniswap is EeseePeripheryBase, EeseeUniswapBase {
    constructor(
        IUniswapV2Router02 _uniswapRouter, 
        IEeseeAccessManager _accessManager,
        address _trustedForwarder
    ) EeseeUniswapBase(_uniswapRouter, _accessManager, _trustedForwarder){}
}