// SPDX-License-Identifier: GPL-3.0-only
pragma solidity 0.8.21;

import "./EeseePeripheryBase.sol";
import "../abstract/EeseeOneInchBase.sol";

contract EeseePeripheryOneInch is EeseePeripheryBase, EeseeOneInchBase {
    constructor(
        address _oneInchRouter, 
        IEeseeAccessManager _accessManager,
        address _trustedForwarder
    ) EeseeOneInchBase(_oneInchRouter, _accessManager, _trustedForwarder) {}
}