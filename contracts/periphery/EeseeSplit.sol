// SPDX-License-Identifier: BUSL-1.1
// Copyright Split.sol from thirdweb Licensed under the Apache License, Version 2.0 (the «License»);
pragma solidity 0.8.21;

import "@thirdweb-dev/contracts/prebuilts/split/Split.sol";

contract EeseeSplit is Split {
    struct Recipient {
        address payable recipient;
        uint16 bps;
    }

    ///@dev Share of total sale that goes to royalty. [10000 = 100%].
    uint256 public royaltyShare;

    error InvalidRoyaltyShare();

    function initializeWithRoyaltyShare(
        uint256 _royaltyShare,
        address _defaultAdmin,
        string memory _contractURI,
        address[] memory _trustedForwarders,
        address[] memory _payees,
        uint256[] memory _shares
    ) external {
        if(_royaltyShare > 10000) revert InvalidRoyaltyShare();
        royaltyShare = _royaltyShare;
        this.initialize(_defaultAdmin, _contractURI, _trustedForwarders, _payees, _shares);
    }

    ///@dev RoyaltyEngine requires this function to be present for Multi Recipient ERC2981 Royalties.
    function getRecipients() external view returns (Recipient[] memory splitRecipients){
        uint256 _payeeCount = payeeCount();
        splitRecipients = new Recipient[](_payeeCount);

        uint256 _royaltyShare = royaltyShare;
        uint256 _totalShares = totalShares();
        for(uint256 i; i < _payeeCount;){
            address _payee = payee(i);
            splitRecipients[i] = Recipient({
                recipient: payable(_payee),
                bps: uint16(_royaltyShare * shares(_payee) / _totalShares)
            }); 
            unchecked{ ++i; }
        }
    }
}