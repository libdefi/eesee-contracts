// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../interfaces/IEesee.sol";

contract EeseeWinner {
    ///@dev Eesee contract.
    IEesee public immutable Eesee;
    ///@dev Contract that provides Eesee with random.
    IEeseeRandom public immutable random;
    ///@dev In case random request fails to get delivered {returnInterval} seconds after the lot was closed, unlock Reclaim functions.
    uint32 public immutable returnInterval;

    error InvalidEesee();
    error LotNotExists();
    error LotNotFulfilled();
    error LotExpired();
    error NoTicketsBought();

    constructor(IEesee _Eesee) {
        if(address(_Eesee) == address(0)) revert InvalidEesee();

        Eesee = _Eesee;
        random = Eesee.random();
        returnInterval = Eesee.RETURN_INTERVAL();
    }

    // =============== External View Functions ==================

    /**
     * @dev Get the winner of eesee lot. Return address(0) if no winner found.
     * @param ID - ID of the lot.
     
     * @return winner - Lot winner. Returns address(0) if no winner chosen.
     * @return isAssetWinner - True if winner recieves asset. False if winner receives ESE tokens.
     */
    function getLotWinner(uint256 ID) external view returns(address winner, bool isAssetWinner) {
        (bool success, bytes memory data) = address(Eesee).staticcall(abi.encodeWithSelector(IEesee.lots.selector, ID));
        if(!success) revert LotNotExists();

        AssetType assetType;
        {
        bool buyout;
        assembly {
            buyout := mload(add(data, 288))
            assetType := mload(add(data, 544))
        }
        if(buyout){
            return (Eesee.getBuyTicketsRecipient(ID, 0), assetType != AssetType.ESE);
        }
        }

        uint256 randomWord;
        {
        uint32 endTimestamp;
        assembly {
            endTimestamp := mload(add(data, 192))
        }
        if(endTimestamp == 0) revert LotNotExists();
        
        uint256 randomTimestamp;
        (randomWord, randomTimestamp) = random.getRandomFromTimestamp(endTimestamp);
        if(randomTimestamp == 0) revert LotNotFulfilled();
        unchecked{ if(randomTimestamp > endTimestamp + returnInterval) revert LotExpired(); }
        }

        uint32 ticketsBought;
        uint32 bonusTickets;
        assembly {
            ticketsBought := mload(add(data, 96))
            bonusTickets := mload(add(data, 64))
        }
        if(ticketsBought == 0) revert NoTicketsBought();

        bool closed;
        assembly {
            closed := mload(add(data, 256))
        }

        unchecked {
            uint256 IDmod256 = ID % 256;
            uint256 circularShiftWord = (randomWord << IDmod256) | (randomWord  >> (256 - IDmod256));
            return (Eesee.getLotTicketHolder(ID, uint32(circularShiftWord % (ticketsBought + bonusTickets))), (closed && assetType != AssetType.ESE));
        }
    }
}