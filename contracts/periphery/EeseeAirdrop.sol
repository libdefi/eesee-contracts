// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "../abstract/roles/EeseeRoleHandler.sol";

contract EeseeAirdrop is EeseeRoleHandler {
    using SafeERC20 for IERC20;

    struct Recipient {
        address recipient;
        uint256 amount;
    }

    ///@dev Token to airdrop.
    IERC20 public immutable ESE;
    ///@dev Airdrop role af defined in {accessManager}.
    bytes32 private constant AIRDROP_ROLE = keccak256("AIRDROP_ROLE");

    error InvalidESE();

    constructor(IERC20 _ESE, IEeseeAccessManager _accessManager) EeseeRoleHandler(_accessManager) {
        if(address(_ESE) == address(0)) revert InvalidESE();
        ESE = _ESE;
    }

    /**
     * @dev Airdrops ESE tokens to recipients.
     * @param recipients - Recipients' addresses and amounts they receive.
     * Note: This function can only be called by AIRDROP_ROLE.
     */
    function airdrop(Recipient[] calldata recipients) external onlyRole(AIRDROP_ROLE) {
        for(uint256 i; i < recipients.length;) {
            ESE.safeTransfer(recipients[i].recipient, recipients[i].amount);
            unchecked { ++i; }
        }
    }
}
