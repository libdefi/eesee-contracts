// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/utils/introspection/ERC165.sol";
import {IAny2EVMMessageReceiver} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IAny2EVMMessageReceiver.sol";
import {Client} from "@chainlink/contracts-ccip/src/v0.8/ccip/libraries/Client.sol";
import "../abstract/EeseeOffRampBase.sol";

contract EeseeOffRampCCIP is EeseeOffRampBase, IAny2EVMMessageReceiver, ERC165 {
    constructor(IERC20 _ESE, address _recipient) EeseeOffRampBase(_ESE, _recipient){}

    // ============ External Write Functions ============

    /**
     * @dev Whenever this contract receives CCIP message, transfers all ESE in balance to recipient.
       Note that this function does not have means of access control, meaning anyone can call it.
     */
    function ccipReceive(Client.Any2EVMMessage calldata /*message*/) external override {
        _forward();
    }

    // ============ Public View Functions ============

    function supportsInterface(bytes4 interfaceId) public view override(ERC165) returns (bool) {
        return interfaceId == type(IAny2EVMMessageReceiver).interfaceId || super.supportsInterface(interfaceId);
    }
}
