// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../abstract/EeseeOffRampBase.sol";

contract EeseeOffRampAxelar is EeseeOffRampBase{
    bytes32 internal constant EXECUTE_SUCCESS = keccak256('its-execute-success');

    constructor(IERC20 _ESE, address _recipient) EeseeOffRampBase(_ESE, _recipient){}

    /**
     * @dev Whenever this contract receives Axelar message, transfers all ESE in balance to recipient.
       Note that this function does not have means of access control, meaning anyone can call it.
     */
    function executeWithInterchainToken(
        bytes32 /*commandId*/,
        string calldata /*sourceChain*/,
        bytes calldata /*sourceAddress*/,
        bytes calldata /*data*/,
        bytes32 /*tokenId*/,
        address /*token*/,
        uint256 /*amount*/
    ) external returns (bytes32) {
        _forward();
        return EXECUTE_SUCCESS;
    }
}
