// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../../abstract/EeseeOnRampImplementationBase.sol";
import "../../../../abstract/crosschain/AxelarInterchainTokenServiceCaller.sol";

/// Note: WIP, this should be either protected by access control or be using Axelar's on onchain gas estimation, which has not released yet.
contract EeseeOnRampImplementationAxelar is EeseeOnRampImplementationBase, AxelarInterchainTokenServiceCaller {
    ///@dev Token Id used in Axelar Interchain Token Service.
    bytes32 immutable public tokenId;

    error CantReceiveMessages();
    error InvalidTokenId();

    constructor(
        IERC20 _ESE, 
        bytes32 _tokenId,
        address _interchainTokenService
    ) EeseeOnRampImplementationBase(_ESE) AxelarInterchainTokenServiceCaller(_interchainTokenService) {
        address interchainToken = IInterchainTokenService(interchainTokenService).validTokenAddress(_tokenId);
        if(interchainToken != address(_ESE)) revert InvalidTokenId();
        tokenId = _tokenId;
    }

    // ============ Public Write Functions ============

    function initialize(bytes memory data) public override {
        AddressWithChain memory _offRamp = abi.decode(data, (AddressWithChain));
        if(address(_offRamp._address) == address(0)) revert InvalidOffRamp();
        offRamp = _offRamp;
    }
    
    // ============ Internal Write Functions ============

    function _forward(uint256 amount, bytes memory /*data*/) internal override {
        _sendAxelarMessageWithInterchainToken( //TODO: update interchain token transfer to include Axelar GasEstimation
            address(this),
            offRamp,
            tokenId,
            amount,
            "",
            msg.value
        );
    }

    // ============ Internal View Functions ============

    function _executeWithInterchainToken(
        bytes32 /*commandId*/,
        AddressWithChain memory /*source*/,
        bytes calldata /*payload*/,
        bytes32 /*_tokenId*/,
        IERC20 /*token*/,
        uint256 /*amount*/
    ) internal override pure {
        revert CantReceiveMessages();
    }
}