// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "../../abstract/EeseeOnRampImplementationBase.sol";

contract EeseeOnRampImplementationTransfer is EeseeOnRampImplementationBase {
    using SafeERC20 for IERC20;

    error InvalidChainSelector();
    error InvalidMsgValue();

    constructor(IERC20 _ESE) EeseeOnRampImplementationBase(_ESE) {}

    // ============ Public Write Functions ============

    /**
     * @dev Initialize onRamp with provided data. 
     * Data is abi-encoded tuple(0x,address) offRamp.
     * Note: Proxies using this implementation should implement access control for this function.
     */
    function initialize(bytes memory data) public override {
        AddressWithChain memory _offRamp = abi.decode(data, (AddressWithChain));
        if(address(_offRamp._address) == address(0)) revert InvalidOffRamp();
        if(_offRamp.chainSelector.length > 0) revert InvalidChainSelector();
        offRamp = _offRamp;
    }

    // ============ Internal Write Functions ============

    function _forward(uint256 amount, bytes memory /*data*/) internal override {
        if(msg.value != 0) revert InvalidMsgValue();
        ESE.safeTransfer(offRamp._address, amount);
    }
}
