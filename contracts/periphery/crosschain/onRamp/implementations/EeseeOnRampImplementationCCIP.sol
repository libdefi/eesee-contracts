// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../../abstract/EeseeOnRampImplementationBase.sol";
import "../../../../abstract/crosschain/CCIPCaller.sol";

contract EeseeOnRampImplementationCCIP is EeseeOnRampImplementationBase, CCIPCaller {
    ///@dev Gas Limit used in CCIP call.
    uint256 public gasLimit;

    error CantReceiveMessages();
    error InvalidGasLimit();

    constructor(
        IERC20 _ESE, 
        IRouterClient _CCIPRouter
    ) EeseeOnRampImplementationBase(_ESE) CCIPCaller(_CCIPRouter) {}

    // ============ Public Write Functions ============

    /**
     * @dev Initialize onRamp with provided data. 
     * Data is abi-encoded (uint256 gasLimit, tuple(bytes,address) offRamp).
     * Note: Proxies using this implementation should implement access control for this function.
     */
    function initialize(bytes memory data) public override {
        (uint256 _gasLimit, AddressWithChain memory _offRamp) = abi.decode(data, (uint256, AddressWithChain));
        if(_gasLimit == 0) revert InvalidGasLimit();
        if(address(_offRamp._address) == address(0)) revert InvalidOffRamp();
        
        gasLimit = _gasLimit;
        offRamp = _offRamp;
    }

    // ============ Public View Functions ============

    function supportsInterface(bytes4 interfaceId) public view virtual override(CCIPCaller) returns (bool) {
        return ERC165.supportsInterface(interfaceId);
    }

    // ============ Internal Write Functions ============

    function _forward(uint256 amount, bytes memory /*data*/) internal override {
        Client.EVMTokenAmount[] memory tokenAmounts = new Client.EVMTokenAmount[](1);
        tokenAmounts[0] = Client.EVMTokenAmount({token: address(ESE), amount: amount});

        _sendCCIPMessage(
            address(this), 
            offRamp,
            "",
            tokenAmounts,
            msg.value,
            gasLimit
        );
    }

    // ============ Internal View Functions ============

    function _ccipReceive(
        AddressWithChain memory /*source*/, 
        bytes memory /*data*/, 
        Client.EVMTokenAmount[] memory /*destTokenAmounts*/
    ) internal override pure {
        revert CantReceiveMessages();
    }
}
