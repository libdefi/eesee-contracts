// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../../../abstract/roles/EeseePausable.sol";
import "../../../abstract/roles/EeseeAdmin.sol";
import "../../../interfaces/IEeseeOnRampImplementation.sol";

contract EeseeOnRampProxy is EeseeAdmin, EeseePausable {
    ///@dev onRamp contract implementation.
    address public onRampImplementation;

    event UpdateOnRampImplementation(
        address indexed oldOnRampImplementation,
        address indexed newOnRampImplementation,
        bytes initData
    );

    error ChangedOnRampImplementation();
    error InvalidOnRampImplementation();

    constructor(
        address _onRampImplementation,
        bytes memory initData,
        IEeseeAccessManager _accessManager
    ) EeseeRoleHandler(_accessManager) {
        _updateOnRampImplementation(_onRampImplementation, initData);
    }

    // ============ External Write Functions ============

    /**
     * @dev Delegates coll to onRampImplementation.
     */
    function forward(bytes memory data) external payable whenNotPaused returns (bytes memory returnData){
        returnData = _delegateToOnRampImplementation(IEeseeOnRampImplementation.forward.selector, data);
    }

    /**
     * @dev Updates onRamp implementation. Note: can only be called by ADMIN_ROLE.
     * @param _onRampImplementation - New onRampImplementation.
     * @param initData - Data with which initialize function in implementation is called.
     */
    function updateOnRampImplementation(address _onRampImplementation, bytes memory initData) external onlyAdmin {
        _updateOnRampImplementation(_onRampImplementation, initData);
    }
    
    // ============ Internal Write Functions ============

    function _updateOnRampImplementation(address _onRampImplementation, bytes memory initData) internal {
        if(_onRampImplementation == address(0)) revert InvalidOnRampImplementation();
        emit UpdateOnRampImplementation(onRampImplementation, _onRampImplementation, initData);
        onRampImplementation = _onRampImplementation;

        _delegateToOnRampImplementation(IEeseeOnRampImplementation.initialize.selector, initData);
    }

    function _delegateToOnRampImplementation(bytes4 selector, bytes memory data) internal returns(bytes memory returnData) {
        address _onRampImplementation = onRampImplementation;
        bytes memory encodedInitData = abi.encodeWithSelector(selector, data);
        bool success;
        (success, returnData) = _onRampImplementation.delegatecall(encodedInitData);
        if(!success) {
            if (returnData.length == 0) revert();
            assembly { revert(add(32, returnData), mload(returnData)) }
        }
        if(_onRampImplementation != onRampImplementation) revert ChangedOnRampImplementation();
    }
}
