// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../../../abstract/crosschain/AxelarCaller.sol";
import "../../../libraries/CompareAddressWithChain.sol";
import "./EeseeMessageInterface.sol";

abstract contract AxelarCallerEeseeMessageInterface is AxelarCaller, EeseeMessageInterface {  
    
    // ============ Internal Write Functions ============

    function _sendMessage(
        address /*msgSender*/,
        AddressWithChain memory destination, 
        bytes memory payload,
        uint256 gas,
        bytes calldata /*additionalData*/
    ) internal override {
        _sendAxelarMessage(destination, payload, gas);
    }

    function _execute(AddressWithChain memory source, bytes calldata data) internal override {
        _receiveMessage(source, data);
    }
}
