// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../../../abstract/crosschain/CCIPCaller.sol";
import "./EeseeMessageInterface.sol";

abstract contract CCIPCallerEeseeMessageInterface is CCIPCaller, EeseeMessageInterface {
     
    // ============ Internal Write Functions ============

    function _sendMessage(
        address msgSender,
        AddressWithChain memory destination, 
        bytes memory payload,
        uint256 gas,
        bytes calldata additionalData
    ) internal override {
        uint256 gasLimit = abi.decode(additionalData, (uint256));
        _sendCCIPMessage(msgSender, destination, payload, new Client.EVMTokenAmount[](0), gas, gasLimit);
    }

    function _ccipReceive(
        AddressWithChain memory source, 
        bytes memory data, 
        Client.EVMTokenAmount[] memory destTokenAmounts
    ) internal override {
        _receiveMessage(source, data);
    }
}
