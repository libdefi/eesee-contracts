// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../../../types/AddressWithChain.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

abstract contract EeseeMessageInterface {
    function _sendMessage(
        address msgSender,
        AddressWithChain memory destination, 
        bytes memory payload,
        uint256 gas,
        bytes calldata additionalData
    ) internal virtual;

    function _receiveMessage(AddressWithChain memory source, bytes memory data) internal virtual;
}