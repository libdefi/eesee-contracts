// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/metatx/ERC2771Context.sol";
import "@openzeppelin/contracts/token/ERC721/utils/ERC721Holder.sol";
import "@openzeppelin/contracts/token/ERC1155/utils/ERC1155Holder.sol";
import "../../../libraries/CompareAddressWithChain.sol";
import "../../../types/Call.sol";
import "../../../types/Asset.sol";
import "../../../interfaces/IEeseeAssetSpoke.sol";
import "../../../interfaces/IRoyaltyEngineV1.sol";
import "../../../abstract/roles/EeseePausable.sol";
import "./EeseeMessageInterface.sol";
import "../../../errors/EeseeErrors.sol";

/// @dev Crosschain contract that allows users to wrap their assets and send them to a different chain.
abstract contract EeseeAssetSpokeBase is IEeseeAssetSpoke, ERC2771Context, ERC721Holder, ERC1155Holder, EeseePausable, EeseeMessageInterface {
    using SafeERC20 for IERC20;
    using CompareAddressWithChain for AddressWithChain;
    
    /// @dev AddressWithChain struct for the current contract.
    AddressWithChain public eeseeSpoke;
    /// @dev AddressWithChain struct for EeseeAssetHub contract.
    AddressWithChain public eeseeAssetHub;

    ///@dev The Royalty Engine is a contract that provides an easy way for any marketplace to look up royalties for any given token contract.
    IRoyaltyEngineV1 immutable private royaltyEngine;

    /// @dev Maps asset hash to the asset itself.
    mapping(bytes32 => Asset) public assetsStorage;
    /// @dev Amount of assets stuck for a given account.
    mapping(address => mapping(bytes32 => uint256)) public stuckAmount;

    event Stuck(
        Asset asset,
        bytes32 indexed assetHash,
        address indexed recipient,
        bytes err
    );
    event Unstuck(
        Asset asset,
        bytes32 indexed assetHash,
        address indexed sender, 
        address indexed recipient
    );

    error InvalidConstructor();
    error InvalidSender();
    error InvalidInterface();
    error InvalidTokenID();
    error InvalidAsset();
    error InvalidRecipient();
    error OnlySelf();
    error NoAssetsStuck();
    error TransferNotSuccessful();
    error InvalidAssetsLength();

    modifier onlySelf() {
        if (msg.sender != address(this)) revert OnlySelf();
        _;
    }

    constructor(
        IEeseeAccessManager _accessManager,
        bytes memory _currentChainSelector,
        AddressWithChain memory _eeseeAssetHub,

        IRoyaltyEngineV1 _royaltyEngine,
        address trustedForwarder
    ) ERC2771Context(trustedForwarder) EeseeRoleHandler(_accessManager) {
        if(
            _eeseeAssetHub._address == address(0) ||
            address(_royaltyEngine) == address(0)
        ) revert InvalidConstructor();

        eeseeSpoke = AddressWithChain({chainSelector: _currentChainSelector, _address: address(this)});
        eeseeAssetHub = _eeseeAssetHub;

        royaltyEngine = _royaltyEngine;
    }

    // ============ External Write Functions ============
    
    /**
     * @dev Wraps specified assets and sends them to {recipient} in the form of ERC1155 tokens.
     * @param assets - Assets to wrap.
     * @param crosschainCalls - Calls to make on destination chain. 
     * @param fallbackRecipient - Address to transfer leftover tokens after crosschainCalls. 
     * @param additionalData - Additional information to pass to crosschain protocol. 
     
     * @return gasPaid - Gas paid for the crosschain call.
     */
    function wrap(
        Asset[] calldata assets, 
        Call[] calldata crosschainCalls,
        address fallbackRecipient,
        bytes calldata additionalData
    ) whenNotPaused external payable returns(uint256 gasPaid){
        if(assets.length == 0) revert InvalidAssetsLength();
        if(fallbackRecipient == address(0)) revert InvalidRecipient();
        address msgSender = _msgSender();

        bytes32[] memory assetHashes; uint256[] memory amounts; bytes[] memory royaltyData;
        (assetHashes, amounts, royaltyData, gasPaid) = _wrap(assets, msgSender);
        _sendMessage(
            msgSender,
            eeseeAssetHub,
            abi.encode(assetHashes, amounts, royaltyData, crosschainCalls, fallbackRecipient),
            gasPaid,
            additionalData
        ); 
    }

    /**
     * @dev Reclaim assets that might have been stuck after _unwrap. Emits Unstuck event.
     * Note: Must be called from asset recipient address that had their asset stuck.
     * @param stuckAssetHash - Hash of the asset that got stuck.
     * @param recipient - Address to transfer asset to.
     
     * @return asset - Asset sent to {recipient}.
     */
    function unstuck(bytes32 stuckAssetHash, address recipient) external returns(Asset memory asset){
        if(recipient == address(0)) revert InvalidRecipient();

        address msgSender = _msgSender();
        uint256 _stuckAmount = stuckAmount[msgSender][stuckAssetHash];
        if(_stuckAmount == 0) revert NoAssetsStuck();

        delete stuckAmount[msgSender][stuckAssetHash];
        asset = this._transferAssetTo(stuckAssetHash, _stuckAmount, recipient);
        emit Unstuck(asset, stuckAssetHash, msgSender, recipient);
    }

    // ============ External View Functions ============

    /**
     * @dev Allows calculating token ID on L2 chain before wrapping.
     * @param asset - Asset to calculate token Id for.
     
     * @return uint256 - Calculated token Id for wrapped asset on L2 chain.
     */
    function getTokenIdL2(Asset calldata asset) external view returns (uint256) {
        return getTokenIdL2(getAssetHash(asset));
    }

    // ============ Public View Functions ============

    /**
     * @dev Allows calculating token ID on L2 chain before wrapping.
     * @param assetHash - Asset hash to calculate token Id for.
     
     * @return uint256 - Calculated token Id for wrapped asset on L2 chain.
     */
    function getTokenIdL2(bytes32 assetHash) public view returns (uint256) {
        AssetHashWithSource memory assetHashWithSource = AssetHashWithSource(eeseeSpoke, assetHash);
        return uint256(keccak256(abi.encode(assetHashWithSource)));
    }

    /**
     * @dev Calculates asset hash.
     * @param asset - Asset to calculate asset hash for.
     */
    function getAssetHash(Asset calldata asset) public pure returns (bytes32) {
        return keccak256(abi.encode(asset.token, asset.tokenID, asset.assetType));
    }

    // ============ Internal Write Functions ============

    function _receiveMessage(AddressWithChain memory source, bytes memory data) internal override {
        if(!source.eq(eeseeAssetHub)) revert InvalidSender();

        (bytes32[] memory assetHashes, uint256[] memory amounts, address recipient) = abi.decode(data, (bytes32[], uint256[], address));
        for(uint256 i; i < assetHashes.length;){
            try this._transferAssetTo(assetHashes[i], amounts[i], recipient) returns (Asset memory asset) {
                emit Unwrap(asset, assetHashes[i], recipient);
            } catch (bytes memory err) {
                Asset memory asset = assetsStorage[assetHashes[i]];
                asset.amount = amounts[i];

                stuckAmount[recipient][assetHashes[i]] += amounts[i];
                emit Stuck(asset, assetHashes[i], recipient, err);
            }

            unchecked{ ++i; }
        }
    }

    function _transferAssetTo(bytes32 assetHash, uint256 amount, address to) external onlySelf returns(Asset memory asset){
        asset = assetsStorage[assetHash];
        if(asset.amount < amount) revert InvalidAmount();
        assetsStorage[assetHash].amount = asset.amount - amount;
        asset.amount = amount;

        if (asset.assetType == AssetType.ERC721) {
            IERC721(asset.token).safeTransferFrom(address(this), to, asset.tokenID);
        } else if (asset.assetType == AssetType.ERC1155) {
            IERC1155(asset.token).safeTransferFrom(address(this), to, asset.tokenID, amount, "");
        } else if (asset.assetType == AssetType.ERC20) {
            IERC20(asset.token).safeTransfer(to, amount);
        } else {
            // This is Native asset
            (bool success,) = to.call{value: amount}("");
            if(!success) revert TransferNotSuccessful(); 
        }
    }

    // ============ Internal View Functions =============
    
    function _msgSender() internal view override(Context, ERC2771Context) returns (address) {
        return ERC2771Context._msgSender();
    }

    function _msgData() internal view override(Context, ERC2771Context) returns (bytes calldata) {
        return ERC2771Context._msgData();
    }

    function _contextSuffixLength() internal view override(Context, ERC2771Context) returns (uint256) {
        return ERC2771Context._contextSuffixLength();
    }

    // ============ Private Write Functions =============

    function _wrap(
        Asset[] calldata assets,
        address msgSender
    ) private returns (bytes32[] memory assetHashes, uint256[] memory amounts, bytes[] memory royaltyData, uint256 valueLeft){
        assetHashes = new bytes32[](assets.length);
        amounts = new uint256[](assets.length);
        royaltyData = new bytes[](assets.length);

        valueLeft = msg.value;
        for(uint256 i; i < assets.length;){
            (assetHashes[i], royaltyData[i], amounts[i], valueLeft) = _transferAssetFrom(assets[i], msgSender, valueLeft);
            unchecked{ ++i; }
        }
    }

    function _transferAssetFrom(
        Asset calldata asset, 
        address msgSender, 
        uint256 availableValue
    ) private returns(bytes32 assetHash, bytes memory royaltyData, uint256 amount, uint256 valueLeft){
        valueLeft = availableValue;
        if (asset.assetType == AssetType.ERC721) {
            if(asset.amount != 1) revert InvalidAmount();
            { // Scope to avoid Stack too Deep
            (bool success, bytes memory res) = asset.token.staticcall(abi.encodeWithSelector(IERC165.supportsInterface.selector, type(IERC721).interfaceId));
            if(!success || abi.decode(res, (bool)) == false) revert InvalidInterface();

            (address payable[] memory royaltyRecipients, uint256[] memory royaltyAmounts) = royaltyEngine.getRoyalty(asset.token, asset.tokenID, 10000);
            royaltyData = abi.encode(royaltyRecipients, royaltyAmounts);
            }

            amount = 1;
            IERC721(asset.token).safeTransferFrom(msgSender, address(this), asset.tokenID);
        } else if (asset.assetType == AssetType.ERC1155) {
            if(asset.amount == 0) revert InvalidAmount();
            { // Scope to avoid Stack too Deep
            (bool success, bytes memory res) = asset.token.staticcall(abi.encodeWithSelector(IERC165.supportsInterface.selector, type(IERC1155).interfaceId));
            if(!success || abi.decode(res, (bool)) == false) revert InvalidInterface();

            (address payable[] memory royaltyRecipients, uint256[] memory royaltyAmounts) = royaltyEngine.getRoyalty(asset.token, asset.tokenID, 10000);
            royaltyData = abi.encode(royaltyRecipients, royaltyAmounts);
            }

            amount = asset.amount;
            IERC1155(asset.token).safeTransferFrom(msgSender, address(this), asset.tokenID, asset.amount, "");
        } else if (asset.assetType == AssetType.ERC20) {
            // Most ERC-20 tokens do not implement supportsInterface, so we check against IERC721 to ensure we are not calling ERC721 contract.
            // ERC1155 will revert anyway because of different safeTransferFrom scheme.
            (bool success, bytes memory res) = asset.token.staticcall(abi.encodeWithSelector(IERC165.supportsInterface.selector, type(IERC721).interfaceId));
            if(success && abi.decode(res, (bool)) == true) revert InvalidInterface();
            if(asset.tokenID != 0) revert InvalidTokenID();

            uint256 balanceBefore = IERC20(asset.token).balanceOf(address(this));
            IERC20(asset.token).safeTransferFrom(msgSender, address(this), asset.amount);
            uint256 balanceAfter = IERC20(asset.token).balanceOf(address(this));
            
            // For fee on transfer tokens we compare balances.
            amount = balanceAfter - balanceBefore;
            if(amount == 0) revert InvalidAmount();
        } else if (asset.assetType == AssetType.Native) {
            if(asset.amount == 0) revert InvalidAmount();
            if(availableValue < asset.amount) revert InsufficientValue();
            if(asset.token != address(0)) revert InvalidToken();
            if(asset.tokenID != 0) revert InvalidTokenID();

            unchecked{ valueLeft -= asset.amount; }
            amount = asset.amount;
        } else revert InvalidAsset();

        Asset memory newAsset = Asset({
            token: asset.token,
            tokenID: asset.tokenID,
            amount: amount,
            assetType: asset.assetType,
            data: ""
        });
        assetHash = getAssetHash(asset);
        emit Wrap(newAsset, assetHash, msgSender);

        newAsset.amount += assetsStorage[assetHash].amount;
        assetsStorage[assetHash] = newAsset;
    }
}