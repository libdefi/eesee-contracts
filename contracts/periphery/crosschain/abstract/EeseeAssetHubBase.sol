// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/Base64.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/token/ERC1155/utils/ERC1155Holder.sol";
import "@openzeppelin/contracts/proxy/Clones.sol";
import "@openzeppelin/contracts/token/common/ERC2981.sol";
import "@openzeppelin/contracts/metatx/ERC2771Context.sol";
import "@openzeppelin/contracts/token/ERC1155/ERC1155.sol";
import "../../../interfaces/IEeseeAssetHub.sol";
import "../../../types/AddressWithChain.sol";
import "../../../libraries/CompareAddressWithChain.sol";
import "../../../abstract/MulticallExternal.sol";
import { EeseeSplit } from "../../EeseeSplit.sol";
import { EeseeVault } from "../../EeseeVault.sol";
import "./EeseeMessageInterface.sol";

abstract contract EeseeAssetHubBase is IEeseeAssetHub, ERC1155, ERC2981, ERC1155Holder, ERC2771Context, MulticallExternal, EeseeMessageInterface {
    using SafeERC20 for IERC20;
    using Strings for uint256;
    using Strings for address;
    using Base64 for bytes;
    using CompareAddressWithChain for AddressWithChain;

    /// @dev An image all tokens will use.
    string private image;
    /// @dev Name prefix all tokens will have in metadata.
    string private namePrefix;
    /// @dev Description all tokens will have in metadata.
    string private description;

    /// @dev Implementation of EeseeSplit contract for royalty splitting.
    address immutable public splitImplementation;
    /// @dev EeseeVault contract used for handling stuck ERC1155 after errors.
    EeseeVault immutable public vault;

    /// @dev Maps tokenID to its asset and source info.
    mapping(uint256 => AssetHashWithSource) public assetHashesWithSources;

    /// @dev Trusted Forwarder from ERC2771Context.
    address public immutable trustedForwarder;

    error InvalidConstructor();
    error InvalidTokenIdsLength();
    error InvalidRecipient();
    error CallerNotAuthorized();
    error OnlySelf();
    
    modifier onlySelf() {
        if (msg.sender != address(this)) revert OnlySelf();
        _;
    }

    constructor(
        string memory _image,
        string memory _namePrefix,
        string memory _description,

        address _trustedForwarder
    ) ERC2771Context(_trustedForwarder) ERC1155("") {
        if(bytes(_image).length == 0 || bytes(_description).length == 0) revert InvalidConstructor();

        image = _image;
        namePrefix = _namePrefix;
        description = _description;

        vault = new EeseeVault(IERC1155(this), _trustedForwarder);
        splitImplementation = address(new EeseeSplit());
        trustedForwarder = _trustedForwarder;

        _addToBlacklist(address(this), this._setRoyalty.selector);
        _addToBlacklist(address(vault), EeseeVault.onERC1155Received.selector);
        _addToBlacklist(address(vault), EeseeVault.onERC1155BatchReceived.selector);
    }

    // ============ External Write Functions ============
    
    /**
     * @dev Unwraps specified tokens to their source chain. 
       Note: The caller must either own those tokens, or have them transfered to this contract beforehand. 
       !WARNING! Never send any tokens to this contract from an EOA, or by contract in a separate transaction or they will be lost.
       Only send funds to this contract if you spend them by calling unwrap function in the same transaction.
     * @param tokenIds - Token IDs to unwrap. All tokens must have the same {source}.
     * @param amounts - Amount of tokens to unwrap.
     * @param recipient - Address to unwrap and send tokens to.
     * @param additionalData - Additional information to pass to crosschain protocol. 
     */
    function unwrap(
        uint256[] calldata tokenIds, 
        uint256[] calldata amounts,
        address recipient, 
        bytes calldata additionalData
    ) external payable {
        if(tokenIds.length == 0 || tokenIds.length != amounts.length) revert InvalidTokenIdsLength();
        if(recipient == address(0)) revert InvalidRecipient();
        address msgSender = _msgSender();

        (AddressWithChain memory destination, bytes32[] memory assetHashes) = _unwrap(tokenIds, amounts, msgSender, recipient);
        _sendMessage(
            msgSender,
            destination,
            abi.encode(assetHashes, amounts, recipient),
            msg.value,
            additionalData
        ); 
    }

    // ============ Public View Functions ============

    /**
     * @dev Builds and returns tokenId's token URI.
     * @param tokenId - Token ID to check.
     
     * @return string Token URI.
     */
    function uri(uint256 tokenId) override view public returns(string memory) {
        string memory metadata = string(abi.encodePacked(
            "{\"name\":\"", namePrefix, tokenId.toString(), "\",",
            "\"image\":\"", image, "\",",
            "\"description\":\"", description, "\","
        ));

        // Divide into 2 encodePacked to avoid stack too deep error
        AssetHashWithSource memory assetHashWithSource = assetHashesWithSources[tokenId];
        metadata = string(abi.encodePacked(
            metadata,
            "\"properties\":{",
            "\"sourceChainSelector\":\"", _chainSelectorToString(assetHashWithSource.source.chainSelector), "\","
            "\"sourceSpoke\":\"", assetHashWithSource.source._address.toHexString(), "\","
            "\"assetHash\":\"", uint256(assetHashWithSource.assetHash).toHexString(), "\"}}"
        ));

        return string(abi.encodePacked(
            "data:application/json;base64,",
            bytes(metadata).encode()
        ));
    }

    /**
     * @dev Allows calculating token IDs before wrapping.
     * @param assetHashWithSource - Asset hash together with it's source.
     
     * @return uint256 - Calculated token Id for wrapped asset.
     */
    function getTokenId(AssetHashWithSource memory assetHashWithSource) public pure returns (uint256) {
        return uint256(keccak256(abi.encode(assetHashWithSource)));
    }

    function supportsInterface(bytes4 interfaceId) public view virtual override(ERC1155, ERC1155Receiver, ERC2981) returns (bool) {
        return 
            ERC1155.supportsInterface(interfaceId) || 
            ERC2981.supportsInterface(interfaceId) ||
            ERC1155Receiver.supportsInterface(interfaceId);
    }

    // ============ Internal Write Functions ============

    function _receiveMessage(AddressWithChain memory source, bytes memory data) internal override {
        (bytes32[] memory assetHashes, uint256[] memory amounts, bytes[] memory royaltyData, Call[] memory calls, address fallbackRecipient) = abi.decode(data, (bytes32[], uint256[], bytes[], Call[], address));

        uint256[] memory tokenIds = _wrap(source, assetHashes, amounts, royaltyData);
        _tryMulticall(calls);
        _transferLeftoverERC1155s(tokenIds, fallbackRecipient);
    }

    function _setRoyalty(uint256 tokenId, bytes memory assetData) external onlySelf {
        if(assetData.length == 0) return;
        (address[] memory royaltyRecipients, uint256[] memory feeNumerators) = abi.decode(assetData, (address[], uint256[]));
        if(royaltyRecipients.length == 0 || royaltyRecipients.length != feeNumerators.length) return;

        address payable receiver;
        uint256 feeNumerator;
        if(royaltyRecipients.length == 1){
            if(royaltyRecipients[0] == address(0) || feeNumerators[0] == 0) return;
            receiver = payable(royaltyRecipients[0]);
            feeNumerator = feeNumerators[0];
        } else {
            uint256 validRecipients;
            address[] memory _royaltyRecipients = new address[](feeNumerators.length);
            uint256[] memory _feeNumerators = new uint256[](feeNumerators.length);
            for(uint256 j; j < feeNumerators.length;){
                if(royaltyRecipients[j] != address(0) && feeNumerators[j] != 0) {
                    _royaltyRecipients[validRecipients] = royaltyRecipients[j];
                    _feeNumerators[validRecipients] = feeNumerators[j];

                    feeNumerator += feeNumerators[j];
                    unchecked { ++validRecipients; }
                }
                unchecked { ++j; }
            }
            if(validRecipients == 0) return;
            assembly { 
                let decreaseBy := sub(mload(feeNumerators), validRecipients)
                mstore(_royaltyRecipients, sub(mload(_royaltyRecipients), decreaseBy)) 
                mstore(_feeNumerators, sub(mload(_feeNumerators), decreaseBy))
            }

            address[] memory trustedForwarders = new address[](1);
            trustedForwarders[0] = trustedForwarder;
            receiver = payable(Clones.clone(splitImplementation));
            EeseeSplit(receiver).initializeWithRoyaltyShare(
                feeNumerator,
                address(0), 
                "",  
                trustedForwarders, 
                _royaltyRecipients, 
                _feeNumerators
            );
        }
        _setTokenRoyalty(tokenId, receiver, uint96(feeNumerator));
    }

    // ============ Internal View Functions =============

    function _chainSelectorToString(bytes memory chainSelector) internal pure virtual returns(string memory);

    function _msgSender() internal view override(Context, ERC2771Context) returns (address) {
        return ERC2771Context._msgSender();
    }

    function _msgData() internal view override(Context, ERC2771Context) returns (bytes calldata) {
        return ERC2771Context._msgData();
    }

    function _contextSuffixLength() internal view override(Context, ERC2771Context) returns (uint256) {
        return ERC2771Context._contextSuffixLength();
    }

    // ============ Private Write Functions =============

    function _unwrap(
        uint256[] calldata tokenIds, 
        uint256[] calldata amounts, 
        address msgSender, 
        address recipient
    ) private returns (AddressWithChain memory destination, bytes32[] memory assetHashes){
        assetHashes = new bytes32[](tokenIds.length);

        for(uint256 i; i < tokenIds.length;){
            uint256 tokenId = tokenIds[i];
            AssetHashWithSource memory assetHashWithSource = assetHashesWithSources[tokenId];
            if(i == 0){
                destination = assetHashWithSource.source;
            } else if(!destination.eq(assetHashWithSource.source)) revert InvalidDestination();
            assetHashes[i] = assetHashWithSource.assetHash;

            address burnFrom = balanceOf(address(this), tokenId) < amounts[i] ? msgSender : address(this);
            _burn(burnFrom, tokenId, amounts[i]);
            emit Unwrap(tokenId, amounts[i], assetHashWithSource, msgSender, recipient);
            unchecked { ++i; }
        }
    }

    function _wrap(
        AddressWithChain memory source,
        bytes32[] memory assetHashes,
        uint256[] memory amounts,
        bytes[] memory royaltyData
    ) private returns(uint256[] memory tokenIds){
        tokenIds = new uint256[](assetHashes.length);
        for(uint256 i; i < assetHashes.length;){
            AssetHashWithSource memory assetHashWithSource = AssetHashWithSource({source: source, assetHash: assetHashes[i]});

            uint256 tokenId = getTokenId(assetHashWithSource);
            assetHashesWithSources[tokenId] = assetHashWithSource;
            tokenIds[i] = tokenId;
            try this._setRoyalty(tokenId, royaltyData[i]) {} catch {}

            emit Wrap(tokenId, amounts[i], assetHashWithSource);
            unchecked { ++i; }
        }
        _mintBatch(address(this), tokenIds, amounts, "");
    }

    function _transferLeftoverERC1155s(uint256[] memory tokenIds, address fallbackRecipient) private {      
        for(uint256 i; i < tokenIds.length;){
            uint256 tokenId = tokenIds[i];
            uint256 balance = balanceOf(address(this), tokenId);
            if(balance > 0) try this.safeTransferFrom(address(this), fallbackRecipient, tokenId, balance, "") {} catch (bytes memory /*err*/) {
                // In case safeTransfer fails, transfer tokens to vault where fallbackRecipient will be able to claim them by calling unstuck function.
                _safeTransferFrom(address(this), address(vault), tokenId, balance, abi.encode(fallbackRecipient));
            }
            unchecked { ++i; }
        }
    }
}
