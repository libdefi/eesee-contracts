// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../../../types/AddressWithChain.sol";
import "../../../interfaces/IEeseeOnRampImplementation.sol";

/// @dev Forwards ESE tokens in balance to the specified offRamp.
abstract contract EeseeOnRampImplementationBase is IEeseeOnRampImplementation {
    ///@dev Proxy contract stores implementation in the first slot, so we leave a gap for it.
    address private _gap;
    ///@dev ESE token to forward.
    IERC20 immutable public ESE;
    ///@dev Info for the destination offRamp contract.
    AddressWithChain public offRamp;

    error InvalidOffRamp();
    error InvalidESE();
    error InsufficientBalance();

    constructor(IERC20 _ESE) {
        if(address(_ESE) == address(0)) revert InvalidESE();
        ESE = _ESE;
    }

    // ============ External Write Functions ============

    /**
     * @dev Forwards ESE balance from this contract to {offRamp}.
     */
    function forward(bytes memory data) external payable {
        uint256 balance = ESE.balanceOf(address(this));
        if(balance == 0) revert InsufficientBalance();

        _forward(balance, data);
        emit Forward(offRamp, balance);
    }

    // ============ Public Write Functions ============

    /**
     * @dev Initialize onRamp with provided data. Data is abi-encoded tuple(bytes,address) offRamp.
     * Note: Proxies using this implementation should implement access control for this function.
     */
    function initialize(bytes memory data) public virtual;

    // ============ Internal Write Functions ============

    function _forward(uint256 amount, bytes memory data) internal virtual;
}
