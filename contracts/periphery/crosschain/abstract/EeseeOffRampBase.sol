// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "../../../interfaces/IEeseeOffRamp.sol";

/// @dev Forwards ESE in balance to the specified recipient.
abstract contract EeseeOffRampBase is IEeseeOffRamp {
    using SafeERC20 for IERC20;

    ///@dev ESE token to forward.
    IERC20 public immutable ESE;
    ///@dev Address to forward ESE token to.
    address public immutable recipient;

    error InvalidConstructor();

    constructor(IERC20 _ESE, address _recipient) {
        if(
            address(_ESE) == address(0) ||
            address(_recipient) == address(0)
        ) revert InvalidConstructor();

        ESE = _ESE;
        recipient = _recipient;
    }

    // ============ Internal Write Functions ============

    function _forward() internal {
        uint256 balance = ESE.balanceOf(address(this));

        if(balance > 0){
            ESE.safeTransfer(recipient, balance);
            emit Forward(balance);
        }
    }
}
