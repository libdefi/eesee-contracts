// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import { AddressToString } from "@axelar-network/axelar-gmp-sdk-solidity/contracts/libs/AddressString.sol";

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/metatx/ERC2771Context.sol";
import "../../interfaces/ISquidRouter.sol";
import "../../interfaces/IEeseeAccessManager.sol";
import "../../abstract/roles/EeseePausable.sol";

/// @dev Squid Router extension that adds permit support and the ability to use ERC20 tokens from contract's balance.
contract EeseeExpress is ERC2771Context, EeseePausable {
    using Address for address;
    using AddressToString for address;
    using SafeERC20 for IERC20;

    struct AddressWithChain{
        string chain;
        address _address;
    }

    struct TokenData{
        IERC20 token;
        uint256 amount;
        bytes permit;
    }

    /// @dev Contract address for Squid Router.
    ISquidRouter immutable public squid;

    error InvalidConstructor();
    error InvalidRecipient();

    constructor(
        IEeseeAccessManager _accessManager,
        ISquidRouter _squid,
        address trustedForwarder
    ) ERC2771Context(trustedForwarder) EeseeRoleHandler(_accessManager) {
        if(address(_squid) == address(0)) revert InvalidConstructor();

        squid = _squid;
    }

    // ============ External Write Functions ============

    /**
     * @dev Send tokens to another chain with Squid and call multicall on both chains.
        Note: The caller must either own those tokens, or have them transfered to this contract beforehand. 
       !WARNING! Never send any tokens to this contract from an EOA, or by contract in a separate transaction or they will be lost.
       Only send funds to this contract if you spend them by calling callBridgeCall function in the same transaction.
     * @param squidRouter - Struct with destination chain and the address of SquidRouter on that chain.
     * @param tokenToSymbol - Token symbol to transfer to {squidRouter.chain}.

     * @param tokenFrom - Token to collect from sender or use from this contract's balance with abi-encoded permit containing approveAmount, deadline, v, r and s. Set to empty bytes to skip permit.

     * @param sourceCalls - Calls to make on this chain. 
     * @param destinationCalls - Calls to make on {squidRouter.chain}. 
     * @param refundRecipient - In case calls on destination chain fail, this is the address that will receive the tokens instead. 
     * @param isExpress - Set to true for GMP express calls. 
     */
    function callBridgeCall(
        AddressWithChain calldata squidRouter,
        string calldata tokenToSymbol,
        TokenData calldata tokenFrom,
        
        ISquidMulticall.Call[] calldata sourceCalls,
        ISquidMulticall.Call[] calldata destinationCalls,
        address refundRecipient,
        bool isExpress
    ) whenNotPaused external payable {
        if(refundRecipient == address(0)) revert InvalidRecipient();
        {
        address msgSender = _msgSender();
        if(tokenFrom.token != IERC20(0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE)){
            _permit(tokenFrom.token, msgSender, tokenFrom.permit);
            _transferFrom(tokenFrom.token, tokenFrom.amount, msgSender);
            _approve(tokenFrom.token, tokenFrom.amount, address(squid));
        }
        }

        _callBridgeCall(
            tokenFrom.token, 
            tokenFrom.amount, 
            sourceCalls, 
            tokenToSymbol, 
            squidRouter, 
            abi.encode(destinationCalls, refundRecipient),
            isExpress
        );
    }

    // ============ Internal Write Functions ============

    function _permit(IERC20 token, address _for, bytes calldata permit) internal {
        if(permit.length == 0) return;

        (uint256 approveAmount, uint256 deadline, uint8 v, bytes32 r, bytes32 s) = abi.decode(permit, (uint256, uint256, uint8, bytes32, bytes32));
        IERC20Permit(address(token)).permit(_for, address(this), approveAmount, deadline, v, r, s);
    }

    function _transferFrom(IERC20 token, uint256 amount, address from) internal {
        uint256 balance = token.balanceOf(address(this));
        if(balance < amount) { unchecked{ token.safeTransferFrom(from, address(this), amount - balance); }}
    }

    function _approve(IERC20 token, uint256 amount, address spender) internal {
        // nor approve, nor safeApprove, nor safeIncreaseAllowance work in this case, so we use custom logic.
        if(token.allowance(address(this), spender) < amount) {
            // Logic from _callOptionalReturn function from SafeERC20.
            bytes memory returndata = address(token).functionCall(
                abi.encodeWithSelector(token.approve.selector, spender, type(uint256).max), 
                "SafeERC20: low-level call failed"
            );
            require(returndata.length == 0 || abi.decode(returndata, (bool)), "SafeERC20: ERC20 operation did not succeed");
        }
    }

    function _callBridgeCall(
        IERC20 token, 
        uint256 amount, 
        ISquidMulticall.Call[] calldata calls, 
        string calldata tokenToSymbol,
        AddressWithChain calldata destination,
        bytes memory payload,
        bool isExpress
    ) internal {
        squid.callBridgeCall{value: msg.value}(
            address(token),
            amount,
            calls,
            tokenToSymbol,
            destination.chain,
            destination._address.toString(), 
            payload, 
            tx.origin, 
            isExpress
        );
    }

    // =============== Internal View Functions ===============
    
    function _msgSender() internal view override(Context, ERC2771Context) returns (address) {
        return ERC2771Context._msgSender();
    }

    function _msgData() internal view override(Context, ERC2771Context) returns (bytes calldata) {
        return ERC2771Context._msgData();
    }

    function _contextSuffixLength() internal view override(Context, ERC2771Context) returns (uint256) {
        return ERC2771Context._contextSuffixLength();
    }
}
