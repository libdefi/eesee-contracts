// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../abstract/EeseeAssetSpokeBase.sol";
import "../abstract/AxelarCallerEeseeMessageInterface.sol";

contract EeseeAssetSpokeAxelar is EeseeAssetSpokeBase, AxelarCallerEeseeMessageInterface {
    constructor(
        IEeseeAccessManager _accessManager,
        bytes memory _currentChainSelector,
        AddressWithChain memory _eeseeAssetHub,
        IRoyaltyEngineV1 _royaltyEngine,

        IAxelarGateway _gateway,
        IAxelarGasService _gasService,

        address trustedForwarder
    ) EeseeAssetSpokeBase(_accessManager, _currentChainSelector, _eeseeAssetHub, _royaltyEngine, trustedForwarder) AxelarGatewayBase(_gateway, _gasService) {}
}