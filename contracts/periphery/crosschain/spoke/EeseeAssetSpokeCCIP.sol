// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../abstract/EeseeAssetSpokeBase.sol";
import "../abstract/CCIPCallerEeseeMessageInterface.sol";

contract EeseeAssetSpokeCCIP is EeseeAssetSpokeBase, CCIPCallerEeseeMessageInterface {
    constructor(
        IEeseeAccessManager _accessManager,
        bytes memory _currentChainSelector,
        AddressWithChain memory _eeseeAssetHub,

        IRoyaltyEngineV1 _royaltyEngine,
        IRouterClient _CCIProuter,
        address trustedForwarder
    ) EeseeAssetSpokeBase(_accessManager, _currentChainSelector, _eeseeAssetHub, _royaltyEngine, trustedForwarder) CCIPCaller(_CCIProuter) {}

    // ============ Public View Functions ============

    function supportsInterface(bytes4 interfaceId) public view override(CCIPCaller, ERC1155Receiver) returns (bool) {
        return ERC1155Receiver.supportsInterface(interfaceId) || CCIPCaller.supportsInterface(interfaceId);
    }
}