// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../abstract/EeseeAssetHubBase.sol";
import "../abstract/CCIPCallerEeseeMessageInterface.sol";

contract EeseeAssetHubCCIP is EeseeAssetHubBase, CCIPCallerEeseeMessageInterface {
    using Strings for uint256;

    constructor(
        string memory _image,
        string memory _namePrefix,
        string memory _description,

        IRouterClient _CCIProuter,
        address _trustedForwarder
    ) EeseeAssetHubBase(_image, _namePrefix, _description, _trustedForwarder) CCIPCaller(_CCIProuter) {}

    // ============ Public View Functions ============

    function supportsInterface(bytes4 interfaceId) public view override(EeseeAssetHubBase, CCIPCaller) returns (bool) {
        return EeseeAssetHubBase.supportsInterface(interfaceId) || CCIPCaller.supportsInterface(interfaceId);
    }

    // ============ Internal View Functions ============

    function _chainSelectorToString(bytes memory chainSelector) internal pure override returns(string memory){
        uint256 chainSelectorUint = abi.decode(chainSelector, (uint64));
        return chainSelectorUint.toString();
    }
}
