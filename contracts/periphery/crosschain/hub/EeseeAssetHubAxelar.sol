// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../abstract/EeseeAssetHubBase.sol";
import "../abstract/AxelarCallerEeseeMessageInterface.sol";

contract EeseeAssetHubAxelar is EeseeAssetHubBase, AxelarCallerEeseeMessageInterface {
    constructor(
        string memory _image,
        string memory _namePrefix,
        string memory _description,

        IAxelarGateway _gateway, 
        IAxelarGasService _gasService,

        address _trustedForwarder
    ) EeseeAssetHubBase(_image, _namePrefix, _description, _trustedForwarder) AxelarGatewayBase(_gateway, _gasService) {}

    // ============ Internal View Functions ============

    function _chainSelectorToString(bytes memory chainSelector) internal pure override returns(string memory){
        return abi.decode(chainSelector, (string));
    }
}
