// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.17;

import "@biconomy/account-contracts/contracts/smart-account/modules/MultichainECDSAValidator.sol";
import "@biconomy/account-contracts/contracts/smart-account/common/Enum.sol";
import "@biconomy/account-contracts/contracts/smart-account/SmartAccount.sol";

interface IBlastPoints {
	function configurePointsOperator(address operator) external;
}

contract MultichainECDSAValidatorWithBlastPoints is MultichainECDSAValidator {
    address public immutable blastPointsAddress;
    mapping(address => bool) public isInitializedBlastPointsOperatorForAccount;

    error BlastPointsOperatorAlreadyInitialized();
    error InvalidBlastPointsAddress();
    error InvalidPointsOperator();
    error ExecTransacionFromModuleFailed();
    error InvalidSmartAccount();

    constructor (address _blastPointsAddress) {
        if (_blastPointsAddress == address(0)) revert InvalidBlastPointsAddress();
            
        blastPointsAddress = _blastPointsAddress;
    }

    function initializeBlastPointsOperator(address smartAccountAddress, address pointsOperator) external {
        if (pointsOperator == address(0)) revert InvalidPointsOperator();
        if (smartAccountAddress == address(0)) revert InvalidSmartAccount();
        if (isInitializedBlastPointsOperatorForAccount[smartAccountAddress]) revert BlastPointsOperatorAlreadyInitialized();
        isInitializedBlastPointsOperatorForAccount[smartAccountAddress] = true;

        bool success = SmartAccount(payable(smartAccountAddress)).execTransactionFromModule(
            blastPointsAddress, 
            0, 
            abi.encodeWithSelector(IBlastPoints.configurePointsOperator.selector, pointsOperator),
            Enum.Operation.Call,
            0
        );
        if (!success) revert ExecTransacionFromModuleFailed();
    }
}
