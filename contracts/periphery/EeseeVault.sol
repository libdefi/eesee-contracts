// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC1155/utils/ERC1155Holder.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import "@openzeppelin/contracts/metatx/ERC2771Context.sol";

/// @dev Contract to store and reclaim stuck eeseeAssetHub tokens.
contract EeseeVault is ERC2771Context, ERC1155Holder {
    ///@dev Asset hub ERC1155s this contract accepts.
    IERC1155 immutable public assetHub;
    /// @dev Amount of tokens stuck for a given account and tokenId.
    mapping(address => mapping(uint256 => uint256)) public stuck;

    error InvalidConstructor();
    error InvalidRecipient();
    error NoTokensStuck();
    error InvalidTokenSent();
    error InvalidTokenIdsLength();

    constructor(IERC1155 _assetHub, address _trustedForwarder) ERC2771Context(_trustedForwarder){
        if(address(_assetHub) == address(0)) revert InvalidConstructor();
        assetHub = _assetHub;
    }

    // ============ External Write Functions ============

    /**
     * @dev Reclaim assetHub ERC1155s that might have been stuck in this contract.
     * Note: Must be called from an address that had their asset stuck.
     * @param tokenIds - Tokens ids to claim.
     * @param recipient - Address to transfer tokens to.
     
     * @return amounts - Amounts of each tokenId claimed.
     */
    function unstuck(uint256[] calldata tokenIds, address recipient) external returns (uint256[] memory amounts) {
        if(recipient == address(0)) revert InvalidRecipient();
        if(tokenIds.length == 0) revert InvalidTokenIdsLength();
        amounts = new uint256[](tokenIds.length);

        address msgSender = _msgSender();
        for(uint256 i; i < tokenIds.length;){
            amounts[i] = stuck[msgSender][tokenIds[i]];
            if(amounts[i] == 0) revert NoTokensStuck();
            
            delete stuck[msgSender][tokenIds[i]];
            unchecked { ++i; }
        }
        assetHub.safeBatchTransferFrom(address(this), recipient, tokenIds, amounts, "");
    }

    /**
     * @dev Whennever an assetHub ERC1155 gets sent to this contract, write it to storage for later claiming with {unstuck} function.
     */
    function onERC1155Received(
        address /*from*/,
        address /*to*/,
        uint256 tokenId,
        uint256 amount,
        bytes memory data
    ) public override returns (bytes4) {
        if(msg.sender != address(assetHub)) revert InvalidTokenSent();

        address claimer = abi.decode(data, (address));
        _stuck(tokenId, amount, claimer);

        return this.onERC1155Received.selector;
    }

    /**
     * @dev Whennever an assetHub ERC1155 gets sent to this contract, write it to storage for later claiming with {unstuck} function.
     */
    function onERC1155BatchReceived(
        address /*from*/,
        address /*to*/,
        uint256[] memory tokenIds,
        uint256[] memory amounts,
        bytes memory data
    ) public override returns (bytes4) {     
        if(msg.sender != address(assetHub)) revert InvalidTokenSent();

        address claimer = abi.decode(data, (address));
        for(uint256 i; i < tokenIds.length;){
            _stuck(tokenIds[i], amounts[i], claimer);
            unchecked { ++i; }
        }

        return this.onERC1155BatchReceived.selector;
    }

    // ============ Internal Write Functions ============

    function _stuck(uint256 tokenId, uint256 amount, address claimer) internal {
        stuck[claimer][tokenId] += amount;
    }
}
