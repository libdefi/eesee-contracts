// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "./EeseeSwapBase.sol";
import "../abstract/EeseeOneInchBase.sol";

contract EeseeSwapOneInch is EeseeSwapBase, EeseeOneInchBase {
    constructor(
        address _treasury, 
        address _oneInchRouter, 
        IEeseeAccessManager _accessManager,
        address _trustedForwarder
    ) EeseeSwapBase(_treasury) EeseeOneInchBase(_oneInchRouter, _accessManager, _trustedForwarder) {}
}