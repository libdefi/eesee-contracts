// SPDX-License-Identifier: GPL-3.0-only
pragma solidity 0.8.21;

import "./EeseeSwapBase.sol";
import "../abstract/EeseeUniswapV3Base.sol";

contract EeseeSwapUniswapV3 is EeseeSwapBase, EeseeUniswapV3Base {
    constructor(
        address _treasury, 
        address _uniswapRouterV3, 
        IEeseeAccessManager _accessManager,
        address _trustedForwarder
    ) EeseeSwapBase(_treasury) EeseeUniswapV3Base(_uniswapRouterV3, _accessManager, _trustedForwarder) {}
}