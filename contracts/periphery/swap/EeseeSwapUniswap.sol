// SPDX-License-Identifier: GPL-3.0-only
pragma solidity 0.8.21;

import "./EeseeSwapBase.sol";
import "../abstract/EeseeUniswapBase.sol";

contract EeseeSwapUniswap is EeseeSwapBase, EeseeUniswapBase {
    constructor(
        address _treasury, 
        IUniswapV2Router02 _uniswapRouter, 
        IEeseeAccessManager _accessManager,
        address _trustedForwarder
    ) EeseeSwapBase(_treasury) EeseeUniswapBase(_uniswapRouter, _accessManager, _trustedForwarder) {}
}