// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../abstract/EeseeExecuteWithSwapBase.sol";
import "../../interfaces/IEeseeMarketplaceRouter.sol";
import "../../interfaces/IEeseeSwap.sol";

abstract contract EeseeSwapBase is IEeseeSwap, EeseeExecuteWithSwapBase {
    using SafeERC20 for IERC20;

    ///@dev Treasury contract address that leftover tokens get sent to.
    address public treasury;

    event CollectToTreasury(
        IERC20 indexed token,
        uint256 amount,
        address indexed treasury
    );

    event UpdateTreasury(
        address indexed previousTreasury,
        address indexed newTreasury
    );

    error InvalidRecipient();
    error InvalidTreasury();

    constructor(address _treasury) {
        _updateTreasury(_treasury);
    }

    // ============ External Write Functions ============

    /**
     * @dev Swap tokens and receive assets the sender chooses. Emits {ReceiveAsset} event for each of the asset received.
        Note: Any dust left after swap or asset purchases goes to eesee's treasury as fee.
     * @param swapParams - Struct containing swap data and addresses with data to call marketplace routers with.
     * @param recipient - Address to send rewards to.

     * @return srcToken - srcToken used in swap.
     * @return srcSpent - srcToken amount used.
     * @return assets - Assets received.
     */
    function swapTokensForAssets(
        SwapParams calldata swapParams,
        address recipient
    ) external payable returns(
        IERC20 srcToken,
        uint256 srcSpent,
        Asset[] memory assets
    ){
        if(recipient == address(0)) revert InvalidRecipient();
        bytes memory _data; uint256 srcDust;
        (srcToken, srcSpent, srcDust,,,,_data) = _executeWithSwap(
            swapParams.swapData, 
            abi.encode(swapParams.marketplaceRoutersData, recipient)
        );
        srcSpent += srcDust;
        assets = abi.decode(_data, (Asset[]));
    }

    /**
     * @dev Update treasury account address leftover tokens are sent to.
     * @param _treasury - New treasury account.
     */
    function updateTreasury(address _treasury) onlyAdmin external {
        _updateTreasury(_treasury);
    }

    // ============ Internal Write Functions ============

    function _execute(
        bytes memory targetCalldata,
        IERC20 dstToken,
        uint256 dstReturned
    ) internal override returns(bytes memory) {
        (MarketplaceRouterData[] memory marketplaceRoutersData, address recipient) = abi.decode(targetCalldata, (MarketplaceRouterData[], address));
        
        uint256 marketplaceRoutersLength = marketplaceRoutersData.length;
        Asset[] memory assets = new Asset[](marketplaceRoutersLength);

        uint256 tokenAmount = dstReturned;
        bool isETH = _isETH(dstToken);
        for(uint256 i; i < marketplaceRoutersLength;) {
            uint256 spent;
            if(isETH) {
                (assets[i], spent) = marketplaceRoutersData[i].router.purchaseAsset{value: tokenAmount}(marketplaceRoutersData[i].data, recipient);
                tokenAmount -= spent;
            } else {
                dstToken.safeTransfer(address(marketplaceRoutersData[i].router), tokenAmount);
                (assets[i], spent) = marketplaceRoutersData[i].router.purchaseAsset(marketplaceRoutersData[i].data, recipient);
                tokenAmount -= spent;
            }

            emit ReceiveAsset(assets[i], dstToken, spent, recipient);
            unchecked{ ++i; }
        }
        return abi.encode(assets);
    }

    function _ETHTransferValidation(IERC20 /*token*/, uint256 /*amount*/) internal view override returns(uint256) {
        return address(this).balance;
    }

    function _ERC20TransferValidation(IERC20 token, uint256 /*amount*/) internal view override returns(uint256){
        return token.balanceOf(address(this));
    }

    // ============ Internal View Functions ============

    function _refundRecipient() internal view override returns(address){
        return treasury;
    }

    // ============ Private Write Functions ============

    function _updateTreasury(address _treasury) private {
        if(_treasury == address(0)) revert InvalidTreasury();
        emit UpdateTreasury(treasury, _treasury);
        treasury = _treasury;
    }
}