// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.17;

import "@biconomy/account-contracts/contracts/smart-account/factory/SmartAccountFactory.sol";
import "./MultichainECDSAValidatorWithBlastPoints.sol";

contract SmartAccountFactoryWrapper{
    SmartAccountFactory immutable smartAccountFactory;
    MultichainECDSAValidatorWithBlastPoints immutable multichainECDSAValidatorWithBlastPoints;

    error InvalidSmartAccountFactory();
    error InvalidModule();

    constructor(
        SmartAccountFactory _smartAccountFactory, 
        MultichainECDSAValidatorWithBlastPoints _multichainECDSAValidatorWithBlastPoints
    ){
        if(address(_smartAccountFactory) == address(0)) revert InvalidSmartAccountFactory();
        if(address(_multichainECDSAValidatorWithBlastPoints) == address(0)) revert InvalidModule();

        smartAccountFactory = _smartAccountFactory;
        multichainECDSAValidatorWithBlastPoints = _multichainECDSAValidatorWithBlastPoints;
    }

    /**
     * @dev Deploys and initializes account if it was not deployed yet.
     * @param eoaOwner - Owner to deploy account for.
     * @param pointsOperator - Blast points operator.
     */
    function tryDeployCounterFactualAccount(address eoaOwner, address pointsOperator) external returns(address account){
        bytes memory moduleSetupData = abi.encodeWithSelector(
            multichainECDSAValidatorWithBlastPoints.initForSmartAccount.selector, 
            abi.encode(eoaOwner)
        );

        account = smartAccountFactory.getAddressForCounterFactualAccount(
            address(multichainECDSAValidatorWithBlastPoints), 
            moduleSetupData, 
            0
        );
        if(!_isSmartContract(account)){
            smartAccountFactory.deployCounterFactualAccount(
                address(multichainECDSAValidatorWithBlastPoints), 
                moduleSetupData, 
                0
            );
            multichainECDSAValidatorWithBlastPoints.initializeBlastPointsOperator(account, pointsOperator);
        }
    }

    /**
     * @dev Checks if the address provided is a smart contract.
     * @param account Address to be checked.
     */
    function _isSmartContract(address account) internal view returns (bool) {
        uint256 size;
        assembly {
            size := extcodesize(account)
        }
        return size > 0;
    }
}