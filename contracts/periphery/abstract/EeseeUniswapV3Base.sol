// SPDX-License-Identifier: GPL-3.0-only
pragma solidity 0.8.21;

import '@uniswap/v3-periphery/contracts/interfaces/ISwapRouter.sol';
import 'solidity-bytes-utils/contracts/BytesLib.sol';
import "../../libraries/SafeApproveMax.sol";
import "../../interfaces/IWETH.sol";
import "./EeseeExecuteWithSwapBase.sol";

interface IWETH9User{
    function WETH9() external view returns(address);
}    
abstract contract EeseeUniswapV3Base is EeseeExecuteWithSwapBase {
    using BytesLib for bytes;
    using SafeApproveMax for IERC20;
    
    ///@dev Uniswap router used for token swaps.
    address immutable public uniswapRouterV3;
    uint256 private constant ADDR_SIZE = 20;
    ///@dev WETH token address.
    address immutable public WETH;

    error InvalidRouter();
    error InvalidSelector();

    constructor(
        address _uniswapRouterV3,
        IEeseeAccessManager _accessManager,
        address trustedForwarder
    ) EeseeExecuteWithSwapBase(_accessManager, trustedForwarder) {
        if(_uniswapRouterV3 == address(0)) revert InvalidRouter();

        WETH = _getWETH(_uniswapRouterV3);
        uniswapRouterV3 = _uniswapRouterV3;
    }

    // ============ Internal Write Functions ============

    function _swap(bytes calldata swapData) internal override returns(
        IERC20 srcToken, 
        uint256 srcSpent, 
        IERC20 dstToken, 
        uint256 dstReturned
    ){
        bytes4 selector = bytes4(swapData[:4]);
        if(selector == ISwapRouter.exactOutputSingle.selector){
            ISwapRouter.ExactOutputSingleParams memory exactOutputSingleParams = abi.decode(swapData[4:], (ISwapRouter.ExactOutputSingleParams));
        
            srcToken = IERC20(exactOutputSingleParams.tokenIn);
            dstToken = IERC20(exactOutputSingleParams.tokenOut);
            exactOutputSingleParams.amountInMaximum = _transferValidation(srcToken, exactOutputSingleParams.amountInMaximum);

            if(srcToken == dstToken) {
                srcSpent = exactOutputSingleParams.amountInMaximum;
                dstReturned = exactOutputSingleParams.amountInMaximum;
            } else {
                uint256 value;
                if(_isETH(srcToken)){
                    exactOutputSingleParams.tokenIn = WETH;
                    value = exactOutputSingleParams.amountInMaximum;
                } else {
                    srcToken.safeApproveMax(exactOutputSingleParams.amountInMaximum, uniswapRouterV3);
                    if(_isETH(dstToken)) exactOutputSingleParams.tokenOut = WETH;
                }

                exactOutputSingleParams.recipient = address(this);
                exactOutputSingleParams.deadline = block.timestamp;
                (bool success, bytes memory returnData) = uniswapRouterV3.call{value: value}(abi.encodeWithSelector(selector, exactOutputSingleParams));
                if(!success) {
                    if (returnData.length == 0) revert();
                    assembly { revert(add(32, returnData), mload(returnData)) }
                }
                srcSpent = abi.decode(returnData, (uint256));
                dstReturned = exactOutputSingleParams.amountOut;
                if(_isETH(dstToken)) IWETH(WETH).withdraw(dstReturned);
            }
        } else if (selector == ISwapRouter.exactOutput.selector){
            ISwapRouter.ExactOutputParams memory exactOutputParams = abi.decode(swapData[4:], (ISwapRouter.ExactOutputParams));
        
            (srcToken, dstToken) = _decodeInOutTokensInPool(exactOutputParams.path);
            exactOutputParams.amountInMaximum = _transferValidation(srcToken, exactOutputParams.amountInMaximum);

            if(srcToken == dstToken) {
                srcSpent = exactOutputParams.amountInMaximum;
                dstReturned = exactOutputParams.amountInMaximum;
            } else {
                uint256 value;
                if(_isETH(srcToken)){
                    exactOutputParams.path = abi.encodePacked(exactOutputParams.path.slice(0, exactOutputParams.path.length - ADDR_SIZE), WETH);
                    value = exactOutputParams.amountInMaximum;
                } else {
                    srcToken.safeApproveMax(exactOutputParams.amountInMaximum, uniswapRouterV3);
                    if(_isETH(dstToken)) exactOutputParams.path = abi.encodePacked(WETH, exactOutputParams.path.slice(ADDR_SIZE, exactOutputParams.path.length - ADDR_SIZE));
                }
                
                exactOutputParams.recipient = address(this);
                exactOutputParams.deadline = block.timestamp;
                (bool success, bytes memory returnData) = uniswapRouterV3.call{value: value}(abi.encodeWithSelector(selector, exactOutputParams));
                if(!success) {
                    if (returnData.length == 0) revert();
                    assembly { revert(add(32, returnData), mload(returnData)) }
                }
                srcSpent = abi.decode(returnData, (uint256));
                dstReturned = exactOutputParams.amountOut;
                if(_isETH(dstToken)) IWETH(WETH).withdraw(dstReturned);
            }
        } else revert InvalidSelector();
    }

    function _decodeInOutTokensInPool(bytes memory path) internal pure returns (IERC20 tokenIn, IERC20 tokenOut) {
        tokenIn = IERC20(path.toAddress(path.length - ADDR_SIZE));
        tokenOut = IERC20(path.toAddress(0));
    }

    function _getWETH(address _uniswapRouterV3) internal view virtual returns(address){
        return IWETH9User(_uniswapRouterV3).WETH9();
    }
}