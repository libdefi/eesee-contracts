// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/metatx/ERC2771Context.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "../../abstract/roles/EeseePausable.sol";
import "../../abstract/roles/EeseeAdmin.sol";

abstract contract EeseeExecuteWithSwapBase is ERC2771Context, EeseePausable, EeseeAdmin {
    using SafeERC20 for IERC20;

    error EthDepositRejected();
    error InvalidMsgValue();
    error InvalidAmount();

    constructor(
        IEeseeAccessManager _accessManager,
        address trustedForwarder
    ) EeseeRoleHandler(_accessManager) ERC2771Context(trustedForwarder) {}

    receive() external payable {
        //Reject deposits from EOA
        if (_msgSender() == tx.origin) revert EthDepositRejected();
    }

    // =============== Internal Write Functions ===============

    function _executeWithSwap(
        bytes calldata swapData,
        bytes memory _data
    ) internal whenNotPaused returns(
        IERC20 srcToken,
        uint256 srcSpent,
        uint256 srcDust,
        IERC20 dstToken,
        uint256 dstAmount,
        uint256 dstDust,
        bytes memory returnData
    ){
        (srcToken, srcSpent, dstToken,) = _swap(swapData);
        if(srcToken != dstToken) srcDust = _refund(srcToken);
        
        uint256 dstAvailable = _balance(dstToken);
        returnData = _execute(_data, dstToken, dstAvailable);
        dstDust = _refund(dstToken);

        unchecked {
            if(dstAvailable > dstDust){
                dstAmount = dstAvailable - dstDust;
            } else {
                dstAmount = dstDust - dstAvailable;
            }
        }
    }

    function _swap(bytes calldata swapData) internal virtual returns(IERC20 srcToken, uint256 srcSpent, IERC20 dstToken, uint256 dstReturned);

    function _execute(bytes memory _data, IERC20 dstToken, uint256 dstAvailable) internal virtual returns(bytes memory returnData);

    function _transferValidation(IERC20 token, uint256 amount) internal returns (uint256 _amount) {
        if(_isETH(token)){
            _amount = _ETHTransferValidation(token, amount);
        } else {
            if(msg.value != 0) revert InvalidMsgValue();
            _amount = _ERC20TransferValidation(token, amount);
        }
        if(_amount == 0) revert InvalidAmount();
    }

    function _ETHTransferValidation(IERC20 token, uint256 amount) internal virtual returns(uint256);

    function _ERC20TransferValidation(IERC20 token, uint256 amount) internal virtual returns(uint256);

    function _refund(IERC20 token) internal returns(uint256 dust){
        if(_isETH(token)){
            dust = address(this).balance;
            if(dust > 0) _refundRecipient().call{value: dust, gas: 5000}(""); // Continue execution even if this fails
        }else{
            dust = token.balanceOf(address(this));
            if(dust > 0) token.safeTransfer(_refundRecipient(), dust);
        }
    }

    // =============== Internal View Functions ===============

    function _refundRecipient() internal view virtual returns(address);

    function _isETH(IERC20 token) internal pure returns(bool) {
        return (address(token) == address(0) || address(token) == address(0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE));
    }

    function _balance(IERC20 token) internal view returns(uint256) {
        return _isETH(token) ? address(this).balance : token.balanceOf(address(this));
    }
    
    function _msgSender() internal view override(Context, ERC2771Context) returns (address) {
        return ERC2771Context._msgSender();
    }

    function _msgData() internal view override(Context, ERC2771Context) returns (bytes calldata) {
        return ERC2771Context._msgData();
    }

    function _contextSuffixLength() internal view override(Context, ERC2771Context) returns (uint256) {
        return ERC2771Context._contextSuffixLength();
    }
}