// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../../libraries/SafeApproveMax.sol";
import "../../interfaces/IAggregationRouterV5.sol";
import "./EeseeExecuteWithSwapBase.sol";

abstract contract EeseeOneInchBase is EeseeExecuteWithSwapBase {
    using SafeApproveMax for IERC20;

    ///@dev 1inch router used for token swaps.
    address immutable public oneInchRouter;

    error InvalidSwapDescription();
    error SwapNotSuccessful();
    error InvalidRouter();

    constructor(
        address _oneInchRouter,
        IEeseeAccessManager _accessManager,
        address trustedForwarder
    ) EeseeExecuteWithSwapBase(_accessManager, trustedForwarder) {
        if(_oneInchRouter == address(0)) revert InvalidRouter();
        oneInchRouter = _oneInchRouter;
    }

    // ============ Internal Write Functions ============

    function _swap(bytes calldata swapData) internal override returns(
        IERC20 srcToken, 
        uint256 srcSpent, 
        IERC20 dstToken, 
        uint256 dstReturned
    ){
        (,IAggregationRouterV5.SwapDescription memory desc,) = abi.decode(swapData[4:], (address, IAggregationRouterV5.SwapDescription, bytes));
        srcToken = desc.srcToken;
        dstToken = desc.dstToken;
        _transferValidation(srcToken, desc.amount);

        if(srcToken == dstToken) {
            srcSpent = desc.amount;
            dstReturned = desc.amount;
        } else {
            if(
                bytes4(swapData[:4]) != IAggregationRouterV5.swap.selector || 
                desc.dstReceiver != address(this)
            ) revert InvalidSwapDescription();

            uint256 value;
            if(_isETH(srcToken)) {
                value = desc.amount;
            }else srcToken.safeApproveMax(desc.amount, oneInchRouter);

            (bool success, bytes memory data) = oneInchRouter.call{value: value}(swapData);
            if(!success) revert SwapNotSuccessful();
            (dstReturned, srcSpent) = abi.decode(data, (uint256, uint256));
        }
    }
}