// SPDX-License-Identifier: GPL-3.0-only
pragma solidity 0.8.21;

import "../../libraries/SafeApproveMax.sol";
import "../../interfaces/IUniswapV2Router02.sol";
import "./EeseeExecuteWithSwapBase.sol";

abstract contract EeseeUniswapBase is EeseeExecuteWithSwapBase {
    using SafeApproveMax for IERC20;
    
    ///@dev Uniswap router used for token swaps.
    IUniswapV2Router02 immutable public uniswapRouter;
    ///@dev WETH token address.
    address immutable public WETH;

    error InvalidRouter();

    constructor(
        IUniswapV2Router02 _uniswapRouter,
        IEeseeAccessManager _accessManager,
        address trustedForwarder
    ) EeseeExecuteWithSwapBase(_accessManager, trustedForwarder) {
        if(address(_uniswapRouter) == address(0)) revert InvalidRouter();

        WETH = _getWETH(address(_uniswapRouter));
        uniswapRouter = _uniswapRouter;
    }

    // ============ Internal Write Functions ============

    function _swap(bytes calldata swapData) internal override returns(
        IERC20 srcToken, 
        uint256 srcSpent, 
        IERC20 dstToken, 
        uint256 dstReturned
    ){
        (uint256 amountOut, uint256 amountInMax, address[] memory path) = abi.decode(swapData, (uint256, uint256, address[]));
                
        uint256 lastIndex = path.length - 1;
        srcToken = IERC20(path[0]);
        dstToken = IERC20(path[lastIndex]);
        amountInMax = _transferValidation(srcToken, amountInMax);

        if(srcToken == dstToken) {
            srcSpent = amountInMax;
            dstReturned = amountInMax;
        } else {
            uint256[] memory amounts;
            if(_isETH(dstToken)){
                path[lastIndex] = WETH;
                srcToken.safeApproveMax(amountInMax, address(uniswapRouter));
                amounts = uniswapRouter.swapTokensForExactETH(amountOut, amountInMax, path, address(this), block.timestamp);
            }else{
                if(_isETH(srcToken)){
                    path[0] = WETH;
                    amounts = uniswapRouter.swapETHForExactTokens{value: amountInMax}(amountOut, path, address(this), block.timestamp);
                }else{
                    srcToken.safeApproveMax(amountInMax, address(uniswapRouter));
                    amounts = uniswapRouter.swapTokensForExactTokens(amountOut, amountInMax, path, address(this), block.timestamp);
                }
            }
            dstReturned = amountOut;
            srcSpent = amounts[0];
        }
    }

    function _getWETH(address _uniswapRouter) internal view virtual returns(address){
        return IUniswapV2Router02(_uniswapRouter).WETH();
    }
}