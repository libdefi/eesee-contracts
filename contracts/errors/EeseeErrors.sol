// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

error InvalidToken();
error InvalidAmount();
error InsufficientValue();