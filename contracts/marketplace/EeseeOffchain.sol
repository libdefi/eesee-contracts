// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/token/ERC1155/IERC1155.sol";
import "@openzeppelin/contracts/metatx/ERC2771Context.sol";
import "@openzeppelin/contracts/token/ERC721/utils/ERC721Holder.sol";
import "@openzeppelin/contracts/token/ERC1155/utils/ERC1155Holder.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/utils/cryptography/EIP712.sol";
import "../abstract/roles/EeseeRoleHandler.sol";
import "../abstract/BlastPointsReceiver.sol";
import "../types/Asset.sol";

///@dev Contract for offchain eesee lots logic.
contract EeseeOffchain is ERC2771Context, ERC721Holder, ERC1155Holder, EIP712, EeseeRoleHandler, BlastPointsReceiver {
    using SafeERC20 for IERC20;

    struct SignatureData{
        uint256 nonce;
        uint256 deadline;
        bytes signature;
    }

    ///@dev Is given nonce used.
    mapping(uint256 => bool) public nonceUsed;

    ///@dev Signer role af defined in {accessManager}.
    bytes32 private constant SIGNER_ROLE = keccak256("SIGNER_ROLE"); 

    ///@dev Lot typehash.
    bytes32 private constant CREATE_LOT_TYPEHASH = keccak256("CreateLot(bytes32 assetHash,address sender,bytes32 dataHash,uint256 nonce,uint256 deadline)");
    ///@dev Ticket typehash.
    bytes32 private constant BUY_TICKETS_TYPEHASH = keccak256("BuyTickets(bytes32 ID,bytes32 assetHash,address sender,address recipient,uint256 nonce,uint256 deadline)");
    ///@dev Asset claim typehash.
    bytes32 private constant CLAIM_ASSET_TYPEHASH = keccak256("ClaimAsset(bytes32 ID,bytes32 assetHash,address sender,address recipient,bytes32 callHash,uint256 nonce,uint256 deadline)");

    event CreateLot(
        bytes32 indexed ID, 
        Asset asset, 
        address indexed sender, 
        bytes data, 
        uint256 indexed nonce
    );
    event BuyTickets(
        bytes32 indexed ID, 
        Asset asset, 
        address sender, 
        address indexed recipient, 
        uint256 indexed nonce
    );
    event ClaimAsset(
        bytes32 indexed ID, 
        Asset asset, 
        address indexed sender, 
        address recipient, 
        uint256 indexed nonce
    );
    event RevokeSignature(uint256 indexed nonce);

    error InvalidRecipient();
    error ExpiredDeadline();
    error NonceUsed();
    error InvalidAmount();
    error InvalidSignature();
    error InvalidValue();
    error TransferNotSuccessful();
    error InvalidAssetType();
    
    modifier isValidRecipient(address recipient){
        if(recipient == address(0)) revert InvalidRecipient();
        _;
    }

    constructor(
        IEeseeAccessManager _accessManager,
        IBlastPoints _blastPoints,
        address _blastPointsOperator,
        address trustedForwarder
    ) ERC2771Context(trustedForwarder) BlastPointsReceiver(_blastPoints, _blastPointsOperator) EIP712("EeseeOffchain", "1") EeseeRoleHandler(_accessManager) {}

    // ============ Public Write Functions ============

    /**
     * @dev Creates lot with offchain logic. Emits {CreateLot} event.
     * @param asset - Assets to list. Note: The sender must have them approved for this contract.
     * @param data - Lot data for offchain logic.
     * @param signatureData - Signature for asset and data signed by SIGNER_ROLE.

     * @return ID - ID of created lot.
     */
    function createLot(
        Asset calldata asset,
        bytes calldata data,
        SignatureData calldata signatureData
    ) external payable returns(bytes32 ID){
        address msgSender = _msgSender();
        _checkSignature(
            msgSender, 
            _getCreateLotStructHash(asset, msgSender, data, signatureData.nonce, signatureData.deadline), 
            signatureData
        );
        _transferAssetFrom(asset, msgSender);
        ID = getLotId(signatureData.nonce);

        emit CreateLot(ID, asset, msgSender, data, signatureData.nonce);
    }

    /**
     * @dev Buys tickets to participate in a lot. Emits {BuyTickets} event.
     * @param ID - ID of lot to buy tickets in.
     * @param asset - Asset to buy tickets with.
     * @param recipient - Recipient of tickets.
     * @param signatureData - Signature for ID, asset and recipient signed by SIGNER_ROLE.
     */
    function buyTickets(
        bytes32 ID,
        Asset calldata asset,
        address recipient,
        SignatureData calldata signatureData
    ) isValidRecipient(recipient) external payable {
        address msgSender = _msgSender();
        _checkSignature(
            msgSender, 
            _getBuyTicketsStructHash(ID, asset, msgSender, recipient, signatureData.nonce, signatureData.deadline), 
            signatureData
        );
        _transferAssetFrom(asset, msgSender);

        emit BuyTickets(ID, asset, msgSender, recipient, signatureData.nonce);
    }

    /**
     * @dev Claims asset from a lot. Emits {ClaimAsset} event.
     * @param ID - ID of lot to claim asset in.
     * @param asset - Asset to claim and send to recipient.
     * @param recipient - Recipient of asset.
     * @param _call - Additional external call to make.
     * @param signatureData - Signature for ID, asset, msgSender, recipient and _call signed by SIGNER_ROLE.

     * @return returnData - Data returned from external call.
     */
    function claimAsset(
        bytes32 ID,
        Asset calldata asset, 
        address recipient,
        bytes calldata _call,
        SignatureData calldata signatureData
    ) isValidRecipient(recipient) external payable returns (bytes memory returnData){
        address msgSender = _msgSender();
        _checkSignature(
            msgSender, 
            _getClaimAssetStructHash(ID, asset, msgSender, recipient, _call, signatureData.nonce, signatureData.deadline), 
            signatureData
        );
        _transferAssetTo(asset, recipient);

        if(_call.length != 0) {
            (address target, uint256 value, bytes memory callData) = abi.decode(_call, (address, uint256, bytes));
            if(msg.value != value) revert InvalidValue();
            bool success;
            (success, returnData) = target.call{value: value}(callData);
            if (!success) {
                if (returnData.length == 0) revert();
                assembly { revert(add(32, returnData), mload(returnData)) }
            }
        }
        emit ClaimAsset(ID, asset, msgSender, recipient, signatureData.nonce);
    }

    /**
     * @dev Callable by SIGNER_ROLE to revoke signatures. Emits {RevokeSignature} for each signature revoked.
     * @param nonces - Signature nonces to revoke.
     */
    function revokeSignatures(uint256[] calldata nonces) external {
        if(!_isSigner(_msgSender())) revert CallerNotAuthorized();
        for(uint256 i; i < nonces.length;){
            if(nonceUsed[nonces[i]] == false){
                nonceUsed[nonces[i]] = true;
                emit RevokeSignature(nonces[i]);
            }
            unchecked { ++i; }
        }
    }

    // ============ Public View Functions ============

    function getLotId(uint256 nonce) public pure returns (bytes32){
        return keccak256(abi.encode(nonce));
    }

    // ============ Internal Write Functions ============

    function _checkSignature(
        address msgSender,
        bytes32 structHash,
        SignatureData calldata signatureData
    ) internal {
        if(nonceUsed[signatureData.nonce]) revert NonceUsed();
        nonceUsed[signatureData.nonce] = true;
        if(_isSigner(msgSender)) return;
        if(block.timestamp > signatureData.deadline) revert ExpiredDeadline();

        address signer = ECDSA.recover(_hashTypedDataV4(structHash), signatureData.signature);
        if(!_isSigner(signer)) revert InvalidSignature();
    }
    
    function _transferAssetFrom(
        Asset calldata asset,
        address from
    ) internal {
        if(asset.amount == 0) {
            if(msg.value != 0) revert InvalidValue();
            return;
        }
        
        if (asset.assetType == AssetType.Native) {
            if(msg.value != asset.amount) revert InvalidValue();
        } else {
            if(msg.value != 0) revert InvalidValue();
            if (asset.assetType == AssetType.ERC721) {
                if(asset.amount != 1) revert InvalidAmount();
                IERC721(asset.token).safeTransferFrom(from, address(this), asset.tokenID, asset.data);
            } else if (asset.assetType == AssetType.ERC1155) {
                IERC1155(asset.token).safeTransferFrom(from, address(this), asset.tokenID, asset.amount, asset.data);
            } else if (asset.assetType == AssetType.ERC20) {
                _permit(asset.token, from, asset.data);
                IERC20(asset.token).safeTransferFrom(from, address(this), asset.amount);
            } else if (asset.assetType == AssetType.ESE) {

            } else revert InvalidAssetType();
        }
    }

    function _transferAssetTo(
        Asset calldata asset,
        address to
    ) internal {
        if(asset.amount == 0) return;
        if (asset.assetType == AssetType.ERC721) {
            IERC721(asset.token).safeTransferFrom(address(this), to, asset.tokenID, asset.data);
        } else if (asset.assetType == AssetType.ERC1155) {
            IERC1155(asset.token).safeTransferFrom(address(this), to, asset.tokenID, asset.amount, asset.data);
        } else if (asset.assetType == AssetType.ERC20) {
            IERC20(asset.token).safeTransfer(to, asset.amount);
        } else if (asset.assetType == AssetType.Native) {
            (bool success,) = to.call{value: asset.amount}(asset.data);
            if(!success) revert TransferNotSuccessful(); 
        } else revert InvalidAssetType();
    }

    function _permit(address token, address spender, bytes calldata permit) internal {
        if(permit.length == 0) return;
        (uint256 approveAmount, uint256 deadline, uint8 v, bytes32 r, bytes32 s) = abi.decode(permit, (uint256, uint256, uint8, bytes32, bytes32));
        IERC20Permit(token).permit(spender, address(this), approveAmount, deadline, v, r, s);
    }

    // ============ Internal View Functions ============

    function _getCreateLotStructHash(
        Asset calldata asset, 
        address sender, 
        bytes calldata data, 
        uint256 nonce, 
        uint256 deadline
    ) internal pure returns (bytes32){
        return keccak256(abi.encode(
            CREATE_LOT_TYPEHASH, 
            keccak256(abi.encode(asset)),
            sender,
            keccak256(data),
            nonce,
            deadline
        ));
    }

    function _getBuyTicketsStructHash(
        bytes32 ID, 
        Asset calldata asset, 
        address sender, 
        address recipient, 
        uint256 nonce, 
        uint256 deadline
    ) internal pure returns (bytes32){
        return keccak256(abi.encode(
            BUY_TICKETS_TYPEHASH, 
            ID,
            keccak256(abi.encode(asset)),
            sender,
            recipient,
            nonce,
            deadline
        ));
    }

    function _getClaimAssetStructHash(
        bytes32 ID, 
        Asset calldata asset, 
        address sender, 
        address recipient, 
        bytes calldata _call,
        uint256 nonce, 
        uint256 deadline
    ) internal pure returns (bytes32){
        return keccak256(abi.encode(
            CLAIM_ASSET_TYPEHASH, 
            ID,
            keccak256(abi.encode(asset)),
            sender,
            recipient,
            keccak256(_call),
            nonce,
            deadline
        ));
    }

    function _isSigner(address _addr) internal view returns(bool){
        return _hasRole(SIGNER_ROLE, _addr);
    }

    function _msgSender() internal view override(Context, ERC2771Context) returns (address) {
        return ERC2771Context._msgSender();
    }

    function _msgData() internal view override(Context, ERC2771Context) returns (bytes calldata) {
        return ERC2771Context._msgData();
    }

    function _contextSuffixLength() internal view override(Context, ERC2771Context) returns (uint256) {
        return ERC2771Context._contextSuffixLength();
    }
}