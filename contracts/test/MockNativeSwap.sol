// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract MockNativeSwap  {
    IERC20 immutable token;
    constructor(IERC20 _token) {
        token = _token;
    }
    function nativeSwap(address recipient) external payable {
        token.transfer(recipient, msg.value);
    }
}