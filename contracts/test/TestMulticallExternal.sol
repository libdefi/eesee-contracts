// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../abstract/MulticallExternal.sol";

contract TestMulticallExternal is MulticallExternal {
    function addToBlacklist(address target, bytes4 functionSignature) external {
        _addToBlacklist(target, functionSignature);
    }

    function removeFromBlacklist(address target, bytes4 functionSignature) external {
        _removeFromBlacklist(target, functionSignature);
    }
}