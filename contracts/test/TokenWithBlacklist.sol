// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import '@openzeppelin/contracts/token/ERC20/ERC20.sol';

contract TokenWithBlacklist is ERC20 {
    mapping (address => bool) isBlacklisted;
    bool private isReentrant;
    constructor(uint256 amount) ERC20("erc", "ERC20") {
        _mint(msg.sender, amount);
    }
    function decimals() public pure override returns (uint8) {
        return 6;
    }
    function blacklist(address _account) external{
        isBlacklisted[_account] = true;
    }
    function unBlacklist(address _account) external {
        isBlacklisted[_account] = false;
    }
    function transferAndCall(address to, uint256 amount, bytes calldata) external returns (bool){
        _transfer(msg.sender, to, amount);
    }
    function _beforeTokenTransfer(address from, address to, uint256) internal view override {
        if (isBlacklisted[to] || isBlacklisted[from]) revert();
    }
}