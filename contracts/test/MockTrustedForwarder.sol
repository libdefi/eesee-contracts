// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

contract MockTrustedForwarder {
    error CallFailed();

    function callExternal(
        address to,
        bytes memory data
    ) external payable {
        (bool success,) = to.call{value: msg.value}(abi.encodePacked(data, msg.sender));
        if(!success) revert CallFailed();
    }
}