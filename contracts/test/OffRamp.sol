// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import {Internal} from "@chainlink/contracts-ccip/src/v0.8/ccip/libraries/Internal.sol";

import {Client} from "@chainlink/contracts-ccip/src/v0.8/ccip/libraries/Client.sol";
import {Address} from "@chainlink/contracts-ccip/src/v0.8/vendor/openzeppelin-solidity/v4.8.0/contracts/utils/Address.sol";
import {ERC165Checker} from "@chainlink/contracts-ccip/src/v0.8/vendor/openzeppelin-solidity/v4.8.0/contracts/utils/introspection/ERC165Checker.sol";
import "../interfaces/IEeseeAccessManager.sol";
import {IAny2EVMMessageReceiver} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IAny2EVMMessageReceiver.sol";
import {IRouter} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IRouter.sol";

contract OffRamp  {
  using Address for address;
  using ERC165Checker for address;

  error AlreadyAttempted(uint64 sequenceNumber);
  error AlreadyExecuted(uint64 sequenceNumber);
  error ZeroAddressNotAllowed();
  error ExecutionError(bytes error);

  error CanOnlySelfCall();
  error ReceiverError(bytes error);
  error TokenHandlingError(bytes error);
  error EmptyReport();

  error UnauthorizedTransmitter();

  /// @dev Atlas depends on this event, if changing, please notify Atlas.
  /// @dev RMN depends on this event, if changing, please notify the RMN maintainers.
  event ExecutionStateChanged(
    uint64 indexed sequenceNumber,
    bytes32 indexed messageId,
    Internal.MessageExecutionState state,
    bytes returnData
  );


  IRouter internal immutable i_router;

  ///@dev Access manager for Eesee contract ecosystem.
  IEeseeAccessManager public immutable accessManager;
  ///@dev Pauser role in {accessManager}.
  bytes32 public constant TRANSMITTER_ROLE = keccak256("TRANSMITTER_ROLE");

  modifier onlyTransmitter(){
      if(!accessManager.hasRole(TRANSMITTER_ROLE, msg.sender)) revert UnauthorizedTransmitter();
      _;
  }

  constructor(
    IEeseeAccessManager _accessManager,
    IRouter router
  ){
    if (address(_accessManager) == address(0)) revert ZeroAddressNotAllowed();
    // already has roots committed.
    accessManager = _accessManager;
    i_router = router;
  }

  // ================================================================
  // │                          Messaging                           │
  // ================================================================

  /// @notice Entrypoint for execution, called by the OCR network
  /// @dev Expects an encoded ExecutionReport
  function transmit(Internal.EVM2EVMMessage calldata message) external onlyTransmitter {
    _execute(message, new uint256[](0));
  }

  /// @notice Executes a report, executing each message in order.
  /// @param message The execution report containing the messages and proofs.
  /// @dev If called from the DON, this array is always empty.
  /// @dev If called from manual execution, this array is always same length as messages.
  function _execute(Internal.EVM2EVMMessage memory message, uint256[] memory) internal {
      (Internal.MessageExecutionState newState, bytes memory returnData) = _trialExecute(message);
      emit ExecutionStateChanged(message.sequenceNumber, message.messageId, newState, returnData);
  }

  /// @notice Try executing a message.
  /// @param message Internal.EVM2EVMMessage memory message.
  /// @return the new state of the message, being either SUCCESS or FAILURE.
  /// @return revert data in bytes if CCIP receiver reverted during execution.
  function _trialExecute(
    Internal.EVM2EVMMessage memory message
  ) internal returns (Internal.MessageExecutionState, bytes memory) {
    try this.executeSingleMessage(message) {} catch (bytes memory err) {
      if (ReceiverError.selector == bytes4(err) || TokenHandlingError.selector == bytes4(err)) {
        // If CCIP receiver execution is not successful, bubble up receiver revert data,
        // prepended by the 4 bytes of ReceiverError.selector or TokenHandlingError.selector
        // Max length of revert data is Router.MAX_RET_BYTES, max length of err is 4 + Router.MAX_RET_BYTES
        return (Internal.MessageExecutionState.FAILURE, err);
      } else {
        // If revert is not caused by CCIP receiver, it is unexpected, bubble up the revert.
        revert ExecutionError(err);
      }
    }
    // If message execution succeeded, no CCIP receiver return data is expected, return with empty bytes.
    return (Internal.MessageExecutionState.SUCCESS, "");
  }
  
  /// @notice Execute a single message.
  /// @param message The message that will be executed.
  /// @dev We make this external and callable by the contract itself, in order to try/catch
  /// its execution and enforce atomicity among successful message processing and token transfer.
  /// @dev We use ERC-165 to check for the ccipReceive interface to permit sending tokens to contracts
  /// (for example smart contract wallets) without an associated message.
  function executeSingleMessage(Internal.EVM2EVMMessage memory message) external {
    if (msg.sender != address(this)) revert CanOnlySelfCall();
    Client.EVMTokenAmount[] memory destTokenAmounts = new Client.EVMTokenAmount[](0);
    if (
      !message.receiver.isContract() || !message.receiver.supportsInterface(type(IAny2EVMMessageReceiver).interfaceId)
    ) return;

    (bool success, bytes memory returnData, ) = i_router.routeMessage(
      Internal._toAny2EVMMessage(message, destTokenAmounts),
      Internal.GAS_FOR_CALL_EXACT_CHECK,
      message.gasLimit,
      message.receiver
    );
    // If CCIP receiver execution is not successful, revert the call including token transfers
    if (!success) revert ReceiverError(returnData);
  }
}
