// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import {Client} from "@chainlink/contracts-ccip/src/v0.8/ccip/libraries/Client.sol";
import {IRouterClient} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IRouterClient.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "../abstract/crosschain/CCIPCaller.sol";
import "../types/AddressWithChain.sol";

contract MockCCIPCaller is CCIPCaller {
    constructor(IRouterClient _CCIPRouter) CCIPCaller(_CCIPRouter) {}

    function transferTokens(IERC20 token, address useTokensFrom, uint256 tokenAmount) external {
        _transferTokens(token, useTokensFrom, tokenAmount);
    }

    function _ccipReceive(
        AddressWithChain memory source, 
        bytes memory data, 
        Client.EVMTokenAmount[] memory destTokenAmounts
    ) internal override {

    }
}
