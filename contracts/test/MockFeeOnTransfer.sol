// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Permit.sol";

contract MockFeeOnTransfer is ERC20Permit {
    error Stuck();
    
    bool public stuck;
    constructor(uint _totalSupply) ERC20("erc", "ERC20") ERC20Permit("ERC20") {
        _mint(msg.sender, _totalSupply);
    }

    function toggleStuck() external {
        stuck = !stuck;
    }

    function getReceivedAmount(
        address, /*_from */
        address, /*_to*/
        uint _sentAmount
    ) public pure returns (uint receivedAmount, uint feeAmount) {
        receivedAmount = (_sentAmount * 99) / 100;
        feeAmount = _sentAmount - receivedAmount;
    }

    function _transfer(
        address from,
        address to,
        uint value
    ) internal override {
        if(stuck) revert Stuck();
        (uint transferAmount, uint burnAmount) = getReceivedAmount(from, to, value);
        _burn(from, burnAmount);
        super._transfer(from, to, transferAmount);
    }
}