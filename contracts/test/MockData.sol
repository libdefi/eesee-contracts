// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@axelar-network/axelar-gmp-sdk-solidity/contracts/libs/AddressBytes.sol";

contract MockData {
    using AddressBytes for address;

    function encode(address address1, address address2) external pure returns (bytes memory params) {
        return abi.encode(address1.toBytes(), address2);
    }
}