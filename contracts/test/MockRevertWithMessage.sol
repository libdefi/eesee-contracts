// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

contract MockRevertWithMessage {
    function initialize(bytes memory) external{
        revert("goodbye world");
    }
}