
// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

contract MockBlastPoints {
    address public invalidOperator;

    event Registered(address operator);

    constructor(address _invalidOperator) {
        invalidOperator = _invalidOperator;
    }

	function configurePointsOperator(address operator) external {
        if (operator == invalidOperator) revert();

        emit Registered(operator);
    }
}
