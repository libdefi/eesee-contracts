// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../interfaces/IGelatoVRFConsumer.sol";

contract MockVRFGelatoOperator {
    IGelatoVRFConsumer public consumer;

    function setConsumer(IGelatoVRFConsumer _consumer) external {
        consumer = _consumer;
    }

    function fulfillWords(uint256 requestId, uint256 round, bytes calldata data) external {
        bytes memory dataWithRound = abi.encode(round, data);
        consumer.fulfillRandomness(requestId, dataWithRound);
    }
}
