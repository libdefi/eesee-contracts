// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;
import {ITypeAndVersion} from "@chainlink/contracts-ccip/src/v0.8/shared/interfaces/ITypeAndVersion.sol";
import {IRouterClient} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IRouterClient.sol";
import {IRouter} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IRouter.sol";
import {IEVM2AnyOnRamp} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IEVM2AnyOnRamp.sol";
import {IARM} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IARM.sol";
import {IWrappedNative} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IWrappedNative.sol";
import {IAny2EVMMessageReceiver} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IAny2EVMMessageReceiver.sol";

import {Client} from "@chainlink/contracts-ccip/src/v0.8/ccip/libraries/Client.sol";
import {Internal} from "@chainlink/contracts-ccip/src/v0.8/ccip/libraries/Internal.sol";
import {CallWithExactGas} from "@chainlink/contracts-ccip/src/v0.8/shared/call/CallWithExactGas.sol";
import {OwnerIsCreator} from "@chainlink/contracts-ccip/src/v0.8/shared/access/OwnerIsCreator.sol";

import {EnumerableSet} from "@chainlink/contracts-ccip/src/v0.8/vendor/openzeppelin-solidity/v4.8.0/contracts/utils/structs/EnumerableSet.sol";
import {SafeERC20} from "@chainlink/contracts-ccip/src/v0.8/vendor/openzeppelin-solidity/v4.8.0/contracts/token/ERC20/utils/SafeERC20.sol";
import {IERC20} from "@chainlink/contracts-ccip/src/v0.8/vendor/openzeppelin-solidity/v4.8.0/contracts/token/ERC20/IERC20.sol";

/// @title Router
/// @notice This is the entry point for the end user wishing to send data across chains.
/// @dev This contract is used as a router for both on-ramps and off-ramps
contract MockCCIPRouter is IRouter, IRouterClient, OwnerIsCreator {
  using SafeERC20 for IERC20;
  using EnumerableSet for EnumerableSet.UintSet;

  error FailedToSendValue();
  error InvalidRecipientAddress(address to);
  error OffRampMismatch(uint64 chainSelector, address offRamp);
  error BadARMSignal();

  event OnRampSet(uint64 indexed destChainSelector, address onRamp);
  event OffRampAdded(uint64 indexed sourceChainSelector, address offRamp);
  event OffRampRemoved(uint64 indexed sourceChainSelector, address offRamp);
  event MessageExecuted(bytes32 messageId, uint64 sourceChainSelector, address offRamp, bytes32 calldataHash);

  struct OnRamp {
    uint64 destChainSelector;
    address onRamp;
  }

  struct OffRamp {
    uint64 sourceChainSelector;
    address offRamp;
  }

  // solhint-disable-next-line chainlink-solidity/all-caps-constant-storage-variables
  string public constant typeAndVersion = "Router 1.2.0";
  // We limit return data to a selector plus 4 words. This is to avoid
  // malicious contracts from returning large amounts of data and causing
  // repeated out-of-gas scenarios.
  uint16 public constant MAX_RET_BYTES = 4 + 4 * 32;
  // STATIC CONFIG
  // Address of arm proxy contract.
  address private immutable i_armProxy;

  // DYNAMIC CONFIG
  address private s_wrappedNative;
  // destChainSelector => onRamp address
  // Only ever one onRamp enabled at a time for a given destChainSelector.
  mapping(uint256 destChainSelector => address onRamp) private s_onRamps;
  // Stores [sourceChainSelector << 160 + offramp] as a pair to allow for
  // lookups for specific chain/offramp pairs.
  EnumerableSet.UintSet private s_chainSelectorAndOffRamps;

  constructor(/*address wrappedNative, address armProxy*/) {
    // Zero address indicates unsupported auto-wrapping, therefore, unsupported
    // native fee token payments.
    // s_wrappedNative = wrappedNative;
    // i_armProxy = armProxy;
  }

  // ================================================================
  // │                       Message sending                        │
  // ================================================================

  /// @inheritdoc IRouterClient
  function getFee(
    uint64 destinationChainSelector,
    Client.EVM2AnyMessage memory message
  ) external view returns (uint256 fee) {
    return 10000;
  }

  /// @inheritdoc IRouterClient
  function getSupportedTokens(uint64 chainSelector) external view returns (address[] memory) {
    if (!isChainSupported(chainSelector)) {
      return new address[](0);
    }
    return IEVM2AnyOnRamp(s_onRamps[uint256(chainSelector)]).getSupportedTokens(chainSelector);
  }

  /// @inheritdoc IRouterClient
  function isChainSupported(uint64 chainSelector) public view returns (bool) {
    return true;
  }

  /// @inheritdoc IRouterClient
  function ccipSend(
    uint64 destinationChainSelector,
    Client.EVM2AnyMessage memory message
  ) external payable returns (bytes32) {
    uint256 feeTokenAmount = 10000;
    if (message.feeToken == address(0)) {
      if (msg.value < feeTokenAmount) revert InsufficientFeeTokenAmount();
    } else {
      if (msg.value > 0) revert InvalidMsgValue();
      IERC20(message.feeToken).transferFrom(msg.sender, address(this), feeTokenAmount);
    }

    // Transfer the tokens to the token pools.
    for (uint256 i = 0; i < message.tokenAmounts.length; ++i) {
      IERC20 token = IERC20(message.tokenAmounts[i].token);
      // We rely on getPoolBySourceToken to validate that the token is whitelisted.
      token.safeTransferFrom(
        msg.sender,
        abi.decode(message.receiver, (address)),
        message.tokenAmounts[i].amount
      );
    }

    return 0x0000000000000000000000000000000000000000000000000000000000000023;
  }

  // ================================================================
  // │                      Message execution                       │
  // ================================================================

  /// @inheritdoc IRouter
  /// @dev _callWithExactGas protects against return data bombs by capping the return data size at MAX_RET_BYTES.
  function routeMessage(
    Client.Any2EVMMessage calldata message,
    uint16 gasForCallExactCheck,
    uint256 gasLimit,
    address receiver
  ) external override returns (bool success, bytes memory retData, uint256 gasUsed) {

    // We encode here instead of the offRamps to constrain specifically what functions
    // can be called from the router.
    bytes memory data = abi.encodeWithSelector(IAny2EVMMessageReceiver.ccipReceive.selector, message);

    (success, retData, gasUsed) = CallWithExactGas._callWithExactGasSafeReturnData(
      data,
      receiver,
      gasLimit,
      gasForCallExactCheck,
      Internal.MAX_RET_BYTES
    );

    emit MessageExecuted(message.messageId, message.sourceChainSelector, msg.sender, keccak256(data));
    return (success, retData, gasUsed);
  }

  // @notice Merges a chain selector and offRamp address into a single uint256 by shifting the
  // chain selector 160 bits to the left.
  function _mergeChainSelectorAndOffRamp(
    uint64 sourceChainSelector,
    address offRampAddress
  ) internal pure returns (uint256) {
    return (uint256(sourceChainSelector) << 160) + uint160(offRampAddress);
  }

  // ================================================================
  // │                           Config                             │
  // ================================================================

  /// @notice Gets the wrapped representation of the native fee coin.
  /// @return The address of the ERC20 wrapped native.
  function getWrappedNative() external view returns (address) {
    return s_wrappedNative;
  }

  /// @notice Sets a new wrapped native token.
  /// @param wrappedNative The address of the new wrapped native ERC20 token.
  function setWrappedNative(address wrappedNative) external onlyOwner {
    s_wrappedNative = wrappedNative;
  }

  /// @notice Gets the arm address
  /// @return The address of the ARM proxy contract.
  function getArmProxy() external view returns (address) {
    return i_armProxy;
  }

  /// @notice Return the configured onramp for specific a destination chain.
  /// @param destChainSelector The destination chain Id to get the onRamp for.
  /// @return The address of the onRamp.
  function getOnRamp(uint64 destChainSelector) external view returns (address) {
    return s_onRamps[destChainSelector];
  }

  function getOffRamps() external view returns (OffRamp[] memory) {
    uint256[] memory encodedOffRamps = s_chainSelectorAndOffRamps.values();
    OffRamp[] memory offRamps = new OffRamp[](encodedOffRamps.length);
    for (uint256 i = 0; i < encodedOffRamps.length; ++i) {
      uint256 encodedOffRamp = encodedOffRamps[i];
      offRamps[i] = OffRamp({
        sourceChainSelector: uint64(encodedOffRamp >> 160),
        offRamp: address(uint160(encodedOffRamp))
      });
    }
    return offRamps;
  }

  function isOffRamp(uint64 sourceChainSelector, address offRamp) public view returns (bool) {
    // We have to encode the sourceChainSelector and offRamp into a uint256 to use as a key in the set.
    return s_chainSelectorAndOffRamps.contains(_mergeChainSelectorAndOffRamp(sourceChainSelector, offRamp));
  }

  /// @notice applyRampUpdates applies a set of ramp changes which provides
  /// the ability to add new chains and upgrade ramps.
  function applyRampUpdates(
    OnRamp[] calldata onRampUpdates,
    OffRamp[] calldata offRampRemoves,
    OffRamp[] calldata offRampAdds
  ) external onlyOwner {
    // Apply egress updates.
    // We permit zero address as way to disable egress.
    for (uint256 i = 0; i < onRampUpdates.length; ++i) {
      OnRamp memory onRampUpdate = onRampUpdates[i];
      s_onRamps[onRampUpdate.destChainSelector] = onRampUpdate.onRamp;
      emit OnRampSet(onRampUpdate.destChainSelector, onRampUpdate.onRamp);
    }

    // Apply ingress updates.
    for (uint256 i = 0; i < offRampRemoves.length; ++i) {
      uint64 sourceChainSelector = offRampRemoves[i].sourceChainSelector;
      address offRampAddress = offRampRemoves[i].offRamp;

      // If the selector-offRamp pair does not exist, revert.
      if (!s_chainSelectorAndOffRamps.remove(_mergeChainSelectorAndOffRamp(sourceChainSelector, offRampAddress)))
        revert OffRampMismatch(sourceChainSelector, offRampAddress);

      emit OffRampRemoved(sourceChainSelector, offRampAddress);
    }

    for (uint256 i = 0; i < offRampAdds.length; ++i) {
      uint64 sourceChainSelector = offRampAdds[i].sourceChainSelector;
      address offRampAddress = offRampAdds[i].offRamp;

      if (s_chainSelectorAndOffRamps.add(_mergeChainSelectorAndOffRamp(sourceChainSelector, offRampAddress))) {
        emit OffRampAdded(sourceChainSelector, offRampAddress);
      }
    }
  }

  /// @notice Provides the ability for the owner to recover any tokens accidentally
  /// sent to this contract.
  /// @dev Must be onlyOwner to avoid malicious token contract calls.
  /// @param tokenAddress ERC20-token to recover
  /// @param to Destination address to send the tokens to.
  function recoverTokens(address tokenAddress, address to, uint256 amount) external onlyOwner {
    if (to == address(0)) revert InvalidRecipientAddress(to);

    if (tokenAddress == address(0)) {
      (bool success, ) = to.call{value: amount}("");
      if (!success) revert FailedToSendValue();
      return;
    }
    IERC20(tokenAddress).safeTransfer(to, amount);
  }
}

