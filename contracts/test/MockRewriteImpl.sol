// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

contract MockRewriteImpl {
    ///@dev Address to forward ESE to.
    address public onRampImplementation;
    function initialize(bytes memory) external{
        onRampImplementation = address(0);
    }
}