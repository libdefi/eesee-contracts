// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import '@openzeppelin/contracts/token/ERC20/ERC20.sol';
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

contract USDCWrapper {
    using SafeERC20 for IERC20;

    IERC20 immutable public USDC;
    IERC20 immutable public aUSDC;

    constructor(IERC20 _USDC, IERC20 _aUSDC){
        USDC = _USDC;
        aUSDC = _aUSDC;
    }
    
    function wrap(uint256 amount, address recipient) external {
        USDC.safeTransferFrom(msg.sender, address(this), amount);
        aUSDC.safeTransfer(recipient, amount);
    }

    function unwrap(uint256 amount, address recipient) external {
        aUSDC.safeTransferFrom(msg.sender, address(this), amount);
        USDC.safeTransfer(recipient, amount);
    }
}