// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../abstract/crosschain/AxelarTokenServiceCaller.sol";
import "../libraries/CompareAddressWithChain.sol";

contract MockAxelarTokenCallerChild is AxelarTokenServiceCaller {  
    constructor(
        IAxelarGateway _gateway, 
        IAxelarGasService _gasService
    ) AxelarGatewayBase(_gateway, _gasService) {}

    function sendAxelarMessageWithToken(
        address useTokensFrom,
        AddressWithChain memory destination,
        string memory tokenSymbol,
        uint256 tokenAmount,
        bytes memory payload
    ) external payable {
        _sendAxelarMessageWithToken(
            useTokensFrom,
            destination,
            tokenSymbol,
            tokenAmount,
            payload,
            msg.value
        );
    }

    function _executeWithToken(
        AddressWithChain memory source,
        bytes calldata payload,
        IERC20 token,
        uint256 amount
    ) internal override {
        
    }
}
