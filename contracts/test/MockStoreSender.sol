// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../types/AddressWithChain.sol";

interface IEeseeHub {
    function callAccount(bytes calldata executeBatchData, AddressWithChain calldata spoke) external;
}

interface IEeseeSpoke {
    function callAccount(bytes calldata executeBatchData) external;
}

contract MockStoreSender {
    address public sender;
    IEeseeSpoke public immutable eeseeSpoke;
    IEeseeHub public immutable eeseeHub;

    constructor(IEeseeSpoke _eeseeSpoke, IEeseeHub _eeseeHub) {
        eeseeSpoke = _eeseeSpoke;
        eeseeHub = _eeseeHub;
    }

    function storeSender() external {
        sender = msg.sender;
    }
}