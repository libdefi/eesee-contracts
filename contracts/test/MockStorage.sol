// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;


contract MockStorage {
    string public newStr;

    function storeString(string calldata newString) external {
        newStr = newString;
    }
}