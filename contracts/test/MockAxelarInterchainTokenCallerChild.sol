// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../abstract/crosschain/AxelarInterchainTokenServiceCaller.sol";
import "../libraries/CompareAddressWithChain.sol";

contract MockAxelarInterchainTokenCallerChild is AxelarInterchainTokenServiceCaller {  
    constructor(
        address _interchainTokenService
    ) AxelarInterchainTokenServiceCaller(_interchainTokenService) {}

    function sendAxelarMessageWithInterchainToken(
        address useTokensFrom,
        AddressWithChain memory destination,
        bytes32 tokenId,
        uint256 tokenAmount,
        bytes memory payload
    ) external payable {
        _sendAxelarMessageWithInterchainToken(
            useTokensFrom,
            destination,
            tokenId,
            tokenAmount,
            payload,
            msg.value
        );
    }

    function _executeWithInterchainToken(
        bytes32 commandId,
        AddressWithChain memory source,
        bytes calldata payload,
        bytes32 tokenId,
        IERC20 token,
        uint256 amount
    ) internal override {
        
    }
}
