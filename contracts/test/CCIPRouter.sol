// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import {IRouter} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IRouter.sol";
import {IEVM2AnyOnRamp} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IEVM2AnyOnRamp.sol";
import {IWrappedNative} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IWrappedNative.sol";
import {IAny2EVMMessageReceiver} from "@chainlink/contracts-ccip/src/v0.8/ccip/interfaces/IAny2EVMMessageReceiver.sol";

import {Client} from "@chainlink/contracts-ccip/src/v0.8/ccip/libraries/Client.sol";
import {Internal} from "@chainlink/contracts-ccip/src/v0.8/ccip/libraries/Internal.sol";
import {CallWithExactGas} from "@chainlink/contracts-ccip/src/v0.8/shared/call/CallWithExactGas.sol";

import {EnumerableSet} from "@chainlink/contracts-ccip/src/v0.8/vendor/openzeppelin-solidity/v4.8.0/contracts/utils/structs/EnumerableSet.sol";
import {SafeERC20} from "@chainlink/contracts-ccip/src/v0.8/vendor/openzeppelin-solidity/v4.8.0/contracts/token/ERC20/utils/SafeERC20.sol";
import {IERC20} from "@chainlink/contracts-ccip/src/v0.8/vendor/openzeppelin-solidity/v4.8.0/contracts/token/ERC20/IERC20.sol";

/// @title Router
/// @notice This is the entry point for the end user wishing to send data across chains.
/// @dev This contract is used as a router for both on-ramps and off-ramps
contract CCIPRouter is IRouter {
  using SafeERC20 for IERC20;
  using EnumerableSet for EnumerableSet.UintSet;

  error FailedToSendValue();
  error InvalidRecipientAddress(address to);
  error OffRampMismatch(uint64 chainSelector, address offRamp);
  error BadARMSignal();
  error UnsupportedDestinationChain(uint64);
  error InsufficientFeeTokenAmount();

  event OnRampSet(uint64 indexed destChainSelector, address onRamp);
  event OffRampAdded(uint64 indexed sourceChainSelector, address offRamp);
  event OffRampRemoved(uint64 indexed sourceChainSelector, address offRamp);
  event MessageExecuted(bytes32 messageId, uint64 sourceChainSelector, address offRamp, bytes32 calldataHash);

  struct OnRamp {
    uint64 destChainSelector;
    address onRamp;
  }

  struct OffRamp {
    uint64 sourceChainSelector;
    address offRamp;
  }

  // We limit return data to a selector plus 4 words. This is to avoid
  // malicious contracts from returning large amounts of data and causing
  // repeated out-of-gas scenarios.
  uint16 public constant MAX_RET_BYTES = 4 + 4 * 32;

  // DYNAMIC CONFIG
  address private s_wrappedNative;
  // destChainSelector => onRamp address
  // Only ever one onRamp enabled at a time for a given destChainSelector.
  mapping(uint256 destChainSelector => address onRamp) private s_onRamps;
  // Stores [sourceChainSelector << 160 + offramp] as a pair to allow for
  // lookups for specific chain/offramp pairs.
  EnumerableSet.UintSet private s_chainSelectorAndOffRamps;

  constructor(
    OnRamp[] memory onRampUpdates,
    OffRamp[] memory offRampAdds,
    address wrappedNative
  ) {
    s_wrappedNative = wrappedNative;
    applyRampUpdates(onRampUpdates, offRampAdds);
  }

  // ================================================================
  // │                       Message sending                        │
  // ================================================================

  function getFee(
    uint64 destinationChainSelector,
    Client.EVM2AnyMessage memory message
  ) external view returns (uint256 fee) {
    message.feeToken = s_wrappedNative;
    address onRamp = s_onRamps[destinationChainSelector];
    if (onRamp == address(0)) revert UnsupportedDestinationChain(destinationChainSelector);
    return IEVM2AnyOnRamp(onRamp).getFee(destinationChainSelector, message);
  }

  function ccipSend(
    uint64 destinationChainSelector,
    Client.EVM2AnyMessage memory message
  ) external payable returns (bytes32) {
    address onRamp = s_onRamps[destinationChainSelector];
    if (onRamp == address(0)) revert UnsupportedDestinationChain(destinationChainSelector);

    // for fee calculation we check the wrapped native price as we wrap
    // as part of the native fee coin payment.
    message.feeToken = s_wrappedNative;
    // We rely on getFee to validate that the feeToken is whitelisted.
    uint256 feeTokenAmount = IEVM2AnyOnRamp(onRamp).getFee(destinationChainSelector, message);
    // Ensure sufficient native.
    if (msg.value < feeTokenAmount) revert InsufficientFeeTokenAmount();
    // Wrap and send native payment.
    // Note we take the whole msg.value regardless if its larger.
    feeTokenAmount = msg.value;
    IWrappedNative(message.feeToken).deposit{value: feeTokenAmount}();
    IERC20(message.feeToken).safeTransfer(onRamp, feeTokenAmount);

    return IEVM2AnyOnRamp(onRamp).forwardFromRouter(destinationChainSelector, message, feeTokenAmount, msg.sender);
  }

  // ================================================================
  // │                      Message execution                       │
  // ================================================================

  /// @inheritdoc IRouter
  /// @dev _callWithExactGas protects against return data bombs by capping the return data size at MAX_RET_BYTES.
  function routeMessage(
    Client.Any2EVMMessage calldata message,
    uint16 gasForCallExactCheck,
    uint256 gasLimit,
    address receiver
  ) external override returns (bool success, bytes memory retData, uint256 gasUsed) {
    // We only permit offRamps to call this function.
    if (!isOffRamp(message.sourceChainSelector, msg.sender)) revert OnlyOffRamp();

    // We encode here instead of the offRamps to constrain specifically what functions
    // can be called from the router.
    bytes memory data = abi.encodeWithSelector(IAny2EVMMessageReceiver.ccipReceive.selector, message);

    (success, retData, gasUsed) = CallWithExactGas._callWithExactGasSafeReturnData(
      data,
      receiver,
      gasLimit,
      gasForCallExactCheck,
      Internal.MAX_RET_BYTES
    );

    emit MessageExecuted(message.messageId, message.sourceChainSelector, msg.sender, keccak256(data));
    return (success, retData, gasUsed);
  }

  // @notice Merges a chain selector and offRamp address into a single uint256 by shifting the
  // chain selector 160 bits to the left.
  function _mergeChainSelectorAndOffRamp(
    uint64 sourceChainSelector,
    address offRampAddress
  ) internal pure returns (uint256) {
    return (uint256(sourceChainSelector) << 160) + uint160(offRampAddress);
  }

  // ================================================================
  // │                           Config                             │
  // ================================================================

  function isOffRamp(uint64 sourceChainSelector, address offRamp) public view returns (bool) {
    // We have to encode the sourceChainSelector and offRamp into a uint256 to use as a key in the set.
    return s_chainSelectorAndOffRamps.contains(_mergeChainSelectorAndOffRamp(sourceChainSelector, offRamp));
  }

  /// @notice applyRampUpdates applies a set of ramp changes which provides
  /// the ability to add new chains and upgrade ramps.
  function applyRampUpdates(
    OnRamp[] memory onRampUpdates,
    OffRamp[] memory offRampAdds
  ) internal {
    // Apply egress updates.
    // We permit zero address as way to disable egress.
    for (uint256 i = 0; i < onRampUpdates.length; ++i) {
      OnRamp memory onRampUpdate = onRampUpdates[i];
      s_onRamps[onRampUpdate.destChainSelector] = onRampUpdate.onRamp;
      emit OnRampSet(onRampUpdate.destChainSelector, onRampUpdate.onRamp);
    }

    for (uint256 i = 0; i < offRampAdds.length; ++i) {
      uint64 sourceChainSelector = offRampAdds[i].sourceChainSelector;
      address offRampAddress = offRampAdds[i].offRamp;

      if (s_chainSelectorAndOffRamps.add(_mergeChainSelectorAndOffRamp(sourceChainSelector, offRampAddress))) {
        emit OffRampAdded(sourceChainSelector, offRampAddress);
      }
    }
  }
}
