// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import { IERC20 } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/interfaces/IERC20.sol';
import { IAxelarGasService } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/interfaces/IAxelarGasService.sol';
import { IAxelarGateway } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/interfaces/IAxelarGateway.sol';
import { ExpressExecutorTracker } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/express/ExpressExecutorTracker.sol';
import { Upgradable } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/upgradable/Upgradable.sol';
import { Create3Address } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/deploy/Create3Address.sol';
import { SafeTokenTransferFrom } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/libs/SafeTransfer.sol';
import { AddressBytes } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/libs/AddressBytes.sol';
import { StringToBytes32, Bytes32ToString } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/libs/Bytes32String.sol';
import { Multicall } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/utils/Multicall.sol';
import { Pausable } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/utils/Pausable.sol';
import { InterchainAddressTracker } from '@axelar-network/axelar-gmp-sdk-solidity/contracts/utils/InterchainAddressTracker.sol';
import { ITokenManagerType} from '@axelar-network/interchain-token-service/contracts/interfaces/ITokenManagerType.sol';
import { IInterchainTokenExecutable } from '@axelar-network/interchain-token-service/contracts/interfaces/IInterchainTokenExecutable.sol';

/**
 * @title The Interchain Token Service
 * @notice This contract is responsible for facilitating interchain token transfers.
 * It (mostly) does not handle tokens, but is responsible for the messaging that needs to occur for interchain transfers to happen.
 * @dev The only storage used in this contract is for Express calls.
 * Furthermore, no ether is intended to or should be sent to this contract except as part of deploy/interchainTransfer payable methods for gas payment.
 */
contract MockInterchainTokenService is ITokenManagerType
{
    error InvalidTokenManagerImplementationType(address implementation);
    error InvalidChainName();
    error NotRemoteService();
    error TokenManagerDoesNotExist(bytes32 tokenId);
    error NotToken(address caller, address token);
    error ExecuteWithInterchainTokenFailed(address contractAddress);
    error ExpressExecuteWithInterchainTokenFailed(address contractAddress);
    error GatewayToken();
    error TokenManagerDeploymentFailed(bytes error);
    error InterchainTokenDeploymentFailed(bytes error);
    error InvalidMessageType(uint256 messageType);
    error InvalidMetadataVersion(uint32 version);
    error ExecuteWithTokenNotSupported();
    error InvalidExpressMessageType(uint256 messageType);
    error TakeTokenFailed(bytes data);
    error GiveTokenFailed(bytes data);
    error TokenHandlerFailed(bytes data);
    error ZeroAddress();
    error EmptyData();
    event InterchainTransfer(
        bytes32 indexed tokenId,
        address indexed sourceAddress,
        string destinationChain,
        bytes destinationAddress,
        uint256 amount,
        bytes32 indexed dataHash
    );
    event InterchainTransferReceived(
        bytes32 indexed commandId,
        bytes32 indexed tokenId,
        string sourceChain,
        bytes sourceAddress,
        address indexed destinationAddress,
        uint256 amount,
        bytes32 dataHash
    );
    event TokenManagerDeploymentStarted(
        bytes32 indexed tokenId,
        string destinationChain,
        TokenManagerType indexed tokenManagerType,
        bytes params
    );
    event InterchainTokenDeploymentStarted(
        bytes32 indexed tokenId,
        string tokenName,
        string tokenSymbol,
        uint8 tokenDecimals,
        bytes minter,
        string destinationChain
    );
    event TokenManagerDeployed(bytes32 indexed tokenId, address tokenManager, TokenManagerType indexed tokenManagerType, bytes params);
    event InterchainTokenDeployed(
        bytes32 indexed tokenId,
        address tokenAddress,
        address indexed minter,
        string name,
        string symbol,
        uint8 decimals
    );
    event InterchainTokenIdClaimed(bytes32 indexed tokenId, address indexed deployer, bytes32 indexed salt);
    using StringToBytes32 for string;
    using Bytes32ToString for bytes32;
    using AddressBytes for bytes;
    using AddressBytes for address;
    using SafeTokenTransferFrom for IERC20;

    IAxelarGateway public immutable gateway;
    IAxelarGasService public immutable gasService;
    bytes32 public immutable chainNameHash;


    bytes32 internal constant PREFIX_INTERCHAIN_TOKEN_ID = keccak256('its-interchain-token-id');
    bytes32 internal constant PREFIX_INTERCHAIN_TOKEN_SALT = keccak256('its-interchain-token-salt');

    bytes32 private constant CONTRACT_ID = keccak256('interchain-token-service');
    bytes32 private constant EXECUTE_SUCCESS = keccak256('its-execute-success');
    bytes32 private constant EXPRESS_EXECUTE_SUCCESS = keccak256('its-express-execute-success');

    /**
     * @dev The message types that are sent between InterchainTokenService on different chains.
     */

    uint256 private constant MESSAGE_TYPE_INTERCHAIN_TRANSFER = 0;
    uint256 private constant MESSAGE_TYPE_DEPLOY_INTERCHAIN_TOKEN = 1;
    uint256 private constant MESSAGE_TYPE_DEPLOY_TOKEN_MANAGER = 2;

    /**
     * @dev Tokens and token managers deployed via the Token Factory contract use a special deployer address.
     * This removes the dependency on the address the token factory was deployed too to be able to derive the same tokenId.
     */
    address internal constant TOKEN_FACTORY_DEPLOYER = address(0);

    /**
     * @dev Latest version of metadata that's supported.
     */

    enum MetadataVersion {
        CONTRACT_CALL,
        EXPRESS_CALL
    }

    uint32 internal constant LATEST_METADATA_VERSION = 1;


    constructor(
        address gateway_,
        address gasService_,
        string memory chainName_
    ) {
        if (
            gasService_ == address(0) ||
            gateway_ == address(0)
        ) revert ZeroAddress();

        gateway = IAxelarGateway(gateway_);
        gasService = IAxelarGasService(gasService_);
        if (bytes(chainName_).length == 0) revert InvalidChainName();
        chainNameHash = keccak256(bytes(chainName_));
    }
    /**
     * @notice Returns the address of a TokenManager from a specific tokenId.
     * @dev The TokenManager needs to exist already.
     * @param tokenId The tokenId.
     * @return tokenManagerAddress_ The deployment address of the TokenManager.
     */
    function validTokenManagerAddress(bytes32 tokenId) public view returns (address tokenManagerAddress_) {
       
    }
    /**
     * @notice Returns the address of the token that an existing tokenManager points to.
     * @param tokenId The tokenId.
     * @return tokenAddress The address of the token.
     */
    function validTokenAddress(bytes32 tokenId) public view returns (address tokenAddress) {
        return address(bytes20(tokenId));
    }

    /**
     * @notice Initiates an interchain call contract with interchain token to a destination chain.
     * @param tokenId The unique identifier of the token to be transferred.
     * @param destinationChain The destination chain to send the tokens to.
     * @param destinationAddress The address on the destination chain to send the tokens to.
     * @param amount The amount of tokens to be transferred.
     * @param data Additional data to be passed along with the transfer.
     */
    function callContractWithInterchainToken(
        bytes32 tokenId,
        string calldata destinationChain,
        bytes calldata destinationAddress,
        uint256 amount,
        bytes memory data,
        uint256 gasValue
    ) external payable {
        _takeToken(tokenId, msg.sender, amount, false);
    }
        /**
     * @notice Executes operations based on the payload and selector.
     * @param commandId The unique message id.
     * @param sourceChain The chain where the transaction originates from.
     * @param sourceAddress The address of the remote ITS where the transaction originates from.
     * @param payload The encoded data payload for the transaction.
     */
    function execute(
        bytes32 commandId,
        string calldata sourceChain,
        string calldata sourceAddress,
        bytes calldata payload
    ) external {

        _processInterchainTransferPayload(commandId, address(0), sourceChain, payload);
    }
        /**
     * @notice Processes the payload data for a send token call.
     * @param commandId The unique message id.
     * @param expressExecutor The address of the express executor. Equals `address(0)` if it wasn't expressed.
     * @param sourceChain The chain where the transaction originates from.
     * @param payload The encoded data payload to be processed.
     */
    function _processInterchainTransferPayload(
        bytes32 commandId,
        address expressExecutor,
        string calldata sourceChain,
        bytes calldata payload
    ) internal {
        bytes32 tokenId;
        bytes memory sourceAddress;
        address destinationAddress;
        uint256 amount;
        bytes memory data;
        {
            bytes memory destinationAddressBytes;
            (, tokenId, sourceAddress, destinationAddressBytes, amount, data) = abi.decode(
                payload,
                (uint256, bytes32, bytes, bytes, uint256, bytes)
            );
            destinationAddress = destinationAddressBytes.toAddress();
        }

        address tokenAddress;
        (amount, tokenAddress) = _giveToken(tokenId, destinationAddress, amount);

        // slither-disable-next-line reentrancy-events
        emit InterchainTransferReceived(
            commandId,
            tokenId,
            sourceChain,
            sourceAddress,
            destinationAddress,
            amount,
            data.length == 0 ? bytes32(0) : keccak256(data)
        );

        if (data.length != 0) {
            bytes32 result = IInterchainTokenExecutable(destinationAddress).executeWithInterchainToken(
                commandId,
                sourceChain,
                sourceAddress,
                data,
                tokenId,
                validTokenAddress(tokenId),
                amount
            );

            if (result != EXECUTE_SUCCESS) revert ExecuteWithInterchainTokenFailed(destinationAddress);
        }
    }

    function contractCallWithTokenValue(
        string calldata /*sourceChain*/,
        string calldata /*sourceAddress*/,
        bytes calldata /*payload*/,
        string calldata /*symbol*/,
        uint256 /*amount*/
    ) public view virtual returns (address, uint256) {
        revert ExecuteWithTokenNotSupported();
    }

    function expressExecuteWithToken(
        bytes32 /*commandId*/,
        string calldata /*sourceChain*/,
        string calldata /*sourceAddress*/,
        bytes calldata /*payload*/,
        string calldata /*tokenSymbol*/,
        uint256 /*amount*/
    ) external payable {
        revert ExecuteWithTokenNotSupported();
    }

    function executeWithToken(
        bytes32 /*commandId*/,
        string calldata /*sourceChain*/,
        string calldata /*sourceAddress*/,
        bytes calldata /*payload*/,
        string calldata /*tokenSymbol*/,
        uint256 /*amount*/
    ) external pure {
        revert ExecuteWithTokenNotSupported();
    
    }
    /**
     * @notice Calls a contract on a specific destination chain with the given payload
     * @param destinationChain The target chain where the contract will be called.
     * @param payload The data payload for the transaction.
     * @param gasValue The amount of gas to be paid for the transaction.
     */
    function _callContract(
        string calldata destinationChain,
        bytes memory payload,
        MetadataVersion metadataVersion,
        uint256 gasValue
    ) internal {
        // string memory destinationAddress = trustedAddress(destinationChain);
        // if (bytes(destinationAddress).length == 0) revert UntrustedChain();
        string memory destinationAddress = '';
        if (gasValue > 0) {
            if (metadataVersion == MetadataVersion.CONTRACT_CALL) {
                gasService.payNativeGasForContractCall{ value: gasValue }(
                    address(this),
                    destinationChain,
                    destinationAddress,
                    payload, // solhint-disable-next-line avoid-tx-origin
                    tx.origin
                );
            } else if (metadataVersion == MetadataVersion.EXPRESS_CALL) {
                gasService.payNativeGasForExpressCall{ value: gasValue }(
                    address(this),
                    destinationChain,
                    destinationAddress,
                    payload, // solhint-disable-next-line avoid-tx-origin
                    tx.origin
                );
            } else {
                revert InvalidMetadataVersion(uint32(metadataVersion));
            }
        }

        gateway.callContract(destinationChain, destinationAddress, payload);
    }


    /**
     * @notice Transmit a callContractWithInterchainToken for the given tokenId.
     * @param tokenId The tokenId of the TokenManager (which must be the msg.sender).
     * @param sourceAddress The address where the token is coming from, which will also be used for gas reimbursement.
     * @param destinationChain The name of the chain to send tokens to.
     * @param destinationAddress The destinationAddress for the interchainTransfer.
     * @param amount The amount of tokens to send.
     * @param metadataVersion The version of the metadata.
     * @param data The data to be passed with the token transfer.
     */
    function _transmitInterchainTransfer(
        bytes32 tokenId,
        address sourceAddress,
        string calldata destinationChain,
        bytes memory destinationAddress,
        uint256 amount,
        MetadataVersion metadataVersion,
        bytes memory data,
        uint256 gasValue
    ) internal {
        // slither-disable-next-line reentrancy-events
        emit InterchainTransfer(
            tokenId,
            sourceAddress,
            destinationChain,
            destinationAddress,
            amount,
            data.length == 0 ? bytes32(0) : keccak256(data)
        );

        bytes memory payload = abi.encode(
            MESSAGE_TYPE_INTERCHAIN_TRANSFER,
            tokenId,
            sourceAddress.toBytes(),
            destinationAddress,
            amount,
            data
        );

        _callContract(destinationChain, payload, metadataVersion, gasValue);
    }

        /**
     * @dev Gives token to recipient via the token service.
     */
    function _giveToken(bytes32 tokenId, address to, uint256 amount) internal returns (uint256, address) {
        address tokenAddress = validTokenAddress(tokenId);
        IERC20(tokenAddress).transfer(to, amount);

        return (amount, tokenAddress);
    }

    /**
     * @dev Takes token from a sender via the token service. `tokenOnly` indicates if the caller should be restricted to the token only.
     */
    function _takeToken(bytes32 tokenId, address from, uint256 amount, bool tokenOnly) internal returns (uint256) {
        address tokenAddress = validTokenAddress(tokenId);
        IERC20(tokenAddress).safeTransferFrom(from, address(this), amount);
        return amount;
    }
}
