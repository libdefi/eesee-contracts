// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "./Asset.sol";

struct AddressWithChain{
    bytes chainSelector;
    address _address;
}

struct AssetHashWithSource {
    AddressWithChain source;
    bytes32 assetHash;
}