// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../types/AddressWithChain.sol";
import "./CompareBytes.sol";
    
error InvalidDestination();

library CompareAddressWithChain {
    using CompareBytes for bytes;

    function eq(AddressWithChain memory a, AddressWithChain memory b) internal pure returns (bool){
        return (a._address == b._address) && a.chainSelector.eq(b.chainSelector);
    }
}