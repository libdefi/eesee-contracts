// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;
    
library CompareBytes {
    function eq(bytes memory a, bytes memory b) internal pure returns (bool){
        return (a.length == b.length) && (keccak256(a) == keccak256(b));
    }
}