// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/Address.sol";

library SafeApproveMax {
    using Address for address;

    function safeApproveMax(
        IERC20 token, 
        uint256 amount, 
        address spender
    ) internal {
        if(token.allowance(address(this), spender) < amount) {
            // Logic from _callOptionalReturn function from SafeERC20.
            bytes memory returndata = address(token).functionCall(
                abi.encodeWithSelector(token.approve.selector, spender, type(uint256).max), 
                "SafeERC20: low-level call failed"
            );
            require(returndata.length == 0 || abi.decode(returndata, (bool)), "SafeERC20: ERC20 operation did not succeed");
        }
    }
}