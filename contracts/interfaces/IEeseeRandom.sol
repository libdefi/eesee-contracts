// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "./IEeseeAccessManager.sol";
import "../types/Random.sol";

interface IEeseeRandom {
    event FulfillRandom(uint256 indexed random);
    event RequestRandom();

    function random(uint256) external view returns(uint256 word, uint256 timestamp);
    function lastRandomRequestTimestamp() external view returns(uint64);
    function randomRequestInterval() external view returns(uint32);
    function MAX_RANDOM_REQUEST_INTERVAL() external view returns(uint48);

    function getRandomFromTimestamp(uint256 _timestamp) external view returns (uint256 word, uint256 timestamp);
    function canRequestRandom() external view returns (bool randomNeeded);
}