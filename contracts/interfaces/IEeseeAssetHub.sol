// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../types/AddressWithChain.sol";

interface IEeseeAssetHub {
    function assetHashesWithSources(uint256) external view returns(AddressWithChain memory source, bytes32 assetHash);

    event Wrap(
        uint256 indexed tokenId,
        uint256 amount,
        AssetHashWithSource asset
    );
    event Unwrap(
        uint256 indexed tokenId,
        uint256 amount,
        AssetHashWithSource asset,
        address indexed sender,
        address indexed recipient
    );

    function unwrap(
        uint256[] calldata tokenIds, 
        uint256[] calldata amounts,
        address recipient, 
        bytes calldata additionalData
    ) external payable;

    function getTokenId(AssetHashWithSource memory assetHashWithSource) external view returns (uint256);
}
