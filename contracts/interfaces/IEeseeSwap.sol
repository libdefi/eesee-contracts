// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "./IEeseeMarketplaceRouter.sol";

interface IEeseeSwap {
    /** 
     * @dev Struct used for NFT swap.
     * {swapData} - Swap data to use.
     * {marketplaceRoutersData} - Addresses with data to call marketplace routers with. 
    */
    struct SwapParams{
        bytes swapData;
        MarketplaceRouterData[] marketplaceRoutersData;
    }

    struct MarketplaceRouterData{
        IEeseeMarketplaceRouter router;
        bytes data;
    }

    event ReceiveAsset(
        Asset assetBought,
        IERC20 indexed tokenSpent,
        uint256 spent,
        address indexed recipient
    ); 
    
    function swapTokensForAssets(SwapParams calldata swapParams, address recipient) external payable returns(
        IERC20 srcToken,
        uint256 srcSpent,
        Asset[] memory assets
    );
}