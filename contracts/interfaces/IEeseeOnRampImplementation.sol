// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../types/AddressWithChain.sol";

interface IEeseeOnRampImplementation {
    event Forward(AddressWithChain destination, uint256 amount);

    function ESE() external view returns(IERC20);
    function offRamp() external view returns(bytes memory chainSelector, address _address);

    function initialize(bytes memory data) external;
    function forward(bytes memory data) external payable;
}
