// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../types/Call.sol";
import "../types/AddressWithChain.sol";

interface IEeseeAssetSpoke {  
    function eeseeAssetHub() external view returns (bytes memory chainSelector, address _address);

    function assetsStorage(bytes32) external view returns (
        address token,
        uint256 tokenID,
        uint256 amount,
        AssetType assetType,
        bytes memory data
    );

    event Wrap(
        Asset asset,
        bytes32 indexed assetHash,
        address indexed sender
    );
    event Unwrap(
        Asset asset,
        bytes32 indexed assetHash,
        address indexed recipient
    );

    function wrap(
        Asset[] calldata assets, 
        Call[] calldata crosschainCalls,
        address fallbackRecipient,
        bytes calldata additionalData
    ) external payable returns(uint256 gasPaid);
    function unstuck(bytes32 stuckAssetHash, address recipient) external returns(Asset memory asset);

    function getTokenIdL2(Asset calldata asset) external view returns (uint256);
    function getTokenIdL2(bytes32 assetHash) external view returns (uint256);
    function getAssetHash(Asset calldata asset) external pure returns (bytes32);
}