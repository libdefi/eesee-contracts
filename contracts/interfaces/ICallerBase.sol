// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "../types/AddressWithChain.sol";

interface ICallerBase {
    event CrosschainSend(bytes32 indexed projectIdentifier, AddressWithChain destination, bytes additionalData);
    event CrosschainReceive(bytes32 indexed projectIdentifier, AddressWithChain source, bytes additionalData);
}
