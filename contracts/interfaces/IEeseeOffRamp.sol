// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

interface IEeseeOffRamp {
    function ESE() external view returns(IERC20);
    function recipient() external view returns(address);

    event Forward(uint256 amount);
}
