// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

interface IEeseePeriphery {
    function executeWithSwap(
        bytes calldata swapData,
        address target,
        bytes calldata targetCalldata
    ) external payable returns(
        IERC20 srcToken,
        uint256 srcSpent,
        uint256 srcDust,
        IERC20 dstToken,
        uint256 dstAmount,
        uint256 dstDust,
        bytes memory returnData
    );
}