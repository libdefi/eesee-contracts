// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "../../interfaces/IEeseeRandom.sol";
import "../../libraries/RandomArray.sol";
import "../../abstract/roles/EeseeAdmin.sol";

abstract contract EeseeRandomBase is IEeseeRandom, EeseeAdmin {
    using RandomArray for Random[];

    ///@dev Random words and timestamps received.
    Random[] public random;

    ///@dev REQUEST_RANDOM_ROLE role as defined in {accessManager}.
    bytes32 private constant REQUEST_RANDOM_ROLE = keccak256("REQUEST_RANDOM_ROLE");

    ///@dev Last random request timestamp.
    uint64 public lastRandomRequestTimestamp;

    ///@dev Random request frequency.
    uint32 public randomRequestInterval = 12 hours;
    ///@dev Max possible {randomRequestInterval}.
    uint48 public constant MAX_RANDOM_REQUEST_INTERVAL = 24 hours;

    event ChangeRandomRequestInterval(
        uint32 indexed previousRandomRequestInterval,
        uint32 indexed newRandomRequestInterval
    );

    error RandomRequestNotNeeded();
    error InvalidRandomRequestInterval();
    error CallOnTheSameBlock();

    constructor(IEeseeAccessManager _accessManager) EeseeRoleHandler(_accessManager) {}

    // ============ External Write Functions ============
    
    /**
     * @dev Request random immediately, skipping canRequestRandom() check. Emits {RequestRandom} event.
     * Note: This function can only be called by REQUEST_RANDOM_ROLE.
     */
    function requestRandomImmediate() external onlyRole(REQUEST_RANDOM_ROLE) {
        _requestRandom();
    }

    /**
     * @dev Changes randomRequestInterval. Emits {ChangeRandomRequestInterval} event.
     * @param _randomRequestInterval - New randomRequestInterval.
     * Note: This function can only be called by ADMIN_ROLE.
     */
    function changeRandomRequestInterval(uint32 _randomRequestInterval) external onlyRole(ADMIN_ROLE) {
        if(_randomRequestInterval > MAX_RANDOM_REQUEST_INTERVAL) revert InvalidRandomRequestInterval();

        emit ChangeRandomRequestInterval(randomRequestInterval, _randomRequestInterval);
        randomRequestInterval = _randomRequestInterval;
    }

    // ============ External View Functions ============

    function getRandomFromTimestamp(uint256 _timestamp) external view returns (uint256 word, uint256 timestamp){
        Random memory _random = random.findUpperBound(_timestamp);
        return (_random.word, _random.timestamp);
    }

    // ============ Public View Functions ============

    /**
     * @dev Check if random needs to be requested.
     */
    function canRequestRandom() public virtual view returns (bool randomNeeded){
        unchecked { if(block.timestamp - lastRandomRequestTimestamp >= randomRequestInterval) randomNeeded = true; }
    }

    // ============ Internal Write Functions ============

    function _requestRandomWithCanRequestRandomCheck() internal {
        if(!canRequestRandom()) revert RandomRequestNotNeeded();
        _requestRandom();
    }

    function _fulfillRandom(uint256 _random) internal {
        uint256 randomLength = random.length;
        unchecked { if(randomLength > 0 && random[randomLength - 1].timestamp == block.timestamp) revert CallOnTheSameBlock(); }

        random.push(Random({
            word: _random,
            timestamp: block.timestamp
        }));

        emit FulfillRandom(_random);
    }

    // ============ Internal View Functions ============

    /**
     * @dev Random request logic specific to a chosen project.
     */
    function _requestExternalRandom() internal virtual;

    // ============ Private Write Functions ============

    function _requestRandom() private {
        _requestExternalRandom();
        lastRandomRequestTimestamp = uint64(block.timestamp);
        emit RequestRandom();
    }
}