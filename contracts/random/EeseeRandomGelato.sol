// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "./abstract/GelatoVRFConsumerBase.sol";
import "./abstract/EeseeRandomBase.sol";

contract EeseeRandomGelato is EeseeRandomBase, GelatoVRFConsumerBase {
    ///@dev Gelato VRF operator.
    address immutable public vrfGelatoOperator;

    error InvalidOperator();

    constructor(
        address _vrfGelatoOperator,
        IEeseeAccessManager _accessManager
    ) EeseeRandomBase(_accessManager) {
        if(_vrfGelatoOperator == address(0)) revert InvalidOperator();
        vrfGelatoOperator = _vrfGelatoOperator;
    }

    // ============ External Write Functions ============

    /**
     * @dev Requests random from Gelato. Emits {RequestRandom} event.
     */
    function requestRandom() external {
        _requestRandomWithCanRequestRandomCheck();
    }

    // ============ External View Functions ============

    /**
     * @dev Called by Gelato to determine if requestRandom() needs to be called.
     */
    function checker() external view returns (bool canExec, bytes memory execPayload){
        canExec = canRequestRandom();
        execPayload = abi.encodeWithSelector(this.requestRandom.selector);
    }

    // ============ Internal Write Functions ============

    /**
     * @dev This function is called by Gelato. Writes randomness to storage. Emits {FulfillRandom} event.
     * @param randomness - Random value sent by Gelato.
     */
    function _fulfillRandomness(uint256 randomness, uint256 /*requestId*/, bytes memory /*extraData*/) internal override {
        _fulfillRandom(randomness);
    }

    function _requestExternalRandom() internal override {
        _requestRandomness("");
    }

    // ============ Internal View Functions ============

    function _operator() internal view override returns (address) {
        return vrfGelatoOperator;
    }
}