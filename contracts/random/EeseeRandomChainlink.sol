// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@chainlink/contracts/src/v0.8/interfaces/VRFCoordinatorV2Interface.sol";
import "@chainlink/contracts/src/v0.8/AutomationCompatible.sol";
import "@chainlink/contracts/src/v0.8/VRFConsumerBaseV2.sol";
import "./abstract/EeseeRandomBase.sol";

contract EeseeRandomChainlink is EeseeRandomBase, AutomationCompatibleInterface, VRFConsumerBaseV2 {
    ///@dev Because of the Stack too deep error, we combine some constructor arguments into a single stuct
    struct ChainlinkContructorArgs{
        address vrfCoordinator;
        bytes32 keyHash;
        uint16 minimumRequestConfirmations;
        uint32 callbackGasLimit;
    }

    ///@dev Chainlink VRF V2 subscription ID.
    uint64 immutable public subscriptionID;

    ///@dev Chainlink VRF V2 gas limit to call fulfillRandomWords() with.
    uint32 public callbackGasLimit;
    ///@dev Chainlink VRF V2 request confirmations.
    uint16 immutable private minimumRequestConfirmations;

    ///@dev Chainlink VRF V2 coordinator.
    VRFCoordinatorV2Interface immutable public vrfCoordinator;
    ///@dev Chainlink VRF V2 key hash to call requestRandomWords() with.
    bytes32 immutable private keyHash;

    event ChangeCallbackGasLimit(
        uint32 indexed previousCallbackGasLimit,
        uint32 indexed newCallbackGasLimit
    );

    constructor(
        ChainlinkContructorArgs memory chainlinkArgs,
        IEeseeAccessManager _accessManager
    ) VRFConsumerBaseV2(chainlinkArgs.vrfCoordinator) EeseeRandomBase(_accessManager) {
        // ChainLink stuff. Create subscription for VRF V2.
        vrfCoordinator = VRFCoordinatorV2Interface(chainlinkArgs.vrfCoordinator);
        subscriptionID = vrfCoordinator.createSubscription();
        vrfCoordinator.addConsumer(subscriptionID, address(this));
        keyHash = chainlinkArgs.keyHash;
        minimumRequestConfirmations = chainlinkArgs.minimumRequestConfirmations;
        callbackGasLimit = chainlinkArgs.callbackGasLimit;
    }

    // ============ External Write Functions ============

    /**
     * @dev Requests random from Chainlink. Emits {RequestRandom} event.
     */
    function performUpkeep(bytes calldata /*performData*/) external override {
        _requestRandomWithCanRequestRandomCheck();
    }

    /**
     * @dev Changes callbackGasLimit. Emits {ChangeCallbackGasLimit} event.
     * @param _callbackGasLimit - New callbackGasLimit.
     * Note: This function can only be called by ADMIN_ROLE.
     */
    function changeCallbackGasLimit(uint32 _callbackGasLimit) external onlyRole(ADMIN_ROLE){
        emit ChangeCallbackGasLimit(callbackGasLimit, _callbackGasLimit);
        callbackGasLimit = _callbackGasLimit;
    }

    // ============ External View Functions ============

    /**
     * @dev Called by Chainlink Keeper to determine if performUpkeep() needs to be called.
     */
    function checkUpkeep(bytes memory /* checkData */) external view override returns (bool upkeepNeeded, bytes memory /* performData */){
        upkeepNeeded = canRequestRandom();
    }

    // ============ Internal Write Functions ============

    /**
     * @dev This function is called by Chainlink. Writes random word to storage. Emits {FulfillRandom} event.
     * @param randomWords - Random values sent by Chainlink.
     */
    function fulfillRandomWords(uint256 /*requestID*/, uint256[] memory randomWords) internal override {
        _fulfillRandom(randomWords[0]);
    }

    function _requestExternalRandom() internal override {
        vrfCoordinator.requestRandomWords(keyHash, subscriptionID, minimumRequestConfirmations, callbackGasLimit, 1);
    }
}