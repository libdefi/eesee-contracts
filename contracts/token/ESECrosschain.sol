// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Permit.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";

/**
 * @dev Implementation of the {IERC20} interface with burn/mint mechanisms and EIP-2612 permit.
 */
contract ESECrosschain is ERC20Permit, ERC20Burnable {
    ///@dev Address who can initialize this contract.
    address private immutable initializer;
    ///@dev Address who can mint tokens.
    address private minter;

    event Initialize(address indexed minter);

    modifier onlyBeforeInitialization() {
        require(minter == address(0), "ESE: Already initialized");
        require(_msgSender() == initializer, "ESE: Caller not initializer");
        _;
    }

    modifier onlyMinter() {
        require(_msgSender() == minter, "ESE: Caller not minter");
        _;
    }

    constructor() ERC20("eesee","$ESE") ERC20Permit("eesee") {
        initializer = _msgSender();
    }

    // =========== External Write Functions ===========

    /**
     * @dev Mints tokens for {account}.
     * @param account - Address to mint tokens to.
     * @param amount - Amount to mint.
     * Note: Can only be called by {minter}.
     */
    function mint(address account, uint256 amount) external onlyMinter {
        _mint(account, amount);
    }

    /**
     * @dev Initializes this contract with the minter address. Emits {Initialize} event.
     * @param _minter - Address that will have the rights to mint tokens.
     * Note: Can only be called in an unitialized state by {initializer}.
     */
    function initialize(address _minter) external onlyBeforeInitialization {
        if(_minter == address(0)) revert("ESE: Invalid minter");
        minter = _minter;
        emit Initialize(_minter);
    }
}
