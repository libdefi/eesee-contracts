// SPDX-License-Identifier: BUSL-1.1
pragma solidity 0.8.21;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/IERC20Permit.sol";
import "@openzeppelin/contracts/metatx/ERC2771Context.sol";
import "../abstract/roles/EeseeAdmin.sol";

///@dev ESE token wrapper that can only be used with eesee's approved functions.
contract BESE is ERC20, ReentrancyGuard, ERC2771Context, EeseeAdmin {
    // ESE is ERC20 compliant, so no safeERC20 needed.

    struct UserApprovalData {
        uint256 lastApprovalId;
        uint256 availableForUnwrap;
    }

    struct Function {
        address _contract;
        bytes4 functionSelector;
    }

    ///@dev ESE token contract address.
    IERC20 public immutable ESE;
    ///@dev Functions approved for use with this token.
    mapping(bytes32 => bool) public approvedFunctions;

    ///@dev Increments every time an approval is revoked. In such case, all current token holders are able to unwrap their tokens back to ERC20. 
    uint256 public approvalId;
    ///@dev User's last {approvalId} and available funds for unwrap.
    mapping(address => UserApprovalData) public userApprovalData;

    event GrantApproval(Function _function);
    event RevokeApproval(Function _function, uint256 indexed newApprovalId);

    error TransferNotAvailable();
    error InvalidContract(address _contract);
    error FunctionNotApproved(Function _function);
    error InsufficientAvailableBalance(uint256 available);
    error ApproveNotAvailable();
    error InvalidESE();
    error FunctionAlreadyApproved(Function _function);
    error InvalidFunctionsLength();
    error InvalidFunctionSelector(bytes data);

    constructor(
        IERC20 _ESE, 
        IEeseeAccessManager _accessManager,
        address trustedForwarder
    ) ERC2771Context(trustedForwarder) ERC20("Bonus ESE", "BESE") EeseeRoleHandler(_accessManager) {
        if(address(_ESE) == address(0)) revert InvalidESE();
        ESE = _ESE;
    }

    // ============ External Write Functions =============

    /**
     * @dev Burns ESE from sender and wraps them to BESE.
     * @param amount - Amount of tokens to wrap.
     * @param to - Address to mint BESE to.
     * @param permit - Abi-encoded ESE permit data containing approveAmount, deadline, v, r and s. Set to empty bytes to skip permit.
     */
    function wrap(uint256 amount, address to, bytes calldata permit) external {
        _mint(to, amount);

        address msgSender = _msgSender();
        if(permit.length != 0){
            (uint256 approveAmount, uint256 deadline, uint8 v, bytes32 r, bytes32 s) = abi.decode(permit, (uint256, uint256, uint8, bytes32, bytes32));
            IERC20Permit(address(ESE)).permit(msgSender, address(this), approveAmount, deadline, v, r, s);
        }
        ESE.transferFrom(msgSender, address(this), amount);
    }

    /**
     * @dev Unwraps BESE tokens back to ESE. Is only callable with the liquidity wrapped prior to the last {approvalId} increment.
     * @param amount - Amount of tokens to unwrap.
     * @param to - Address to transfer ESE to.
     */
    function unwrap(uint256 amount, address to) external {
        address msgSender = _msgSender();
    
        _burn(msgSender, amount);
        uint256 availableForUnwrap = userApprovalData[msgSender].availableForUnwrap;
        if(amount > availableForUnwrap) revert InsufficientAvailableBalance(availableForUnwrap);
        unchecked { userApprovalData[msgSender].availableForUnwrap = availableForUnwrap - amount; }

        ESE.transfer(to, amount);
    }

    /**
     * @dev Call external function from the approvedFunctions list. Uses BESE from msgSender's balance.
     * @param _contract - Contract to call.
     * @param data - Calldata to call {_contract} with.
     *
     * @return balanceIncreased - Has contract balance increased after this call. 
        If true, BESE is minted to msgSender, else BESE is burned from msgSender.
     * @return amount - Amount of BESE minted or burned.
     * @return returnData - Data returned from external call.
     */
    function callExternal(address _contract, bytes calldata data) external nonReentrant payable returns (bool balanceIncreased, uint256 amount, bytes memory returnData) {
        Function memory _function;
        if(data.length == 0){
            _function = Function(_contract, 0x00000000);
        }else if(data.length >= 4){
            _function = Function(_contract, bytes4(data[:4]));
        }else revert InvalidFunctionSelector(data);

        if(!approvedFunctions[keccak256(abi.encode(_function))]) revert FunctionNotApproved(_function);

        uint256 balanceBefore = ESE.balanceOf(address(this));
        ESE.approve(_contract, balanceBefore);

        bool success;
        (success, returnData) = _contract.call{value: msg.value}(data);
        if (!success) {
            if (returnData.length == 0) revert();
            assembly {
                revert(add(32, returnData), mload(returnData))
            }
        }

        uint256 balanceAfter = ESE.balanceOf(address(this));
        ESE.approve(_contract, 0);

        balanceIncreased = balanceBefore < balanceAfter;
        if(balanceIncreased) {
            unchecked { amount = balanceAfter - balanceBefore; }
            _mint(_msgSender(), amount);
        } else { 
            unchecked { amount = balanceBefore - balanceAfter; }
            _burn(_msgSender(), amount);
        }
    }

    // ============ Admin Functions =============

    /**
     * @dev Grants approvals to call selected functions in selected contracts using {callExternal} function. Emits {GrantApproval} event for each function approved.
     * @param functions - Contract addresses with function selectors to approve.
     * Note: This function is only callable by ADMIN_ROLE.
     */
    function grantFunctionApprovals(Function[] calldata functions) external onlyAdmin {
        if(functions.length == 0) revert InvalidFunctionsLength();

        for(uint256 i; i < functions.length;){
            Function memory _function = functions[i];
            if(_function._contract == address(ESE) || _function._contract == address(this)) revert InvalidContract(_function._contract);

            bytes32 functionHash = keccak256(abi.encode(_function));
            if(approvedFunctions[functionHash]) revert FunctionAlreadyApproved(_function);

            approvedFunctions[functionHash] = true;
            emit GrantApproval(_function);
            unchecked { ++i; }
        }
    }

    /**
     * @dev Revokes granted approvals and increments approvalId. Emits {RevokeApproval} event for each approval revoked.
     * @param functions - Contract addresses with function selectors to revoke approval from.
     * Note: This function is only callable by ADMIN_ROLE.
     */
    function revokeFunctionApprovals(Function[] calldata functions) external onlyAdmin {
        if(functions.length == 0) revert InvalidFunctionsLength();

        uint256 _approvalId;
        unchecked { _approvalId = approvalId + 1; }
        approvalId = _approvalId;

        for(uint256 i; i < functions.length;){
            Function memory _function = functions[i];
            bytes32 functionHash = keccak256(abi.encode(_function));
            if(!approvedFunctions[functionHash]) revert FunctionNotApproved(_function);
            
            approvedFunctions[functionHash] = false;
            emit RevokeApproval(_function, _approvalId);
            unchecked { ++i; }
        }
    }

    // ============ External View Functions =============

    /**
     * @dev Checks if a given function is approved in this contract.
     * @param _function - Function to check.
     */
    function isFunctionApproved(Function calldata _function) external view returns(bool){
        bytes32 functionHash = keccak256(abi.encode(_function));
        return approvedFunctions[functionHash];
    }

    // ============ Internal Write Functions =============

    function _beforeTokenTransfer(address from, address to, uint256 /*amount*/) internal override {
        if(from != address(0) && to != address(0)) revert TransferNotAvailable();

        uint256 _approvalId = approvalId;
        UserApprovalData storage _userApprovalData = userApprovalData[to];
        if(_userApprovalData.lastApprovalId == _approvalId) return;

        _userApprovalData.availableForUnwrap = balanceOf(to == address(0) ? from : to);
        _userApprovalData.lastApprovalId = _approvalId;
    }

    function _approve(address /*owner*/, address /*spender*/, uint256 /*amount*/) internal override pure {
        revert ApproveNotAvailable();
    }

    // =============== Internal View Functions ===============
    
    function _msgSender() internal view override(Context, ERC2771Context) returns (address) {
        return ERC2771Context._msgSender();
    }

    function _msgData() internal view override(Context, ERC2771Context) returns (bytes calldata) {
        return ERC2771Context._msgData();
    }

    function _contextSuffixLength() internal view override(Context, ERC2771Context) returns (uint256) {
        return ERC2771Context._contextSuffixLength();
    }
}
