const {
    time, mine
  } = require('@nomicfoundation/hardhat-network-helpers')
  const { expect } = require('chai')
  const { ethers, network } = require('hardhat')
  const { anyValue } = require("@nomicfoundation/hardhat-chai-matchers/withArgs");
  const createPermit = require('./utils/createPermit')
  const assert = require('assert')
  const {convertArrayToObject, compareObjects} = require('./utils/compareObjects');
  ((network.name != 'multipleNetworks') ? describe : describe.skip)('BESE', function () {
    let ESE
    let BESE
    let eesee
    let accessManager
    let signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, acc10
    let snapshotId, snapshotId2, snapshotId3
    let mockRouter
    let proxy
    let TGE
    const zeroAddress = "0x0000000000000000000000000000000000000000"
    const oneAddress = "0x0000000000000000000000000000000000000001"
    this.beforeAll(async () => {
      [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, acc10] = await ethers.getSigners()
      const _AssetTransfer = await hre.ethers.getContractFactory("AssetTransfer");
      const assetTransfer = await _AssetTransfer.deploy()
      await assetTransfer.deployed()

      const _NFT = await hre.ethers.getContractFactory("EeseeNFT");
      const _MockRouter = await hre.ethers.getContractFactory("MockRouter");
      const _eesee = await hre.ethers.getContractFactory("Eesee", {libraries: { AssetTransfer: assetTransfer.address }});
      const _MockERC20 = await hre.ethers.getContractFactory("MockERC20");
      const _BESE = await hre.ethers.getContractFactory("BESE");
      const _eeseeAccessManager = await ethers.getContractFactory("EeseeAccessManager");
      const _royaltyEngine = await hre.ethers.getContractFactory("MockRoyaltyEngine");
      const _eeseeStaking = await hre.ethers.getContractFactory("EeseeStaking");
      const _eeseeProxy = await hre.ethers.getContractFactory("EeseeProxy");

      mockRouter = await _MockRouter.deploy();
      await mockRouter.deployed()

      proxy = await _eeseeProxy.deploy();
      await proxy.deployed()

      const royaltyEninge = await _royaltyEngine.deploy();
      await royaltyEninge.deployed()

      accessManager = await _eeseeAccessManager.deploy()
      await accessManager.deployed()
  
      ESE = await _MockERC20.deploy('20000000000000000000000000000')
      await ESE.deployed()

      BESE = await _BESE.deploy(ESE.address, accessManager.address, zeroAddress)
      await BESE.deployed()

      staking = await _eeseeStaking.deploy(
        ESE.address, 
        [{volumeBreakpoint: 500, rewardRateFlexible: 500000, rewardRateLocked: 500000}], 
        accessManager.address,
        zeroAddress
      )
      await staking.deployed()

      eesee = await _eesee.deploy(
        ESE.address, 
        staking.address, 
        proxy.address,
        oneAddress,
        oneAddress,
        oneAddress, 
        royaltyEninge.address, 
        accessManager.address,
        zeroAddress,
        zeroAddress,
        zeroAddress
    )
    await eesee.deployed()

    NFT = await _NFT.deploy()
    await NFT.initialize({
        name: "APES",
        symbol:"bayc",
        baseURI: "/",
        revealedURI: "",
        contractURI:'/',
        royaltyReceiver: zeroAddress,
        royaltyFeeNumerator: 0
    },10, signer.address)

    await NFT.deployed()
    await NFT.approve(eesee.address, 1)
  })

  it('Wraps', async () => {
    let balanceBeforeESE = await ESE.balanceOf(signer.address)
    const balanceBeforeBESE = await BESE.balanceOf(signer.address)
    await ESE.approve(BESE.address, 100)
    await expect(BESE.wrap(100, signer.address, "0x")).to.emit(BESE, "Transfer").withArgs(zeroAddress, signer.address, 100)
    let balanceAfterESE = await ESE.balanceOf(signer.address)
    const balanceAfterBESE = await BESE.balanceOf(signer.address)
    assert.equal(balanceBeforeESE.sub(balanceAfterESE).toString(), 100, "ESE balance changed")
    assert.equal(balanceAfterBESE.sub(balanceBeforeBESE).toString(), 100, "BESE balance changed")

    balanceBeforeESE = await ESE.balanceOf(signer.address)
    const balanceBeforeBESE_ = await BESE.balanceOf(acc2.address)
    const currentTimestamp = (await ethers.provider.getBlock()).timestamp
    const deadline = currentTimestamp + 10000
    const correctPermit = await createPermit(signer, BESE, '100', deadline, ESE)
    const params_ = ethers.utils.defaultAbiCoder.encode(
        ["uint256", "uint256", "uint8", "bytes32", "bytes32"],
        ['100', deadline, correctPermit.v, correctPermit.r, correctPermit.s]
      );
    await expect(BESE.wrap(100, acc2.address, params_)).to.emit(BESE, "Transfer").withArgs(zeroAddress, acc2.address, 100)
    balanceAfterESE = await ESE.balanceOf(signer.address)
    const balanceAfterBESE_ = await BESE.balanceOf(acc2.address)
    assert.equal(balanceBeforeESE.sub(balanceAfterESE).toString(), 100, "ESE balance changed")
    assert.equal(balanceAfterBESE_.sub(balanceBeforeBESE_).toString(), 100, "BESE balance changed")
  })

  it('Reverts ERC20 features', async () => {
    await expect(BESE.transfer(acc2.address, 100)).to.be.revertedWithCustomError(BESE, "TransferNotAvailable")
    await expect(BESE.approve(acc2.address, 100)).to.be.revertedWithCustomError(BESE, "ApproveNotAvailable")
  })

  it('Admin stuff', async () => {
    await expect(BESE.connect(acc9).grantFunctionApprovals([{_contract: BESE.address, functionSelector:"0x00000000"}])).to.be.revertedWithCustomError(BESE, "CallerNotAuthorized")
    await expect(BESE.grantFunctionApprovals([])).to.be.revertedWithCustomError(BESE, "InvalidFunctionsLength")
    await expect(BESE.grantFunctionApprovals([{_contract: BESE.address, functionSelector:"0x00000000"}])).to.be.revertedWithCustomError(BESE, "InvalidContract")
    await expect(BESE.grantFunctionApprovals([{_contract: ESE.address, functionSelector:"0x00000000"}])).to.be.revertedWithCustomError(BESE, "InvalidContract")
    await expect(BESE.grantFunctionApprovals([
      {_contract: eesee.address, functionSelector: "0x127b3be5"},
      {_contract: acc7.address, functionSelector: "0x00000000"},
      {_contract: acc8.address, functionSelector: "0x00000000"},
      {_contract: acc9.address, functionSelector: "0x00000000"},
      {_contract: mockRouter.address, functionSelector: "0x3e19c9e0"},
      {_contract: eesee.address, functionSelector: "0x64629f2c"}
    ]))
.to.emit(BESE, "GrantApproval").withArgs(((i) => compareObjects(convertArrayToObject(i), {_contract: eesee.address, functionSelector: "0x127b3be5"})))
.and.to.emit(BESE, "GrantApproval").withArgs(((i) => compareObjects(convertArrayToObject(i), {_contract: acc7.address, functionSelector: "0x00000000"})))
.and.to.emit(BESE, "GrantApproval").withArgs(((i) => compareObjects(convertArrayToObject(i), {_contract: acc8.address, functionSelector: "0x00000000"})))
.and.to.emit(BESE, "GrantApproval").withArgs(((i) => compareObjects(convertArrayToObject(i), {_contract: acc9.address, functionSelector: "0x00000000"})))
.and.to.emit(BESE, "GrantApproval").withArgs(((i) => compareObjects(convertArrayToObject(i), {_contract: mockRouter.address, functionSelector: "0x3e19c9e0"})))
.and.to.emit(BESE, "GrantApproval").withArgs(((i) => compareObjects(convertArrayToObject(i), {_contract: eesee.address, functionSelector: "0x64629f2c"})))
    await expect(BESE.grantFunctionApprovals([{_contract: acc7.address, functionSelector: "0x00000000"}]))
    .to.be.revertedWithCustomError(BESE, "FunctionAlreadyApproved")
    .withArgs(((i) => compareObjects(convertArrayToObject(i), {_contract: acc7.address, functionSelector: "0x00000000"})))

    
    assert.equal(await BESE.isFunctionApproved({_contract: eesee.address, functionSelector: "0x127b3be5"}), true, "Contract not approved")

    await expect(BESE.connect(acc9).revokeFunctionApprovals([{_contract: BESE.address, functionSelector:"0x00000000"}])).to.be.revertedWithCustomError(BESE, "CallerNotAuthorized")
    await expect(BESE.revokeFunctionApprovals([{_contract: BESE.address, functionSelector:"0x00000000"}]))
      .to.be.revertedWithCustomError(BESE, "FunctionNotApproved")
      .withArgs(((i) => compareObjects(convertArrayToObject(i), {_contract: BESE.address, functionSelector:"0x00000000"})))
    await expect(BESE.revokeFunctionApprovals([])).to.be.revertedWithCustomError(BESE, "InvalidFunctionsLength")
    await expect(BESE.revokeFunctionApprovals([
      {_contract: acc7.address, functionSelector: "0x00000000"},
      {_contract: acc8.address, functionSelector: "0x00000000"}
    ]))
      .to.emit(BESE, "RevokeApproval").withArgs(((i) => compareObjects(convertArrayToObject(i), {_contract: acc7.address, functionSelector: "0x00000000"})), 1)
      .and.to.emit(BESE, "RevokeApproval").withArgs(((i) => compareObjects(convertArrayToObject(i), {_contract: acc7.address, functionSelector: "0x00000000"})), 1)

    assert.equal(await BESE.approvalId(), 1, "Approval ID increased")
  })

  it('Call external', async () => {
    await eesee.connect(signer).createLots(
      [{token: NFT.address, tokenID: 1, amount: 1, assetType: 0, data:'0x'}], 
      [{totalTickets: 100, ticketPrice: 2, duration: 86400, owner: signer.address, signer: signer.address, signatureData: "0x"}],
      600
    )

    const balanceBeforeBESE = await BESE.balanceOf(signer.address)
    const buyTickets = eesee.interface.encodeFunctionData('buyTickets', [[0], [20], signer.address, "0x"]);
    const _buyTickets = eesee.interface.encodeFunctionData('buyTickets', [[0], [2000], signer.address, "0x"]);
    const buyTickets2 = eesee.interface.encodeFunctionData('buyTickets', [[0], [31], signer.address, "0x"]);
    
    await expect(BESE.callExternal(acc7.address, buyTickets)).to.be.revertedWithCustomError(BESE, "FunctionNotApproved")
      .withArgs(((i) => compareObjects(convertArrayToObject(i), {_contract: acc7.address, functionSelector: "0x127b3be5"})))
    await expect(BESE.callExternal(acc7.address, "0x00")).to.be.revertedWithCustomError(BESE, "InvalidFunctionSelector")
      .withArgs("0x00")
    await expect(BESE.callExternal(eesee.address, "0x")).to.be.revertedWithCustomError(BESE, "FunctionNotApproved")
      .withArgs(((i) => compareObjects(convertArrayToObject(i), {_contract: eesee.address, functionSelector: "0x00000000"})))
      
    await expect(BESE.callExternal(eesee.address, buyTickets)).to.emit(eesee, "BuyTickets").withArgs(0, signer.address, 0, 20, 2*20, ((20*20 / 100 * 0.25)))
    await expect(BESE.callExternal(eesee.address, _buyTickets)).to.be.revertedWithCustomError(eesee, "BuyLimitExceeded")
    await expect(BESE.callExternal(eesee.address, buyTickets2)).to.be.revertedWith("ERC20: burn amount exceeds balance")
    const balanceAfterBESE = await BESE.balanceOf(signer.address)

    assert.equal(balanceBeforeBESE.sub(balanceAfterBESE).toString(), 40, "Invalid balance change")

    // Mint branch
    await ESE.transfer(proxy.address, 100)
    const transferESE = ESE.interface.encodeFunctionData('transfer', [BESE.address, 100]);
    const callExternal = eesee.interface.encodeFunctionData('callExternal', [ESE.address, transferESE]);
    await expect(BESE.callExternal(eesee.address, callExternal)).to.emit(BESE, "Transfer").withArgs(zeroAddress, signer.address, 100)

    // Null bytes revert branch
    const purchaseAsset = mockRouter.interface.encodeFunctionData('purchaseAsset', ["0x", zeroAddress]);
    await expect(BESE.callExternal(mockRouter.address, purchaseAsset)).to.be.reverted
  })

  it('reentrancy', async () => {
    const reenterBESE = BESE.interface.encodeFunctionData('callExternal', [zeroAddress, "0x"]);
    const callExternal = eesee.interface.encodeFunctionData('callExternal', [BESE.address, reenterBESE]);
    await expect(BESE.callExternal(eesee.address, callExternal)).to.be.revertedWith("ReentrancyGuard: reentrant call")
  })

  it('unwraps', async () => {
    await ESE.approve(BESE.address, 150)
    await BESE.wrap(100, acc3.address, "0x")

    await expect(BESE.revokeFunctionApprovals([{_contract: acc9.address, functionSelector: "0x00000000"}]))
    .to.emit(BESE, "RevokeApproval").withArgs(((i) => compareObjects(convertArrayToObject(i), {_contract: acc9.address, functionSelector: "0x00000000"})), 2)

    let userApprovalData = await BESE.userApprovalData(acc3.address)
    assert.equal(userApprovalData.lastApprovalId, 1, "Invalid lastApprovalId")
    assert.equal(userApprovalData.availableForUnwrap, 0, "Invalid availableForUnwrap")

    await BESE.wrap(50, acc3.address, "0x")

    userApprovalData = await BESE.userApprovalData(acc3.address)
    assert.equal(userApprovalData.lastApprovalId, 2, "Invalid lastApprovalId")
    assert.equal(userApprovalData.availableForUnwrap, 100, "Invalid availableForUnwrap")

    const balanceBeforeESE = await ESE.balanceOf(acc3.address)
    const balanceBeforeBESE = await BESE.balanceOf(acc3.address)
    await expect(BESE.connect(acc3).unwrap(50, acc3.address)).to.emit(BESE, "Transfer").withArgs(acc3.address, zeroAddress, 50)
    const balanceAfterBESE = await BESE.balanceOf(acc3.address)
    const balanceAfterESE = await ESE.balanceOf(acc3.address)
    assert.equal(balanceBeforeBESE.sub(balanceAfterBESE).toString(), 50, "Invalid balance change")
    assert.equal(balanceAfterESE.sub(balanceBeforeESE).toString(), 50, "Invalid balance change")

    assert.equal((await BESE.userApprovalData(acc3.address)).availableForUnwrap, 50, "Invalid availableForUnwrap")
    await expect(BESE.connect(acc3).unwrap(51, acc3.address)).to.be.revertedWithCustomError(BESE, "InsufficientAvailableBalance").withArgs(50)
  })

  it('reverts constructor', async () => {
    const _BESE = await hre.ethers.getContractFactory("BESE");
    await expect(_BESE.deploy(
      zeroAddress, 
      accessManager.address,
      zeroAddress
    )).to.be.revertedWithCustomError(_BESE, "InvalidESE")

    await expect(_BESE.deploy(
      ESE.address, 
      zeroAddress,
      zeroAddress
    )).to.be.revertedWithCustomError(_BESE, "InvalidAccessManager")
  })
})