const {
    time,
    loadFixture,
  } = require('@nomicfoundation/hardhat-network-helpers');
  const fs = require('fs')
    const path = require('path')
  const { anyValue } = require('@nomicfoundation/hardhat-chai-matchers/withArgs');
  const { expect } = require('chai');
  const { ethers, network } = require('hardhat');
  const assert = require('assert');
  const { StandardMerkleTree } = require('@openzeppelin/merkle-tree');
  const { getContractAddress } = require('@ethersproject/address')
  const { keccak256 } = require('@ethersproject/keccak256')
  const createPermit = require('./utils/createPermit')
  const { BigNumber } = ethers
  const {convertArrayToObject, compareObjects} = require('./utils/compareObjects');
  const compareErrors = require('./utils/compareErrors');
  ((network.name == 'multipleNetworks') ? describe : describe.skip)('Cross Chain', function () {
    let ERC20;
    let ERC20WithFeeOnTransfer;
    let NFT;
    let signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector;
    let royaltyEninge;
    let eeseeAssetSpoke
    let accessManager
    let eeseeAssetHub
    let mockAxelarGasServiceL1
    let mockAxelarGasServiceL2
    let mockAxelarGatewayL1
    let mockAxelarGatewayL2
    const abi = ethers.utils.defaultAbiCoder
    const chainSelectorL2 = abi.encode(['string'], ['l2'])
    const chainSelectorL1 = abi.encode(['string'], ['l1'])
    const tokenSymbolUSDC = 'USDC'

    const zeroBytes32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
    const zeroAddress = '0x0000000000000000000000000000000000000000'
    const oneAddress = '0x0000000000000000000000000000000000000001'
    const mainnetAddresses = JSON.parse(fs.readFileSync(path.resolve(__dirname, './constants/mainnetAddresses.json'), 'utf-8'))
    let snapshotId
    const messageId = '0x0000000000000000000000000000000000000000000000000000000000000023'
    async function changeNetwork(name){
        await hre.changeNetwork(name);
        if(signer == undefined){
            [signer, acc2, acc3, acc4, acc7, acc8, acc9, royaltyCollector] = await ethers.getSigners();
        }else{
            let [_signer, _acc2, _acc3, _acc4, _acc7, _acc8, _acc9, _royaltyCollector] = await ethers.getSigners();
            if(
                _signer.address != signer.address ||
                _acc2.address != acc2.address ||
                _acc3.address != acc3.address ||
                _acc4.address != acc4.address ||
                _acc7.address != acc7.address ||
                _acc8.address != acc8.address ||
                _acc9.address != acc9.address ||
                _royaltyCollector.address != royaltyCollector.address
            ) throw Error("Accounts for networks not match");
            [signer, acc2, acc3, acc4, acc7, acc8, acc9, royaltyCollector] = [_signer, _acc2, _acc3, _acc4, _acc7, _acc8, _acc9, _royaltyCollector]
        }
    }
    this.beforeAll(async() => {   
        await changeNetwork('testnet1');
        const _MockERC20L1 = await hre.ethers.getContractFactory('MockERC20');
        const _MockERC20WithFeeOnTransfer = await hre.ethers.getContractFactory('MockFeeOnTransfer');
        const _NFT = await hre.ethers.getContractFactory('EeseeNFT');
        const _royaltyEngine = await hre.ethers.getContractFactory('MockRoyaltyEngine');
        const _eeseeAssetSpoke = await hre.ethers.getContractFactory('EeseeAssetSpokeAxelar')
        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");
        const _mockAxelarGasServiceL1 = await hre.ethers.getContractFactory('MockAxelarGasService')
        const _mockAxelarGatewayL1 = await hre.ethers.getContractFactory('MockAxelarGateway')
        
        accessManager =  await _eeseeAccessManager.deploy()
        await accessManager.deployed()

        // PAUSER_ROLE
        await accessManager.grantRole('0x65d7a28e3265b37a6474929f336521b332c1681b933f6cb9f3376673440d862a', acc9.address)

        ERC20 = await _MockERC20L1.deploy('20000000000000000000000000000')
        await ERC20.deployed()

        ERC20WithFeeOnTransfer = await _MockERC20WithFeeOnTransfer.deploy('20000000000000000000000000000')
        await ERC20WithFeeOnTransfer.deployed()

        royaltyEninge = await _royaltyEngine.deploy();
        await royaltyEninge.deployed()

        NFT = await _NFT.deploy()
        await NFT.initialize({
            name: 'APES',
            symbol:'bayc',
            baseURI: '/',
            revealedURI: '',
            contractURI:'/',
            royaltyReceiver: zeroAddress,
            royaltyFeeNumerator: 0
        },100, signer.address)
        await NFT.deployed()


            await changeNetwork('testnet2');
            const _MockERC20L2 = await hre.ethers.getContractFactory('MockERC20');
            const _mockAxelarGasServiceL2 = await hre.ethers.getContractFactory('MockAxelarGasService')
            const _mockAxelarGatewayL2 = await hre.ethers.getContractFactory('MockAxelarGateway')
            
            const _eeseeAssetHub = await hre.ethers.getContractFactory('EeseeAssetHubAxelar')
            mockAxelarGatewayL2 = await _mockAxelarGatewayL2.deploy()
            await mockAxelarGatewayL2.deployed()
            mockAxelarGasServiceL2 = await _mockAxelarGasServiceL2.deploy(acc3.address)
            await mockAxelarGasServiceL2.deployed()
            mockUSDCL2 = await _MockERC20L2.deploy('20000000000000000000000000000')
            await mockUSDCL2.deployed()
            const deployTokenParamsL2 = abi.encode(
                ['string', 'string', 'uint8', 'uint256', 'address', 'uint256'],
                ['USD Coin', tokenSymbolUSDC, 6, 0, mockUSDCL2.address, 0]
            )
            await mockAxelarGatewayL2.deployToken(deployTokenParamsL2, zeroBytes32)
            await mockUSDCL2.transfer(mockAxelarGatewayL2.address, '10000000000000')

            eeseeAssetHub = await _eeseeAssetHub.deploy(
                'img/',
                'prefix#',
                '_description_',

                mockAxelarGatewayL2.address,
                mockAxelarGasServiceL2.address,
                zeroAddress
            )
            await eeseeAssetHub.deployed()

        
        await changeNetwork('testnet1');
        mockAxelarGatewayL1 = await _mockAxelarGatewayL1.deploy()
        await mockAxelarGatewayL1.deployed()
        mockUSDCL1 = await _MockERC20L1.deploy('20000000000000000000000000000')
        await mockUSDCL1.deployed()
        mockAxelarGasServiceL1 = await _mockAxelarGasServiceL1.deploy(acc3.address)
        await mockAxelarGasServiceL1.deployed()

        const deployTokenParamsL1 = abi.encode(
            ['string', 'string', 'uint8', 'uint256', 'address', 'uint256'],
            ['USD Coin', tokenSymbolUSDC, 6, 0, mockUSDCL1.address, 0]
        )
        await mockAxelarGatewayL1.deployToken(deployTokenParamsL1, zeroBytes32)
        eeseeAssetSpoke = await _eeseeAssetSpoke.deploy(
            accessManager.address,

            chainSelectorL1,
            {chainSelector: chainSelectorL2, _address: eeseeAssetHub.address},

            royaltyEninge.address,

            mockAxelarGatewayL1.address,
            mockAxelarGasServiceL1.address,

            zeroAddress
        )
        await eeseeAssetSpoke.deployed()
    })

    function _compareArrays(arr1, arr2){
        var isSame = true
        arr1.map((val,i) => {if(val.toString() != arr2[i].toString()){isSame = false}})
        return isSame
    }

    let tokenIDL2
    let assetHash_
    it('wrap', async () => {
        // === L1 ===

        const sourceL1 = {
            chainSelector: chainSelectorL1,
            _address: eeseeAssetSpoke.address
        }

        const asset = {
            token: NFT.address,
            tokenID: 1,
            amount: 1,
            assetType: 0,
            data: "0x"
        }

        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))
        assetHash_ = assetHash
        assert.equal(await eeseeAssetSpoke.getAssetHash(asset), assetHash, "Is assetHash correct")

        const assetHashWithSource = {
            source: sourceL1,
            assetHash: assetHash
        }
    
        tokenIDL2 = await eeseeAssetHub.getTokenId(assetHashWithSource)
        const _tokenIDL2 = await eeseeAssetSpoke["getTokenIdL2((address,uint256,uint256,uint8,bytes))"](asset)
        const __tokenIDL2 = await eeseeAssetSpoke["getTokenIdL2(bytes32)"](assetHash)
        assert.equal(tokenIDL2.toString(), _tokenIDL2.toString(), "Is tokenId correct")
        assert.equal(_tokenIDL2.toString(), __tokenIDL2.toString(), "Is tokenId correct")
        const transferNFTEncodedData = eeseeAssetHub.interface.encodeFunctionData('safeTransferFrom', [eeseeAssetHub.address, acc8.address, tokenIDL2, asset.amount, "0x"])

        await NFT.approve(eeseeAssetSpoke.address, 1)
        const gasPaid = 10000
        await compareErrors(
            eeseeAssetSpoke.wrap(
                [asset],
                [{target: eeseeAssetHub.address, callData: transferNFTEncodedData}],
                zeroAddress,
                "0x",
                {value: gasPaid}
            ),
            'InvalidRecipient()'
        )
        await compareErrors(
            eeseeAssetSpoke.wrap(
                [{...asset, amount: 2}],
                [{target: eeseeAssetHub.address, callData: transferNFTEncodedData}],
                acc2.address,
                "0x",
                {value: gasPaid}
            ),
            'InvalidAmount()'
        )
        await compareErrors(
            eeseeAssetSpoke.wrap(
                [{...asset, token: eeseeAssetSpoke.address}],
                [{target: eeseeAssetHub.address, callData: transferNFTEncodedData}],
                acc2.address,
                "0x",
                {value: gasPaid}
            ),
            'InvalidInterface()'
        )
        await compareErrors(
            eeseeAssetSpoke.wrap(
                [{...asset, token: ERC20.address}],
                [{target: eeseeAssetHub.address, callData: transferNFTEncodedData}],
                acc2.address,
                "0x",
                {value: gasPaid}
            ),
            'InvalidInterface()'
        )

        const royalties = await royaltyEninge.getRoyalty(asset.token, asset.tokenID, 10000)
        const assetData = abi.encode(
            ['address[]', 'uint256[]'],
            [royalties.recipients, royalties.amounts]
        )
        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [[assetHash], [asset.amount], [assetData],  [[eeseeAssetHub.address, transferNFTEncodedData]], acc2.address]
        )
        const payloadHash = keccak256(payload)
        await expect(eeseeAssetSpoke.wrap(
            [asset],
            [{target: eeseeAssetHub.address, callData: transferNFTEncodedData}],
            acc2.address,
            "0x",
            {value: gasPaid + 1}
        ))
        .to.emit(eeseeAssetSpoke, 'CrosschainSend').withArgs(
            await eeseeAssetSpoke.AXELAR_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL2, _address: eeseeAssetHub.address})), 
            abi.encode(['uint256'], [gasPaid + 1])
        )
        .to.emit(eeseeAssetSpoke, 'Wrap').withArgs(
            ((i) => compareObjects(convertArrayToObject(i), asset)), 
            assetHash,
            signer.address
        )
        .and.to.emit(mockAxelarGatewayL1, 'ContractCall')
        .withArgs(
            eeseeAssetSpoke.address, 
            abi.decode(['string'], chainSelectorL2)[0], 
            eeseeAssetHub.address.toLowerCase(),
            payloadHash,
            payload
        )
        .and.to.emit(mockAxelarGasServiceL1, 'NativeGasPaidForContractCall')
        .withArgs(
            eeseeAssetSpoke.address,
            abi.decode(['string'], chainSelectorL2)[0],
            eeseeAssetHub.address.toLowerCase(),
            payloadHash,
            gasPaid+1,
            signer.address
        )

        assert.equal(await NFT.ownerOf(1), eeseeAssetSpoke.address, 'Owner of nft asset is correct')

        // === L2 ===

        await changeNetwork('testnet2');
    
        assetWithSource = {
            "source": {
                "chainSelector": chainSelectorL1,
                "_address": eeseeAssetSpoke.address
            },
            "assetHash": assetHash
        }

        const commandId = keccak256(abi.encode(['uint256'],['1']))
        let approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL1)[0], eeseeAssetSpoke.address.toLowerCase(), eeseeAssetHub.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL2.approveContractCall(
            approveContractCall,
            commandId
        )

        await expect(eeseeAssetHub.execute(
            commandId,
            abi.decode(['string'], chainSelectorL1)[0],
            eeseeAssetSpoke.address.toLowerCase(),
            payload,
            {gasLimit: 30000000}
        )).to.emit(eeseeAssetHub, 'CrosschainReceive')
        .withArgs(
            await eeseeAssetHub.AXELAR_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), assetWithSource.source)),
            "0x"
        )
        .and.to.emit(eeseeAssetHub, 'Wrap')
        .withArgs(
            tokenIDL2, 
            asset.amount,
            ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetWithSource.assetHash))
        )
        .and.to.emit(eeseeAssetHub, 'TransferBatch')
        .withArgs(signer.address, zeroAddress, eeseeAssetHub.address, ((i) => _compareArrays(i, [tokenIDL2])), ((i) => _compareArrays(i, [asset.amount])))  
        .and.to.emit(eeseeAssetHub, 'TransferSingle')
        .withArgs(eeseeAssetHub.address, eeseeAssetHub.address, acc8.address, tokenIDL2, asset.amount)
    })

    it('Token URI is correct', async () => {
        const tokenURI = await eeseeAssetHub.uri(tokenIDL2)
        const json = Buffer.from(tokenURI.substring(29), "base64").toString();
        const result = JSON.parse(json);

        assert.equal(result.name, 'prefix#' + tokenIDL2)
        assert.equal(result.image, 'img/')
        assert.equal(result.description, '_description_')
        assert.equal(result.properties.sourceChainSelector, abi.decode(['string'], chainSelectorL1)[0])
        assert.equal(result.properties.sourceSpoke, eeseeAssetSpoke.address.toLowerCase())
        if(network.name != 'testnet') { //Some wierdness with string conversion on hardhat node. This is ok with normal forking.
            assert.equal(result.properties.assetHash, assetHash_.toLowerCase())
        }
    })
    
    let assetsL1
    const tokenIDsL2 = []
    it('wrap - different asset types', async () => {
        await changeNetwork('testnet1');
        //
        const _ERC1155 = await hre.ethers.getContractFactory("Mock1155");
        const ERC1155 = await _ERC1155.deploy("", 1, 10000, royaltyCollector.address, 10000)
        await ERC1155.deployed()
        await ERC1155.setApprovalForAll(eeseeAssetSpoke.address, 10000)
        const _MockERC20 = await hre.ethers.getContractFactory('MockERC20');
        const ERC20 = await _MockERC20.deploy('20000000000000000000000000000')
        await ERC20.deployed()
        await ERC20.approve(eeseeAssetSpoke.address, 10000)
        const gasPaid = 10000
        const assets = [
            {
                token: ERC1155.address, 
                tokenID: 1, 
                amount: 10000, 
                assetType: 1, 
                data:"0x"
            },
            {
                token: ERC20.address, 
                tokenID: 0, 
                amount: 10000, 
                assetType: 2,
                data:"0x"
            },
            {
                token: zeroAddress, 
                tokenID: 0, 
                amount: 10000, 
                assetType: 3, 
                data:"0x"
            }
        ]
        assetsL1 = assets

        tokenIDsL2.push(await eeseeAssetSpoke["getTokenIdL2((address,uint256,uint256,uint8,bytes))"](assets[0]))
        tokenIDsL2.push(await eeseeAssetSpoke["getTokenIdL2((address,uint256,uint256,uint8,bytes))"](assets[1]))
        tokenIDsL2.push(await eeseeAssetSpoke["getTokenIdL2((address,uint256,uint256,uint8,bytes))"](assets[2]))

        const assetHashes = assets.map((asset) => keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType])))
        const assetAmounts = assets.map((asset) => asset.amount)

        const encodedRoyaltiesPromises = assets.map(async (asset) => {
            if(asset.assetType !== 1) {
                return '0x'
            }
            const royalties = await royaltyEninge.getRoyalty(asset.token, asset.tokenID, 10000)
            return abi.encode(
                ['address[]', 'uint256[]'],
                [royalties.recipients, royalties.amounts]
            )
        })
        const encodedRoyalties = await Promise.all(encodedRoyaltiesPromises)
        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [assetHashes, assetAmounts, encodedRoyalties, [], signer.address]
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: ERC1155.address, 
                        tokenID: 1, 
                        amount: 100, 
                        assetType: 4, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                "0x",
                {value: gasPaid}
            ),
            'InvalidAsset()'
        )

        // ERC1155 reverts

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: ERC1155.address, 
                        tokenID: 1, 
                        amount: 0, 
                        assetType: 1, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                "0x",
                {value: gasPaid}
            ),
            'InvalidAmount()'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: eeseeAssetSpoke.address, 
                        tokenID: 1, 
                        amount: 10000, 
                        assetType: 1, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                "0x",
                {value: gasPaid}
            ),
            'InvalidInterface()'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: ERC20.address, 
                        tokenID: 1, 
                        amount: 10000, 
                        assetType: 1, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                "0x",
                {value: gasPaid}
            ),
            'InvalidInterface()'
        )

        //ERC20 reverts

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: ERC20.address, 
                        tokenID: 0, 
                        amount: 0, 
                        assetType: 2, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                "0x",
                {value: gasPaid}
            ),
            'InvalidAmount()'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: eeseeAssetSpoke.address, 
                        tokenID: 0, 
                        amount: 10000, 
                        assetType: 2, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                "0x",
                {value: gasPaid}
            ),
            '0x'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: NFT.address, 
                        tokenID: 0, 
                        amount: 10000, 
                        assetType: 2, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                "0x",
                {value: gasPaid}
            ),
            'InvalidInterface()'
        )
        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: ERC20.address, 
                        tokenID: 1, 
                        amount: 10000, 
                        assetType: 2, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                "0x",
                {value: gasPaid}
            ),
            'InvalidTokenID()'
        )

        // Native

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: zeroAddress, 
                        tokenID: 0, 
                        amount: 0, 
                        assetType: 3, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                "0x",
                {value: gasPaid}
            ),
            'InvalidAmount()'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: zeroAddress, 
                        tokenID: 0, 
                        amount: 1000, 
                        assetType: 3, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                "0x",
                {value: 0}
            ),
            'InsufficientValue()'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: ERC20.address, 
                        tokenID: 0, 
                        amount: 1000, 
                        assetType: 3, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                "0x",
                {value: gasPaid}
            ),
            'InvalidToken()'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: zeroAddress, 
                        tokenID: 10, 
                        amount: 1000, 
                        assetType: 3, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                "0x",
                {value: gasPaid}
            ),
            'InvalidTokenID()'
        )

        await expect(eeseeAssetSpoke.wrap(
            assets,
            [],
            signer.address,
            "0x",
            {value: gasPaid + assets[2].amount}
        ))
        .to.emit(eeseeAssetSpoke, 'CrosschainSend').withArgs(
            await eeseeAssetSpoke.AXELAR_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL2, _address: eeseeAssetHub.address})), 
            abi.encode(['uint256'], [gasPaid])
        )
        .to.emit(eeseeAssetSpoke, 'Wrap').withArgs(
            anyValue, 
            assetHashes[0],
            signer.address,
        )
        .to.emit(eeseeAssetSpoke, 'Wrap').withArgs(
            anyValue, 
            assetHashes[1],
            signer.address,
        )
        .to.emit(eeseeAssetSpoke, 'Wrap').withArgs(
            anyValue, 
            assetHashes[2],
            signer.address,
        )
        

        // === L2 ===

        await changeNetwork('testnet2');

        let commandId = keccak256(abi.encode(['uint256'],['2']))
        let payloadHash = keccak256(payload)
        let approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL1)[0], eeseeAssetSpoke.address.toLowerCase(), eeseeAssetHub.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL2.approveContractCall(
            approveContractCall,
            commandId
        )
        await expect(eeseeAssetHub.execute(
            commandId,
            abi.decode(['string'], chainSelectorL1)[0],
            eeseeAssetSpoke.address.toLowerCase(),
            payload,
            {gasLimit: 30000000}
        )).to.emit(eeseeAssetHub, 'CrosschainReceive')
        .withArgs(
            await eeseeAssetHub.AXELAR_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL1, _address: eeseeAssetSpoke.address})),
            "0x"
        )
        .and.to.emit(eeseeAssetHub, 'Wrap')
        .withArgs(
            tokenIDsL2[0], 
            assetAmounts[0],
            ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetHashes[0])), 
        )
        .to.emit(eeseeAssetHub, 'Wrap')
        .withArgs(
            tokenIDsL2[1], 
            assetAmounts[1],
            ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetHashes[1])), 
        )
        .to.emit(eeseeAssetHub, 'Wrap')
        .withArgs(
            tokenIDsL2[2], 
            assetAmounts[2],
            ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash,  assetHashes[2])), 
        )
        .and.to.emit(eeseeAssetHub, 'TransferBatch')
        .withArgs(signer.address, zeroAddress, eeseeAssetHub.address, ((i) => _compareArrays(i, tokenIDsL2)), ((i) => _compareArrays(i, assetAmounts)))  
        .and.to.emit(eeseeAssetHub, 'TransferSingle')
        .withArgs(eeseeAssetHub.address, eeseeAssetHub.address, signer.address, tokenIDsL2[0], assetAmounts[0]) 
        .and.to.emit(eeseeAssetHub, 'TransferSingle')
        .withArgs(eeseeAssetHub.address, eeseeAssetHub.address, signer.address, tokenIDsL2[1], assetAmounts[1])  
        .and.to.emit(eeseeAssetHub, 'TransferSingle')
        .withArgs(eeseeAssetHub.address, eeseeAssetHub.address, signer.address, tokenIDsL2[2], assetAmounts[2])   

        await changeNetwork('testnet1');
        const snapshotId = await network.provider.send('evm_snapshot')
        const payload_ = abi.encode(
            ['bytes32[]', 'uint256[]', 'address'], 
            [assetHashes, assetAmounts, signer.address]
        )        

        commandId = keccak256(abi.encode(['uint256'],['3']))
        payloadHash = keccak256(payload_)
        approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL1)[0], eeseeAssetHub.address.toLowerCase(), eeseeAssetSpoke.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL1.approveContractCall(
            approveContractCall,
            commandId
        )
        await compareErrors(
            eeseeAssetSpoke.execute(
                commandId,
                abi.decode(['string'], chainSelectorL1)[0],
                eeseeAssetHub.address.toLowerCase(),
                payload_,
                {gasLimit: 30000000}
            ),
            'InvalidSender()'
        )

        approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL2)[0], eeseeAssetSpoke.address.toLowerCase(), eeseeAssetSpoke.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL1.approveContractCall(
            approveContractCall,
            commandId
        )
        await compareErrors(
            eeseeAssetSpoke.execute(
                commandId,
                abi.decode(['string'], chainSelectorL2)[0],
                eeseeAssetSpoke.address.toLowerCase(),
                payload_,
                {gasLimit: 30000000}
            ),
            'InvalidSender()'
        )

        approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL2)[0], eeseeAssetHub.address.toLowerCase(), eeseeAssetSpoke.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL1.approveContractCall(
            approveContractCall,
            commandId
        )
        await expect(eeseeAssetSpoke.execute(
            commandId,
            abi.decode(['string'], chainSelectorL2)[0],
            eeseeAssetHub.address.toLowerCase(),
            payload_,
            {gasLimit: 30000000}
        )).and.to.emit(eeseeAssetSpoke, 'Unwrap')
        .withArgs(anyValue, assetHashes[0], signer.address)
        .and.to.emit(eeseeAssetSpoke, 'Unwrap')
        .withArgs(anyValue, assetHashes[1], signer.address)
        .and.to.emit(eeseeAssetSpoke, 'Unwrap')
        .withArgs(anyValue, assetHashes[2], signer.address)
        await network.provider.send("evm_revert", [snapshotId])
    })

    it('execute', async () => {
        const snapshotId = await network.provider.send('evm_snapshot')

        const asset = {
            token: NFT.address, 
            tokenID: 3, 
            amount: 1, 
            assetType: 0, 
            data:'0x'
        }

        await NFT.approve(eeseeAssetSpoke.address, 3)
        await eeseeAssetSpoke.wrap(
            [asset],
            [],
            signer.address,
            "0x",
            {value: 10000}
        )

        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'address'], 
            [[assetHash], [asset.amount], signer.address]
        )

        let commandId = keccak256(abi.encode(['uint256'],['3']))
        let payloadHash = keccak256(payload)
        let approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL2)[0], eeseeAssetHub.address.toLowerCase(), eeseeAssetSpoke.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL1.approveContractCall(
            approveContractCall,
            commandId
        )
        await expect(
            eeseeAssetSpoke.execute(
                commandId,
                abi.decode(['string'], chainSelectorL2)[0],
                eeseeAssetHub.address.toLowerCase(),
                payload,
                {gasLimit: 30000000}
            )).to.emit(eeseeAssetSpoke, 'Unwrap')
        .withArgs(anyValue, assetHash, signer.address)
        await network.provider.send("evm_revert", [snapshotId])
    })
    
    it('execute - stuck asset', async () => {
        const snapshotId = await network.provider.send('evm_snapshot')

        const asset = {
            token: NFT.address, 
            tokenID: 3, 
            amount: 1, 
            assetType: 0, 
            data:'0x'
        }
        
        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'address'], 
            [[assetHash], [asset.amount], signer.address]
            )

        let commandId = keccak256(abi.encode(['uint256'],['3']))
        let payloadHash = keccak256(payload)
        let approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL2)[0], eeseeAssetHub.address.toLowerCase(), eeseeAssetSpoke.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL1.approveContractCall(
            approveContractCall,
            commandId
        )
        await expect(eeseeAssetSpoke.execute(
                commandId,
                abi.decode(['string'], chainSelectorL2)[0],
                eeseeAssetHub.address.toLowerCase(),
                payload,
                {gasLimit: 30000000}
        )).to.emit(eeseeAssetSpoke, 'Stuck')
        .withArgs(anyValue, assetHash, signer.address, anyValue)

        await NFT.approve(eeseeAssetSpoke.address, 3)
        await eeseeAssetSpoke.wrap(
            [asset],
            [],
            signer.address,
            "0x",
            {value: 10000}
        )

        await compareErrors(
            eeseeAssetSpoke.unstuck(assetHash, zeroAddress),
            'InvalidRecipient()'
        )
        await compareErrors(
            eeseeAssetSpoke.unstuck(zeroBytes32, signer.address),
            'NoAssetsStuck()'
        )
        
        await expect(eeseeAssetSpoke.unstuck(assetHash, signer.address))
        .to.emit(eeseeAssetSpoke, 'Unstuck')
        .withArgs(anyValue, assetHash, signer.address, signer.address)

        await compareErrors(
            eeseeAssetSpoke.unstuck(assetHash, signer.address),
            'NoAssetsStuck()'
        )
        
        await network.provider.send("evm_revert", [snapshotId])
    })

    it('execute - stuck Native asset', async () => {
        const snapshotId = await network.provider.send('evm_snapshot')
        const asset = {
            token: zeroAddress, 
            tokenID: 0, 
            amount: 100, 
            assetType: 3, 
            data:'0x'
        }
        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))
        await eeseeAssetSpoke.wrap(
            [asset],
            [],
            signer.address,
            "0x",
            {value: asset.amount + 100000}
        )

        const _MockRecipient = await hre.ethers.getContractFactory('MockRecipient')
        const recipient = await _MockRecipient.deploy()
        await recipient.deployed()

        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'address'], 
            [[assetHash], [asset.amount], recipient.address]
        )
        
        let commandId = keccak256(abi.encode(['uint256'],['3']))
        let payloadHash = keccak256(payload)
        let approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL2)[0], eeseeAssetHub.address.toLowerCase(), eeseeAssetSpoke.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL1.approveContractCall(
            approveContractCall,
            commandId
        )
        await expect(eeseeAssetSpoke.execute(
                commandId,
                abi.decode(['string'], chainSelectorL2)[0],
                eeseeAssetHub.address.toLowerCase(),
                payload,
                {gasLimit: 30000000}
        )).to.emit(eeseeAssetSpoke, 'Stuck')
        .withArgs(anyValue, assetHash, recipient.address, anyValue)

        const unstuckEncodedDataRevert = eeseeAssetSpoke.interface.encodeFunctionData('unstuck', [assetHash, signer.address])
        await expect(recipient.callExternal(eeseeAssetSpoke.address, unstuckEncodedDataRevert))
        .to.emit(eeseeAssetSpoke, 'Unstuck')
        .withArgs(((i) => compareObjects(convertArrayToObject(i), asset)), assetHash, recipient.address, signer.address)
        
        await network.provider.send("evm_revert", [snapshotId])
    })
    it('eeseeAssetSpoke onlySelf check', async () => {
        await compareErrors(
            eeseeAssetSpoke._transferAssetTo(zeroBytes32, 300, signer.address),
            'OnlySelf()'
        )
    })
    it('eeseeAssetHub onlySelf check', async () => {
        await changeNetwork('testnet2');
        await compareErrors(eeseeAssetHub._setRoyalty(0, '0x'), 'OnlySelf()')
    })   
    
    it('unwrap', async () => {
        const source = {
            "chainSelector": chainSelectorL1,
            "_address": eeseeAssetSpoke.address
        }

        const gasPaid = 10000
        const assetHashes = [assetsL1[0], assetsL1[1]].map((asset) => keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType])))

        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [assetsL1[2].token, assetsL1[2].tokenID, assetsL1[2].assetType]))

        const snapshotId = await network.provider.send('evm_snapshot')

        const chainSelectorL3 = abi.encode(['string'], ['l3'])
        const eeseeAssetSpokeL3 = {address: zeroAddress}
        
        let payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [[assetHash], [assetsL1[2].amount], ['0x'], [],  signer.address]
        )

        const commandId = keccak256(abi.encode(['uint256'],['3']))
        let payloadHash = keccak256(payload)
        let approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL3)[0], eeseeAssetSpoke.address.toLowerCase(), eeseeAssetHub.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL2.approveContractCall(
            approveContractCall,
            commandId
        )
        await eeseeAssetHub.execute(
            commandId,
            abi.decode(['string'], chainSelectorL3)[0],
            eeseeAssetSpoke.address.toLowerCase(),
            payload,
            {gasLimit: 30000000}
        )
        const tokenID_ = await eeseeAssetHub.getTokenId({source:{chainSelector: chainSelectorL3, _address: eeseeAssetSpokeL3.address}, assetHash})

        await compareErrors(
            eeseeAssetHub.unwrap(
                [tokenIDsL2[0], tokenID_],
                [10000, assetsL1[2].amount],
                acc2.address,
                "0x"
            ),
            'InvalidDestination()'
        )

        payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [[zeroBytes32], [0], ['0x'], [], signer.address]
        )
        payloadHash = keccak256(payload)
        approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL3)[0], eeseeAssetSpoke.address.toLowerCase(), eeseeAssetHub.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL2.approveContractCall(
            approveContractCall,
            commandId
        )
        await eeseeAssetHub.execute(
            commandId,
            abi.decode(['string'], chainSelectorL3)[0],
            eeseeAssetSpoke.address.toLowerCase(),
            payload,
            {gasLimit: 30000000}
        )
        const tokenID__ = await eeseeAssetHub.getTokenId({source:{chainSelector: chainSelectorL3, _address: eeseeAssetSpoke.address}, assetHash: zeroBytes32})
        await compareErrors(
            eeseeAssetHub.unwrap(
                [tokenIDsL2[0], tokenID__],
                [10000, assetsL1[2].amount],
                acc2.address,
                "0x"
            ),
            'InvalidDestination()'
        )
        await compareErrors(
            eeseeAssetHub.unwrap(
                [tokenIDsL2[0], tokenIDsL2[1]],
                [10000, assetsL1[2].amount],
                zeroAddress,
                "0x"
            ),
            'InvalidRecipient()'
        )
        await compareErrors(
            eeseeAssetHub.unwrap(
                [],
                [10000, assetsL1[2].amount],
                acc2.address,
                "0x"
            ),
            'InvalidTokenIdsLength()'
        )
        await compareErrors(
            eeseeAssetHub.unwrap(
                [tokenIDsL2[0], tokenIDsL2[1]],
                [10000],
                acc2.address,
                "0x"
            ),
            'InvalidTokenIdsLength()'
        )

        await expect(eeseeAssetHub.unwrap(
            [tokenIDsL2[0], tokenIDsL2[1]],
            [10000, assetsL1[2].amount],
            acc2.address,
            "0x",
            {value: gasPaid}
        ))
        .to.emit(eeseeAssetHub, 'Unwrap').withArgs(
            tokenIDsL2[0], 
            10000,
            ((i) => compareObjects(convertArrayToObject(i.source), source) && compareObjects(i.assetHash, assetHashes[0])), 
            signer.address,
            acc2.address
        )
        .to.emit(eeseeAssetHub, 'Unwrap').withArgs(
            tokenIDsL2[1], 
            assetsL1[2].amount,
            ((i) => compareObjects(convertArrayToObject(i.source), source) && compareObjects(i.assetHash, assetHashes[1])), 
            signer.address, 
            acc2.address
        )
        .and.to.emit(eeseeAssetHub, 'CrosschainSend')
        .withArgs(
            await eeseeAssetHub.AXELAR_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL1, _address: eeseeAssetSpoke.address})), 
            abi.encode(['uint256'], [gasPaid])
        )

        await compareErrors(
            eeseeAssetHub.connect(acc8).unwrap(
                [tokenIDsL2[2]],
                [10000],
                acc2.address,
                "0x",
                {value: gasPaid}
            ),
            '0x' // ERC1155: burn amount exceeds balance
        )

        // Use from contract's balance
        await eeseeAssetHub.safeTransferFrom(signer.address, eeseeAssetHub.address, tokenIDsL2[2], 10000, "0x")
        await expect(eeseeAssetHub.connect(acc8).unwrap(
            [tokenIDsL2[2]],
            [10000],
            acc2.address,
            "0x",
            {value: gasPaid}
        )).to.emit(eeseeAssetHub, 'Unwrap').withArgs(
            tokenIDsL2[2], 
            10000,
            ((i) => compareObjects(convertArrayToObject(i.source), source) && compareObjects(i.assetHash, assetHash)), 
            acc8.address, 
            acc2.address
        )

        await network.provider.send("evm_revert", [snapshotId])
    })

    it('Eesee Vault', async () => {
        await changeNetwork('testnet1');
        const source = {
            "chainSelector": chainSelectorL1,
            "_address": eeseeAssetSpoke.address
        }

        const assetHashes = [assetsL1[0], assetsL1[1]].map((asset) => keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType])))
        const assetAmounts = [assetsL1[0], assetsL1[1]].map((asset) => asset.amount)
        
        const tokenIds = []
        tokenIds.push(await eeseeAssetSpoke["getTokenIdL2(bytes32)"](assetHashes[0]))
        tokenIds.push(await eeseeAssetSpoke["getTokenIdL2(bytes32)"](assetHashes[1]))

        await changeNetwork('testnet2');
        const snapshotId = await network.provider.send('evm_snapshot')

        const _MockRecipient = await hre.ethers.getContractFactory('MockRecipient')
        const recipient = await _MockRecipient.deploy()
        await recipient.deployed()

        let payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [assetHashes, assetAmounts, ['0x', '0x'], [], recipient.address] // fallback recipient is unable to receive erc1155s
        )

        const vault = await eeseeAssetHub.vault()

        let commandId = keccak256(abi.encode(['uint256'],['3']))
        let payloadHash = keccak256(payload)
        let approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL1)[0], eeseeAssetSpoke.address.toLowerCase(), eeseeAssetHub.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL2.approveContractCall(
            approveContractCall,
            commandId
        )
        await expect(eeseeAssetHub.execute(
            commandId,
            abi.decode(['string'], chainSelectorL1)[0],
            eeseeAssetSpoke.address.toLowerCase(),
            payload,
            {gasLimit: 30000000}
        ))
        .to.emit(eeseeAssetHub, 'CrosschainReceive')
        .and.to.emit(eeseeAssetHub, 'TransferSingle')
        .withArgs(signer.address, eeseeAssetHub.address, vault, tokenIDsL2[0], assetAmounts[0]) 
        .and.to.emit(eeseeAssetHub, 'TransferSingle')
        .withArgs(signer.address, eeseeAssetHub.address, vault, tokenIDsL2[1], assetAmounts[1])  
    
        const _ERC1155 = await hre.ethers.getContractFactory("Mock1155");
        const ERC1155 = await _ERC1155.deploy("", 0, 10000, royaltyCollector.address, 10000)
        await ERC1155.deployed()

        await compareErrors(
            ERC1155.safeTransferFrom(signer.address, vault, 0, 100, "0x"),
            '0x'//ERC1155: transfer to non-ERC1155Receiver implementer
        )
        await compareErrors(
            ERC1155.safeBatchTransferFrom(signer.address, vault, [0], [100], "0x"),
            '0x'//ERC1155: transfer to non-ERC1155Receiver implementer
        )

        const _Vault = await hre.ethers.getContractFactory("EeseeVault");
        const _vault = _Vault.attach(vault);

        let call = _vault.interface.encodeFunctionData('onERC1155Received', [zeroAddress, zeroAddress, 0, 0, "0x"]);
        await compareErrors(
            eeseeAssetHub.multicall([{target: vault, callData: call}]),
            'FunctionBlacklisted(address,bytes4)'
        )
        call = _vault.interface.encodeFunctionData('onERC1155BatchReceived', [zeroAddress, zeroAddress, [0], [0], "0x"]);
        await compareErrors(
            eeseeAssetHub.multicall([{target: vault, callData: call}]),
            'FunctionBlacklisted(address,bytes4)'
        )
        await compareErrors(
            _vault.unstuck([], signer.address),
            'InvalidTokenIdsLength()'
        )
        await compareErrors(
            _vault.unstuck(tokenIds, zeroAddress),
            'InvalidRecipient()'
        )
        await compareErrors(
            _vault.unstuck(tokenIds, signer.address),
            'NoTokensStuck()'
        )

        call = _vault.interface.encodeFunctionData('unstuck', [tokenIds, acc2.address]);
        await expect(recipient.callExternal(_vault.address, call))
        .to.emit(eeseeAssetHub, "TransferBatch")
        .withArgs(_vault.address, _vault.address, acc2.address, ((i) => _compareArrays(i, tokenIds)), ((i) => _compareArrays(i, assetAmounts)))  
        
        assert.equal((await eeseeAssetHub.balanceOf(acc2.address, tokenIds[0])).toString(), assetAmounts[0], "balance not correct")
        assert.equal((await eeseeAssetHub.balanceOf(acc2.address, tokenIds[1])).toString(), assetAmounts[1], "balance not correct")
        
        const endodedAddr = abi.encode(['address'], [acc2.address])
        await eeseeAssetHub.connect(acc2).safeTransferFrom(acc2.address, vault, tokenIds[0], 100, endodedAddr)
        await _vault.connect(acc2).unstuck([tokenIds[0]], acc4.address)
        assert.equal((await eeseeAssetHub.balanceOf(acc4.address, tokenIds[0])).toString(), 100, "balance not correct")

        await compareErrors(
            _Vault.deploy(zeroAddress, oneAddress),
            'InvalidConstructor()'
        )

        await network.provider.send("evm_revert", [snapshotId])
    })

    it('Asset amount increases', async () => {
        await changeNetwork('testnet1');
        const _MockERC20 = await hre.ethers.getContractFactory('MockERC20')
        const mockERC20 = await _MockERC20.deploy(1000000)
        await mockERC20.deployed()
        await mockERC20.approve(eeseeAssetSpoke.address, 1000000)

        const asset = {
            token: mockERC20.address, 
            tokenID: 0, 
            amount: 500000, 
            assetType: 2, 
            data:'0x'
        }
        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

        await eeseeAssetSpoke.wrap(
            [asset],
            [],
            signer.address,
            "0x",
            {value: 10000}
        )
        assert.equal((await eeseeAssetSpoke.assetsStorage(assetHash)).amount, asset.amount, "Amount is correct")

        await eeseeAssetSpoke.wrap(
            [asset],
            [],
            signer.address,
            "0x",
            {value: 10000}
        )
        assert.equal((await eeseeAssetSpoke.assetsStorage(assetHash)).amount, asset.amount + asset.amount, "Amount is correct")
    })

    it('Works with fee on transfer tokens', async () => {
        await ERC20WithFeeOnTransfer.approve(eeseeAssetSpoke.address, 500000)

        const asset = {
            token: ERC20WithFeeOnTransfer.address, 
            tokenID: 0, 
            amount: 500000, 
            assetType: 2, 
            data:'0x'
        }
        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

        const newAssetAmount = asset.amount * 0.99
        await expect(eeseeAssetSpoke.wrap(
            [asset],
            [],
            signer.address,
            "0x",
            {value: 10000}
        )).to.emit(eeseeAssetSpoke, "Wrap").withArgs(((i) => compareObjects(convertArrayToObject(i), {...asset, amount: newAssetAmount})), assetHash, signer.address)
        
        asset.amount = newAssetAmount
        assert.equal((await eeseeAssetSpoke.assetsStorage(assetHash)).amount.toString(), asset.amount.toString(), "Amount is correct")

        // ==== unwrap ====

        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'address'], 
            [[assetHash], [asset.amount], signer.address]
        )

        let commandId = keccak256(abi.encode(['uint256'],['3']))
        let payloadHash = keccak256(payload)
        let approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL2)[0], eeseeAssetHub.address.toLowerCase(), eeseeAssetSpoke.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL1.approveContractCall(
            approveContractCall,
            commandId
        )
        const snapshotId = await network.provider.send('evm_snapshot')
        await expect(eeseeAssetSpoke.execute(
            commandId,
            abi.decode(['string'], chainSelectorL2)[0],
            eeseeAssetHub.address.toLowerCase(),
            payload,
            {gasLimit: 30000000}
        )).to.emit(eeseeAssetSpoke, 'Unwrap')
        .withArgs(((i) => compareObjects(convertArrayToObject(i), asset)), assetHash, signer.address)
        await network.provider.send("evm_revert", [snapshotId])

        await ERC20WithFeeOnTransfer.toggleStuck()
        await expect(eeseeAssetSpoke.execute(
            commandId,
            abi.decode(['string'], chainSelectorL2)[0],
            eeseeAssetHub.address.toLowerCase(),
            payload,
            {gasLimit: 30000000}
        )).to.emit(eeseeAssetSpoke, 'Stuck')
        .withArgs(((i) => compareObjects(convertArrayToObject(i), asset)), assetHash, signer.address, anyValue)
        await ERC20WithFeeOnTransfer.toggleStuck()

        await expect(eeseeAssetSpoke.unstuck(assetHash, signer.address))
        .to.emit(eeseeAssetSpoke, 'Unstuck')
        .withArgs(((i) => compareObjects(convertArrayToObject(i), asset)), assetHash, signer.address, signer.address)
    })

    it('eeseeAssetHub execute, different royalties', async () => {
        await changeNetwork('testnet2');
        // Empty royalties array
        let asset = {
            token: NFT.address, 
            tokenID: 5, 
            amount: 1, 
            assetType: 0, 
            data:'0x'
        }
        let assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

        let assetData = abi.encode(
            ['address[]', 'uint256[]'],
            [[], []]
        )

        let tokenIDL2 = await eeseeAssetHub.getTokenId({source: {chainSelector:chainSelectorL1, _address: eeseeAssetSpoke.address}, assetHash})
        let payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [[assetHash], [asset.amount], [assetData], [[eeseeAssetSpoke.address, '0x']], signer.address]
        )
        any2EVMMessage = {
            messageId,
            sourceChainSelector: chainSelectorL1,
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
            data: payload,
            destTokenAmounts: []
        }
        let assetWithSource = {
            "source": {
                "chainSelector": chainSelectorL1,
                "_address": eeseeAssetSpoke.address
            },
            "assetHash": assetHash
        }

        let call = eeseeAssetHub.interface.encodeFunctionData('_setRoyalty', [0, "0x"]);
        await compareErrors(
            eeseeAssetHub.multicall([{target: eeseeAssetHub.address, callData: call}]),
            'FunctionBlacklisted(address,bytes4)'
        );
        
        call = eeseeAssetHub.interface.encodeFunctionData('setApprovalForAll', [eeseeAssetHub.address, true]);// ERC1155: setting approval status for self
        await compareErrors(
            eeseeAssetHub.multicall([{target: eeseeAssetHub.address, callData: call}]),
            'MulticallReverted(bytes)'
        );

        let commandId = keccak256(abi.encode(['uint256'],['3']))
        let payloadHash = keccak256(payload)
        let approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL1)[0], eeseeAssetSpoke.address.toLowerCase(), eeseeAssetHub.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL2.approveContractCall(
            approveContractCall,
            commandId
        )
        await expect(eeseeAssetHub.execute(
            commandId,
            abi.decode(['string'], chainSelectorL1)[0],
            eeseeAssetSpoke.address.toLowerCase(),
            payload,
            {gasLimit: 30000000}
        ))
        .to.emit(eeseeAssetHub, 'Wrap').withArgs(
            tokenIDL2, 
            asset.amount,
            ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetWithSource.assetHash))
        )
        .to.emit(eeseeAssetHub, 'MulticallFailed')
        // Royalties array length > 1, 2 valid recipiens
        asset = {
            token: NFT.address, 
            tokenID: 6, 
            amount: 1, 
            assetType: 0, 
            data:'0x'
        }
        assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

        assetData = abi.encode(
            ['address[]', 'uint256[]'],
            [[acc7.address, acc8.address, zeroAddress], [100000, 150000, 0]]
        )
        tokenIDL2 = await eeseeAssetHub.getTokenId({source: {chainSelector:chainSelectorL1, _address: eeseeAssetSpoke.address}, assetHash})
        payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [[assetHash], [asset.amount], [assetData], [], signer.address]
        )
        any2EVMMessage = {
            messageId,
            sourceChainSelector: chainSelectorL1,
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
            data: payload,
            destTokenAmounts: []
        }
        assetWithSource = {
            "source": {
                "chainSelector": chainSelectorL1,
                "_address": eeseeAssetSpoke.address
            },
            "assetHash": assetHash
        }
        payloadHash = keccak256(payload)
        approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL1)[0], eeseeAssetSpoke.address.toLowerCase(), eeseeAssetHub.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL2.approveContractCall(
            approveContractCall,
            commandId
        )
        await expect(eeseeAssetHub.execute(
            commandId,
            abi.decode(['string'], chainSelectorL1)[0],
            eeseeAssetSpoke.address.toLowerCase(),
            payload,
            {gasLimit: 30000000}
        ))
        .to.emit(eeseeAssetHub, 'Wrap').withArgs(
            tokenIDL2, 
            asset.amount,
            ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetWithSource.assetHash)), 
        )
        // Royalties array length != recipiens length
        asset = {
            token: NFT.address, 
            tokenID: 70, 
            amount: 1, 
            assetType: 0, 
            data:'0x'
        }
        assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

        assetData = abi.encode(
            ['address[]', 'uint256[]'],
            [[acc7.address, acc8.address, acc9.address], [100000, 150000]]
        )
        tokenIDL2 = await eeseeAssetHub.getTokenId({source: {chainSelector:chainSelectorL1, _address: eeseeAssetSpoke.address}, assetHash})
        payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [[assetHash], [asset.amount], [assetData], [], signer.address]
        )
        any2EVMMessage = {
            messageId,
            sourceChainSelector: chainSelectorL1,
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
            data: payload,
            destTokenAmounts: []
        }
        assetWithSource = {
            "source": {
                "chainSelector": chainSelectorL1,
                "_address": eeseeAssetSpoke.address
            },
            "assetHash": assetHash
        }
        payloadHash = keccak256(payload)
        approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL1)[0], eeseeAssetSpoke.address.toLowerCase(), eeseeAssetHub.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL2.approveContractCall(
            approveContractCall,
            commandId
        )
        await expect(eeseeAssetHub.execute(
            commandId,
            abi.decode(['string'], chainSelectorL1)[0],
            eeseeAssetSpoke.address.toLowerCase(),
            payload,
            {gasLimit: 30000000}
        ))
        .to.emit(eeseeAssetHub, 'Wrap').withArgs(
            tokenIDL2, 
            asset.amount,
            ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetWithSource.assetHash)), 
        )
        assert.equal((await eeseeAssetHub.royaltyInfo(tokenIDL2, 10000))[0], zeroAddress, "royalty set")
        assert.equal((await eeseeAssetHub.royaltyInfo(tokenIDL2, 10000))[1].toString(), "0", "royalty set")

        // Royalties array length > 1, 0 valid recipiens
        asset = {
            token: NFT.address, 
            tokenID: 7, 
            amount: 1, 
            assetType: 0, 
            data:'0x06'
        }
        assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

        assetData = abi.encode(
            ['address[]', 'uint256[]'],
            [[acc8.address, zeroAddress], [0, 10000]]
        )
        tokenIDL2 = await eeseeAssetHub.getTokenId({source: {chainSelector:chainSelectorL1, _address: eeseeAssetSpoke.address}, assetHash})
        payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [[assetHash], [asset.amount], [assetData], [], signer.address]
        )
        any2EVMMessage = {
            messageId,
            sourceChainSelector: chainSelectorL1,
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
            data: payload,
            destTokenAmounts: []
        }
        assetWithSource = {
            "source": {
                "chainSelector": chainSelectorL1,
                "_address": eeseeAssetSpoke.address
            },
            "assetHash": assetHash
        }
        payloadHash = keccak256(payload)
        approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL1)[0], eeseeAssetSpoke.address.toLowerCase(), eeseeAssetHub.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL2.approveContractCall(
            approveContractCall,
            commandId
        )
        await expect(eeseeAssetHub.execute(
            commandId,
            abi.decode(['string'], chainSelectorL1)[0],
            eeseeAssetSpoke.address.toLowerCase(),
            payload,
            {gasLimit: 30000000}
        ))
        .to.emit(eeseeAssetHub, 'Wrap').withArgs(
            tokenIDL2, 
            asset.amount,
            ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetWithSource.assetHash))
        )
         // Royalties array length == 1, feeNumerators[0] == 0
        asset = {
            token: NFT.address, 
            tokenID: 7, 
            amount: 1, 
            assetType: 0, 
            data:'0x01'
        }
        assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

        assetData = abi.encode(
            ['address[]', 'uint256[]'],
            [[acc8.address], [0]]
        )
        tokenIDL2 = await eeseeAssetHub.getTokenId({source: {chainSelector:chainSelectorL1, _address: eeseeAssetSpoke.address}, assetHash})
        payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [[assetHash], [asset.amount], [assetData], [], signer.address]
        )
        any2EVMMessage = {
            messageId,
            sourceChainSelector: chainSelectorL1,
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
            data: payload,
            destTokenAmounts: []
        }
        assetWithSource = {
            "source": {
                "chainSelector": chainSelectorL1,
                "_address": eeseeAssetSpoke.address
            },
            "assetHash": assetHash
        }       
        payloadHash = keccak256(payload)
        approveContractCall = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL1)[0], eeseeAssetSpoke.address.toLowerCase(), eeseeAssetHub.address, payloadHash, commandId, 1]
        )
        await mockAxelarGatewayL2.approveContractCall(
            approveContractCall,
            commandId
        ) 
        await expect(eeseeAssetHub.execute(
            commandId,
            abi.decode(['string'], chainSelectorL1)[0],
            eeseeAssetSpoke.address.toLowerCase(),
            payload,
            {gasLimit: 30000000}
        ))
        .to.emit(eeseeAssetHub, 'Wrap').withArgs(
            tokenIDL2, 
            asset.amount,
            ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetWithSource.assetHash))
        )
    })

    it('pausable', async () => {
        await changeNetwork('testnet1');

        await compareErrors(
            eeseeAssetSpoke.pause(),
            'CallerNotAuthorized()'
        )
        await expect(eeseeAssetSpoke.connect(acc9).pause()).to.emit(eeseeAssetSpoke, "Paused").withArgs(acc9.address)

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [],
                [],
                zeroAddress,
                "0x",
            ),
            '0x'//Pausable: paused
        )
        await compareErrors(
            eeseeAssetSpoke.unpause(),
            'CallerNotAuthorized()'
        )
        await expect(eeseeAssetSpoke.connect(acc9).unpause()).to.emit(eeseeAssetSpoke, "Unpaused").withArgs(acc9.address)

        
        await compareErrors(
            eeseeAssetSpoke.wrap(
                [],
                [],
                zeroAddress,
                "0x",
            ),
            'InvalidAssetsLength()'//Other error
        )
    })

    it('reverts constructor spoke', async () => {
        const _eeseeAssetSpoke = await hre.ethers.getContractFactory('EeseeAssetSpokeAxelar')
        await compareErrors(
            _eeseeAssetSpoke.deploy(
                accessManager.address,
                chainSelectorL1,
                {chainSelector: chainSelectorL2, _address: zeroAddress},
                oneAddress,

                oneAddress,
                oneAddress,

                oneAddress,
            ),
            'InvalidConstructor()'
        )

        await compareErrors(
            _eeseeAssetSpoke.deploy(
                accessManager.address,
                chainSelectorL1,
                {chainSelector: chainSelectorL2, _address: oneAddress},
                zeroAddress,
    
                oneAddress,
                oneAddress,
    
                oneAddress
            ),
            'InvalidConstructor()'
        )

        await compareErrors(
            _eeseeAssetSpoke.deploy(
                zeroAddress,
                chainSelectorL1,
                {chainSelector: chainSelectorL2, _address: oneAddress},
                oneAddress,
    
                oneAddress,
                oneAddress,
    
                oneAddress,
            ),
            'InvalidAccessManager()'
        )

        await compareErrors(
            _eeseeAssetSpoke.deploy(
                accessManager.address,
                chainSelectorL1,
                {chainSelector: chainSelectorL2, _address: oneAddress},
                oneAddress,
    
                zeroAddress,
                oneAddress,
    
                oneAddress,
            ),
            'InvalidGateway()'
        )

        await compareErrors(
            _eeseeAssetSpoke.deploy(
                accessManager.address,
                chainSelectorL1,
                {chainSelector: chainSelectorL2, _address: oneAddress},
                oneAddress,
    
                oneAddress,
                zeroAddress,
    
                oneAddress,
            ),
            'InvalidGasService()'
        )
    })

    it('reverts constructor hub', async () => {
        const _eeseeAssetHub = await hre.ethers.getContractFactory('EeseeAssetHubAxelar')
        await compareErrors(
            _eeseeAssetHub.deploy(
                '/',
                'prefix',
                'description',
    
                oneAddress,
                zeroAddress,
    
                oneAddress,
            ),
            'InvalidGasService()'
        )

        await compareErrors(
            _eeseeAssetHub.deploy(
                '',
                'prefix',
                'description',
    
                oneAddress,
                oneAddress,
    
                oneAddress,
            ),
            'InvalidConstructor()'
        )

        await compareErrors(
            _eeseeAssetHub.deploy(
                '/',
                'prefix',
                '',
    
                oneAddress,
                oneAddress,
    
                oneAddress,
            ),
            'InvalidConstructor()'
        )

        await compareErrors(
            _eeseeAssetHub.deploy(
                '/',
                'prefix',
                'description',
    
                zeroAddress,
                oneAddress,
    
                oneAddress,
            ),
            'InvalidGateway()'
        )
    })

    it('supports interface', async () => {
        assert.equal(await eeseeAssetHub.supportsInterface('0x2a55205a'), true, 'eeseeAssetHub supports ERC2981')
        assert.equal(await eeseeAssetHub.supportsInterface('0x4e2312e0'), true, 'eeseeAssetHub supports ERC1155Receiver')
        assert.equal(await eeseeAssetHub.supportsInterface('0x0e89341c'), true, 'eeseeAssetHub supports ERC1155')
        assert.equal(await eeseeAssetHub.supportsInterface('0xd9b67a26'), true, 'eeseeAssetHub supports ERC1155')
        assert.equal(await eeseeAssetHub.supportsInterface('0x00000001'), false, 'eeseeAssetHub supports 0x00000001')
    })

    it('removes multicall blacklist', async () => {
        const _testMulticallExternal = await hre.ethers.getContractFactory('TestMulticallExternal')
        multicallExternal =  await _testMulticallExternal.deploy()
        await multicallExternal.deployed()

        await expect(multicallExternal.addToBlacklist(zeroAddress, "0x00010203")).to.emit(multicallExternal, "AddToBlacklist").withArgs(zeroAddress, "0x00010203")
        await expect(multicallExternal.addToBlacklist(zeroAddress, "0x00010203")).to.not.emit(multicallExternal, "AddToBlacklist")

        await compareErrors(
            multicallExternal.multicall([{target: zeroAddress, callData: "0x00010203"}]),
            'FunctionBlacklisted(address,bytes4)'
        )
     
        await expect(multicallExternal.removeFromBlacklist(zeroAddress, "0x00010203")).to.emit(multicallExternal, "RemoveFromBlacklist").withArgs(zeroAddress, "0x00010203")
        await expect(multicallExternal.removeFromBlacklist(zeroAddress, "0x00010203")).to.not.emit(multicallExternal, "RemoveFromBlacklist")

        await compareErrors(
            multicallExternal.multicall([{target: zeroAddress, callData: "0x00010203"}]),
            'MulticallReverted(bytes)'
        )
    })
})