const {
    time,
    loadFixture,
  } = require('@nomicfoundation/hardhat-network-helpers');
  const fs = require('fs')
    const path = require('path')
  const { anyValue } = require('@nomicfoundation/hardhat-chai-matchers/withArgs');
  const { expect } = require('chai');
  const { ethers, network } = require('hardhat');
  const assert = require('assert');
  const { StandardMerkleTree } = require('@openzeppelin/merkle-tree');
  const { getContractAddress } = require('@ethersproject/address')
  const { keccak256 } = require('@ethersproject/keccak256')
  const createPermit = require('./utils/createPermit')
  const { BigNumber } = ethers
  const {convertArrayToObject, compareObjects} = require('./utils/compareObjects');
  ((network.name != 'multipleNetworks') ? describe : describe.skip)('Cross Chain', function () {
    let ERC20;
    let ERC20WithFeeOnTransfer;
    let NFT;
    let signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector;
    let royaltyEninge;
    let eeseeAssetSpoke
    let accessManager
    let eeseeAssetHub
    let mockAxelarGasService
    let mockAxelarGatewayL1
    let mockAxelarGatewayL2

    const abi = ethers.utils.defaultAbiCoder
    const chainSelectorL2 = abi.encode(['string'], ['l2'])
    const chainSelectorL1 = abi.encode(['string'], ['l1'])

    const tokenSymbolUSDC = 'USDC'
    const zeroBytes32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
    const zeroAddress = '0x0000000000000000000000000000000000000000'
    const oneAddress = '0x0000000000000000000000000000000000000001'
    const mainnetAddresses = JSON.parse(fs.readFileSync(path.resolve(__dirname, './constants/mainnetAddresses.json'), 'utf-8'))
    let snapshotId
    let mockAxelarTokenCallerChildL1
    let mockAxelarTokenCallerChildL2
    let mockInterchainTokenServiceL1, mockInterchainTokenServiceL2
    let mockAxelarInterchainTokenCallerChildL1, mockAxelarInterchainTokenCallerChildL2
    const messageId = '0x0000000000000000000000000000000000000000000000000000000000000023'
    this.beforeAll(async() => {   
        [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector, royaltyCollector] = await ethers.getSigners()
        ticketBuyers = [acc2,acc3, acc4, acc5, acc6,  acc7]
        const _MockERC20 = await hre.ethers.getContractFactory('MockERC20');
        const _MockERC20WithFeeOnTransfer = await hre.ethers.getContractFactory('MockFeeOnTransfer');
        const _NFT = await hre.ethers.getContractFactory('EeseeNFT');
        const _royaltyEngine = await hre.ethers.getContractFactory('MockRoyaltyEngine');
        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");
        const _mockAxelarGasService = await hre.ethers.getContractFactory('MockAxelarGasService')
        const _mockAxelarGateway = await hre.ethers.getContractFactory('MockAxelarGateway')
        const _mockAxelarTokenCallerChild = await hre.ethers.getContractFactory('MockAxelarTokenCallerChild')
        const _mockAxelarInterchainTokenCallerChild = await hre.ethers.getContractFactory('MockAxelarInterchainTokenCallerChild')
        const _mockInterchainTokenService = await hre.ethers.getContractFactory('MockInterchainTokenService')
        
        accessManager =  await _eeseeAccessManager.deploy()
        await accessManager.deployed()

        // PAUSER_ROLE
        await accessManager.grantRole('0x65d7a28e3265b37a6474929f336521b332c1681b933f6cb9f3376673440d862a', acc9.address)

        royaltyEninge = await _royaltyEngine.deploy();
        await royaltyEninge.deployed()
        

        mockAxelarGatewayL1 = await _mockAxelarGateway.deploy()
        await mockAxelarGatewayL1.deployed()
        mockAxelarGatewayL2 = await _mockAxelarGateway.deploy()
        await mockAxelarGatewayL2.deployed()
        mockAxelarGasService = await _mockAxelarGasService.deploy(acc3.address)
        await mockAxelarGasService.deployed()

        mockUSDCL1 = await _MockERC20.deploy('20000000000000000000000000000')
        await mockUSDCL1.deployed()
        mockUSDCL2 = await _MockERC20.deploy('20000000000000000000000000000')
        await mockUSDCL2.deployed()

        // Deploy params: (string memory name, string memory symbol, uint8 decimals, uint256 cap, address tokenAddress, uint256 mintLimit)
        const deployTokenParamsL1 = abi.encode(
            ['string', 'string', 'uint8', 'uint256', 'address', 'uint256'],
            ['USD Coin', tokenSymbolUSDC, 6, 0, mockUSDCL1.address, 0]
        )
        await mockAxelarGatewayL1.deployToken(deployTokenParamsL1, zeroBytes32)
        await mockUSDCL1.transfer(mockAxelarGatewayL1.address, '10000000000000')

        const deployTokenParamsL2 = abi.encode(
            ['string', 'string', 'uint8', 'uint256', 'address', 'uint256'],
            ['USD Coin', tokenSymbolUSDC, 6, 0, mockUSDCL2.address, 0]
        )
        await mockAxelarGatewayL2.deployToken(deployTokenParamsL2, zeroBytes32)
        await mockUSDCL2.transfer(mockAxelarGatewayL2.address, '10000000000000')

        mockAxelarTokenCallerChildL1 = await _mockAxelarTokenCallerChild.deploy(mockAxelarGatewayL1.address, mockAxelarGasService.address)
        await mockAxelarTokenCallerChildL1.deployed() 
        await mockUSDCL1.approve(mockAxelarTokenCallerChildL1.address, '10000000000000')

        mockAxelarTokenCallerChildL2 = await _mockAxelarTokenCallerChild.deploy(mockAxelarGatewayL2.address, mockAxelarGasService.address)
        await mockAxelarTokenCallerChildL2.deployed() 
        await mockUSDCL2.approve(mockAxelarTokenCallerChildL2.address, '10000000000000')

        mockInterchainTokenServiceL1 = await _mockInterchainTokenService.deploy(mockAxelarGatewayL1.address, mockAxelarGasService.address, "l1")
        await mockInterchainTokenServiceL1.deployed()

        mockInterchainTokenServiceL2 = await _mockInterchainTokenService.deploy(mockAxelarGatewayL2.address, mockAxelarGasService.address, "l2")
        await mockInterchainTokenServiceL2.deployed()

        mockAxelarInterchainTokenCallerChildL1 = await _mockAxelarInterchainTokenCallerChild.deploy(mockInterchainTokenServiceL1.address)
        await mockAxelarInterchainTokenCallerChildL1.deployed()
        await mockUSDCL1.approve(mockAxelarInterchainTokenCallerChildL1.address, '10000000000000')

        mockAxelarInterchainTokenCallerChildL2 = await _mockAxelarInterchainTokenCallerChild.deploy(mockInterchainTokenServiceL2.address)
        await mockAxelarInterchainTokenCallerChildL2.deployed()
        await mockUSDCL2.approve(mockAxelarInterchainTokenCallerChildL2.address, '10000000000000')
    })

    it('sendAxelarMessageWithToken', async () => {
        snapshotId = await network.provider.send('evm_snapshot');
        const dst = {
            chainSelector: chainSelectorL2,
            _address: mockAxelarTokenCallerChildL2.address
        }
        await expect(mockAxelarTokenCallerChildL1.sendAxelarMessageWithToken(signer.address, dst, tokenSymbolUSDC, 0, "0x", {value: 50000}))
        .to.be.revertedWithCustomError(mockAxelarTokenCallerChildL1, "InvalidAmount")
        await expect(mockAxelarTokenCallerChildL1.sendAxelarMessageWithToken(signer.address, dst, "NULL", 100, "0x", {value: 50000}))
        .to.be.revertedWithCustomError(mockAxelarTokenCallerChildL1, "InvalidToken")


        await expect(mockAxelarTokenCallerChildL1.sendAxelarMessageWithToken(signer.address, dst, tokenSymbolUSDC, 100, "0x", {value: 50000}))
        .to.emit(mockAxelarTokenCallerChildL1, "CrosschainSend")
        .withArgs(
            await mockAxelarTokenCallerChildL1.AXELAR_TOKEN_SERVICE_CALLER_IDENTIFIER(),
            ((i) => compareObjects(convertArrayToObject(i), dst)),
            abi.encode(['address', 'uint256', 'uint256'], [mockUSDCL1.address, 100, 50000])
        )

        await mockUSDCL1.transfer(mockAxelarTokenCallerChildL1.address, 200)
        await expect(mockAxelarTokenCallerChildL1.sendAxelarMessageWithToken(mockAxelarTokenCallerChildL1.address, dst, tokenSymbolUSDC, 200, "0x", {value: 50000}))
        .to.emit(mockAxelarTokenCallerChildL1, "CrosschainSend")
        .withArgs(
            await mockAxelarTokenCallerChildL1.AXELAR_TOKEN_SERVICE_CALLER_IDENTIFIER(),
            ((i) => compareObjects(convertArrayToObject(i), dst)),
            abi.encode(['address', 'uint256', 'uint256'], [mockUSDCL1.address, 200, 50000])
        )
    })

    it('receivesAxelarMessageWithToken', async () => {
        const src = {
            chainSelector: chainSelectorL1,
            _address: mockAxelarTokenCallerChildL1.address
        }

        const payload = "0x"
        const payloadHash = keccak256(payload)
        const commandId = keccak256(abi.encode(['uint256'],['1']))

        let approveContractCallWithMintParams = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'string', 'uint256', 'bytes32', 'uint256'],
            [abi.decode(['string'], chainSelectorL1)[0], mockAxelarTokenCallerChildL1.address.toLowerCase(), mockAxelarTokenCallerChildL2.address, payloadHash, tokenSymbolUSDC, 20, commandId, 1]
        )
        await expect(mockAxelarTokenCallerChildL2.executeWithToken(
            commandId,
            abi.decode(['string'], chainSelectorL1)[0],
            mockAxelarTokenCallerChildL1.address.toLowerCase(),
            payload,
            tokenSymbolUSDC,
            20,
            {gasLimit: 30000000}
        )).to.be.revertedWithCustomError(mockAxelarTokenCallerChildL2, "NotApprovedByGateway")

        await mockAxelarGatewayL2.approveContractCallWithMint(
            approveContractCallWithMintParams,
            commandId
        )

        await expect(mockAxelarTokenCallerChildL2.executeWithToken(
            commandId,
            abi.decode(['string'], chainSelectorL1)[0],
            mockAxelarTokenCallerChildL1.address.toLowerCase(),
            payload,
            tokenSymbolUSDC,
            20,
            {gasLimit: 30000000}
        )).to.emit(mockAxelarTokenCallerChildL2, 'CrosschainReceive')
        .withArgs(
            await mockAxelarTokenCallerChildL2.AXELAR_TOKEN_SERVICE_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), src)),
            abi.encode(['address', 'uint256'], [mockUSDCL2.address, 20])
        )

        await network.provider.send("evm_revert", [snapshotId]);
    })

    it('sendAxelarMessageWithInterchainToken', async () => {
        const dst = {
            chainSelector: chainSelectorL2,
            _address: mockAxelarInterchainTokenCallerChildL2.address
        }

        await expect(mockAxelarInterchainTokenCallerChildL1.sendAxelarMessageWithInterchainToken(signer.address, dst, zeroBytes32, 0, "0x", {value: 50000}))
        .to.be.revertedWithCustomError(mockAxelarInterchainTokenCallerChildL1, "InvalidAmount");
        await expect(mockAxelarInterchainTokenCallerChildL1.sendAxelarMessageWithInterchainToken(signer.address, dst, zeroBytes32, 100, "0x", {value: 50000}))
        .to.be.revertedWithCustomError(mockAxelarInterchainTokenCallerChildL1, "InvalidToken");
        let tokenAmount = 100
        let gas = 5000
        let tokenId = (mockUSDCL1.address + "000000000000000000000000").toLowerCase()
        await mockUSDCL1.transfer(mockAxelarInterchainTokenCallerChildL1.address, tokenAmount)
        await expect(mockAxelarInterchainTokenCallerChildL1.sendAxelarMessageWithInterchainToken(mockAxelarInterchainTokenCallerChildL1.address, dst, tokenId, tokenAmount, "0x", {value: gas}))
        .to.emit(mockAxelarInterchainTokenCallerChildL1, "CrosschainSend")
        .withArgs(
            await mockAxelarInterchainTokenCallerChildL1.AXELAR_INTERCHAIN_TOKEN_SERVICE_CALLER_IDENTIFIER(),
            ((i) => compareObjects(convertArrayToObject(i), dst)),
            abi.encode(['address', 'uint256', 'uint256'], [mockUSDCL1.address, tokenAmount, gas])
        )

        tokenAmount = 200
        await mockUSDCL1.approve(mockAxelarInterchainTokenCallerChildL1.address, tokenAmount)
        await expect(mockAxelarInterchainTokenCallerChildL1.sendAxelarMessageWithInterchainToken(signer.address, dst, tokenId, tokenAmount, "0x", {value: gas}))
        .to.emit(mockAxelarInterchainTokenCallerChildL1, "CrosschainSend")
        .withArgs(
            await mockAxelarInterchainTokenCallerChildL1.AXELAR_INTERCHAIN_TOKEN_SERVICE_CALLER_IDENTIFIER(),
            ((i) => compareObjects(convertArrayToObject(i), dst)),
            abi.encode(['address', 'uint256', 'uint256'], [mockUSDCL1.address, tokenAmount, gas])
        )
    })

    it('receivesAxelarMessageWithInterchainToken', async () => {
        const src = {
            chainSelector: chainSelectorL1,
            _address: mockAxelarInterchainTokenCallerChildL1.address
        }
        const commandId = keccak256(abi.encode(['uint256'],['1']))
        const tokenId = (mockUSDCL2.address + "000000000000000000000000").toLowerCase()
        const amount = 20
        const payload = abi.encode(
            ['uint256', 'bytes32', 'bytes', 'bytes', 'uint256', 'bytes'], 
            [0, tokenId, mockAxelarInterchainTokenCallerChildL1.address, mockAxelarInterchainTokenCallerChildL2.address, amount, "0x20"]);

        await mockUSDCL2.transfer(mockInterchainTokenServiceL2.address, 200)
        await expect(mockInterchainTokenServiceL2.execute(
            commandId, 
            abi.decode(['string'], chainSelectorL1)[0],
            mockAxelarTokenCallerChildL1.address.toLowerCase(), 
            payload
        ))
        .to.emit(mockAxelarInterchainTokenCallerChildL2, 'CrosschainReceive')
        .withArgs(
            await mockAxelarInterchainTokenCallerChildL2.AXELAR_INTERCHAIN_TOKEN_SERVICE_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), src)),
            abi.encode(['address', 'uint256'], [mockUSDCL2.address, amount])
        );
    })

    it('cannot deploy AxelarInterchainTokenCaller with zero address interchainTokenService', async () => {
        const _mockAxelarInterchainTokenCallerChild = await hre.ethers.getContractFactory('MockAxelarInterchainTokenCallerChild');
        await expect(_mockAxelarInterchainTokenCallerChild.deploy(zeroAddress))
        .to.be.revertedWithCustomError(_mockAxelarInterchainTokenCallerChild, "InvalidInterchainTokenService");
    })
})
