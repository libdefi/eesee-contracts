  const fs = require('fs')
    const path = require('path')
  const { anyValue } = require('@nomicfoundation/hardhat-chai-matchers/withArgs');
  const { expect } = require('chai');
  const { ethers, network } = require('hardhat');
  const { getContractAddress } = require('@ethersproject/address')
  const { keccak256 } = require('@ethersproject/keccak256')
  const {convertArrayToObject, compareObjects} = require('./utils/compareObjects');
  const compareErrors = require('./utils/compareErrors');

  ((network.name == 'multipleNetworks') ? describe : describe.skip)('Cross Chain', function () {
    let ERC20;
    let NFT;
    let signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector;
    let royaltyEninge;
    let eeseeAssetSpoke
    let accessManagerL1
    let accessManagerL2
    let eeseeAssetHub
    let mockCCIPRouterL1
    let mockCCIPRouterL2
    let mockOnRampL1
    let mockOnRampL2
    let WETHL1
    let WETHL2
    let mockOffRampL1
    let mockOffRampL2
    const abi = ethers.utils.defaultAbiCoder
    const chainSelectorL2 = abi.encode(['uint64'], [2])
    const chainSelectorL1 = abi.encode(['uint64'], [1])
    const tokenSymbolUSDC = 'USDC'

    const zeroBytes32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
    const zeroAddress = '0x0000000000000000000000000000000000000000'
    const oneAddress = '0x0000000000000000000000000000000000000001'
    let snapshotId

    async function changeNetwork(name){
        await hre.changeNetwork(name);
        if(signer == undefined){
            [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector] = await ethers.getSigners();
        }else{
            let [_signer, _acc2, _acc3, _acc4, _acc5, _acc6, _acc7, _acc8, _acc9, _feeCollector] = await ethers.getSigners();
            if(
                _signer.address != signer.address ||
                _acc2.address != acc2.address ||
                _acc3.address != acc3.address ||
                _acc4.address != acc4.address ||
                _acc5.address != acc5.address ||
                _acc6.address != acc6.address ||
                _acc7.address != acc7.address ||
                _acc8.address != acc8.address ||
                _acc9.address != acc9.address ||
                _feeCollector.address != feeCollector.address
            ) throw Error("Accounts for networks not match");
            [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector] = [_signer, _acc2, _acc3, _acc4, _acc5, _acc6, _acc7, _acc8, _acc9, _feeCollector]
        }
    }

    this.beforeAll(async() => {   
        await changeNetwork('testnet1');
        const _NFT = await hre.ethers.getContractFactory('EeseeNFT');
        const _royaltyEngine = await hre.ethers.getContractFactory('MockRoyaltyEngine');
        const _eeseeAssetSpoke = await hre.ethers.getContractFactory('EeseeAssetSpokeCCIP')
        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");
        const _mockCCIPRouter = await hre.ethers.getContractFactory('CCIPRouter')
        const _onRamp = await hre.ethers.getContractFactory('OnRamp')
        const _offRamp = await hre.ethers.getContractFactory('OffRamp')
        const _mockWeth = await hre.ethers.getContractFactory('WETH9')
        WETHL1 = await _mockWeth.deploy()
        await WETHL1.deployed()

        accessManagerL1 =  await _eeseeAccessManager.deploy()
        await accessManagerL1.deployed()

        // TRANSMITTER_ROLE
        await accessManagerL1.grantRole('0xd6a0f92c822ccba0fa91b30f08f085e68bc8eb3bb140aa16f8dc33ea47eb6cf2', acc9.address)

        royaltyEninge = await _royaltyEngine.deploy();
        await royaltyEninge.deployed()

        NFT = await _NFT.deploy()
        await NFT.initialize({
            name: 'APES',
            symbol:'bayc',
            baseURI: '/',
            revealedURI: '',
            contractURI:'/',
            royaltyReceiver: zeroAddress,
            royaltyFeeNumerator: 0
        },10, signer.address)

        await NFT.deployed()

        let transactionCount = await signer.getTransactionCount()
        const futureRouterL1 = getContractAddress({
            from: signer.address,
            nonce: transactionCount + 2
        })


        mockOffRampL1 = await _offRamp.deploy(accessManagerL1.address, futureRouterL1)
        await mockOffRampL1.deployed()
        mockOnRampL1 = await _onRamp.deploy({
            chainSelector: abi.decode(['uint64'], chainSelectorL1)[0],
            destChainSelector: abi.decode(['uint64'], chainSelectorL2)[0]
        }, {
            router: futureRouterL1,
            maxDataBytes: '30000',
            maxPerMsgGasLimit: '2000000',
        })
        await mockOnRampL1.deployed()

        await changeNetwork('testnet2');
            const _eeseeAssetHub = await hre.ethers.getContractFactory('EeseeAssetHubCCIP')
            const _mockCCIPRouterL2 = await hre.ethers.getContractFactory('CCIPRouter')
            const _offRampL2 = await hre.ethers.getContractFactory('OffRamp')
            const _onRampL2 = await hre.ethers.getContractFactory('OnRamp')
            const _eeseeAccessManagerL2 = await hre.ethers.getContractFactory("EeseeAccessManager");
            const _mockWethL2 = await hre.ethers.getContractFactory('WETH9')
            WETHL2 = await _mockWethL2.deploy()
            await WETHL2.deployed()

            accessManagerL2 =  await _eeseeAccessManagerL2.deploy()
            await accessManagerL2.deployed()

            // TRANSMITTER_ROLE
            await accessManagerL2.grantRole('0xd6a0f92c822ccba0fa91b30f08f085e68bc8eb3bb140aa16f8dc33ea47eb6cf2', acc9.address)

            transactionCount = await signer.getTransactionCount()
            const futureRouterL2 = getContractAddress({
                from: signer.address,
                nonce: transactionCount + 2
            })
            mockOffRampL2 = await _offRampL2.deploy(accessManagerL2.address, futureRouterL2)
            await mockOffRampL2.deployed()

            mockOnRampL2 = await _onRampL2.deploy({
                chainSelector: abi.decode(['uint64'], chainSelectorL2)[0],
                destChainSelector: abi.decode(['uint64'], chainSelectorL1)[0]
            }, {
                router: futureRouterL2,
                maxDataBytes: '30000',
                maxPerMsgGasLimit: '2000000',
            })
            await mockOnRampL2.deployed()

            mockCCIPRouterL2 = await _mockCCIPRouterL2.deploy(
                [{destChainSelector:abi.decode(['uint64'], chainSelectorL1)[0], onRamp:mockOnRampL2.address}], 
                [{sourceChainSelector:abi.decode(['uint64'], chainSelectorL1)[0], offRamp:mockOffRampL2.address}],
                WETHL2.address
            )
            await mockCCIPRouterL2.deployed()

            eeseeAssetHub = await _eeseeAssetHub.deploy(
                'img/',
                'prefix#',
                '_description_',

                mockCCIPRouterL2.address,
                zeroAddress
            )
            await eeseeAssetHub.deployed()

        await changeNetwork('testnet1');

        mockCCIPRouterL1 = await _mockCCIPRouter.deploy(
            [{destChainSelector:abi.decode(['uint64'], chainSelectorL2)[0], onRamp:mockOnRampL1.address}], 
            [{sourceChainSelector:abi.decode(['uint64'], chainSelectorL2)[0], offRamp:mockOffRampL1.address}],
            WETHL1.address
        )
        await mockCCIPRouterL1.deployed()
            
        eeseeAssetSpoke = await _eeseeAssetSpoke.deploy(
            accessManagerL1.address,

            chainSelectorL1,
            {chainSelector: chainSelectorL2, _address: eeseeAssetHub.address},

            royaltyEninge.address,
            mockCCIPRouterL1.address,
            zeroAddress
        )
        await eeseeAssetSpoke.deployed()
    })

    let tokenIDL2
    let assetHash

    it('wrap', async () => {
        gasLimit = 1000000
        gasPaid = gasLimit*10
        const sourceL1 = {
            chainSelector: chainSelectorL1,
            _address: eeseeAssetSpoke.address
        }

        const asset = {
            token: NFT.address,
            tokenID: 1,
            amount: 1,
            assetType: 0,
            data: "0x"
        }

        assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))
        tokenIDL2 = await eeseeAssetSpoke["getTokenIdL2((address,uint256,uint256,uint8,bytes))"](asset)
        const transferNFTEncodedData = eeseeAssetHub.interface.encodeFunctionData('safeTransferFrom', [eeseeAssetHub.address, acc8.address, tokenIDL2, asset.amount, "0x"])
        await NFT.approve(eeseeAssetSpoke.address, 1)

        const royalties = await royaltyEninge.getRoyalty(asset.token, asset.tokenID, 10000)
        const assetData = abi.encode(
            ['address[]', 'uint256[]'],
            [royalties.recipients, royalties.amounts]
        )
        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [[assetHash], [asset.amount], [assetData], [[eeseeAssetHub.address, transferNFTEncodedData]], acc2.address]
        )
        const ccipObj = {
            sourceChainSelector: abi.decode(['uint64'], chainSelectorL1)[0],
            sender: eeseeAssetSpoke.address,
            receiver: eeseeAssetHub.address,
            sequenceNumber: 1,
            gasLimit: gasLimit,
            strict: false,
            nonce: 1,
            feeToken: WETHL1.address,
            feeTokenAmount: gasPaid,
            data: payload,
            tokenAmounts: [],
            sourceTokenData: [],
            messageId: zeroBytes32
        }

        await expect(eeseeAssetSpoke.wrap(
            [asset],
            [{target: eeseeAssetHub.address, callData: transferNFTEncodedData}],
            acc2.address,
            abi.encode(['uint256'], [gasLimit]),
            {value: gasPaid}
        ))
        .to.emit(eeseeAssetSpoke, 'CrosschainSend').withArgs(
            await eeseeAssetSpoke.CCIP_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL2, _address: eeseeAssetHub.address})), 
            anyValue
        )
        .to.emit(mockOnRampL1, 'CCIPSendRequested').withArgs(
            ((i) => compareObjects({...convertArrayToObject(i), messageId:zeroBytes32}, ccipObj))
        )


        // ================ L2 ================

        await changeNetwork('testnet2');
        
        await expect(mockOffRampL2.connect(acc9).transmit(ccipObj))
            .to.emit(mockCCIPRouterL2, "MessageExecuted").withArgs(ccipObj.messageId, ccipObj.sourceChainSelector, mockOffRampL2.address, anyValue)
            .to.emit(eeseeAssetHub, 'CrosschainReceive')
            .withArgs(
                await eeseeAssetSpoke.CCIP_CALLER_IDENTIFIER(), 
                ((i) => compareObjects(convertArrayToObject(i), sourceL1)),
                abi.encode(['bytes32', 'tuple[](address, uint256)'], [ccipObj.messageId, []])
            )
    })

    it('unwrap', async () => {
        const sourceL2 = {
            chainSelector: chainSelectorL2,
            _address: eeseeAssetHub.address
        }

        gasLimit = 1000000
        gasPaid = gasLimit*10

        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'address'],
            [[assetHash], [1], acc2.address]
        )
        const ccipObj = {
            sourceChainSelector: abi.decode(['uint64'], chainSelectorL2)[0],
            sender: eeseeAssetHub.address,
            receiver: eeseeAssetSpoke.address,
            sequenceNumber: 1,
            gasLimit: gasLimit,
            strict: false,
            nonce: 1,
            feeToken: WETHL2.address,
            feeTokenAmount: gasPaid,
            data: payload,
            tokenAmounts: [],
            sourceTokenData: [],
            messageId: zeroBytes32
        }

        await expect(eeseeAssetHub.connect(acc8).unwrap(
            [tokenIDL2], 
            [1], 
            acc2.address, 
            abi.encode(['uint256'], [gasLimit]), 
            {value: gasPaid}
        ))
        .to.emit(eeseeAssetHub, 'CrosschainSend').withArgs(
            await eeseeAssetHub.CCIP_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL1, _address: eeseeAssetSpoke.address})), 
            anyValue
        )
        .to.emit(mockOnRampL2, 'CCIPSendRequested').withArgs(
            ((i) => compareObjects({...convertArrayToObject(i), messageId:zeroBytes32}, ccipObj))
        )

        // ================ L1 ================

        await changeNetwork('testnet1');
        
        await expect(mockOffRampL1.connect(acc9).transmit(ccipObj))
            .to.emit(mockCCIPRouterL1, "MessageExecuted").withArgs(ccipObj.messageId, ccipObj.sourceChainSelector, mockOffRampL1.address, anyValue)
            .to.emit(eeseeAssetSpoke, 'CrosschainReceive')
            .withArgs(
                await eeseeAssetSpoke.CCIP_CALLER_IDENTIFIER(), 
                ((i) => compareObjects(convertArrayToObject(i), sourceL2)),
                abi.encode(['bytes32', 'tuple[](address, uint256)'], [ccipObj.messageId, []])
            )
    })

})