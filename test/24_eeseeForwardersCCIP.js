const {
    time,
    loadFixture,
  } = require('@nomicfoundation/hardhat-network-helpers');
  const fs = require('fs')
    const path = require('path')
  const { anyValue } = require('@nomicfoundation/hardhat-chai-matchers/withArgs');
  const { expect } = require('chai');
  const { ethers, network } = require('hardhat');
  const assert = require('assert');
  const { StandardMerkleTree } = require('@openzeppelin/merkle-tree');
  const { getContractAddress } = require('@ethersproject/address')
  const { keccak256 } = require('@ethersproject/keccak256')
  const createPermit = require('./utils/createPermit')
  const { BigNumber } = ethers
  const {convertArrayToObject, compareObjects} = require('./utils/compareObjects');
  ((network.name != 'multipleNetworks') ? describe: describe.skip)('ESE forwarders', function () {
    let ERC20;
    let NFT;
    let signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector;
    let royaltyEninge;
    let eeseeAssetHub
    let mockAxelarGasService
    let mockCCIPRouterL1
    let mockCCIPRouterL2
    let onRamp
    let offRamp
    let onRampImplementation
    const abi = ethers.utils.defaultAbiCoder
    const chainSelectorL2 = abi.encode(['uint64'], [2])
    const chainSelectorL1 = abi.encode(['uint64'], [1])
    const tokenSymbolUSDC = 'USDC'
    const zeroBytes32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
    const zeroAddress = '0x0000000000000000000000000000000000000000'
    const oneAddress = '0x0000000000000000000000000000000000000001'
    const mainnetAddresses = JSON.parse(fs.readFileSync(path.resolve(__dirname, './constants/mainnetAddresses.json'), 'utf-8'))
    let snapshotId
    let onRampInterface
    const messageId = '0x0000000000000000000000000000000000000000000000000000000000000023'
    this.beforeAll(async() => {   
        [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector, royaltyCollector] = await ethers.getSigners()
        const _AssetTransfer = await hre.ethers.getContractFactory('AssetTransfer');
        const assetTransfer = await _AssetTransfer.deploy()
        await assetTransfer.deployed()
        const _MockERC20 = await hre.ethers.getContractFactory('MockERC20');
        const _NFT = await hre.ethers.getContractFactory('EeseeNFT');
        const _royaltyEngine = await hre.ethers.getContractFactory('MockRoyaltyEngine');
        const _eeseeOnRampImplementation = await hre.ethers.getContractFactory('EeseeOnRampImplementationCCIP')
        const _eeseeOnRamp = await hre.ethers.getContractFactory('EeseeOnRampProxy')
        const _eeseeOffRamp = await hre.ethers.getContractFactory('EeseeOffRampCCIP')
        const _mockCCIPRouter = await hre.ethers.getContractFactory('MockCCIPRouter')
        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");

        accessManager = await _eeseeAccessManager.deploy();
        await accessManager.deployed()
        // PAUSER_ROLE
        await accessManager.grantRole('0x65d7a28e3265b37a6474929f336521b332c1681b933f6cb9f3376673440d862a', acc9.address)


        ERC20 = await _MockERC20.deploy('20000000000000000000000000000')
        await ERC20.deployed()

        royaltyEninge = await _royaltyEngine.deploy();
        await royaltyEninge.deployed()

        NFT = await _NFT.deploy()
        await NFT.initialize({
            name: 'APES',
            symbol:'bayc',
            baseURI: '/',
            revealedURI: '',
            contractURI:'/',
            royaltyReceiver: zeroAddress,
            royaltyFeeNumerator: 0
        },10, signer.address)

        await NFT.deployed()
        

        mockCCIPRouterL1 = await _mockCCIPRouter.deploy()
        await mockCCIPRouterL1.deployed()
        mockCCIPRouterL2 = await _mockCCIPRouter.deploy()
        await mockCCIPRouterL2.deployed()
        
        offRamp = await _eeseeOffRamp.deploy(ERC20.address, feeCollector.address) 
        await offRamp.deployed()

        onRampImplementation = await _eeseeOnRampImplementation.deploy(
            ERC20.address, 
            mockCCIPRouterL1.address
        )
        await onRampImplementation.deployed()

        onRamp = await _eeseeOnRamp.deploy(
            onRampImplementation.address,
            abi.encode(['uint256', 'tuple(bytes chainSelector, address _address)'], [200000, {chainSelector: chainSelectorL2, _address: offRamp.address}]),
            accessManager.address
        )
        await onRamp.deployed()

        onRampInterface = await ethers.getContractAt("EeseeOnRampImplementationCCIP", onRamp.address);
    })

    it('forwardsL1', async () => {
        await expect(onRamp.forward("0x")).to.be.revertedWithCustomError(onRampImplementation, "InsufficientBalance")

        await ERC20.transfer(onRamp.address, 100)
        assert.equal(await ERC20.balanceOf(onRamp.address), 100, "Incorrect balance")
        await expect(onRamp.forward("0x")).to.be.revertedWithCustomError(onRampImplementation, "InsufficientValue")

        var gas = 10000
        await expect(onRamp.forward("0x", {value: gas}))
        .to.emit(onRampInterface, "CrosschainSend").withArgs(
            await onRampImplementation.CCIP_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL2, _address: offRamp.address})),
            abi.encode(['bytes32', 'tuple[](address token, uint256 amount)', 'uint256'], [messageId, [{token: ERC20.address, amount: 100}], gas])
        )
        .and.to.emit(onRampInterface, "Forward").withArgs(
            ((i) => (compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL2, _address: offRamp.address}))),
            100
        )
        assert.equal(await ERC20.balanceOf(onRamp.address), 0, "Incorrect balance")
    })

    it('forwardsL2', async () => {
        await expect(offRamp.ccipReceive({
            messageId: messageId,
            sourceChainSelector: chainSelectorL1,
            sender: onRamp.address,
            data: "0x",
            destTokenAmounts: []
        })).to.emit(offRamp, "Forward").withArgs(100)
        assert.equal(await ERC20.balanceOf(feeCollector.address), 100, "Balance is correct")

        await expect(offRamp.ccipReceive({
            messageId: messageId,
            sourceChainSelector: chainSelectorL1,
            sender: onRamp.address,
            data: "0x",
            destTokenAmounts: []
        })).to.not.emit(offRamp, "Forward")
        assert.equal(await ERC20.balanceOf(feeCollector.address), 100, "Balance is correct")
    })

    it('sends back leftover eth', async () => {
        await ERC20.transfer(onRamp.address, 100)
        assert.equal(await ERC20.balanceOf(onRamp.address), 100, "Incorrect balance")

        const balanceBefore = await ethers.provider.getBalance(signer.address); 
        const tx = onRamp.forward("0x", {value: 15000}) 
        await expect(tx)
        .to.emit(onRampInterface, "CrosschainSend").withArgs(
            await onRampImplementation.CCIP_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL2, _address: offRamp.address})),
            abi.encode(['bytes32', 'tuple[](address token, uint256 amount)', 'uint256'], [messageId, [{token: ERC20.address, amount: 100}], 10000])
        )        
        .and.to.emit(onRampInterface, "Forward").withArgs(
            ((i) => (compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL2, _address: offRamp.address}))),
            100
        )
        const _tx = (await (await tx).wait())
        const balanceAfter = await ethers.provider.getBalance(signer.address); 
        assert.equal(
            "10000", 
            balanceBefore.sub(_tx.effectiveGasPrice.mul(_tx.cumulativeGasUsed)).sub(balanceAfter).toString(),
            "Incorrect ETH change"
        )
        assert.equal(await ERC20.balanceOf(onRamp.address), 0, "Incorrect balance")
    })

    it('reverts constructor', async () => {
        const _eeseeOnRamp = await hre.ethers.getContractFactory('EeseeOnRampImplementationCCIP')
        await expect(_eeseeOnRamp.deploy(
            zeroAddress,
            mockCCIPRouterL1.address,
        )).to.be.revertedWithCustomError(_eeseeOnRamp, "InvalidESE")

        await expect(_eeseeOnRamp.deploy(
            ERC20.address,
            zeroAddress,
        )).to.be.revertedWithCustomError(_eeseeOnRamp, "InvalidRouter")

        const _eeseeOffRamp = await hre.ethers.getContractFactory('EeseeOffRampCCIP')
        await expect(_eeseeOffRamp.deploy(
            zeroAddress,
            oneAddress,
        )).to.be.revertedWithCustomError(_eeseeOffRamp, "InvalidConstructor")

        await expect(_eeseeOffRamp.deploy(
            oneAddress,
            zeroAddress,
        )).to.be.revertedWithCustomError(_eeseeOffRamp, "InvalidConstructor")
    })

    it('pausable', async () => {
        await expect(onRamp.pause()).to.be.revertedWithCustomError(onRamp, "CallerNotAuthorized")
        await expect(onRamp.connect(acc9).pause()).to.emit(onRamp, "Paused").withArgs(acc9.address)

        await expect(onRamp.forward("0x")).to.be.revertedWith("Pausable: paused")

        await expect(onRamp.unpause()).to.be.revertedWithCustomError(onRamp, "CallerNotAuthorized")
        await expect(onRamp.connect(acc9).unpause()).to.emit(onRamp, "Unpaused").withArgs(acc9.address)

        await expect(onRamp.forward("0x")).to.be.revertedWithCustomError(onRampImplementation, "InsufficientBalance")//Other error
    })

    it('updates onRampImplementation', async () => {
        const _eeseeOnRampImplementation = await hre.ethers.getContractFactory('EeseeOnRampImplementationCCIP')
        const prevImpl = onRampImplementation.address
        onRampImplementation = await _eeseeOnRampImplementation.deploy(
            ERC20.address, 
            mockCCIPRouterL1.address
        )
        await onRampImplementation.deployed()

        const initData = abi.encode(['uint256', 'tuple(bytes chainSelector, address _address)'], [200000, {chainSelector: chainSelectorL2, _address: offRamp.address}])
        await expect(onRamp.connect(acc9).updateOnRampImplementation(
            onRampImplementation.address, 
            initData
        )).to.be.revertedWithCustomError(onRamp, "CallerNotAuthorized")
        await expect(onRamp.updateOnRampImplementation(
            zeroAddress, 
            initData
        )).to.be.revertedWithCustomError(onRamp, "InvalidOnRampImplementation")
        await expect(onRamp.updateOnRampImplementation(
            onRampImplementation.address, 
            initData
        )).to.emit(onRamp, "UpdateOnRampImplementation").withArgs(prevImpl, onRampImplementation.address, initData)

        let badInitData = abi.encode(['uint256', 'tuple(bytes chainSelector, address _address)'], [0, {chainSelector: chainSelectorL2, _address: offRamp.address}])
        await expect(onRamp.updateOnRampImplementation(
            onRampImplementation.address, 
            badInitData
        )).to.be.revertedWithCustomError(onRampImplementation, "InvalidGasLimit")

        badInitData = abi.encode(['uint256', 'tuple(bytes chainSelector, address _address)'], [200000, {chainSelector: chainSelectorL2, _address: zeroAddress}])
        await expect(onRamp.updateOnRampImplementation(
            onRampImplementation.address, 
            badInitData
        )).to.be.revertedWithCustomError(onRampImplementation, "InvalidOffRamp")

        const _mockRewriteImpl = await hre.ethers.getContractFactory('MockRewriteImpl')
        const mockRewriteImpl = await _mockRewriteImpl.deploy()
        await mockRewriteImpl.deployed()

        await expect(onRamp.updateOnRampImplementation(
            mockRewriteImpl.address, 
            "0x"
        )).to.be.revertedWithCustomError(onRamp, "ChangedOnRampImplementation")

        const _mockRevert = await hre.ethers.getContractFactory('MockRevert')
        const mockRevert = await _mockRevert.deploy()
        await mockRevert.deployed()

        await expect(onRamp.updateOnRampImplementation(
            mockRevert.address, 
            "0x"
        )).to.be.revertedWithoutReason()

        const _mockRevertWithMessage = await hre.ethers.getContractFactory('MockRevertWithMessage')
        const mockRevertWithMessage = await _mockRevertWithMessage.deploy()
        await mockRevertWithMessage.deployed()

        await expect(onRamp.updateOnRampImplementation(
            mockRevertWithMessage.address, 
            "0x"
        )).to.be.revertedWith("goodbye world")
    })

    it('reverts ccip message', async () => {
        const any2EVMMessage = {
            messageId,
            sourceChainSelector: abi.decode(["uint64"], chainSelectorL2)[0],
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [zeroAddress]),
            data: "0x",
            destTokenAmounts: []
        }
        const _eeseeOnRampImplementation = await hre.ethers.getContractFactory('EeseeOnRampImplementationCCIP')
        onRampImplementation = await _eeseeOnRampImplementation.deploy(
            ERC20.address, 
            signer.address
        )
        await onRampImplementation.deployed()
        await expect(onRampImplementation.ccipReceive(any2EVMMessage)).to.be.revertedWithCustomError(onRampImplementation, "CantReceiveMessages")
    })

    it('supports interface', async () => {
        assert.equal(await onRampImplementation.supportsInterface('0x01ffc9a7'), true, 'onRamp supports ERC165')

        assert.equal(await offRamp.supportsInterface('0x01ffc9a7'), true, 'offRamp supports ERC165')
        assert.equal(await offRamp.supportsInterface('0x85572ffb'), true, 'offRamp supports IAny2EVMMessageReceiver')
        assert.equal(await offRamp.supportsInterface('0x00000001'), false, 'offRamp supports 0x00000001')
    })

    it('test mock ccip caller', async () => {
        const _mockCCIPCaller = await hre.ethers.getContractFactory('MockCCIPCaller');
        const mockCCIPCaller = await _mockCCIPCaller.deploy(mockCCIPRouterL1.address);
        await mockCCIPCaller.deployed();

        assert.equal(await mockCCIPCaller.supportsInterface('0x01ffc9a7'), true, 'CCIPCaller supports ERC165');

        const amount = 100;
        await ERC20.approve(mockCCIPCaller.address, amount);
        await expect(mockCCIPCaller.transferTokens(ERC20.address, signer.address, amount)).to.be.not.reverted;
    })
})