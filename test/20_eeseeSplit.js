  const { expect } = require('chai');
  const { ethers, network } = require('hardhat');

  const assert = require("assert");
  const fs = require('fs')
  const path = require('path');
  ((network.name != 'multipleNetworks') ? describe : describe.skip)('Cross Chain', function () {
    let signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector;
    let eeseeSplit;
    let mockProxy;
   
    const zeroAddress = '0x0000000000000000000000000000000000000000'
    const oneAddress = '0x0000000000000000000000000000000000000001'
    const twoAddress = '0x0000000000000000000000000000000000000002'
    this.beforeAll(async() => {   
        [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector, royaltyCollector] = await ethers.getSigners()
        ticketBuyers = [acc2,acc3, acc4, acc5, acc6,  acc7]
        const _EeseeSplit = await hre.ethers.getContractFactory('EeseeSplit');
        eeseeSplit = await _EeseeSplit.deploy()
        await eeseeSplit.deployed()

        await expect(eeseeSplit.initializeWithRoyaltyShare(
            10000,
            zeroAddress,
            "",
            [],
            [oneAddress],
            [10]
        )).to.be.revertedWith("Initializable: contract is already initialized")// Implementation can't be initialized

        const _MockProxy = await hre.ethers.getContractFactory('MockProxy');
        mockProxy = await _MockProxy.deploy(eeseeSplit.address)
        await mockProxy.deployed()

        const abi = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../artifacts/contracts/periphery/EeseeSplit.sol/EeseeSplit.json'), "utf-8"))

        mockProxy = new ethers.Contract(
            mockProxy.address,
            abi.abi,
            ethers.provider
        );
    })

    it('initialize', async () => {
        await expect(mockProxy.connect(signer).initializeWithRoyaltyShare(
            10001,
            zeroAddress,
            "",
            [],
            [oneAddress, twoAddress],
            [10, 30]
        )).to.be.revertedWithCustomError(mockProxy, "InvalidRoyaltyShare")

        await mockProxy.connect(signer).initializeWithRoyaltyShare(
            2000,
            zeroAddress,
            "",
            [],
            [oneAddress, twoAddress],
            [10, 30]
        )

        assert.equal((await mockProxy.getRecipients())[0].recipient, oneAddress, "Invalid recipient 0")
        assert.equal((await mockProxy.getRecipients())[0].bps, 2000*10/40, "Invalid bps 0")

        assert.equal((await mockProxy.getRecipients())[1].recipient, twoAddress, "Invalid recipient 1")
        assert.equal((await mockProxy.getRecipients())[1].bps, 2000*30/40, "Invalid bps 1")
    })
})