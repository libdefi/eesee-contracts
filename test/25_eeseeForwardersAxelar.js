const {
    time,
    loadFixture,
  } = require('@nomicfoundation/hardhat-network-helpers');
  const fs = require('fs')
    const path = require('path')
  const { anyValue } = require('@nomicfoundation/hardhat-chai-matchers/withArgs');
  const { expect } = require('chai');
  const { ethers, network } = require('hardhat');
  const assert = require('assert');
  const { StandardMerkleTree } = require('@openzeppelin/merkle-tree');
  const { getContractAddress } = require('@ethersproject/address')
  const { keccak256 } = require('@ethersproject/keccak256')
  const createPermit = require('./utils/createPermit')
  const { BigNumber } = ethers
  const {convertArrayToObject, compareObjects} = require('./utils/compareObjects');
  ((network.name != 'multipleNetworks') ? describe: describe.skip)('ESE forwarders', function () {
    let ERC20;
    let NFT;
    let signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector;
    let royaltyEninge;
    let eeseeAssetHub
    let mockAxelarGasService
    let mockAxelarGatewayL1
    let mockCCIPRouterL1
    let tokenId
    let mockInterchainTokenServiceL1
    let onRamp
    let offRamp
    let onRampImplementation
    const abi = ethers.utils.defaultAbiCoder
    const chainSelectorL2 = abi.encode(['string'], ['l2'])
    const chainSelectorL1 = abi.encode(['string'], ['l1'])
    const tokenSymbolUSDC = 'USDC'
    const zeroBytes32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
    const zeroAddress = '0x0000000000000000000000000000000000000000'
    const oneAddress = '0x0000000000000000000000000000000000000001'
    const mainnetAddresses = JSON.parse(fs.readFileSync(path.resolve(__dirname, './constants/mainnetAddresses.json'), 'utf-8'))
    let snapshotId
    let onRampInterface
    const messageId = '0x0000000000000000000000000000000000000000000000000000000000000023'
    this.beforeAll(async() => {   
        [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector, royaltyCollector] = await ethers.getSigners()
        const _AssetTransfer = await hre.ethers.getContractFactory('AssetTransfer');
        const assetTransfer = await _AssetTransfer.deploy()
        await assetTransfer.deployed()
        const _MockERC20 = await hre.ethers.getContractFactory('MockERC20');
        const _NFT = await hre.ethers.getContractFactory('EeseeNFT');
        const _royaltyEngine = await hre.ethers.getContractFactory('MockRoyaltyEngine');
        const _eeseeForwarderOffRamp = await hre.ethers.getContractFactory('EeseeOffRampAxelar')
        const _mockAxelarGasService = await hre.ethers.getContractFactory('MockAxelarGasService')
        const _mockAxelarGateway = await hre.ethers.getContractFactory('MockAxelarGateway')
        const _mockInterchainTokenService = await hre.ethers.getContractFactory('MockInterchainTokenService')
        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");

        const _eeseeOnRampImplementation = await hre.ethers.getContractFactory('EeseeOnRampImplementationAxelar')
        const _eeseeOnRamp = await hre.ethers.getContractFactory('EeseeOnRampProxy')

        accessManager = await _eeseeAccessManager.deploy();
        await accessManager.deployed()

        ERC20 = await _MockERC20.deploy('20000000000000000000000000000')
        await ERC20.deployed()

        royaltyEninge = await _royaltyEngine.deploy();
        await royaltyEninge.deployed()

        NFT = await _NFT.deploy()
        await NFT.initialize({
            name: 'APES',
            symbol:'bayc',
            baseURI: '/',
            revealedURI: '',
            contractURI:'/',
            royaltyReceiver: zeroAddress,
            royaltyFeeNumerator: 0
        },10, signer.address)

        await NFT.deployed()

        mockAxelarGasService = await _mockAxelarGasService.deploy(acc3.address)
        await mockAxelarGasService.deployed()
        mockAxelarGatewayL1 = await _mockAxelarGateway.deploy()
        await mockAxelarGatewayL1.deployed()
        mockInterchainTokenServiceL1 = await _mockInterchainTokenService.deploy(mockAxelarGatewayL1.address, mockAxelarGasService.address, "l1")
        await mockInterchainTokenServiceL1.deployed()
        
        
        offRamp = await _eeseeForwarderOffRamp.deploy(ERC20.address, feeCollector.address) 
        await offRamp.deployed()

        tokenId = (ERC20.address + "000000000000000000000000").toLowerCase()
        onRampImplementation = await _eeseeOnRampImplementation.deploy(
            ERC20.address, 
            tokenId,
            mockInterchainTokenServiceL1.address
        )
        await onRampImplementation.deployed()

        onRamp = await _eeseeOnRamp.deploy(
            onRampImplementation.address,
            abi.encode(['tuple(bytes chainSelector, address _address)'], [{chainSelector: chainSelectorL2, _address: offRamp.address}]),
            accessManager.address
        )
        await onRamp.deployed()

        onRampInterface = await ethers.getContractAt("EeseeOnRampImplementationAxelar", onRamp.address);
    })

    it('forwardsL1', async () => {
        await expect(onRamp.forward("0x")).to.be.revertedWithCustomError(onRampImplementation, "InsufficientBalance")

        await ERC20.transfer(onRamp.address, 100)
        assert.equal(await ERC20.balanceOf(onRamp.address), 100, "Incorrect balance")

        var gas = 10000
        await expect(onRamp.forward("0x", {value: gas}))
        .to.emit(onRampInterface, "CrosschainSend").withArgs(
            await onRampImplementation.AXELAR_INTERCHAIN_TOKEN_SERVICE_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL2, _address: offRamp.address})),
            abi.encode(['address' , 'uint256', 'uint256'], [ERC20.address, 100, gas])
        )
        .and.to.emit(onRampInterface, "Forward").withArgs(
            ((i) => (compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL2, _address: offRamp.address}))),
            100
        )
        assert.equal(await ERC20.balanceOf(onRamp.address), 0, "Incorrect balance")
    })

    it('forwardsL2', async () => {
        await ERC20.transfer(offRamp.address, 100)
        await expect(offRamp.executeWithInterchainToken(
            zeroBytes32,
            "a",
            "0x",
            "0x",
            zeroBytes32,
            zeroAddress,
            0
        )).to.emit(offRamp, "Forward").withArgs(100)
        assert.equal(await ERC20.balanceOf(feeCollector.address), 100, "Balance is correct")

        await expect(offRamp.executeWithInterchainToken(
            zeroBytes32,
            "a",
            "0x",
            "0x",
            zeroBytes32,
            zeroAddress,
            0
        )).to.not.emit(offRamp, "Forward")
        assert.equal(await ERC20.balanceOf(feeCollector.address), 100, "Balance is correct")
    })

    it('reverts constructor', async () => {
        await expect(onRampImplementation.initialize(
            abi.encode(['tuple(bytes chainSelector, address _address)'], [{chainSelector: "0x01", _address: zeroAddress}])
        )).to.be.revertedWithCustomError(onRampImplementation, "InvalidOffRamp")

        const _eeseeForwarderOnRamp = await hre.ethers.getContractFactory('EeseeOnRampImplementationAxelar')
        await expect(_eeseeForwarderOnRamp.deploy(
            zeroAddress,
            tokenId,
            mockInterchainTokenServiceL1.address,
        )).to.be.revertedWithCustomError(_eeseeForwarderOnRamp, "InvalidESE")

        await expect(_eeseeForwarderOnRamp.deploy(
            ERC20.address,
            zeroBytes32,
            mockInterchainTokenServiceL1.address,
        )).to.be.revertedWithCustomError(_eeseeForwarderOnRamp, "InvalidTokenId")

        await expect(_eeseeForwarderOnRamp.deploy(
            ERC20.address,
            tokenId,
            zeroAddress,
        )).to.be.revertedWithCustomError(_eeseeForwarderOnRamp, "InvalidInterchainTokenService")

        const _eeseeForwarderOffRamp = await hre.ethers.getContractFactory('EeseeOffRampAxelar')
        await expect(_eeseeForwarderOffRamp.deploy(
            zeroAddress,
            oneAddress,
        )).to.be.revertedWithCustomError(_eeseeForwarderOffRamp, "InvalidConstructor")

        await expect(_eeseeForwarderOffRamp.deploy(
            oneAddress,
            zeroAddress,
        )).to.be.revertedWithCustomError(_eeseeForwarderOffRamp, "InvalidConstructor")
    })

    it('reverts ccip message', async () => {
        const commandId = keccak256(abi.encode(['uint256'],['1']))
        const amount = 20
        const payload = abi.encode(
            ['uint256', 'bytes32', 'bytes', 'bytes', 'uint256', 'bytes'], 
            [0, tokenId, offRamp.address, onRampImplementation.address, amount, "0x20"]
        );
        await expect(mockInterchainTokenServiceL1.execute(
            commandId, 
            abi.decode(['string'], chainSelectorL2)[0],
            onRampImplementation.address.toLowerCase(), 
            payload
        )).to.be.revertedWithCustomError(onRampImplementation, "CantReceiveMessages")
    })
})