const { expect } = require("chai");
const assert = require("assert");
const { time } = require("@nomicfoundation/hardhat-network-helpers");
const { ethers, network } = require("hardhat");

((network.name != 'multipleNetworks') ? describe : describe.skip)("MultichainECDSAValidatorWithBlastPoints", function () {
    let mockBlastPoints;
    let signer, acc2, acc3;
    let smartAccount;
    let multichainECDSAValidatorWithBlastPoints;

    const zeroAddress = "0x0000000000000000000000000000000000000000"

    this.beforeAll(async() => {
        [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, invalidOperator] = await ethers.getSigners()
        const _mockBlastPoints = await hre.ethers.getContractFactory("MockBlastPoints");
        const _smartAccount = await hre.ethers.getContractFactory("SmartAccount");
        const _multichainECDSAValidatorWithBlastPoints = await hre.ethers.getContractFactory("MultichainECDSAValidatorWithBlastPoints");

        mockBlastPoints = await _mockBlastPoints.deploy(invalidOperator.address);
        await mockBlastPoints.deployed();

        multichainECDSAValidatorWithBlastPoints = await _multichainECDSAValidatorWithBlastPoints.deploy(mockBlastPoints.address);
        await multichainECDSAValidatorWithBlastPoints.deployed();

        smartAccount = await _smartAccount.deploy(signer.address);
        await smartAccount.deployed();

        await smartAccount.enableModule(multichainECDSAValidatorWithBlastPoints.address);
    })

    it('successful initializeBlastPointsOperator', async () => {
        const snapshotId = await network.provider.send('evm_snapshot')
        await multichainECDSAValidatorWithBlastPoints.initializeBlastPointsOperator(smartAccount.address, signer.address);
        expect(await multichainECDSAValidatorWithBlastPoints.isInitializedBlastPointsOperatorForAccount(smartAccount.address)).to.equal(true);
        await network.provider.send("evm_revert", [snapshotId])
    })

    it('cannot deploy multichainECDSAValidatorWithBlastPoints with zero address BlastPoints', async () => {
        const _multichainECDSAValidatorWithBlastPoints = await hre.ethers.getContractFactory("MultichainECDSAValidatorWithBlastPoints");
        await expect(_multichainECDSAValidatorWithBlastPoints.deploy(zeroAddress)).to.be.revertedWithCustomError(multichainECDSAValidatorWithBlastPoints, "InvalidBlastPointsAddress");
    })

    it('cannot initializeBlastPointsOperator with zero address pontsOperator', async () => {
        await expect(multichainECDSAValidatorWithBlastPoints.initializeBlastPointsOperator(smartAccount.address, zeroAddress))
            .to.be.revertedWithCustomError(multichainECDSAValidatorWithBlastPoints, "InvalidPointsOperator");
    })

    it('cannot initializeBlastPointsOperator with zero address smartAccount', async () => {
        await expect(multichainECDSAValidatorWithBlastPoints.initializeBlastPointsOperator(zeroAddress, smartAccount.address))
            .to.be.revertedWithCustomError(multichainECDSAValidatorWithBlastPoints, "InvalidSmartAccount");
    })

    it('cannot initializeBlastPointsOperator with ExecTransacionFromModuleFailed', async () => {
        const invalidOperator = await mockBlastPoints.invalidOperator();
        await expect(multichainECDSAValidatorWithBlastPoints.initializeBlastPointsOperator(smartAccount.address, invalidOperator))
            .to.be.revertedWithCustomError(multichainECDSAValidatorWithBlastPoints, "ExecTransacionFromModuleFailed");
    })

    it('cannot initializeBlastPointsOperator twice for the same smart account', async () => {
        await multichainECDSAValidatorWithBlastPoints.initializeBlastPointsOperator(smartAccount.address, smartAccount.address);
        expect(await multichainECDSAValidatorWithBlastPoints.isInitializedBlastPointsOperatorForAccount(smartAccount.address)).to.equal(true);
        await expect(multichainECDSAValidatorWithBlastPoints.initializeBlastPointsOperator(smartAccount.address, smartAccount.address))
            .to.be.revertedWithCustomError(multichainECDSAValidatorWithBlastPoints, "BlastPointsOperatorAlreadyInitialized");
    })

    it('SmartAccountFactoryWrapper', async () => {
        const _smartAccountFactoryWrapper = await hre.ethers.getContractFactory("SmartAccountFactoryWrapper");
        const _smartAccountFactory = await hre.ethers.getContractFactory("SmartAccountFactory");
        let smartAccountFactory = await _smartAccountFactory.deploy(smartAccount.address, signer.address);

        await expect(_smartAccountFactoryWrapper.deploy(zeroAddress, multichainECDSAValidatorWithBlastPoints.address))
            .to.be.revertedWithCustomError(_smartAccountFactoryWrapper, "InvalidSmartAccountFactory")

        await expect(_smartAccountFactoryWrapper.deploy(smartAccountFactory.address, zeroAddress))
            .to.be.revertedWithCustomError(_smartAccountFactoryWrapper, "InvalidModule")

        let smartAccountFactoryWrapper = await _smartAccountFactoryWrapper.deploy(smartAccountFactory.address, multichainECDSAValidatorWithBlastPoints.address)
        await smartAccountFactoryWrapper.deployed()

        await smartAccountFactoryWrapper.tryDeployCounterFactualAccount(signer.address, signer.address) // deploys
        await smartAccountFactoryWrapper.tryDeployCounterFactualAccount(signer.address, signer.address) // does not deploy, but still executes
    })
});
