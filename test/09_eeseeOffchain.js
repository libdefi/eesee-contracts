const {
    time,
    loadFixture,
  } = require("@nomicfoundation/hardhat-network-helpers");
  const fs = require('fs')
    const path = require('path')
  const { anyValue } = require("@nomicfoundation/hardhat-chai-matchers/withArgs");
  const { expect } = require("chai");
  const { ethers, network } = require("hardhat");
  const assert = require("assert");
  const { StandardMerkleTree } = require('@openzeppelin/merkle-tree');
  const { getContractAddress } = require('@ethersproject/address')
  const createPermit = require('./utils/createPermit')
  const { keccak256 } = require('@ethersproject/keccak256');
  ((network.name != 'multipleNetworks') ? describe : describe.skip)("eeseeOffchain", function () {
    let ESE;
    let ERC20;
    let eesee;
    let NFT;
    let ERC1155
    let signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector;
    let ticketBuyers;
    let accessManager
    let mockStorage

    const zeroAddress = "0x0000000000000000000000000000000000000000"
    const oneAddress = "0x0000000000000000000000000000000000000001"
    let snapshotId
    this.beforeAll(async() => {
        [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector, royaltyCollector] = await ethers.getSigners()
        ticketBuyers = [acc2,acc3, acc4, acc5, acc6,  acc7]
        const _ESE = await hre.ethers.getContractFactory("ESE");
        const _MockERC20 = await hre.ethers.getContractFactory("MockERC20");
        const _MockStorage = await hre.ethers.getContractFactory("MockStorage");
        const _eesee = await hre.ethers.getContractFactory("EeseeOffchain");
        const _NFT = await hre.ethers.getContractFactory("EeseeNFT");

        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");

        mockStorage = await _MockStorage.deploy()
        await mockStorage.deployed()

        ESE = await _ESE.deploy([{
                cliff: 0,
                duration: 0,
                TGEMintShare: 10000
            }
        ])
        await ESE.deployed()
        await ESE.addVestingBeneficiaries(0, [{addr: signer.address, amount: '2000000000000000000000000'}])
        await ESE.initialize(0)

        ERC20 = await _MockERC20.deploy('20000000000000000000000000000')
        await ERC20.deployed()

        accessManager = await _eeseeAccessManager.deploy();
        await accessManager.deployed()

        // SIGNER_ROLE
        await accessManager.grantRole('0xe2f4eaae4a9751e85a3e4a7b9587827a877f29914755229b07a7b2da98285f70', acc8.address)

        eesee = await _eesee.deploy(
            accessManager.address,
            zeroAddress,
            zeroAddress,
            zeroAddress
        )
        await eesee.deployed()

        NFT = await _NFT.deploy()
        await NFT.initialize({
            name: "APES",
            symbol:"bayc",
            baseURI: "/",
            revealedURI: "",
            contractURI:'/',
            royaltyReceiver: zeroAddress,
            royaltyFeeNumerator: 0
        },10, signer.address)

        await NFT.deployed()
        await NFT.setApprovalForAll(eesee.address, true)
        await NFT.connect(acc4).setApprovalForAll(eesee.address, true)

        for (let i = 0; i < ticketBuyers.length; i++) {
            await ESE.transfer(ticketBuyers[i].address, '10000000000000000000000')
            await ESE.connect(ticketBuyers[i]).approve(eesee.address, '10000000000000000000000')
        }

        await ESE.transfer(acc8.address, '100000000000000000000')
        await ESE.connect(acc8).approve(eesee.address, '100000000000000000000')

        const _ERC1155 = await hre.ethers.getContractFactory("Mock1155");
        ERC1155 = await _ERC1155.deploy("", 1, 10000, royaltyCollector.address, 10000)
        await ERC1155.deployed()
    })

    let nonce = 0
    const createLot = async (asset, sender, data, deadline, _signer) => {
        const types = {
            CreateLot: [
              {
                name: "assetHash",
                type: "bytes32"
              },
              {
                name: "sender",
                type: "address"
              },
              {
                name: "dataHash",
                type: "bytes32"
              },
              {
                name: "nonce",
                type: "uint256"
              },
              {
                name: "deadline",
                type: "uint256"
              },
            ],
          }
        const eip712Domain = await eesee.eip712Domain()
        const domain = {
            name: eip712Domain.name,
            version: eip712Domain.version,
            chainId: eip712Domain.chainId,
            verifyingContract: eip712Domain.verifyingContract
        }
        const abi = ethers.utils.defaultAbiCoder;
        const assetEncoded = abi.encode(
            ["tuple(address, uint256, uint256, uint8, bytes)"], 
            [[asset.token, asset.tokenID, asset.amount, asset.assetType, asset.data]]
        );

        const values = {assetHash: keccak256(assetEncoded), sender, dataHash: keccak256(data), nonce,deadline}
        let signature = await _signer._signTypedData(domain, types, values)

        const params = {nonce, deadline, signature}
        nonce += 1
        return params
    }

    const buyTickets = async (ID, asset, sender, recipient, deadline, _signer) => {
      const types = {
          BuyTickets: [
            {
              name: "ID",
              type: "bytes32"
            },
            {
              name: "assetHash",
              type: "bytes32"
            },
            {
              name: "sender",
              type: "address"
            },
            {
              name: "recipient",
              type: "address"
            },
            {
              name: "nonce",
              type: "uint256"
            },
            {
              name: "deadline",
              type: "uint256"
            },
          ],
        }
      const eip712Domain = await eesee.eip712Domain()
      const domain = {
          name: eip712Domain.name,
          version: eip712Domain.version,
          chainId: eip712Domain.chainId,
          verifyingContract: eip712Domain.verifyingContract
      }

      const abi = ethers.utils.defaultAbiCoder;
      const assetEncoded = abi.encode(
        ["tuple(address, uint256, uint256, uint8, bytes)"], 
        [[asset.token, asset.tokenID, asset.amount, asset.assetType, asset.data]]
      );

      const values = {ID, assetHash: keccak256(assetEncoded), sender, recipient, nonce, deadline}
      let signature = await _signer._signTypedData(domain, types, values)

      const params = {nonce, deadline, signature}
      nonce += 1
      return params
    }

    const claimAsset = async (ID, asset, sender, recipient, call, deadline, _signer) => {
      const types = {
        ClaimAsset: [
            {
              name: "ID",
              type: "bytes32"
            },
            {
              name: "assetHash",
              type: "bytes32"
            },
            {
              name: "sender",
              type: "address"
            },
            {
              name: "recipient",
              type: "address"
            },
            {
              name: "callHash",
              type: "bytes32"
            },
            {
              name: "nonce",
              type: "uint256"
            },
            {
              name: "deadline",
              type: "uint256"
            },
          ],
        }
      const eip712Domain = await eesee.eip712Domain()
      const domain = {
          name: eip712Domain.name,
          version: eip712Domain.version,
          chainId: eip712Domain.chainId,
          verifyingContract: eip712Domain.verifyingContract
      }
      const abi = ethers.utils.defaultAbiCoder;
      const assetEncoded = abi.encode(
          ["tuple(address, uint256, uint256, uint8, bytes)"], 
          [[asset.token, asset.tokenID, asset.amount, asset.assetType, asset.data]]
      );

      const values = {ID, assetHash: keccak256(assetEncoded), sender, recipient, callHash: keccak256(call), nonce, deadline}
      let signature = await _signer._signTypedData(domain, types, values)

      const params = {nonce, deadline, signature}
      nonce += 1
      return params
    }

    let id

    it('Lists NFT', async () => {
        const asset1 = {token: NFT.address, tokenID: 1, amount: 1, assetType: 0, data:'0x'}
        const asset2 = {token: NFT.address, tokenID: 2, amount: 1, assetType: 0, data:'0x'}
        const sig1 = await createLot(asset1, signer.address, "0x01", '999999999999', acc8)
        const sig2 = await createLot(asset2, signer.address, "0x02", '999999999999', acc8)
        const sig3 = await createLot(asset2, signer.address, "0x02", '999999999999', acc7)
        const sig4 = await createLot(asset2, signer.address, "0x02", '2', acc8)

        id = await eesee.getLotId(1)

        await expect(eesee.connect(signer).createLot(
          asset2, 
          "0x02",
          sig3
        )).to.be.revertedWithCustomError(eesee, "InvalidSignature")

        await expect(eesee.connect(signer).createLot(
          asset2, 
          "0x02",
          sig4
        )).to.be.revertedWithCustomError(eesee, "ExpiredDeadline")

        const _nonce = nonce
        const id1 = await eesee.getLotId(nonce)
        const sig5 = await createLot({...asset1, amount: 0}, signer.address, "0x01", '9999999999999', acc8)
        await expect(eesee.connect(signer).createLot(
          {...asset1, amount: 0}, 
          "0x01",
          sig5
        )).and.to.emit(eesee, "CreateLot").withArgs(id1, anyValue, signer.address, "0x01", _nonce)

        await expect(eesee.connect(acc2).createLot(
          asset2, 
          "0x02",
          sig2
        )).to.be.revertedWithCustomError(eesee, "InvalidSignature")

        await expect(eesee.connect(signer).createLot(
          asset2, 
          "0x02",
          sig2,
          {value: 5}
        )).to.be.revertedWithCustomError(eesee, "InvalidValue")

        const sig6 = await createLot({...asset2, amount: 2}, signer.address, "0x02", '9999999999999', acc8)
        await expect(eesee.connect(signer).createLot(
          {...asset2, amount: 2}, 
          "0x02",
          sig6
        )).to.be.revertedWithCustomError(eesee, "InvalidAmount")

        await expect(eesee.connect(signer).createLot(
          asset2, 
          "0x02",
          sig2
        )).and.to.emit(eesee, "CreateLot").withArgs(id, anyValue, signer.address, "0x02", 1)

        await expect(eesee.connect(signer).createLot(
          asset2, 
          "0x02",
          sig2
        )).to.be.revertedWithCustomError(eesee, "NonceUsed")
    })

    it('Creates lots with ERC20 asset types', async () => {
      await ERC20.approve(eesee.address, 10000)

      const asset1 = {token: ERC20.address, tokenID: 1, amount: 10, assetType: 2, data:'0x'}
      const nonce1 = nonce
      const sig1 = await createLot(asset1, signer.address, "0x01", '999999999999', acc8)

      const _id1 = await eesee.getLotId(nonce1)

      const sig5 = await createLot({...asset1, amount: 0}, signer.address, "0x01", '9999999999999', acc8)
      await expect(eesee.connect(signer).createLot(
        {...asset1, amount: 0}, 
        "0x01",
        sig5,
        {value: 5}
      )).to.be.revertedWithCustomError(eesee, "InvalidValue")

      await expect(eesee.connect(signer).createLot(
        asset1, 
        "0x01",
        sig1
      )).to.emit(eesee, "CreateLot").withArgs(_id1, anyValue, signer.address, "0x01", nonce1)   
    })

    it('Creates lots with ERC1155 asset types', async () => {
        await ERC1155.setApprovalForAll(eesee.address, 10000)

        const asset1 = {token: ERC1155.address, tokenID: 1, amount: 10, assetType: 1, data:'0x'}
        const nonce1 = nonce
        const sig1 = await createLot(asset1, signer.address, "0x01", '999999999999', acc8)

        const _id1 = await eesee.getLotId(nonce1)

        const sig5 = await createLot({...asset1, amount: 0}, signer.address, "0x01", '9999999999999', acc8)

        await expect(eesee.connect(signer).createLot(
          asset1, 
          "0x01",
          sig1
        )).to.emit(eesee, "CreateLot").withArgs(_id1, anyValue, signer.address, "0x01", nonce1)
    })

    it('Creates lots with native asset types', async () => {
      const asset1 = {token: zeroAddress, tokenID: 1, amount: 110, assetType: 3, data:'0x'}
      const nonce1 = nonce
      const sig1 = await createLot(asset1, signer.address, "0x01", '999999999999', acc8)

      const _id1 = await eesee.getLotId(nonce1)

      const sig5 = await createLot({...asset1, amount: 0}, signer.address, "0x01", '9999999999999', acc8)

      await expect(eesee.connect(signer).createLot(
        asset1, 
        "0x01",
        sig1,
        {value: 109}
      )).to.be.revertedWithCustomError(eesee, "InvalidValue")

      await expect(eesee.connect(signer).createLot(
        asset1, 
        "0x01",
        sig1,
        {value: 110}
      )).to.emit(eesee, "CreateLot").withArgs(_id1, anyValue, signer.address, "0x01", nonce1)
    })

    it('ESE type lot', async () => {
      const asset1 = {token: zeroAddress, tokenID: 1, amount: 500, assetType: 4, data:'0x'}
      const nonce1 = nonce
      const sig1 = await createLot(asset1, signer.address, "0x01", '999999999999', acc8)

      const _id1 = await eesee.getLotId(nonce1)

      await expect(eesee.connect(signer).createLot(
        asset1, 
        "0x01",
        sig1,
      )).to.emit(eesee, "CreateLot").withArgs(_id1, anyValue, signer.address, "0x01", nonce1)
    })

    it('ERC721LazyMint type lot', async () => {
      const asset1 = {token: zeroAddress, tokenID: 1, amount: 1, assetType: 5, data:'0x'}
      const sig1 = await createLot(asset1, signer.address, "0x01", '999999999999', acc8)

      await expect(eesee.connect(signer).createLot(
        asset1, 
        "0x01",
        sig1
      )).to.be.revertedWithCustomError(eesee, "InvalidAssetType")
    })

    it('Buys tickets with permit', async () => {
      const asset = {token: ESE.address, tokenID: 0, amount: 40, assetType: 2, data:'0x'}

      await ESE.connect(acc2).approve(eesee.address, 0)
      const currentTimestamp = (await ethers.provider.getBlock()).timestamp
      const deadline = currentTimestamp + 10000
      const correctPermit = await createPermit(acc2, eesee, '40', deadline, ESE)

      const params_ = ethers.utils.defaultAbiCoder.encode(
        ["uint256", "uint256", "uint8", "bytes32", "bytes32"],
        ['40', deadline, correctPermit.v, correctPermit.r, correctPermit.s]
      );

      let sig1 = await buyTickets(id, asset, acc2.address, acc2.address, deadline, acc8)
      await expect(eesee.connect(acc2).buyTickets(id, {...asset, data: params_}, zeroAddress, sig1))
      .to.be.revertedWithCustomError(eesee, "InvalidRecipient")
      const balanceBefore = await ESE.balanceOf(acc2.address)
      const incorrectPermit = await createPermit(acc2, eesee, '10', deadline, ESE)
      const params_incorrect = ethers.utils.defaultAbiCoder.encode(
          ["uint256", "uint256", "uint8", "bytes32", "bytes32"],
          ['10', deadline, incorrectPermit.v, incorrectPermit.r, incorrectPermit.s]
        );

        const params_incorrect2 = ethers.utils.defaultAbiCoder.encode(
          ["uint256", "uint256", "uint8", "bytes32", "bytes32"],
          ['11', deadline, incorrectPermit.v, incorrectPermit.r, incorrectPermit.s]
        );

      sig1 = await buyTickets(id, {...asset, data: params_incorrect2}, acc2.address, acc2.address, deadline, acc8)
      await expect(eesee.connect(acc2).buyTickets(id, {...asset, data: params_incorrect2}, acc2.address, sig1))
      .to.be.revertedWith("ERC20Permit: invalid signature")
      sig1 = await buyTickets(id, {...asset, data: params_incorrect}, acc2.address, acc2.address, deadline, acc8)
      await expect(eesee.connect(acc2).buyTickets(id, {...asset, data: params_incorrect}, acc2.address, sig1))
      .to.be.revertedWith("ERC20: insufficient allowance")

      const nonce1 = nonce
      sig1 = await buyTickets(id, {...asset, data: params_}, acc2.address, acc2.address, deadline, acc8)
      await expect(eesee.connect(acc2).buyTickets(id, {...asset, data: params_}, acc2.address, sig1))
          .to.emit(eesee, "BuyTickets").withArgs(id, anyValue, acc2.address, acc2.address, nonce1)


      const balanceAfter = await ESE.balanceOf(acc2.address)
      assert.equal(BigInt(balanceBefore) - BigInt(balanceAfter), 40, "Price paid is correct")

      await expect(eesee.connect(acc8).buyTickets(id, {...asset, data: "0x"}, acc2.address, {nonce: 9999, deadline: "0", signature: "0x"}))
        .to.emit(eesee, "BuyTickets").withArgs(id, anyValue, acc8.address, acc2.address, 9999)
    })

    it('Claims asset', async () => {
      const _MockRevertWithMessage = await hre.ethers.getContractFactory("MockRevertWithMessage");
      const mockRevertWithMessage = await _MockRevertWithMessage.deploy()
      await mockRevertWithMessage.deployed()

      const abi = ethers.utils.defaultAbiCoder;
      const asset1 = {token: NFT.address, tokenID: 2, amount: 1, assetType: 0, data:'0x'}
      const nonce1 = nonce
      const sig1 = await claimAsset(id, asset1, acc3.address, acc4.address, "0x", '999999999999', acc8)

      await expect(eesee.connect(signer).claimAsset(
        id,
        asset1,  
        zeroAddress,
        "0x",
        sig1
      )).to.be.revertedWithCustomError(eesee, "InvalidRecipient")

      await expect(eesee.connect(acc4).claimAsset(
        id,
        asset1, 
        acc4.address,
        "0x",
        sig1
      )).to.be.revertedWithCustomError(eesee, "InvalidSignature")

      await expect(eesee.connect(acc3).claimAsset(
        id,
        asset1, 
        acc4.address,
        "0x",
        sig1
      ))
      .to.emit(eesee, "ClaimAsset").withArgs(id, anyValue, acc3.address, acc4.address, nonce1)

      assert.equal(await NFT.ownerOf(2), acc4.address, "Asset not transfered")

      await NFT.transferFrom(signer.address, eesee.address, 4)
      let asset = {token: NFT.address, tokenID: 4, amount: 1, assetType: 0, data:'0x'}
      const storeData = mockStorage.interface.encodeFunctionData('storeString', ["Hello World"])
      await expect(eesee.connect(acc8).claimAsset(
        id,
        asset, 
        acc4.address,
        abi.encode(["address", "uint256", "bytes"], [mockStorage.address, 0, storeData]),
        {nonce: 999, deadline: '0', signature: "0x"},
        {value: 5}
      )).to.be.revertedWithCustomError(eesee, "InvalidValue")

      await expect(eesee.connect(acc8).claimAsset(
        id,
        asset, 
        acc4.address,
        abi.encode(["address", "uint256", "bytes"], [mockStorage.address, 5, storeData]),
        {nonce: 999, deadline: '0', signature: "0x"},
        {value: 5}
      )).to.be.reverted


      const mockRevertWithMessage_encode = mockRevertWithMessage.interface.encodeFunctionData('initialize', ["0x"])
      await expect(eesee.connect(acc8).claimAsset(
        id,
        asset, 
        acc4.address,
        abi.encode(["address", "uint256", "bytes"], [mockRevertWithMessage.address, 0, mockRevertWithMessage_encode]),
        {nonce: 999, deadline: '0', signature: "0x"}
      )).to.be.revertedWith("goodbye world")


      await expect(eesee.connect(acc8).claimAsset(
        id,
        asset, 
        acc4.address,
        abi.encode(["address", "uint256", "bytes"], [mockStorage.address, 0, storeData]),
        {nonce: 999, deadline: '0', signature: "0x"}
      ))
      .to.emit(eesee, "ClaimAsset").withArgs(id, anyValue, acc8.address, acc4.address, 999)
      assert.equal(await NFT.ownerOf(4), acc4.address, "Asset not transfered")
      assert.equal(await mockStorage.newStr(), "Hello World", "Call not made")

      asset = {token: ERC1155.address, tokenID: 1, amount: 10, assetType: 1, data:'0x'}
      await expect(eesee.connect(acc8).claimAsset(
        id,
        asset, 
        acc4.address,
        "0x",
        {nonce: 1001, deadline: '0', signature: "0x"},
      ))
      .to.emit(eesee, "ClaimAsset").withArgs(id, anyValue, acc8.address, acc4.address, 1001)
      assert.equal(await ERC1155.balanceOf(acc4.address, 1), 10, "Asset not transfered")
      asset = {token: ERC20.address, tokenID: 1, amount: 10, assetType: 2, data:'0x'}
      await expect(eesee.connect(acc8).claimAsset(
        id,
        asset, 
        acc4.address,
        "0x",
        {nonce: 1002, deadline: '999999999999', signature: "0x"}, 
      ))
      .to.emit(eesee, "ClaimAsset").withArgs(id, anyValue, acc8.address, acc4.address, 1002)
      assert.equal(await ERC20.balanceOf(acc4.address), 10, "Asset not transfered")

      const _balanceBefore = await ethers.provider.getBalance(acc4.address);
      asset = {token: zeroAddress, tokenID: 1, amount: 13, assetType: 3, data:'0x'}
      await expect(eesee.connect(acc8).claimAsset(
        id,
        asset,
        eesee.address,
        "0x",
        {nonce: 1003, deadline: '999999999999', signature: "0x"},
      )).to.be.revertedWithCustomError(eesee, "TransferNotSuccessful")

      await expect(eesee.connect(acc8).claimAsset(
        id,
        asset,
        acc4.address,
        "0x",
        {nonce: 1003, deadline: '999999999999', signature: "0x"},
      ))
      .to.emit(eesee, "ClaimAsset").withArgs(id, anyValue, acc8.address, acc4.address, 1003)
      const _balanceAfter =  await ethers.provider.getBalance(acc4.address);
      assert.equal(_balanceAfter.sub(_balanceBefore).toString(), "13", "Asset not transfered")


      asset = {token: NFT.address, tokenID: 1, amount: 0, assetType: 0, data:'0x'}
      await expect(eesee.connect(acc8).claimAsset(
        id,
        asset,
        acc4.address,
        "0x",
        {nonce: 1004, deadline: '999999999999', signature: "0x"},
      ))
      .to.emit(eesee, "ClaimAsset").withArgs(id, anyValue, acc8.address, acc4.address, 1004)


      asset = {token: ERC20.address, tokenID: 1, amount: 13, assetType: 4, data:'0x'}
      await expect(eesee.connect(acc8).claimAsset(
        id,
        asset,
        acc4.address,
        "0x",
        {nonce: 1005, deadline: '999999999999', signature: "0x"},
      ))
      .to.be.revertedWithCustomError(eesee, "InvalidAssetType")

      asset = {token: ERC20.address, tokenID: 1, amount: 13, assetType: 5, data:'0x'}
      await expect(eesee.connect(acc8).claimAsset(
        id,
        asset,
        acc4.address,
        "0x",
        {nonce: 1006, deadline: '999999999999', signature: "0x"},
      ))
      .to.be.revertedWithCustomError(eesee, "InvalidAssetType")
    })

    it('revokes signature', async () => {
      const asset1 = {token: NFT.address, tokenID: 1, amount: 1, assetType: 0, data:'0x'}
      const _nonce1 = nonce
      const sig1 = await createLot(asset1, signer.address, "0x01", '999999999999', acc8)

      const asset2 = {token: NFT.address, tokenID: 2, amount: 1, assetType: 0, data:'0x'}
      const _nonce2 = nonce
      const sig2 = await createLot(asset2, signer.address, "0x02", '999999999999', acc8)

      await expect(eesee.connect(acc4).revokeSignatures([_nonce1, _nonce2])).to.be.revertedWithCustomError(eesee, "CallerNotAuthorized")
      await expect(eesee.connect(acc8).revokeSignatures([_nonce1, _nonce2]))
      .to.emit(eesee, "RevokeSignature").withArgs(_nonce1)
      .and.to.emit(eesee, "RevokeSignature").withArgs(_nonce2)

      await expect(eesee.connect(acc4).createLot(
        asset1, 
        "0x01",
        sig1
      )).to.be.revertedWithCustomError(eesee, "NonceUsed")

      await expect(eesee.connect(acc4).createLot(
        asset2, 
        "0x02",
        sig2
      )).to.be.revertedWithCustomError(eesee, "NonceUsed")

      await expect(eesee.connect(acc8).revokeSignatures([_nonce1]))
      .to.not.emit(eesee, "RevokeSignature")
    })

    it('Reverts constructor', async () => {
      const _eesee = await hre.ethers.getContractFactory("EeseeOffchain");

      await expect(_eesee.deploy(
          zeroAddress,
          zeroAddress, 
          zeroAddress,
          zeroAddress
      )).to.be.revertedWithCustomError(_eesee, "InvalidAccessManager")
    })
});
