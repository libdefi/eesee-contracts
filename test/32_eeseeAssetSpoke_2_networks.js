  const { anyValue } = require('@nomicfoundation/hardhat-chai-matchers/withArgs');
  const { expect } = require('chai');
  const { ethers, network } = require('hardhat');
  const assert = require('assert');
  const { keccak256 } = require('@ethersproject/keccak256')
  const {convertArrayToObject, compareObjects} = require('./utils/compareObjects')
  const compareErrors = require('./utils/compareErrors');
  
  ((network.name == 'multipleNetworks') ? describe : describe.skip)('Cross Chain 2 networks', function () {
    let ERC20;
    let ERC20WithFeeOnTransfer;
    let NFT;
    let signer, acc2, acc3, acc4, acc7, acc8, acc9, royaltyCollector;
    let royaltyEninge;
    let eeseeAssetSpoke;
    let accessManager;
    let eeseeAssetHub;
    let mockAxelarGasService;
    let mockCCIPRouterL1;
    let mockCCIPRouterL2;
    const abi = ethers.utils.defaultAbiCoder
    const chainSelectorL2 = abi.encode(['uint64'], [2])
    const chainSelectorL1 = abi.encode(['uint64'], [1])

    const zeroBytes32 = '0x0000000000000000000000000000000000000000000000000000000000000000';
    const zeroAddress = '0x0000000000000000000000000000000000000000';
    const oneAddress = '0x0000000000000000000000000000000000000001';
    const messageId = '0x0000000000000000000000000000000000000000000000000000000000000023';
    async function changeNetwork(name){
        await hre.changeNetwork(name);
        if(signer == undefined){
            [signer, acc2, acc3, acc4, acc7, acc8, acc9, royaltyCollector] = await ethers.getSigners();
        }else{
            let [_signer, _acc2, _acc3, _acc4, _acc7, _acc8, _acc9, _royaltyCollector] = await ethers.getSigners();
            if(
                _signer.address != signer.address ||
                _acc2.address != acc2.address ||
                _acc3.address != acc3.address ||
                _acc4.address != acc4.address ||
                _acc7.address != acc7.address ||
                _acc8.address != acc8.address ||
                _acc9.address != acc9.address ||
                _royaltyCollector.address != royaltyCollector.address
            ) throw Error("Accounts for networks not match");
            [signer, acc2, acc3, acc4, acc7, acc8, acc9, royaltyCollector] = [_signer, _acc2, _acc3, _acc4, _acc7, _acc8, _acc9, _royaltyCollector]
        }
    }
    this.beforeAll(async() => {
        await changeNetwork('testnet1');
        const _AssetTransfer = await hre.ethers.getContractFactory('AssetTransfer');
        const assetTransfer = await _AssetTransfer.deploy();
        await assetTransfer.deployed();
        const _MockERC20 = await hre.ethers.getContractFactory('MockERC20');
        const _MockERC20WithFeeOnTransfer = await hre.ethers.getContractFactory('MockFeeOnTransfer');
        const _NFT = await hre.ethers.getContractFactory('EeseeNFT');
        const _royaltyEngine = await hre.ethers.getContractFactory('MockRoyaltyEngine');
        const _eeseeAssetSpoke = await hre.ethers.getContractFactory('EeseeAssetSpokeCCIP');
        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");
        const _mockCCIPRouterL1 = await hre.ethers.getContractFactory('MockCCIPRouter');

        accessManager =  await _eeseeAccessManager.deploy();
        await accessManager.deployed();

        // PAUSER_ROLE
        await accessManager.grantRole('0x65d7a28e3265b37a6474929f336521b332c1681b933f6cb9f3376673440d862a', acc9.address);

        ERC20 = await _MockERC20.deploy('20000000000000000000000000000');
        await ERC20.deployed();

        ERC20WithFeeOnTransfer = await _MockERC20WithFeeOnTransfer.deploy('20000000000000000000000000000');
        await ERC20WithFeeOnTransfer.deployed();

        royaltyEninge = await _royaltyEngine.deploy();
        await royaltyEninge.deployed();
        
        NFT = await _NFT.deploy();
        await NFT.initialize({
            name: 'APES',
            symbol:'bayc',
            baseURI: '/',
            revealedURI: '',
            contractURI:'/',
            royaltyReceiver: zeroAddress,
            royaltyFeeNumerator: 0
        },100, signer.address)

        await NFT.deployed();
        
        
        mockCCIPRouterL1 = await _mockCCIPRouterL1.deploy();
        await mockCCIPRouterL1.deployed();

                await changeNetwork('testnet2');
                const _eeseeAssetHub = await hre.ethers.getContractFactory('EeseeAssetHubCCIP');
                const _mockCCIPRouterL2 = await hre.ethers.getContractFactory('MockCCIPRouter');

                mockCCIPRouterL2 = await _mockCCIPRouterL2.deploy();
                await mockCCIPRouterL2.deployed();

                eeseeAssetHub = await _eeseeAssetHub.deploy(
                    'img/',
                    'prefix#',
                    '_description_',

                    mockCCIPRouterL2.address,
                    zeroAddress
                );
                await eeseeAssetHub.deployed();

        await changeNetwork('testnet1');

        eeseeAssetSpoke = await _eeseeAssetSpoke.deploy(
            accessManager.address,

            chainSelectorL1,
            {chainSelector: chainSelectorL2, _address: eeseeAssetHub.address},

            royaltyEninge.address,
            mockCCIPRouterL1.address,
            zeroAddress
        );
        await eeseeAssetSpoke.deployed();
    })

    function _compareArrays(arr1, arr2){
        var isSame = true;
        arr1.map((val,i) => {if(val.toString() != arr2[i].toString()){isSame = false}});
        return isSame;
    }

    let tokenIDL2
    let assetHash_
    it('wrap', async () => {
        // === L1 ===
        const sourceL1 = {
            chainSelector: chainSelectorL1,
            _address: eeseeAssetSpoke.address
        };

        const asset = {
            token: NFT.address,
            tokenID: 1,
            amount: 1,
            assetType: 0,
            data: "0x"
        };

        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]));
        assetHash_ = assetHash;
        assert.equal(await eeseeAssetSpoke.getAssetHash(asset), assetHash, "Is assetHash correct");

        const assetHashWithSource = {
            source: sourceL1,
            assetHash: assetHash
        };
    
        tokenIDL2 = await eeseeAssetHub.getTokenId(assetHashWithSource);
        const _tokenIDL2 = await eeseeAssetSpoke["getTokenIdL2((address,uint256,uint256,uint8,bytes))"](asset);
        const __tokenIDL2 = await eeseeAssetSpoke["getTokenIdL2(bytes32)"](assetHash);
        assert.equal(tokenIDL2.toString(), _tokenIDL2.toString(), "Is tokenId correct");
        assert.equal(_tokenIDL2.toString(), __tokenIDL2.toString(), "Is tokenId correct");
        const transferNFTEncodedData = eeseeAssetHub.interface.encodeFunctionData('safeTransferFrom', [eeseeAssetHub.address, acc8.address, tokenIDL2, asset.amount, "0x"]);

        await NFT.approve(eeseeAssetSpoke.address, 1);
        const gasPaid = 10000;
        await compareErrors(
            eeseeAssetSpoke.wrap(
                [asset],
                [{target: eeseeAssetHub.address, callData: transferNFTEncodedData}],
                zeroAddress,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidRecipient()'
        )
        await compareErrors(
            eeseeAssetSpoke.wrap(
                [{...asset, amount: 2}],
                [{target: eeseeAssetHub.address, callData: transferNFTEncodedData}],
                acc2.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidAmount()'
        )
        await compareErrors(
            eeseeAssetSpoke.wrap(
                [{...asset, token: eeseeAssetSpoke.address}],
                [{target: eeseeAssetHub.address, callData: transferNFTEncodedData}],
                acc2.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidInterface()'
        )
        await compareErrors(
            eeseeAssetSpoke.wrap(
                [{...asset, token: ERC20.address}],
                [{target: eeseeAssetHub.address, callData: transferNFTEncodedData}],
                acc2.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidInterface()'
        )
        await compareErrors(
            eeseeAssetSpoke.wrap(
                [asset],
                [{target: eeseeAssetHub.address, callData: transferNFTEncodedData}],
                acc2.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid - 1}
            ),
            'InsufficientValue()'
        )

        const royalties = await royaltyEninge.getRoyalty(asset.token, asset.tokenID, 10000);
        const assetData = abi.encode(
            ['address[]', 'uint256[]'],
            [royalties.recipients, royalties.amounts]
        );
        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [[assetHash], [asset.amount], [assetData],  [[eeseeAssetHub.address, transferNFTEncodedData]], acc2.address]
        );
        await expect(eeseeAssetSpoke.wrap(
            [asset],
            [{target: eeseeAssetHub.address, callData: transferNFTEncodedData}],
            acc2.address,
            abi.encode(['tuple[](address, uint256)', 'uint256'], [[], 1000001]),
            {value: gasPaid + 1}
        ))
        .to.emit(eeseeAssetSpoke, 'CrosschainSend').withArgs(
            await eeseeAssetSpoke.CCIP_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL2, _address: eeseeAssetHub.address})), 
            abi.encode(['bytes32', 'tuple[](address, uint256)', 'uint256'], [messageId, [], gasPaid])
        )
        .to.emit(eeseeAssetSpoke, 'Wrap').withArgs(
            ((i) => compareObjects(convertArrayToObject(i), asset)), 
            assetHash,
            signer.address
        );
        assert.equal(await NFT.ownerOf(1), eeseeAssetSpoke.address, 'Owner of nft asset is correct');

        const any2EVMMessage = {
            messageId,
            sourceChainSelector: abi.decode(["uint64"], chainSelectorL1)[0],
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
            data: payload,
            destTokenAmounts: []
        };
        await compareErrors(
            eeseeAssetSpoke.ccipReceive(any2EVMMessage),
            'InvalidRouter()'
        )

        // === L2 ===
                await changeNetwork('testnet2');
                assetWithSource = {
                    "source": {
                        "chainSelector": chainSelectorL1,
                        "_address": eeseeAssetSpoke.address
                    },
                    "assetHash": assetHash
                };

                await compareErrors(
                    eeseeAssetHub.ccipReceive(any2EVMMessage),
                    'InvalidRouter()'
                )

                await expect(mockCCIPRouterL2.routeMessage(
                    any2EVMMessage,
                    5000,
                    1000000,
                    eeseeAssetHub.address,
                    {gasLimit: 30000000}
                )).to.emit(eeseeAssetHub, 'CrosschainReceive')
                .withArgs(
                    await eeseeAssetHub.CCIP_CALLER_IDENTIFIER(), 
                    ((i) => compareObjects(convertArrayToObject(i), assetWithSource.source)), 
                    abi.encode(['bytes32', 'tuple[](address, uint256)'], [messageId, []])
                )
                .and.to.emit(eeseeAssetHub, 'Wrap')
                .withArgs(
                    tokenIDL2, 
                    asset.amount,
                    ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetWithSource.assetHash))
                )
                .and.to.emit(eeseeAssetHub, 'TransferBatch')
                .withArgs(mockCCIPRouterL2.address, zeroAddress, eeseeAssetHub.address, ((i) => _compareArrays(i, [tokenIDL2])), ((i) => _compareArrays(i, [asset.amount])))  
                .and.to.emit(eeseeAssetHub, 'TransferSingle')
                .withArgs(eeseeAssetHub.address, eeseeAssetHub.address, acc8.address, tokenIDL2, asset.amount);

        await changeNetwork('testnet1');
    })

    it('Token URI is correct', async () => {
                await hre.changeNetwork('testnet2');
                const tokenURI = await eeseeAssetHub.uri(tokenIDL2)
                const json = Buffer.from(tokenURI.substring(29), "base64").toString();
                const result = JSON.parse(json);

                assert.equal(result.name, 'prefix#' + tokenIDL2)
                assert.equal(result.image, 'img/')
                assert.equal(result.description, '_description_')
                assert.equal(result.properties.sourceChainSelector, abi.decode(['uint64'] , chainSelectorL1)[0])
                assert.equal(result.properties.sourceSpoke, eeseeAssetSpoke.address.toLowerCase())
                assert.equal(result.properties.assetHash, assetHash_.toLowerCase())
        await changeNetwork('testnet1');
    })
    
    let assetsL1
    const tokenIDsL2 = []
    it('wrap - different asset types', async () => {
        //
        const _ERC1155 = await hre.ethers.getContractFactory("Mock1155");
        const ERC1155 = await _ERC1155.deploy("", 1, 10000, royaltyCollector.address, 10000)
        await ERC1155.deployed()
        await ERC1155.setApprovalForAll(eeseeAssetSpoke.address, 10000)
        const _MockERC20 = await hre.ethers.getContractFactory('MockERC20');
        const ERC20 = await _MockERC20.deploy('20000000000000000000000000000')
        await ERC20.deployed()
        await ERC20.approve(eeseeAssetSpoke.address, 10000)
        const gasPaid = 10000
        const assets = [
            {
                token: ERC1155.address, 
                tokenID: 1, 
                amount: 10000, 
                assetType: 1, 
                data:"0x"
            },
            {
                token: ERC20.address, 
                tokenID: 0, 
                amount: 10000, 
                assetType: 2,
                data:"0x"
            },
            {
                token: zeroAddress, 
                tokenID: 0, 
                amount: 10000, 
                assetType: 3, 
                data:"0x"
            }
        ]
        assetsL1 = assets

        tokenIDsL2.push(await eeseeAssetSpoke["getTokenIdL2((address,uint256,uint256,uint8,bytes))"](assets[0]))
        tokenIDsL2.push(await eeseeAssetSpoke["getTokenIdL2((address,uint256,uint256,uint8,bytes))"](assets[1]))
        tokenIDsL2.push(await eeseeAssetSpoke["getTokenIdL2((address,uint256,uint256,uint8,bytes))"](assets[2]))

        const assetHashes = assets.map((asset) => keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType])))
        const assetAmounts = assets.map((asset) => asset.amount)

        const encodedRoyaltiesPromises = assets.map(async (asset) => {
            if(asset.assetType !== 1) {
                return '0x'
            }
            const royalties = await royaltyEninge.getRoyalty(asset.token, asset.tokenID, 10000)
            return abi.encode(
                ['address[]', 'uint256[]'],
                [royalties.recipients, royalties.amounts]
            )
        })
        const encodedRoyalties = await Promise.all(encodedRoyaltiesPromises)
        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [assetHashes, assetAmounts, encodedRoyalties, [], signer.address]
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: ERC1155.address, 
                        tokenID: 1, 
                        amount: 100, 
                        assetType: 4, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidAsset()'
        )

        // ERC1155 reverts

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: ERC1155.address, 
                        tokenID: 1, 
                        amount: 0, 
                        assetType: 1, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidAmount()'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: eeseeAssetSpoke.address, 
                        tokenID: 1, 
                        amount: 10000, 
                        assetType: 1, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidInterface()'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: ERC20.address, 
                        tokenID: 1, 
                        amount: 10000, 
                        assetType: 1, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidInterface()'
        )

        //ERC20 reverts

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: ERC20.address, 
                        tokenID: 0, 
                        amount: 0, 
                        assetType: 2, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidAmount()'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: eeseeAssetSpoke.address, 
                        tokenID: 0, 
                        amount: 10000, 
                        assetType: 2, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            '0x'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: NFT.address, 
                        tokenID: 0, 
                        amount: 10000, 
                        assetType: 2, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidInterface()'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: ERC20.address, 
                        tokenID: 1, 
                        amount: 10000, 
                        assetType: 2, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidTokenID()'
        )

        // Native

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: zeroAddress, 
                        tokenID: 0, 
                        amount: 0, 
                        assetType: 3, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidAmount()'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: zeroAddress, 
                        tokenID: 0, 
                        amount: 1000, 
                        assetType: 3, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                abi.encode(['uint256'], [1000000]),
                {value: 0}
            ),
            'InsufficientValue()'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: ERC20.address, 
                        tokenID: 0, 
                        amount: 1000, 
                        assetType: 3, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidToken()'
        )

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [
                    {
                        token: zeroAddress, 
                        tokenID: 10, 
                        amount: 1000, 
                        assetType: 3, 
                        data:"0x"
                    }
                ],
                [],
                signer.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            ),
            'InvalidTokenID()'
        )

        await expect(eeseeAssetSpoke.wrap(
            assets,
            [],
            signer.address,
            abi.encode(['uint256'], [1000000]),
            {value: gasPaid + assets[2].amount}
        ))
        .to.emit(eeseeAssetSpoke, 'CrosschainSend').withArgs(
            await eeseeAssetSpoke.CCIP_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL2, _address: eeseeAssetHub.address})), 
            abi.encode(['bytes32', 'tuple[](address, uint256)', 'uint256'], [messageId, [], gasPaid])
        )
        .to.emit(eeseeAssetSpoke, 'Wrap').withArgs(
            anyValue, 
            assetHashes[0],
            signer.address,
        )
        .to.emit(eeseeAssetSpoke, 'Wrap').withArgs(
            anyValue, 
            assetHashes[1],
            signer.address,
        )
        .to.emit(eeseeAssetSpoke, 'Wrap').withArgs(
            anyValue, 
            assetHashes[2],
            signer.address,
        )
 
        // === L2 ===

                await changeNetwork('testnet2');

                let any2EVMMessage = {
                    messageId,
                    sourceChainSelector: abi.decode(["uint64"], chainSelectorL1)[0],
                    sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
                    data: payload,
                    destTokenAmounts: []
                }
                let interface2 = new ethers.utils.Interface([
                    'function ccipReceive(tuple(bytes32 messageId, uint64 sourceChainSelector, bytes sender, bytes data, tuple(address token, uint256 amount)[] destTokenAmounts) message)'
                ]);

                await expect(mockCCIPRouterL2.routeMessage(
                    any2EVMMessage,
                    5000,
                    1000000,
                    eeseeAssetHub.address,
                    {gasLimit: 30000000}
                )).to.emit(eeseeAssetHub, 'CrosschainReceive')
                .withArgs(
                    await eeseeAssetHub.CCIP_CALLER_IDENTIFIER(), 
                    ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL1, _address: eeseeAssetSpoke.address})), 
                    abi.encode(['bytes32', 'tuple[](address, uint256)'], [messageId, []])
                )
                .and.to.emit(eeseeAssetHub, 'Wrap')
                .withArgs(
                    tokenIDsL2[0], 
                    assetAmounts[0],
                    ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetHashes[0])), 
                )
                .to.emit(eeseeAssetHub, 'Wrap')
                .withArgs(
                    tokenIDsL2[1], 
                    assetAmounts[1],
                    ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetHashes[1])), 
                )
                .to.emit(eeseeAssetHub, 'Wrap')
                .withArgs(
                    tokenIDsL2[2], 
                    assetAmounts[2],
                    ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash,  assetHashes[2])), 
                )
                .and.to.emit(eeseeAssetHub, 'TransferBatch')
                .withArgs(mockCCIPRouterL2.address, zeroAddress, eeseeAssetHub.address, ((i) => _compareArrays(i, tokenIDsL2)), ((i) => _compareArrays(i, assetAmounts)))  
                .and.to.emit(eeseeAssetHub, 'TransferSingle')
                .withArgs(eeseeAssetHub.address, eeseeAssetHub.address, signer.address, tokenIDsL2[0], assetAmounts[0]) 
                .and.to.emit(eeseeAssetHub, 'TransferSingle')
                .withArgs(eeseeAssetHub.address, eeseeAssetHub.address, signer.address, tokenIDsL2[1], assetAmounts[1])  
                .and.to.emit(eeseeAssetHub, 'TransferSingle')
                .withArgs(eeseeAssetHub.address, eeseeAssetHub.address, signer.address, tokenIDsL2[2], assetAmounts[2])   
        // === L1 ===

        await changeNetwork('testnet1');

        const snapshotId = await network.provider.send('evm_snapshot')
        const payload_ = abi.encode(
            ['bytes32[]', 'uint256[]', 'address'], 
            [assetHashes, assetAmounts, signer.address]
        )

        any2EVMMessage = {
            messageId,
            sourceChainSelector: abi.decode(["uint64"], chainSelectorL1)[0],
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetHub.address]),
            data: payload_,
            destTokenAmounts: []
        }
        await mockCCIPRouterL1.routeMessage(
            any2EVMMessage,
            5000,
            1000000,
            eeseeAssetSpoke.address,
            {gasLimit: 30000000}
        ) // Even though this is reverted, router won't revert the transaction. OffRamp will revert with ReceiverError


        any2EVMMessage = {
            messageId,
            sourceChainSelector: abi.decode(["uint64"], chainSelectorL2)[0],
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
            data: payload_,
            destTokenAmounts: []
        }
        await mockCCIPRouterL1.routeMessage(
            any2EVMMessage,
            5000,
            1000000,
            eeseeAssetSpoke.address,
            {gasLimit: 30000000}
        ) // Even though this is reverted, router won't revert the transaction. OffRamp will revert with ReceiverError


        any2EVMMessage = {
            messageId,
            sourceChainSelector: abi.decode(["uint64"], chainSelectorL2)[0],
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetHub.address]),
            data: payload_,
            destTokenAmounts: []
        }
        ccipRouterEncodedData = interface2.encodeFunctionData('ccipReceive', [any2EVMMessage])
        await expect(mockCCIPRouterL1.routeMessage(
            any2EVMMessage,
            5000,
            1000000,
            eeseeAssetSpoke.address,
            {gasLimit: 30000000}
        )).and.to.emit(eeseeAssetSpoke, 'Unwrap')
        .withArgs(anyValue, assetHashes[0], signer.address)
        .and.to.emit(eeseeAssetSpoke, 'Unwrap')
        .withArgs(anyValue, assetHashes[1], signer.address)
        .and.to.emit(eeseeAssetSpoke, 'Unwrap')
        .withArgs(anyValue, assetHashes[2], signer.address)
        await network.provider.send("evm_revert", [snapshotId])
    })

    it('execute', async () => {
        const snapshotId = await network.provider.send('evm_snapshot')

        const asset = {
            token: NFT.address, 
            tokenID: 3, 
            amount: 1, 
            assetType: 0, 
            data:'0x'
        }

        await NFT.approve(eeseeAssetSpoke.address, 3)
        await eeseeAssetSpoke.wrap(
            [asset],
            [],
            signer.address,
            abi.encode(['uint256'], [1000000]),
            {value: 10000}
        )

        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'address'], 
            [[assetHash], [asset.amount], signer.address]
        )

        let any2EVMMessage = {
            messageId,
            sourceChainSelector: abi.decode(["uint64"], chainSelectorL2)[0],
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetHub.address]),
            data: payload,
            destTokenAmounts: []
        }
        await expect(mockCCIPRouterL1.routeMessage(
            any2EVMMessage,
            5000,
            1000000,
            eeseeAssetSpoke.address,
            {gasLimit: 30000000}
        )).to.emit(eeseeAssetSpoke, 'Unwrap')
        .withArgs(anyValue, assetHash, signer.address)
        await network.provider.send("evm_revert", [snapshotId])
    })
    
    it('execute - stuck asset', async () => {
        const snapshotId = await network.provider.send('evm_snapshot')

        const asset = {
            token: NFT.address, 
            tokenID: 3, 
            amount: 1, 
            assetType: 0, 
            data:'0x'
        }
    
        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'address'], 
            [[assetHash], [asset.amount], signer.address]
            )
        let any2EVMMessage = {
            messageId,
            sourceChainSelector: abi.decode(["uint64"], chainSelectorL2)[0],
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetHub.address]),
            data: payload,
            destTokenAmounts: []
        }
        await expect(mockCCIPRouterL1.routeMessage(
            any2EVMMessage,
            5000,
            1000000,
            eeseeAssetSpoke.address,
            {gasLimit: 30000000}
        )).to.emit(eeseeAssetSpoke, 'Stuck')
        .withArgs(anyValue, assetHash, signer.address, anyValue)

        await NFT.approve(eeseeAssetSpoke.address, 3)
        await eeseeAssetSpoke.wrap(
            [asset],
            [],
            signer.address,
            abi.encode(['uint256'], [1000000]),
            {value: 10000}
        )

        await compareErrors(
            eeseeAssetSpoke.unstuck(assetHash, zeroAddress),
            'InvalidRecipient()'
        )
        await compareErrors(
            eeseeAssetSpoke.unstuck(zeroBytes32, signer.address),
            'NoAssetsStuck()'
        )
            
        await expect(eeseeAssetSpoke.unstuck(assetHash, signer.address))
        .to.emit(eeseeAssetSpoke, 'Unstuck')
        .withArgs(anyValue, assetHash, signer.address, signer.address)

        
        await compareErrors(
            eeseeAssetSpoke.unstuck(assetHash, signer.address),
            'NoAssetsStuck()'
        )
            
        await network.provider.send("evm_revert", [snapshotId])
    })

    it('execute - stuck Native asset', async () => {
        const snapshotId = await network.provider.send('evm_snapshot')
        const asset = {
            token: zeroAddress, 
            tokenID: 0, 
            amount: 100, 
            assetType: 3, 
            data:'0x'
        }
        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))
        await eeseeAssetSpoke.wrap(
            [asset],
            [],
            signer.address,
            abi.encode(['uint256'], [1000000]),
            {value: asset.amount + 100000}
        )

        const _MockRecipient = await hre.ethers.getContractFactory('MockRecipient')
        const recipient = await _MockRecipient.deploy()
        await recipient.deployed()

        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'address'], 
            [[assetHash], [asset.amount], recipient.address]
        )
        let any2EVMMessage = {
            messageId,
            sourceChainSelector: abi.decode(["uint64"], chainSelectorL2)[0],
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetHub.address]),
            data: payload,
            destTokenAmounts: []
        }
        await expect(mockCCIPRouterL1.routeMessage(
            any2EVMMessage,
            5000,
            1000000,
            eeseeAssetSpoke.address,
            {gasLimit: 30000000}
        )).to.emit(eeseeAssetSpoke, 'Stuck')
        .withArgs(anyValue, assetHash, recipient.address, anyValue)

        const unstuckEncodedDataRevert = eeseeAssetSpoke.interface.encodeFunctionData('unstuck', [assetHash, signer.address])
        await expect(recipient.callExternal(eeseeAssetSpoke.address, unstuckEncodedDataRevert))
        .to.emit(eeseeAssetSpoke, 'Unstuck')
        .withArgs(((i) => compareObjects(convertArrayToObject(i), asset)), assetHash, recipient.address, signer.address)
        
        await network.provider.send("evm_revert", [snapshotId])
    })
    it('eeseeAssetSpoke onlySelf check', async () => {
        await compareErrors(
            eeseeAssetSpoke._transferAssetTo(zeroBytes32, 300, signer.address),
            'OnlySelf()'
        )
    })
    it('eeseeAssetHub onlySelf check', async () => {
            await changeNetwork('testnet2');
            await compareErrors(
                eeseeAssetHub._setRoyalty(0, '0x'),
                'OnlySelf()'
            )
    }) 

    it('unwrap', async () => {
            const source = {
                "chainSelector": chainSelectorL1,
                "_address": eeseeAssetSpoke.address
            }

            const gasPaid = 10000
            const assetHashes = [assetsL1[0], assetsL1[1]].map((asset) => keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType])))

            const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [assetsL1[2].token, assetsL1[2].tokenID, assetsL1[2].assetType]))

            const snapshotId = await network.provider.send('evm_snapshot')

            const chainSelectorL3 = abi.encode(["uint64"], ['3'])
            const eeseeAssetSpokeL3 = {address: zeroAddress}
        
            let payload = abi.encode(
                ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
                [[assetHash], [assetsL1[2].amount], ['0x'], [],  signer.address]
            )
            let any2EVMMessage = {
                messageId,
                sourceChainSelector: abi.decode(["uint64"], chainSelectorL3)[0],
                sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpokeL3.address]),
                data: payload,
                destTokenAmounts: []
            }
            await mockCCIPRouterL2.routeMessage(
                any2EVMMessage,
                5000,
                1000000,
                eeseeAssetHub.address,
                {gasLimit: 30000000}
            )
            const tokenID_ = await eeseeAssetHub.getTokenId({source:{chainSelector: chainSelectorL3, _address: eeseeAssetSpokeL3.address}, assetHash})

            await compareErrors(
                eeseeAssetHub.unwrap(
                    [tokenIDsL2[0], tokenID_],
                    [10000, assetsL1[2].amount],
                    acc2.address,
                    abi.encode(['uint256'], [1000000]),
                ),
                'InvalidDestination()'
            )

            payload = abi.encode(
                ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
                [[zeroBytes32], [0], ['0x'], [], signer.address]
            )
            any2EVMMessage = {
                messageId,
                sourceChainSelector: abi.decode(["uint64"], chainSelectorL3)[0],
                sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
                data: payload,
                destTokenAmounts: []
            }
            await mockCCIPRouterL2.routeMessage(
                any2EVMMessage,
                5000,
                1000000,
                eeseeAssetHub.address,
                {gasLimit: 30000000}
            )
            const tokenID__ = await eeseeAssetHub.getTokenId({source:{chainSelector: chainSelectorL3, _address: eeseeAssetSpoke.address}, assetHash: zeroBytes32})
            await compareErrors(
                eeseeAssetHub.unwrap(
                    [tokenIDsL2[0], tokenID__],
                    [10000, assetsL1[2].amount],
                    acc2.address,
                    abi.encode(['uint256'], [1000000]),
                ),
                'InvalidDestination()'
            )
            await compareErrors(
                eeseeAssetHub.unwrap(
                    [tokenIDsL2[0], tokenIDsL2[1]],
                    [10000, assetsL1[2].amount],
                    zeroAddress,
                    abi.encode(['uint256'], [1000000]),
                ),
                'InvalidRecipient()'
            )
            await compareErrors(
                eeseeAssetHub.unwrap(
                    [],
                    [10000, assetsL1[2].amount],
                    acc2.address,
                    abi.encode(['uint256'], [1000000]),
                ),
                'InvalidTokenIdsLength()'
            )
            await compareErrors(
                eeseeAssetHub.unwrap(
                    [tokenIDsL2[0], tokenIDsL2[1]],
                    [10000],
                    acc2.address,
                    abi.encode(['uint256'], [1000000]),
                ),
                'InvalidTokenIdsLength()'
            )

            await expect(eeseeAssetHub.unwrap(
                [tokenIDsL2[0], tokenIDsL2[1]],
                [10000, assetsL1[2].amount],
                acc2.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            )).to.emit(eeseeAssetHub, 'Unwrap').withArgs(
                tokenIDsL2[0], 
                10000,
                ((i) => compareObjects(convertArrayToObject(i.source), source) && compareObjects(i.assetHash, assetHashes[0])), 
                signer.address, 
                acc2.address
            )
            .to.emit(eeseeAssetHub, 'Unwrap').withArgs(
                tokenIDsL2[1], 
                assetsL1[2].amount,
                ((i) => compareObjects(convertArrayToObject(i.source), source) && compareObjects(i.assetHash, assetHashes[1])), 
                signer.address, 
                acc2.address
            )
            .and.to.emit(eeseeAssetHub, 'CrosschainSend')
            .withArgs(
                await eeseeAssetHub.CCIP_CALLER_IDENTIFIER(), 
                ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL1, _address: eeseeAssetSpoke.address})), 
                abi.encode(['bytes32', 'tuple[](address, uint256)', 'uint256'], [messageId, [], gasPaid])
            )

            await compareErrors(
                eeseeAssetHub.connect(acc8).unwrap(
                    [tokenIDsL2[2]],
                    [10000],
                    acc2.address,
                    abi.encode(['uint256'], [1000000]),
                    {value: gasPaid}
                ),
                '0x'//ERC1155: burn amount exceeds balance
            )

            // Use from contract's balance
            await eeseeAssetHub.safeTransferFrom(signer.address, eeseeAssetHub.address, tokenIDsL2[2], 10000, "0x")
            await expect(eeseeAssetHub.connect(acc8).unwrap(
                [tokenIDsL2[2]],
                [10000],
                acc2.address,
                abi.encode(['uint256'], [1000000]),
                {value: gasPaid}
            )).to.emit(eeseeAssetHub, 'Unwrap').withArgs(
                tokenIDsL2[2], 
                10000,
                ((i) => compareObjects(convertArrayToObject(i.source), source) && compareObjects(i.assetHash, assetHash)), 
                acc8.address, 
                acc2.address
            ) 
            await network.provider.send("evm_revert", [snapshotId])
    })

    it('Eesee Vault', async () => {
        await changeNetwork('testnet1');
        const source = {
            "chainSelector": chainSelectorL1,
            "_address": eeseeAssetSpoke.address
        }

        const assetHashes = [assetsL1[0], assetsL1[1]].map((asset) => keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType])))
        const assetAmounts = [assetsL1[0], assetsL1[1]].map((asset) => asset.amount)
        
        const tokenIds = []
        tokenIds.push(await eeseeAssetSpoke["getTokenIdL2(bytes32)"](assetHashes[0]))
        tokenIds.push(await eeseeAssetSpoke["getTokenIdL2(bytes32)"](assetHashes[1]))

            await changeNetwork('testnet2');
            const snapshotId = await network.provider.send('evm_snapshot')

            const _MockRecipient = await hre.ethers.getContractFactory('MockRecipient')
            const recipient = await _MockRecipient.deploy()
            await recipient.deployed()

            let payload = abi.encode(
                ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
                [assetHashes, assetAmounts, ['0x', '0x'], [], recipient.address] // fallback recipient is unable to receive erc1155s
            )
            let any2EVMMessage = {
                messageId,
                sourceChainSelector: source.chainSelector,
                sender: ethers.utils.defaultAbiCoder.encode(['address'], [source._address]),
                data: payload,
                destTokenAmounts: []
            }
            const vault = await eeseeAssetHub.vault()

            await expect(mockCCIPRouterL2.routeMessage(
                any2EVMMessage,
                5000,
                1000000,
                eeseeAssetHub.address,
                {gasLimit: 30000000}
            )).to.emit(mockCCIPRouterL2, 'MessageExecuted')
            .to.emit(eeseeAssetHub, 'CrosschainReceive')
            .and.to.emit(eeseeAssetHub, 'TransferSingle')
            .withArgs(mockCCIPRouterL2.address, eeseeAssetHub.address, vault, tokenIDsL2[0], assetAmounts[0]) 
            .and.to.emit(eeseeAssetHub, 'TransferSingle')
            .withArgs(mockCCIPRouterL2.address, eeseeAssetHub.address, vault, tokenIDsL2[1], assetAmounts[1])  
            
            const _ERC1155 = await hre.ethers.getContractFactory("Mock1155");
            const ERC1155 = await _ERC1155.deploy("", 0, 10000, royaltyCollector.address, 10000)
            await ERC1155.deployed()

            await compareErrors(
                ERC1155.safeTransferFrom(signer.address, vault, 0, 100, "0x"),
                '0x'//ERC1155: transfer to non-ERC1155Receiver implementer
            )
            await compareErrors(
                ERC1155.safeBatchTransferFrom(signer.address, vault, [0], [100], "0x"),
                '0x'//ERC1155: transfer to non-ERC1155Receiver implementer
            )

            const _Vault = await hre.ethers.getContractFactory("EeseeVault");
            const _vault = _Vault.attach(vault);

            let call = _vault.interface.encodeFunctionData('onERC1155Received', [zeroAddress, zeroAddress, 0, 0, "0x"]);
            await compareErrors(
                eeseeAssetHub.multicall([{target: vault, callData: call}]),
                'FunctionBlacklisted(address,bytes4)'
            )

            call = _vault.interface.encodeFunctionData('onERC1155BatchReceived', [zeroAddress, zeroAddress, [0], [0], "0x"]);
            await compareErrors(
                eeseeAssetHub.multicall([{target: vault, callData: call}]),
                'FunctionBlacklisted(address,bytes4)'
            )

            await compareErrors(
                _vault.unstuck([], signer.address),
                'InvalidTokenIdsLength()'
            )
            await compareErrors(
                _vault.unstuck(tokenIds, zeroAddress),
                'InvalidRecipient()'
            )
            await compareErrors(
                _vault.unstuck(tokenIds, signer.address),
                'NoTokensStuck()'
            )

            call = _vault.interface.encodeFunctionData('unstuck', [tokenIds, acc2.address]);
            await expect(recipient.callExternal(_vault.address, call))
            .to.emit(eeseeAssetHub, "TransferBatch")
            .withArgs(_vault.address, _vault.address, acc2.address, ((i) => _compareArrays(i, tokenIds)), ((i) => _compareArrays(i, assetAmounts)))  
            
            assert.equal((await eeseeAssetHub.balanceOf(acc2.address, tokenIds[0])).toString(), assetAmounts[0], "balance not correct")
            assert.equal((await eeseeAssetHub.balanceOf(acc2.address, tokenIds[1])).toString(), assetAmounts[1], "balance not correct")
            
            const endodedAddr = abi.encode(['address'], [acc2.address])
            await eeseeAssetHub.connect(acc2).safeTransferFrom(acc2.address, vault, tokenIds[0], 100, endodedAddr)
            await _vault.connect(acc2).unstuck([tokenIds[0]], acc4.address)
            assert.equal((await eeseeAssetHub.balanceOf(acc4.address, tokenIds[0])).toString(), 100, "balance not correct")

            await compareErrors(
                _Vault.deploy(zeroAddress, oneAddress),
                'InvalidConstructor()'
            )

            await network.provider.send("evm_revert", [snapshotId])
    })

    it('Asset amount increases', async () => {
        await changeNetwork('testnet1');
        const _MockERC20 = await hre.ethers.getContractFactory('MockERC20')
        const mockERC20 = await _MockERC20.deploy(1000000)
        await mockERC20.deployed()
        await mockERC20.approve(eeseeAssetSpoke.address, 1000000)

        const asset = {
            token: mockERC20.address, 
            tokenID: 0, 
            amount: 500000, 
            assetType: 2, 
            data:'0x'
        }
        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

        await eeseeAssetSpoke.wrap(
            [asset],
            [],
            signer.address,
            abi.encode(['uint256'], [1000000]),
            {value: 10000}
        )
        assert.equal((await eeseeAssetSpoke.assetsStorage(assetHash)).amount, asset.amount, "Amount is correct")

        await eeseeAssetSpoke.wrap(
            [asset],
            [],
            signer.address,
            abi.encode(['uint256'], [1000000]),
            {value: 10000}
        )
        assert.equal((await eeseeAssetSpoke.assetsStorage(assetHash)).amount, asset.amount + asset.amount, "Amount is correct")
    })

    it('Works with fee on transfer tokens', async () => {
        await ERC20WithFeeOnTransfer.approve(eeseeAssetSpoke.address, 500000)

        const asset = {
            token: ERC20WithFeeOnTransfer.address, 
            tokenID: 0, 
            amount: 500000, 
            assetType: 2, 
            data:'0x'
        }
        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

        const newAssetAmount = asset.amount * 0.99
        await expect(eeseeAssetSpoke.wrap(
            [asset],
            [],
            signer.address,
            abi.encode(['uint256'], [1000000]),
            {value: 10000}
        )).to.emit(eeseeAssetSpoke, "Wrap").withArgs(((i) => compareObjects(convertArrayToObject(i), {...asset, amount: newAssetAmount})), assetHash, signer.address)
     
        asset.amount = newAssetAmount
        assert.equal((await eeseeAssetSpoke.assetsStorage(assetHash)).amount.toString(), asset.amount.toString(), "Amount is correct")

        // ==== unwrap ====

        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'address'], 
            [[assetHash], [asset.amount], signer.address]
        )

        let any2EVMMessage = {
            messageId,
            sourceChainSelector: abi.decode(["uint64"], chainSelectorL2)[0],
            sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetHub.address]),
            data: payload,
            destTokenAmounts: []
        }
        const snapshotId = await network.provider.send('evm_snapshot')
        await expect(mockCCIPRouterL1.routeMessage(
            any2EVMMessage,
            5000,
            1000000,
            eeseeAssetSpoke.address,
            {gasLimit: 30000000}
        )).to.emit(eeseeAssetSpoke, 'Unwrap')
        .withArgs(((i) => compareObjects(convertArrayToObject(i), asset)), assetHash, signer.address)
        await network.provider.send("evm_revert", [snapshotId])

        await ERC20WithFeeOnTransfer.toggleStuck()
        await expect(mockCCIPRouterL1.routeMessage(
            any2EVMMessage,
            5000,
            1000000,
            eeseeAssetSpoke.address,
            {gasLimit: 30000000}
        )).to.emit(eeseeAssetSpoke, 'Stuck')
        .withArgs(((i) => compareObjects(convertArrayToObject(i), asset)), assetHash, signer.address, anyValue)
        await ERC20WithFeeOnTransfer.toggleStuck()

        await expect(eeseeAssetSpoke.unstuck(assetHash, signer.address))
        .to.emit(eeseeAssetSpoke, 'Unstuck')
        .withArgs(((i) => compareObjects(convertArrayToObject(i), asset)), assetHash, signer.address, signer.address)
    })

    it('eeseeAssetHub execute, different royalties', async () => {
            await changeNetwork('testnet2');
            // Empty royalties array
            let asset = {
                token: NFT.address, 
                tokenID: 5, 
                amount: 1, 
                assetType: 0, 
                data:'0x'
            }
            let assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

            let assetData = abi.encode(
                ['address[]', 'uint256[]'],
                [[], []]
            )

            let tokenIDL2 = await eeseeAssetHub.getTokenId({source: {chainSelector:chainSelectorL1, _address: eeseeAssetSpoke.address}, assetHash})
            let payload = abi.encode(
                ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
                [[assetHash], [asset.amount], [assetData], [[eeseeAssetSpoke.address, '0x']], signer.address]
            )
            any2EVMMessage = {
                messageId,
                sourceChainSelector: abi.decode(["uint64"], chainSelectorL1)[0],
                sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
                data: payload,
                destTokenAmounts: []
            }
            let assetWithSource = {
                "source": {
                    "chainSelector": chainSelectorL1,
                    "_address": eeseeAssetSpoke.address
                },
                "assetHash": assetHash
            }

            let call = eeseeAssetHub.interface.encodeFunctionData('_setRoyalty', [0, "0x"]);
            await compareErrors(
                eeseeAssetHub.multicall([{target: eeseeAssetHub.address, callData: call}]),
                'FunctionBlacklisted(address,bytes4)'
            );
            
            call = eeseeAssetHub.interface.encodeFunctionData('setApprovalForAll', [eeseeAssetHub.address, true]);// ERC1155: setting approval status for self
            await compareErrors(
                eeseeAssetHub.multicall([{target: eeseeAssetHub.address, callData: call}]),
                'MulticallReverted(bytes)'
            );

            await expect(mockCCIPRouterL2.routeMessage(
                any2EVMMessage,
                5000,
                1000000,
                eeseeAssetHub.address,
                {gasLimit: 30000000}
            ))
            .to.emit(eeseeAssetHub, 'Wrap').withArgs(
                tokenIDL2, 
                asset.amount,
                ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetWithSource.assetHash))
            )
            .to.emit(eeseeAssetHub, 'MulticallFailed')
            // Royalties array length > 1, 2 valid recipiens
            asset = {
                token: NFT.address, 
                tokenID: 6, 
                amount: 1, 
                assetType: 0, 
                data:'0x'
            }
            assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

            assetData = abi.encode(
                ['address[]', 'uint256[]'],
                [[acc7.address, acc8.address, zeroAddress], [100000, 150000, 0]]
            )
            tokenIDL2 = await eeseeAssetHub.getTokenId({source: {chainSelector:chainSelectorL1, _address: eeseeAssetSpoke.address}, assetHash})
            payload = abi.encode(
                ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
                [[assetHash], [asset.amount], [assetData], [], signer.address]
            )
            any2EVMMessage = {
                messageId,
                sourceChainSelector: abi.decode(["uint64"], chainSelectorL1)[0],
                sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
                data: payload,
                destTokenAmounts: []
            }
            assetWithSource = {
                "source": {
                    "chainSelector": chainSelectorL1,
                    "_address": eeseeAssetSpoke.address
                },
                "assetHash": assetHash
            }
            await expect(mockCCIPRouterL2.routeMessage(
                any2EVMMessage,
                5000,
                1000000,
                eeseeAssetHub.address,
                {gasLimit: 30000000}
            ))
            .to.emit(eeseeAssetHub, 'Wrap').withArgs(
                tokenIDL2, 
                asset.amount,
                ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetWithSource.assetHash)), 
            )
            // Royalties array length != recipiens length
            asset = {
                token: NFT.address, 
                tokenID: 70, 
                amount: 1, 
                assetType: 0, 
                data:'0x'
            }
            assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

            assetData = abi.encode(
                ['address[]', 'uint256[]'],
                [[acc7.address, acc8.address, acc9.address], [100000, 150000]]
            )
            tokenIDL2 = await eeseeAssetHub.getTokenId({source: {chainSelector:chainSelectorL1, _address: eeseeAssetSpoke.address}, assetHash})
            payload = abi.encode(
                ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
                [[assetHash], [asset.amount], [assetData], [], signer.address]
            )
            any2EVMMessage = {
                messageId,
                sourceChainSelector: abi.decode(["uint64"], chainSelectorL1)[0],
                sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
                data: payload,
                destTokenAmounts: []
            }
            assetWithSource = {
                "source": {
                    "chainSelector": chainSelectorL1,
                    "_address": eeseeAssetSpoke.address
                },
                "assetHash": assetHash
            }
            await expect(mockCCIPRouterL2.routeMessage(
                any2EVMMessage,
                5000,
                1000000,
                eeseeAssetHub.address,
                {gasLimit: 30000000}
            ))
            .to.emit(eeseeAssetHub, 'Wrap').withArgs(
                tokenIDL2, 
                asset.amount,
                ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetWithSource.assetHash)), 
            )
            assert.equal((await eeseeAssetHub.royaltyInfo(tokenIDL2, 10000))[0], zeroAddress, "royalty set")
            assert.equal((await eeseeAssetHub.royaltyInfo(tokenIDL2, 10000))[1].toString(), "0", "royalty set")

            // Royalties array length > 1, 0 valid recipiens
            asset = {
                token: NFT.address, 
                tokenID: 7, 
                amount: 1, 
                assetType: 0, 
                data:'0x06'
            }
            assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

            assetData = abi.encode(
                ['address[]', 'uint256[]'],
                [[acc8.address, zeroAddress], [0, 10000]]
            )
            tokenIDL2 = await eeseeAssetHub.getTokenId({source: {chainSelector:chainSelectorL1, _address: eeseeAssetSpoke.address}, assetHash})
            payload = abi.encode(
                ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
                [[assetHash], [asset.amount], [assetData], [], signer.address]
            )
            any2EVMMessage = {
                messageId,
                sourceChainSelector: abi.decode(["uint64"], chainSelectorL1)[0],
                sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
                data: payload,
                destTokenAmounts: []
            }
            assetWithSource = {
                "source": {
                    "chainSelector": chainSelectorL1,
                    "_address": eeseeAssetSpoke.address
                },
                "assetHash": assetHash
            }
            await expect(mockCCIPRouterL2.routeMessage(
                any2EVMMessage,
                5000,
                1000000,
                eeseeAssetHub.address,
                {gasLimit: 30000000}
            ))
            .to.emit(eeseeAssetHub, 'Wrap').withArgs(
                tokenIDL2, 
                asset.amount,
                ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetWithSource.assetHash))
            )
             // Royalties array length == 1, feeNumerators[0] == 0
            asset = {
                token: NFT.address, 
                tokenID: 7, 
                amount: 1, 
                assetType: 0, 
                data:'0x01'
            }
            assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))

            assetData = abi.encode(
                ['address[]', 'uint256[]'],
                [[acc8.address], [0]]
            )
            tokenIDL2 = await eeseeAssetHub.getTokenId({source: {chainSelector:chainSelectorL1, _address: eeseeAssetSpoke.address}, assetHash})
            payload = abi.encode(
                ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
                [[assetHash], [asset.amount], [assetData], [], signer.address]
            )
            any2EVMMessage = {
                messageId,
                sourceChainSelector: abi.decode(["uint64"], chainSelectorL1)[0],
                sender: ethers.utils.defaultAbiCoder.encode(['address'], [eeseeAssetSpoke.address]),
                data: payload,
                destTokenAmounts: []
            }
            assetWithSource = {
                "source": {
                    "chainSelector": chainSelectorL1,
                    "_address": eeseeAssetSpoke.address
                },
                "assetHash": assetHash
            }        
            await expect(mockCCIPRouterL2.routeMessage(
                any2EVMMessage,
                5000,
                1000000,
                eeseeAssetHub.address,
                {gasLimit: 30000000}
            ))
            .to.emit(eeseeAssetHub, 'Wrap').withArgs(
                tokenIDL2, 
                asset.amount,
                ((i) => compareObjects(convertArrayToObject(i.source), assetWithSource.source) && compareObjects(i.assetHash, assetWithSource.assetHash))
            )
    })

    it('pausable', async () => {
        await changeNetwork('testnet1');

        await compareErrors(
            eeseeAssetSpoke.pause(),
            'CallerNotAuthorized()'
        )
        await expect(eeseeAssetSpoke.connect(acc9).pause()).to.emit(eeseeAssetSpoke, "Paused").withArgs(acc9.address)

        await compareErrors(
            eeseeAssetSpoke.wrap(
                [],
                [],
                zeroAddress,
                abi.encode(['uint256'], [1000000]),
            ),
            '0x'//Pausable: paused
        )
        await compareErrors(
            eeseeAssetSpoke.unpause(),
            'CallerNotAuthorized()'
        )
        await expect(eeseeAssetSpoke.connect(acc9).unpause()).to.emit(eeseeAssetSpoke, "Unpaused").withArgs(acc9.address)

        
        await compareErrors(
            eeseeAssetSpoke.wrap(
                [],
                [],
                zeroAddress,
                abi.encode(['uint256'], [1000000]),
            ),
            'InvalidAssetsLength()'//Other error
        )
    })

    it('reverts constructor spoke', async () => {
        const _eeseeAssetSpoke = await hre.ethers.getContractFactory('EeseeAssetSpokeCCIP')
        await compareErrors(
            _eeseeAssetSpoke.deploy(
                accessManager.address,
    
                chainSelectorL1,
                {chainSelector: chainSelectorL2, _address: zeroAddress},
    
                oneAddress,
                oneAddress,
                oneAddress,
            ),
            'InvalidConstructor()'
        )

        await compareErrors(
            _eeseeAssetSpoke.deploy(
                accessManager.address,
    
                chainSelectorL1,
                {chainSelector: chainSelectorL2, _address: oneAddress},
    
                oneAddress,
                zeroAddress,
                oneAddress
            ),
            'InvalidRouter()'
        )

        await compareErrors(
            _eeseeAssetSpoke.deploy(
                accessManager.address,
    
                chainSelectorL1,
                {chainSelector: chainSelectorL2, _address: oneAddress},
    
                zeroAddress,
                oneAddress,
                oneAddress,
            ),
            'InvalidConstructor()'
        )

        await compareErrors(
            _eeseeAssetSpoke.deploy(
                zeroAddress,
             
                chainSelectorL1,
                {chainSelector: chainSelectorL2, _address: oneAddress},
    
                oneAddress,
                oneAddress,
                oneAddress,
            ),
            'InvalidAccessManager()'
        )
    })

    it('reverts constructor hub', async () => {
            await changeNetwork('testnet2');
            const _eeseeAssetHub = await hre.ethers.getContractFactory('EeseeAssetHubCCIP')

            await compareErrors(
                _eeseeAssetHub.deploy(
                    '/',
                    'prefix',
                    'description',
                    
                    zeroAddress,
                    oneAddress,
                ),
                'InvalidRouter()'
            )

            await compareErrors(
                _eeseeAssetHub.deploy(
                    '',
                    'prefix',
                    'description',
    
                    oneAddress,
                    oneAddress,
                ),
                'InvalidConstructor()'
            )

            await compareErrors(
                _eeseeAssetHub.deploy(
                    '/',
                    'prefix',
                    '',
    
                    oneAddress,
                    oneAddress,
                ),
                'InvalidConstructor()'
            )
    })

    it('supports interface', async () => {
        await changeNetwork('testnet1');
        assert.equal(await eeseeAssetSpoke.supportsInterface('0x4e2312e0'), true, 'eeseeAssetSpoke supports ERC1155Receiver')
        assert.equal(await eeseeAssetSpoke.supportsInterface('0x01ffc9a7'), true, 'eeseeAssetSpoke supports ERC165')
        assert.equal(await eeseeAssetSpoke.supportsInterface('0x85572ffb'), true, 'eeseeAssetSpoke supports IAny2EVMMessageReceiver')
        assert.equal(await eeseeAssetSpoke.supportsInterface('0x00000001'), false, 'eeseeAssetSpoke supports 0x00000001')

            await changeNetwork('testnet2');
            assert.equal(await eeseeAssetHub.supportsInterface('0x2a55205a'), true, 'eeseeAssetHub supports ERC2981')
            assert.equal(await eeseeAssetHub.supportsInterface('0x4e2312e0'), true, 'eeseeAssetHub supports ERC1155Receiver')
            assert.equal(await eeseeAssetHub.supportsInterface('0x0e89341c'), true, 'eeseeAssetHub supports ERC1155')
            assert.equal(await eeseeAssetHub.supportsInterface('0xd9b67a26'), true, 'eeseeAssetHub supports ERC1155')
            assert.equal(await eeseeAssetHub.supportsInterface('0x85572ffb'), true, 'eeseeAssetHub supports IAny2EVMMessageReceiver')
            assert.equal(await eeseeAssetHub.supportsInterface('0x00000001'), false, 'eeseeAssetHub supports 0x00000001')
    })

    it('removes multicall blacklist', async () => {
        const _testMulticallExternal = await hre.ethers.getContractFactory('TestMulticallExternal')
        multicallExternal =  await _testMulticallExternal.deploy()
        await multicallExternal.deployed()

        await expect(multicallExternal.addToBlacklist(zeroAddress, "0x00010203")).to.emit(multicallExternal, "AddToBlacklist").withArgs(zeroAddress, "0x00010203")
        await expect(multicallExternal.addToBlacklist(zeroAddress, "0x00010203")).to.not.emit(multicallExternal, "AddToBlacklist")

        await compareErrors(
            multicallExternal.multicall([{target: zeroAddress, callData: "0x00010203"}]),
            'FunctionBlacklisted(address,bytes4)'
        )
     
        await expect(multicallExternal.removeFromBlacklist(zeroAddress, "0x00010203")).to.emit(multicallExternal, "RemoveFromBlacklist").withArgs(zeroAddress, "0x00010203")
        await expect(multicallExternal.removeFromBlacklist(zeroAddress, "0x00010203")).to.not.emit(multicallExternal, "RemoveFromBlacklist")

        await compareErrors(
            multicallExternal.multicall([{target: zeroAddress, callData: "0x00010203"}]),
            'MulticallReverted(bytes)'
        )
    })
})