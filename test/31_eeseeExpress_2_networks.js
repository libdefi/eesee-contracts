  const { anyValue } = require('@nomicfoundation/hardhat-chai-matchers/withArgs');
  const { expect } = require('chai');
  const { ethers, network } = require('hardhat');
  const assert = require('assert');
  const { keccak256 } = require('@ethersproject/keccak256')
  const compareErrors = require('./utils/compareErrors');
  
  ((network.name == 'multipleNetworks') ? describe : describe.skip)('Cross Chain', function () {
    let ERC20;
    let NFT;
    let signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector;
    let royaltyEninge;
    let mockAxelarGasServiceL1
    let mockAxelarGatewayL1
    let mockAxelarGasServiceL2
    let mockAxelarGatewayL2
    let eeseeExpress
    const abi = ethers.utils.defaultAbiCoder
    const chainSelectorL2 = 'l2'
    const chainSelectorL1 = 'l1'

    const zeroBytes32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
    const eAddress= '0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE'
    const zeroAddress = '0x0000000000000000000000000000000000000000'
    const oneAddress = '0x0000000000000000000000000000000000000001'
    let accessManager
    let mockSquidL1
    let mockSquidMulticallL1
    let mockSquidL2
    let mockSquidMulticallL2
    let mockUSDCL1
    let mockUSDCL2
    let mockNativeSwap
    let NonERC20
    let NonERC20_2
    tokenSymbolUSDC = 'USDC'
    tokenSymbolNonERC20 = 'Non-ERC20'
    tokenSymbolNonERC20_2 = 'Non-ERC20_2'
    async function changeNetwork(name){
        await hre.changeNetwork(name);
        if(signer == undefined){
            [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector] = await ethers.getSigners();
        }else{
            let [_signer, _acc2, _acc3, _acc4, _acc5, _acc6, _acc7, _acc8, _acc9, _feeCollector] = await ethers.getSigners();
            if(
                _signer.address != signer.address ||
                _acc2.address != acc2.address ||
                _acc3.address != acc3.address ||
                _acc4.address != acc4.address ||
                _acc5.address != acc5.address ||
                _acc6.address != acc6.address ||
                _acc7.address != acc7.address ||
                _acc8.address != acc8.address ||
                _acc9.address != acc9.address ||
                _feeCollector.address != feeCollector.address
            ) throw Error("Accounts for networks not match");
            [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector] = [_signer, _acc2, _acc3, _acc4, _acc5, _acc6, _acc7, _acc8, _acc9, _feeCollector]
        }
    }
    this.beforeAll(async() => {   
        await changeNetwork('testnet1');
        const _AssetTransfer = await hre.ethers.getContractFactory('AssetTransfer');
        const assetTransfer = await _AssetTransfer.deploy()
        await assetTransfer.deployed()
        const _MockERC20 = await hre.ethers.getContractFactory('MockERC20');
        const _NFT = await hre.ethers.getContractFactory('EeseeNFT');
        const _royaltyEngine = await hre.ethers.getContractFactory('MockRoyaltyEngine');

        const _mockNativeSwap = await hre.ethers.getContractFactory("MockNativeSwap");
        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");
        const NonERC20CompliantToken = await hre.ethers.getContractFactory("NonERC20CompliantToken");
        const NonERC20CompliantToken2 = await hre.ethers.getContractFactory("NonERC20CompliantToken2");
        accessManager =  await _eeseeAccessManager.deploy()
        await accessManager.deployed()

        // PAUSER_ROLE
        await accessManager.grantRole('0x65d7a28e3265b37a6474929f336521b332c1681b933f6cb9f3376673440d862a', acc9.address)

        let _ESE = await hre.ethers.getContractFactory("ESE");
        mockUSDCL1 = await _ESE.deploy([{
            cliff: 0,
            duration: 0,
            TGEMintShare: 10000
        }
        ])
        await mockUSDCL1.deployed()
        await mockUSDCL1.addVestingBeneficiaries(0, [{addr: signer.address, amount: '200000000000000000000'}])
        await mockUSDCL1.initialize(0)

        ERC20 = await _MockERC20.deploy('20000000000000000000000000000')
        await ERC20.deployed()

        NonERC20 = await NonERC20CompliantToken.deploy('20000000000000000000000000000')
        await NonERC20.deployed()
        NonERC20_2 = await NonERC20CompliantToken2.deploy('20000000000000000000000000000')
        await NonERC20_2.deployed()

        royaltyEninge = await _royaltyEngine.deploy();
        await royaltyEninge.deployed()

        NFT = await _NFT.deploy()
        await NFT.initialize({
            name: 'APES',
            symbol:'bayc',
            baseURI: '/',
            revealedURI: '',
            contractURI:'/',
            royaltyReceiver: zeroAddress,
            royaltyFeeNumerator: 0
        },10, signer.address)

        await NFT.deployed()

        let _eeseeExpress = await hre.ethers.getContractFactory('EeseeExpress')
        let _mockAxelarGasService = await hre.ethers.getContractFactory('MockAxelarGasService')
        let _mockAxelarGateway = await hre.ethers.getContractFactory('MockAxelarGateway')
        let _mockSquid = await hre.ethers.getContractFactory("MockSquid");
        let _mockSquidMulticall = await hre.ethers.getContractFactory("MockSquidMulticall");

        mockAxelarGatewayL1 = await _mockAxelarGateway.deploy()
        await mockAxelarGatewayL1.deployed()
        mockAxelarGasServiceL1 = await _mockAxelarGasService.deploy(acc8.address)
        await mockAxelarGasServiceL1.deployed()
        mockSquidMulticallL1 = await _mockSquidMulticall.deploy()
        await mockSquidMulticallL1.deployed()
        mockSquidL1 = await _mockSquid.deploy(mockAxelarGatewayL1.address, mockAxelarGasServiceL1.address, mockSquidMulticallL1.address)
        await mockSquidL1.deployed()
        eeseeExpress = await _eeseeExpress.deploy(
            accessManager.address,
            mockSquidL1.address,
            zeroAddress
        )
        await eeseeExpress.deployed()

        // Deploy params: (string memory name, string memory symbol, uint8 decimals, uint256 cap, address tokenAddress, uint256 mintLimit)
        let deployTokenParams = abi.encode(
            ['string', 'string', 'uint8', 'uint256', 'address', 'uint256'],
            ['USD Coin', tokenSymbolUSDC, 6, 0, mockUSDCL1.address, 0]
        )
        await mockAxelarGatewayL1.deployToken(deployTokenParams, zeroBytes32)

        deployTokenParams = abi.encode(
            ['string', 'string', 'uint8', 'uint256', 'address', 'uint256'],
            ['Non ERC20', tokenSymbolNonERC20, 18, 0, NonERC20.address, 0]
        )
        await mockAxelarGatewayL1.deployToken(deployTokenParams, zeroBytes32)

        deployTokenParams = abi.encode(
            ['string', 'string', 'uint8', 'uint256', 'address', 'uint256'],
            ['Non ERC20 2', tokenSymbolNonERC20_2, 18, 0, NonERC20_2.address, 0]
        )
        await mockAxelarGatewayL1.deployToken(deployTokenParams, zeroBytes32)

        mockNativeSwap = await _mockNativeSwap.deploy(mockUSDCL1.address)
        await mockNativeSwap.deployed()
        await mockUSDCL1.transfer(mockNativeSwap.address, 100000)

            await changeNetwork('testnet2');
            _ESE = await hre.ethers.getContractFactory("ESE");
            _eeseeExpress = await hre.ethers.getContractFactory('EeseeExpress')
            _mockAxelarGasService = await hre.ethers.getContractFactory('MockAxelarGasService')
            _mockAxelarGateway = await hre.ethers.getContractFactory('MockAxelarGateway')
            _mockSquid = await hre.ethers.getContractFactory("MockSquid");
            _mockSquidMulticall = await hre.ethers.getContractFactory("MockSquidMulticall");

            mockAxelarGatewayL2 = await _mockAxelarGateway.deploy()
            await mockAxelarGatewayL2.deployed()
            mockAxelarGasServiceL2 = await _mockAxelarGasService.deploy(acc8.address)
            await mockAxelarGasServiceL2.deployed()
            mockSquidMulticallL2 = await _mockSquidMulticall.deploy()
            await mockSquidMulticallL2.deployed()
            mockSquidL2 = await _mockSquid.deploy(mockAxelarGatewayL2.address, mockAxelarGasServiceL2.address, mockSquidMulticallL2.address)
            await mockSquidL2.deployed()

            mockUSDCL2 = await _ESE.deploy([{
                cliff: 0,
                duration: 0,
                TGEMintShare: 10000
            }
            ])
            await mockUSDCL2.deployed()
            await mockUSDCL2.addVestingBeneficiaries(0, [{addr: signer.address, amount: '200000000000000000000'}])
            await mockUSDCL2.initialize(0)

            deployTokenParams = abi.encode(
                ['string', 'string', 'uint8', 'uint256', 'address', 'uint256'],
                ['USD Coin', tokenSymbolUSDC, 6, 0, mockUSDCL2.address, 0]
            )
            await mockAxelarGatewayL2.deployToken(deployTokenParams, zeroBytes32)
            await mockUSDCL2.transfer(mockAxelarGatewayL2.address, 100000)
    })

    it('callBridgeCall', async () => {
        await changeNetwork('testnet1');
        const amount = 100
        const value = 100000000

        const transferEncodedData = mockUSDCL1.interface.encodeFunctionData('transfer', [mockSquidL1.address, 0])
        const _payload = abi.encode(
            ['address', 'uint256'],
            [mockUSDCL1.address, 1]
        )

        const call = {
            callType: 1, //FullTokenBalance
            target: mockUSDCL1.address,
            value: 0,
            callData: transferEncodedData,
            payload: _payload,
        }

        await mockUSDCL1.approve(eeseeExpress.address, amount)
        await compareErrors(
            eeseeExpress.callBridgeCall(
                {chain: chainSelectorL2, _address: mockSquidL1.address},
                tokenSymbolUSDC,
                {
                    token: mockUSDCL1.address,
                    amount: amount,
                    permit: '0x'
                },
    
                [call],
                [],
                zeroAddress,
                true,
                {value: value}
            ),
            'InvalidRecipient()'
        )

        let payload = abi.encode(['tuple(uint8, address, uint256, bytes, bytes)[]', 'address'], [[], acc2.address])
        let payloadHash = keccak256(payload)
        await expect(eeseeExpress.callBridgeCall(
            {chain: chainSelectorL2, _address: mockSquidL1.address},
            tokenSymbolUSDC,
            {
                token: mockUSDCL1.address,
                amount: amount,
                permit: '0x'
            },

            [call],
            [],
            acc2.address,            
            true,
            {value: value}
        )).to.emit(mockAxelarGatewayL1, 'ContractCallWithToken')
        .withArgs(
            mockSquidL1.address, 
            chainSelectorL2, 
            mockSquidL1.address.toLowerCase(),
            payloadHash,
            payload,
            tokenSymbolUSDC,
            amount
        )
        .and.to.emit(mockAxelarGasServiceL1, 'NativeGasPaidForExpressCallWithToken')
        .withArgs(
            mockSquidL1.address,
            chainSelectorL2,
            mockSquidL1.address.toLowerCase(),
            payloadHash,
            tokenSymbolUSDC,
            amount,
            value,
            signer.address
        )

        const nativeSwap = mockNativeSwap.interface.encodeFunctionData('nativeSwap', [mockSquidL1.address])
        const nativeCall = {
            callType: 2, //FullNativeBalance
            target: mockNativeSwap.address,
            value: 0,
            callData: nativeSwap,
            payload: '0x',
        }

        const mockCall = {
            callType: 0,
            target: mockNativeSwap.address,
            value: 0,
            callData: '0x',
            payload: '0x',
        }

        payload = abi.encode(['tuple(uint8, address, uint256, bytes, bytes)[]', 'address'], [[Object.values(mockCall)], acc2.address])
        payloadHash = keccak256(payload)
        await expect(eeseeExpress.callBridgeCall(
            {chain: chainSelectorL2, _address: mockSquidL1.address},
            tokenSymbolUSDC,
            {
                token: eAddress,
                amount: amount,
                permit: '0x'
            },

            [nativeCall],
            [mockCall],
            acc2.address,
            true,
            {value: value}
        )).to.emit(mockAxelarGatewayL1, 'ContractCallWithToken').withArgs(
            mockSquidL1.address, 
            chainSelectorL2, 
            mockSquidL1.address.toLowerCase(),
            payloadHash,
            payload,
            tokenSymbolUSDC,
            amount // This is amount because exchange rate is 1 to 1
        )
        .and.to.emit(mockAxelarGasServiceL1, 'NativeGasPaidForExpressCallWithToken')
        .withArgs(
            mockSquidL1.address,
            chainSelectorL2,
            mockSquidL1.address.toLowerCase(),
            payloadHash,
            tokenSymbolUSDC,
            amount,
            anyValue,
            signer.address
        )
    })

    it('Allows NonERC20 compliant tokens', async () => {
        const amount = 100
        const value = 100000000

        const transferEncodedData = NonERC20.interface.encodeFunctionData('transfer', [mockSquidL1.address, 0])
        const _payload = abi.encode(
            ['address', 'uint256'],
            [NonERC20.address, 1]
        )

        const call = {
            callType: 1, //FullTokenBalance
            target: NonERC20.address,
            value: 0,
            callData: transferEncodedData,
            payload: _payload,
        }

        await NonERC20.approve(eeseeExpress.address, amount)
        let payload = abi.encode(['tuple(uint8, address, uint256, bytes, bytes)[]', 'address'], [[], acc2.address])
        let payloadHash = keccak256(payload)
        await expect(eeseeExpress.callBridgeCall(
            {chain: chainSelectorL2, _address: mockSquidL1.address},
            tokenSymbolNonERC20,
            {
                token: NonERC20.address,
                amount: amount,
                permit: '0x'
            },

            [call],
            [],
            acc2.address,
            true,
            {value: value}
        )).to.emit(mockAxelarGatewayL1, 'ContractCallWithToken')
        .withArgs(
            mockSquidL1.address, 
            chainSelectorL2, 
            mockSquidL1.address.toLowerCase(),
            payloadHash,
            payload,
            tokenSymbolNonERC20,
            amount
        )
        .and.to.emit(mockAxelarGasServiceL1, 'NativeGasPaidForExpressCallWithToken')
        .withArgs(
            mockSquidL1.address,
            chainSelectorL2,
            mockSquidL1.address.toLowerCase(),
            payloadHash,
            tokenSymbolNonERC20,
            amount,
            value,
            signer.address
        )
    })

    it('Allows NonERC20 compliant tokens', async () => {
        const amount = 100
        const value = 100000000

        const transferEncodedData = NonERC20_2.interface.encodeFunctionData('transfer', [mockSquidL1.address, 0])
        const _payload = abi.encode(
            ['address', 'uint256'],
            [NonERC20_2.address, 1]
        )

        const call = {
            callType: 1, //FullTokenBalance
            target: NonERC20.address,
            value: 0,
            callData: transferEncodedData,
            payload: _payload,
        }

        await NonERC20_2.approve(eeseeExpress.address, amount)
        await compareErrors(
            eeseeExpress.callBridgeCall(
                {chain: chainSelectorL2, _address: mockSquidL1.address},
                tokenSymbolNonERC20_2,
                {
                    token: NonERC20_2.address,
                    amount: amount,
                    permit: '0x'
                },
    
                [call],
                [],
                acc2.address,
                true,
                {value: value}
            ),
            '0x' //"SafeERC20: ERC20 operation did not succeed"
        )
    })

    it('executeExpress', async () => {
            await changeNetwork('testnet2');
            const amount = 100

            let _payload = abi.encode(
                ['address'],
                [mockUSDCL2.address]
            )
            const call1 = {
                callType: 3, //CollectTokenBalance
                target: zeroAddress,
                value: 0,
                callData: '0x',
                payload: _payload,
            }

            const transferEncodedData = mockUSDCL2.interface.encodeFunctionData('transfer', [acc7.address, 0])
            _payload = abi.encode(
                ['address', 'uint256'],
                [mockUSDCL2.address, 1]
            )
            const call2 = {
                callType: 1, //FullTokenBalance
                target: mockUSDCL2.address,
                value: 0,
                callData: transferEncodedData,
                payload: _payload,
            }

            let payload = abi.encode(['tuple(uint8, address, uint256, bytes, bytes)[]', 'address'], [[Object.values(call1), Object.values(call2)], acc2.address])
            let payloadHash = keccak256(payload)
            // Approve call
            const commandId = keccak256(abi.encode(['uint256'],['6']))
            const approveContractCallWithMintParams = abi.encode(
                ['string', 'string', 'address', 'bytes32', 'string', 'uint256', 'bytes32', 'uint256'],
                [chainSelectorL1, mockSquidL2.address.toLowerCase(), mockSquidL2.address, payloadHash, tokenSymbolUSDC, amount, commandId, 1]
            )
            await mockAxelarGatewayL2.approveContractCallWithMint(
                approveContractCallWithMintParams,
                commandId
            )

            await expect(mockSquidL2.executeWithToken(
                commandId,
                chainSelectorL1,
                mockSquidL2.address.toLowerCase(),
                payload,
                tokenSymbolUSDC,
                amount
            )).to.emit(mockSquidL2, 'CrossMulticallExecuted')
            .withArgs(payloadHash) 
            assert.equal(await mockUSDCL2.balanceOf(acc7.address), amount, "not correct")
    })

    it('pausable', async () => {
        await changeNetwork('testnet1');
        await compareErrors(
            eeseeExpress.pause(),
            'CallerNotAuthorized()'
        )
        await expect(eeseeExpress.connect(acc9).pause()).to.emit(eeseeExpress, "Paused").withArgs(acc9.address)

        await compareErrors(
            eeseeExpress.callBridgeCall(
                {chain: "", _address: zeroAddress},
                "",
                {
                    token: zeroAddress,
                    amount: 0,
                    permit: '0x'
                },
                [],
                [],
                zeroAddress,
                true
            ),
            '0x' //Pausable: paused
        )

        await compareErrors(
            eeseeExpress.unpause(),
            'CallerNotAuthorized()'
        )
        await expect(eeseeExpress.connect(acc9).unpause()).to.emit(eeseeExpress, "Unpaused").withArgs(acc9.address)

        await compareErrors(
            eeseeExpress.callBridgeCall(
                {chain: "", _address: zeroAddress},
                "",
                {
                    token: zeroAddress,
                    amount: 0,
                    permit: '0x'
                },
                [],
                [],
                zeroAddress,
                true
            ),
            'InvalidRecipient()' //Another error
        )
    })

    it('eeseeExpress', async () => {
        const _eeseeExpress = await hre.ethers.getContractFactory('EeseeExpress')
        await compareErrors(
            _eeseeExpress.deploy(
                accessManager.address,
                zeroAddress,
                zeroAddress
            ),
            'InvalidConstructor()'
        )

        await compareErrors(
            _eeseeExpress.deploy(
                zeroAddress,
                oneAddress,
                oneAddress
            ),
            'InvalidAccessManager()'
        )
    })
})
