const { expect } = require("chai");
const assert = require("assert");
const { time } = require("@nomicfoundation/hardhat-network-helpers");
const { ethers, network } = require("hardhat");

((network.name != 'multipleNetworks') ? describe : describe.skip)("eeseeAirdrop", function () {
    let eeseeAirdrop;
    let ERC20
    let accessManager
    let signer, acc2, acc3, acc4;
    const zeroAddress = "0x0000000000000000000000000000000000000000"

    this.beforeAll(async() => {
        [signer, acc2, acc3, acc4] = await ethers.getSigners()
        const _eeseeAirdrop = await hre.ethers.getContractFactory("EeseeAirdrop");
        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");
        const _MockERC20 = await hre.ethers.getContractFactory("MockERC20");

        accessManager =  await _eeseeAccessManager.deploy()
        await accessManager.deployed()

        // AIRDROP_ROLE
        await accessManager.grantRole('0x3a2f235c9daaf33349d300aadff2f15078a89df81bcfdd45ba11c8f816bddc6f', acc3.address)

        ERC20 = await _MockERC20.deploy('20000000000000000000000000000')
        await ERC20.deployed()

        eeseeAirdrop = await _eeseeAirdrop.deploy(ERC20.address, accessManager.address);
        await eeseeAirdrop.deployed();

        await ERC20.transfer(eeseeAirdrop.address, 1000)
    })

    it('successfuly airdrops', async () => {
        await expect(eeseeAirdrop.airdrop([{recipient: acc2.address, amount: 100}, {recipient: acc4.address, amount: 300}]))
            .to.be.revertedWithCustomError(eeseeAirdrop, "CallerNotAuthorized")
        await expect(eeseeAirdrop.connect(acc3).airdrop([{recipient: acc2.address, amount: 100}, {recipient: acc4.address, amount: 300}]))
            .to.not.be.reverted
        
        assert.equal((await ERC20.balanceOf(acc2.address)).toString(), '100')
        assert.equal((await ERC20.balanceOf(acc4.address)).toString(), '300')
        assert.equal((await ERC20.balanceOf(eeseeAirdrop.address)).toString(), '600')
    })

    it('reverts constructor', async () => {
        const _eeseeAirdrop = await hre.ethers.getContractFactory("EeseeAirdrop")
        await expect(_eeseeAirdrop.deploy(zeroAddress, accessManager.address))
            .to.be.revertedWithCustomError(eeseeAirdrop, "InvalidESE")

        await expect(_eeseeAirdrop.deploy(ERC20.address, zeroAddress))
            .to.be.revertedWithCustomError(eeseeAirdrop, "InvalidAccessManager")
    })
});
