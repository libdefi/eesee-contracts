const {
    time,
    loadFixture,
  } = require('@nomicfoundation/hardhat-network-helpers');
  const fs = require('fs')
    const path = require('path')
  const { anyValue } = require('@nomicfoundation/hardhat-chai-matchers/withArgs');
  const { expect } = require('chai');
  const { ethers, network } = require('hardhat');
  const assert = require('assert');
  const { StandardMerkleTree } = require('@openzeppelin/merkle-tree');
  const { getContractAddress } = require('@ethersproject/address')
  const { keccak256 } = require('@ethersproject/keccak256')
  const createPermit = require('./utils/createPermit')
  const { BigNumber } = ethers
  const {convertArrayToObject, compareObjects} = require('./utils/compareObjects');
  ((network.name != 'multipleNetworks') ? describe: describe.skip)('ESE forwarders', function () {
    let ERC20;
    let NFT;
    let signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector;
    let royaltyEninge;
    let eeseeAssetHub
    let mockAxelarGasService
    let mockAxelarGatewayL1
    let mockCCIPRouterL1
    let tokenId
    let mockInterchainTokenServiceL1
    let onRamp
    let offRamp
    let onRampImplementation
    const abi = ethers.utils.defaultAbiCoder
    const chainSelectorL2 = abi.encode(['string'], ['l2'])
    const chainSelectorL1 = abi.encode(['string'], ['l1'])
    const tokenSymbolUSDC = 'USDC'
    const zeroBytes32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
    const zeroAddress = '0x0000000000000000000000000000000000000000'
    const oneAddress = '0x0000000000000000000000000000000000000001'
    const mainnetAddresses = JSON.parse(fs.readFileSync(path.resolve(__dirname, './constants/mainnetAddresses.json'), 'utf-8'))
    let snapshotId
    let onRampInterface
    const messageId = '0x0000000000000000000000000000000000000000000000000000000000000023'
    this.beforeAll(async() => {   
        [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector, royaltyCollector] = await ethers.getSigners()
        const _MockERC20 = await hre.ethers.getContractFactory('MockERC20');
        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");

        const _eeseeOnRampImplementation = await hre.ethers.getContractFactory('EeseeOnRampImplementationTransfer')
        const _eeseeOnRamp = await hre.ethers.getContractFactory('EeseeOnRampProxy')

        accessManager = await _eeseeAccessManager.deploy();
        await accessManager.deployed()

        ERC20 = await _MockERC20.deploy('20000000000000000000000000000')
        await ERC20.deployed()

        onRampImplementation = await _eeseeOnRampImplementation.deploy(ERC20.address)
        await onRampImplementation.deployed()

        onRamp = await _eeseeOnRamp.deploy(
            onRampImplementation.address,
            abi.encode(['tuple(bytes chainSelector, address _address)'], [{chainSelector: "0x", _address: acc7.address}]),
            accessManager.address
        )
        await onRamp.deployed()

        onRampInterface = await ethers.getContractAt("EeseeOnRampImplementationTransfer", onRamp.address);
    })

    it('forwardsL1', async () => {
        await expect(onRamp.forward("0x")).to.be.revertedWithCustomError(onRampImplementation, "InsufficientBalance")

        await ERC20.transfer(onRamp.address, 100)
        assert.equal(await ERC20.balanceOf(onRamp.address), 100, "Incorrect balance")

        await expect(onRamp.forward("0x", {value: 10000})).to.be.revertedWithCustomError(onRampImplementation, "InvalidMsgValue")
        await expect(onRamp.forward("0x")).to.emit(onRampInterface, "Forward").withArgs(
            ((i) => (compareObjects(convertArrayToObject(i), {chainSelector: "0x", _address: acc7.address}))),
            100
        )
        assert.equal(await ERC20.balanceOf(onRamp.address), 0, "Incorrect balance")
        assert.equal(await ERC20.balanceOf(acc7.address), 100, "Incorrect balance")
    })

    it('reverts constructor', async () => {
        await expect(onRampImplementation.initialize(
            abi.encode(['tuple(bytes chainSelector, address _address)'], [{chainSelector: "0x01", _address: zeroAddress}])
        )).to.be.revertedWithCustomError(onRampImplementation, "InvalidOffRamp")

        await expect(onRampImplementation.initialize(
            abi.encode(['tuple(bytes chainSelector, address _address)'], [{chainSelector: "0x01", _address: oneAddress}])
        )).to.be.revertedWithCustomError(onRampImplementation, "InvalidChainSelector")

        const _eeseeForwarderOnRamp = await hre.ethers.getContractFactory('EeseeOnRampImplementationTransfer')
        await expect(_eeseeForwarderOnRamp.deploy(
            zeroAddress
        )).to.be.revertedWithCustomError(_eeseeForwarderOnRamp, "InvalidESE")
    })
})