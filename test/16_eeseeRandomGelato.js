const { expect } = require("chai");
const assert = require("assert");
const { time } = require("@nomicfoundation/hardhat-network-helpers");
const { ethers, network } = require("hardhat");

((network.name != 'multipleNetworks') ? describe : describe.skip)("eeseeRandomGelato", function () {
    let ERC20;
    let mockVRF;
    let signer, acc2, acc3
    let accessManager
    let eeseeRandom

    const zeroAddress = "0x0000000000000000000000000000000000000000"
    const oneAddress = "0x0000000000000000000000000000000000000001"
    let snapshotId
    let abi = ethers.utils.defaultAbiCoder;
    this.beforeAll(async() => {
        [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector, royaltyCollector] = await ethers.getSigners()
        const _MockERC20 = await hre.ethers.getContractFactory("MockERC20");
        const _mockVRF = await hre.ethers.getContractFactory("MockVRFGelatoOperator");
        const _eeseeRandom = await hre.ethers.getContractFactory("EeseeRandomGelato");
        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");

        const gelatoGenesisTimestamp = 1692803367;
        if (await time.latest() < gelatoGenesisTimestamp){
            await time.setNextBlockTimestamp(gelatoGenesisTimestamp);
        }

        ERC20 = await _MockERC20.deploy('20000000000000000000000000000')
        await ERC20.deployed()

        mockVRF = await _mockVRF.deploy()
        await mockVRF.deployed()

        accessManager = await _eeseeAccessManager.connect(acc8).deploy();
        await accessManager.deployed()

        eeseeRandom = await _eeseeRandom.deploy(
            mockVRF.address,
            accessManager.address
        )
        await eeseeRandom.deployed()
        
        await mockVRF.setConsumer(eeseeRandom.address)

        // REQUEST_RANDOM_ROLE
        await accessManager.connect(acc8).grantRole('0xde459b4f900be5c5c23ad44aead99bf85b310f34ced089e5c232217a86ec12c3', signer.address)
    })

    it('calls requestRandom', async () => {
        const timestamp = (await ethers.provider.getBlock()).timestamp
        const tx = await eeseeRandom.connect(acc3).requestRandom()
        await expect(tx)
        .to.emit(eeseeRandom, "RequestedRandomness")
        .to.emit(eeseeRandom, "RequestRandom")

        const result = await tx.wait();
        const round = result.events[0].args.round;
        const data = result.events[0].args.data;
        let decoded = abi.decode(["uint", "bytes"], data)
        let requestId = decoded[0]

        await expect(mockVRF.fulfillWords(requestId, round, data)).to.emit(eeseeRandom, "FulfillRandom")
        assert.notEqual((await eeseeRandom.random(0)).timestamp, 0, 'timestamp')

        assert.equal((await eeseeRandom.getRandomFromTimestamp(timestamp)).timestamp.toString(), (await eeseeRandom.random(0)).timestamp.toString())
    })

    it('cannot call requestRandom if time is not passed', async () => {
        const checkerResponse = await eeseeRandom.checker();
        expect(checkerResponse.canExec).to.equal(false);
        expect(checkerResponse.execPayload).to.equal("0xda9f7550");
        await expect(eeseeRandom.connect(acc3).requestRandom())
        .to.be.revertedWithCustomError(eeseeRandom, "RandomRequestNotNeeded")
    })

    it('calls requestRandom after time and getRandomFromTimestamp is ok', async () => {
        await time.increase(12*60*60)
        const timestamp1 = (await ethers.provider.getBlock()).timestamp
        assert.equal((await eeseeRandom.getRandomFromTimestamp(timestamp1)).timestamp.toString(), '0')
        const tx = await eeseeRandom.connect(acc3).requestRandom()
        await expect(tx)
        .to.emit(eeseeRandom, "RequestedRandomness")
        .to.emit(eeseeRandom, "RequestRandom")
        
        const result = await tx.wait();
        const round = result.events[0].args.round;
        const data = result.events[0].args.data;
        let decoded = abi.decode(["uint", "bytes"],data)
        let requestId = decoded[0]

        await expect(mockVRF.fulfillWords(requestId, round, data)).to.emit(eeseeRandom, "FulfillRandom")
        const timestamp2 = (await ethers.provider.getBlock()).timestamp
        assert.equal((await eeseeRandom.random(1)).timestamp, timestamp2, 'timestamp')
        assert.equal((await eeseeRandom.getRandomFromTimestamp(timestamp1)).timestamp.toString(), timestamp2, 'getRandomFromTimestamp is correct')
        assert.equal((await eeseeRandom.getRandomFromTimestamp(timestamp2)).timestamp.toString(), '0', 'getRandomFromTimestamp is correct')
    })

    it('can call requestRandomImmediate if admin', async () => {
        await expect(eeseeRandom.connect(acc2).requestRandomImmediate()).to.be.revertedWithCustomError(eeseeRandom, "CallerNotAuthorized")
        await expect(eeseeRandom.connect(acc3).requestRandom())
        .to.be.revertedWithCustomError(eeseeRandom, "RandomRequestNotNeeded")
        await expect(eeseeRandom.connect(signer).requestRandomImmediate())
        .to.emit(eeseeRandom, "RequestedRandomness")
        .to.emit(eeseeRandom, "RequestRandom")
    })

    it('changesRandomRequestInterval', async () => {
        await expect(eeseeRandom.connect(acc2).changeRandomRequestInterval('1')).to.be.revertedWithCustomError(eeseeRandom, "CallerNotAuthorized")
        await expect(eeseeRandom.connect(acc8).changeRandomRequestInterval(24*60*60 + 1)).to.be.revertedWithCustomError(eeseeRandom, "InvalidRandomRequestInterval")
        const newValue = 3600
        await expect(eeseeRandom.connect(acc8).changeRandomRequestInterval(newValue))
        .to.emit(eeseeRandom, "ChangeRandomRequestInterval")
        .withArgs(12*60*60, newValue)
        assert.equal(newValue, await eeseeRandom.randomRequestInterval(), "randomRequestInterval has changed")
    })

    it('reverts constructor with InvalidAccessManager', async () => {
        const _eeseeRandom = await hre.ethers.getContractFactory("EeseeRandomGelato");
        await expect(_eeseeRandom.deploy(
            mockVRF.address,
            zeroAddress
        )).to.be.revertedWithCustomError(eeseeRandom, "InvalidAccessManager")
        await eeseeRandom.deployed()
    })

    it('reverts constructor with InvalidOperator', async () => {
        const _eeseeRandom = await hre.ethers.getContractFactory("EeseeRandomGelato");
        await expect(_eeseeRandom.deploy(
            zeroAddress,
            accessManager.address
        )).to.be.revertedWithCustomError(eeseeRandom, "InvalidOperator")
        await eeseeRandom.deployed()
    })
});
