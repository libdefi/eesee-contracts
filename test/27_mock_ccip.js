const {
    time,
    loadFixture,
  } = require('@nomicfoundation/hardhat-network-helpers');
  const fs = require('fs')
    const path = require('path')
  const { anyValue } = require('@nomicfoundation/hardhat-chai-matchers/withArgs');
  const { expect } = require('chai');
  const { ethers, network } = require('hardhat');
  const assert = require('assert');
  const { StandardMerkleTree } = require('@openzeppelin/merkle-tree');
  const { getContractAddress } = require('@ethersproject/address')
  const { keccak256 } = require('@ethersproject/keccak256')
  const createPermit = require('./utils/createPermit')
  const { BigNumber } = ethers
  const {convertArrayToObject, compareObjects} = require('./utils/compareObjects');

  ((network.name != 'multipleNetworks') ? describe : describe.skip)('Cross Chain', function () {
    let ERC20;
    let NFT;
    let signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector;
    let royaltyEninge;
    let eeseeAssetSpoke
    let accessManager
    let eeseeAssetHub
    let mockAxelarGasService
    let mockCCIPRouter
    let mockOnRamp
    let mockOffRamp
    const abi = ethers.utils.defaultAbiCoder
    const chainSelectorL2 = abi.encode(['uint64'], [2])
    const chainSelectorL1 = abi.encode(['uint64'], [1])
    const tokenSymbolUSDC = 'USDC'
    const zeroBytes32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
    const zeroAddress = '0x0000000000000000000000000000000000000000'
    const oneAddress = '0x0000000000000000000000000000000000000001'
    const mainnetAddresses = JSON.parse(fs.readFileSync(path.resolve(__dirname, './constants/mainnetAddresses.json'), 'utf-8'))
    let snapshotId
    this.beforeAll(async() => {   
        [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector, royaltyCollector] = await ethers.getSigners()
        ticketBuyers = [acc2,acc3, acc4, acc5, acc6,  acc7]
        const _AssetTransfer = await hre.ethers.getContractFactory('AssetTransfer');
        const assetTransfer = await _AssetTransfer.deploy()
        await assetTransfer.deployed()
        const _NFT = await hre.ethers.getContractFactory('EeseeNFT');
        const _royaltyEngine = await hre.ethers.getContractFactory('MockRoyaltyEngine');
        const _eeseeAssetSpoke = await hre.ethers.getContractFactory('EeseeAssetSpokeCCIP')
        const _eeseeAssetHub = await hre.ethers.getContractFactory('EeseeAssetHubCCIP')
        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");
        const _mockCCIPRouter = await hre.ethers.getContractFactory('CCIPRouter')
        const _onRamp = await hre.ethers.getContractFactory('OnRamp')
        const _offRamp = await hre.ethers.getContractFactory('OffRamp')
        accessManager =  await _eeseeAccessManager.deploy()
        await accessManager.deployed()

        // TRANSMITTER_ROLE
        await accessManager.grantRole('0xd6a0f92c822ccba0fa91b30f08f085e68bc8eb3bb140aa16f8dc33ea47eb6cf2', acc9.address)

        royaltyEninge = await _royaltyEngine.deploy();
        await royaltyEninge.deployed()

        NFT = await _NFT.deploy()
        await NFT.initialize({
            name: 'APES',
            symbol:'bayc',
            baseURI: '/',
            revealedURI: '',
            contractURI:'/',
            royaltyReceiver: zeroAddress,
            royaltyFeeNumerator: 0
        },10, signer.address)

        await NFT.deployed()

        const transactionCount = await signer.getTransactionCount()
        const futureRouter = getContractAddress({
            from: signer.address,
            nonce: transactionCount + 2
        })

        mockOnRamp = await _onRamp.deploy({
            chainSelector: abi.decode(['uint64'], chainSelectorL1)[0],
            destChainSelector: abi.decode(['uint64'], chainSelectorL2)[0]
        }, {
            router: futureRouter,
            maxDataBytes: '30000',
            maxPerMsgGasLimit: '2000000',
        })
        await mockOnRamp.deployed()

        mockOffRamp = await _offRamp.deploy(accessManager.address, futureRouter)
        await mockOffRamp.deployed()

        mockCCIPRouter = await _mockCCIPRouter.deploy(
            [{destChainSelector:abi.decode(['uint64'], chainSelectorL2)[0], onRamp:mockOnRamp.address}], 
            [{sourceChainSelector:abi.decode(['uint64'], chainSelectorL1)[0], offRamp:mockOffRamp.address}],
            mainnetAddresses.WETH
        )
        await mockCCIPRouter.deployed()

        eeseeAssetHub = await _eeseeAssetHub.deploy(
            'img/',
            'prefix#',
            '_description_',

            mockCCIPRouter.address,
            zeroAddress
        )
        await eeseeAssetHub.deployed()
            
        eeseeAssetSpoke = await _eeseeAssetSpoke.deploy(
            accessManager.address,

            chainSelectorL1,
            {chainSelector: chainSelectorL2, _address: eeseeAssetHub.address},

            royaltyEninge.address,
            mockCCIPRouter.address,
            zeroAddress
        )
        await eeseeAssetSpoke.deployed()
    })

    it('wrap', async () => {
        gasLimit = 1000000
        gasPaid = gasLimit*10
        const sourceL1 = {
            chainSelector: chainSelectorL1,
            _address: eeseeAssetSpoke.address
        }

        const asset = {
            token: NFT.address,
            tokenID: 1,
            amount: 1,
            assetType: 0,
            data: "0x"
        }

        const assetHash = keccak256(abi.encode(['address', 'uint256', 'uint8'], [asset.token, asset.tokenID, asset.assetType]))
        const tokenIDL2 = await eeseeAssetSpoke["getTokenIdL2((address,uint256,uint256,uint8,bytes))"](asset)
        const transferNFTEncodedData = eeseeAssetHub.interface.encodeFunctionData('safeTransferFrom', [eeseeAssetHub.address, acc8.address, tokenIDL2, asset.amount, "0x"])
        await NFT.approve(eeseeAssetSpoke.address, 1)

        const royalties = await royaltyEninge.getRoyalty(asset.token, asset.tokenID, 10000)
        const assetData = abi.encode(
            ['address[]', 'uint256[]'],
            [royalties.recipients, royalties.amounts]
        )
        const payload = abi.encode(
            ['bytes32[]', 'uint256[]', 'bytes[]', 'tuple(address, bytes)[]', 'address'],
            [[assetHash], [asset.amount], [assetData], [[eeseeAssetHub.address, transferNFTEncodedData]], acc2.address]
        )
        const ccipObj = {
            sourceChainSelector: abi.decode(['uint64'], chainSelectorL1)[0],
            sender: eeseeAssetSpoke.address,
            receiver: eeseeAssetHub.address,
            sequenceNumber: 1,
            gasLimit: gasLimit,
            strict: false,
            nonce: 1,
            feeToken: mainnetAddresses.WETH,
            feeTokenAmount: gasPaid,
            data: payload,
            tokenAmounts: [],
            sourceTokenData: [],
            messageId: zeroBytes32
        }
        await expect(eeseeAssetSpoke.wrap(
            [asset],
            [{target: eeseeAssetHub.address, callData: transferNFTEncodedData}],
            acc2.address,
            abi.encode(['uint256'], [gasLimit]),
            {value: gasPaid}
        ))
        .to.emit(eeseeAssetSpoke, 'CrosschainSend').withArgs(
            await eeseeAssetSpoke.CCIP_CALLER_IDENTIFIER(), 
            ((i) => compareObjects(convertArrayToObject(i), {chainSelector: chainSelectorL2, _address: eeseeAssetHub.address})), 
            anyValue
        )
        .to.emit(mockOnRamp, 'CCIPSendRequested').withArgs(
            ((i) => compareObjects({...convertArrayToObject(i), messageId:zeroBytes32}, ccipObj))
        )

        // ================ L2 ================
        
        await expect(mockOffRamp.connect(acc9).transmit(ccipObj))
            .to.emit(mockCCIPRouter, "MessageExecuted").withArgs(ccipObj.messageId, ccipObj.sourceChainSelector, mockOffRamp.address, anyValue)
            .to.emit(eeseeAssetHub, 'CrosschainReceive')
            .withArgs(
                await eeseeAssetHub.CCIP_CALLER_IDENTIFIER(), 
                ((i) => compareObjects(convertArrayToObject(i), sourceL1)), 
                abi.encode(['bytes32', 'tuple[](address, uint256)'], [ccipObj.messageId, []])
            )
    })

})