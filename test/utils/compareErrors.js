const sha3 = require("js-sha3");
const assert = require('assert');
module.exports =  async function compareErrors(tx, errName){
    try{
        await tx
    }catch(err){
        if(errName == "0x"){
            const id = err.toString().indexOf(',"result":"') + 11
            const errorSignature = err.toString().slice(id, id + 2);
            assert.equal(errorSignature, "0x", "error is incorrect")
        }else{
            const id = err.toString().indexOf(',"result":"') + 11
            const errorSignature = err.toString().slice(id, id + 10);
            const errorSignature_ = "0x" + sha3.keccak_256(errName).slice(0, 8)
            assert.equal(errorSignature, errorSignature_, "error is incorrect")
        }
    }
}