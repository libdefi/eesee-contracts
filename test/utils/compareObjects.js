
const { ethers } = require("hardhat")
const { BigNumber } = ethers

function convertArrayToObject(arr) {
  const obj = {};
  
  // Iterate over the keys of the array
  for (const key in arr) {
    // Check if the key is not a number (which means it's a string key)
    if (isNaN(key)) {
      obj[key] = arr[key];
    }
  }
  return obj;
}

// A helper function to determine if a value is a BigNumber
function isBigNumber(value) {
    return ethers.BigNumber.isBigNumber(value);
  }
  
  // A helper function to determine if a value can be represented as a BigNumber
  function canBeBigNumber(value) {
    try {
      ethers.BigNumber.from(value);
      return true;
    } catch {
      return false;
    }
}

function compareObjects(obj1, obj2) {
    if (typeof obj1 !== 'object' || typeof obj2 !== 'object' || obj1 === null || obj2 === null) {
      if (isBigNumber(obj1) || isBigNumber(obj2) || canBeBigNumber(obj1) || canBeBigNumber(obj2)) {
        // Compare numeric values by converting them to BigNumbers
        try {
          const bn1 = ethers.BigNumber.from(obj1);
          const bn2 = ethers.BigNumber.from(obj2);
          return bn1.eq(bn2);
        } catch {
          // If conversion fails, the values are not equal numbers
          return false;
        }
      }
      // For non-numeric values, use strict equality
      return obj1 === obj2;
    }
  
    const keys1 = Object.keys(obj1);
    const keys2 = Object.keys(obj2);
  
    if (keys1.length !== keys2.length) {
      return false;
    }
  
    for (const key of keys1) {
      if (!keys2.includes(key)) {
        return false;
      }
      if (!compareObjects(obj1[key], obj2[key])) {
        return false;
      }
    }
  
    return true;
  }


module.exports = {convertArrayToObject, compareObjects}