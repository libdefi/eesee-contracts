const { expect } = require("chai");
const assert = require("assert");
const { time } = require("@nomicfoundation/hardhat-network-helpers");
const { ethers, network } = require("hardhat");

((network.name != 'multipleNetworks') ? describe : describe.skip)("eeseeRandomChainlink", function () {
    let ERC20;
    let mockVRF;
    let signer, acc2, acc3
    let accessManager
    let eeseeRandom

    const zeroAddress = "0x0000000000000000000000000000000000000000"
    const oneAddress = "0x0000000000000000000000000000000000000001"
    let snapshotId
    this.beforeAll(async() => {
        [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector, royaltyCollector] = await ethers.getSigners()
        const _MockERC20 = await hre.ethers.getContractFactory("MockERC20");
        const _mockVRF = await hre.ethers.getContractFactory("MockVRFCoordinator");
        const _eeseeRandom = await hre.ethers.getContractFactory("EeseeRandomChainlink");
        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");
        

        ERC20 = await _MockERC20.deploy('20000000000000000000000000000')
        await ERC20.deployed()

        mockVRF = await _mockVRF.deploy()
        await mockVRF.deployed()

        accessManager = await _eeseeAccessManager.connect(acc8).deploy();
        await accessManager.deployed()

        eeseeRandom = await _eeseeRandom.deploy(
            {
                vrfCoordinator: mockVRF.address,
                keyHash: '0x0000000000000000000000000000000000000000000000000000000000000000',
                minimumRequestConfirmations: 1,
                callbackGasLimit: 100000
            },
            accessManager.address
        )
        await eeseeRandom.deployed()
        // REQUEST_RANDOM_ROLE
        await accessManager.connect(acc8).grantRole('0xde459b4f900be5c5c23ad44aead99bf85b310f34ced089e5c232217a86ec12c3', signer.address)
    })

    it('calls performUpkeep', async () => {
        const timestamp = (await ethers.provider.getBlock()).timestamp
        await expect(eeseeRandom.connect(acc3)["performUpkeep(bytes)"]("0x00"))
        .to.emit(eeseeRandom, "RequestRandom")

        await expect(mockVRF.fulfillWords(0)).to.emit(eeseeRandom, "FulfillRandom").withArgs(0)
        assert.notEqual((await eeseeRandom.random(0)).timestamp, 0, 'timestamp')
        assert.notEqual((await eeseeRandom.random(0)).timestamp, 0, 'timestamp')

        assert.equal((await eeseeRandom.getRandomFromTimestamp(timestamp)).timestamp.toString(), (await eeseeRandom.random(0)).timestamp.toString())
    })

    it('cannot call performUpkeep if time is not passed', async () => {
        expect((await eeseeRandom.checkUpkeep("0x00")).upkeepNeeded).to.equal(false);
        await expect(eeseeRandom.connect(acc3)["performUpkeep(bytes)"]("0x00"))
        .to.be.revertedWithCustomError(eeseeRandom, "RandomRequestNotNeeded")
    })

    it('calls performUpkeep after time and getRandomFromTimestamp is ok', async () => {
        await time.increase(12*60*60)
        const timestamp1 = (await ethers.provider.getBlock()).timestamp
        assert.equal((await eeseeRandom.getRandomFromTimestamp(timestamp1)).timestamp.toString(), '0')
        await expect(eeseeRandom.connect(acc3)["performUpkeep(bytes)"]("0x00"))
        .to.emit(eeseeRandom, "RequestRandom")

        await expect(mockVRF.fulfillWords(1)).to.emit(eeseeRandom, "FulfillRandom").withArgs(1)
        const timestamp2 = (await ethers.provider.getBlock()).timestamp
        assert.equal((await eeseeRandom.random(1)).timestamp, timestamp2, 'timestamp')
        assert.equal((await eeseeRandom.getRandomFromTimestamp(timestamp1)).timestamp.toString(), timestamp2, 'getRandomFromTimestamp is correct')
        assert.equal((await eeseeRandom.getRandomFromTimestamp(timestamp2)).timestamp.toString(), '0', 'getRandomFromTimestamp is correct')
    })

    it('can call requestRandomImmediate if admin', async () => {
        await expect(eeseeRandom.connect(acc2).requestRandomImmediate()).to.be.revertedWithCustomError(eeseeRandom, "CallerNotAuthorized")
        await expect(eeseeRandom.connect(acc3)["performUpkeep(bytes)"]("0x00"))
        .to.be.revertedWithCustomError(eeseeRandom, "RandomRequestNotNeeded")
        await expect(eeseeRandom.connect(signer).requestRandomImmediate())
        .to.emit(eeseeRandom, "RequestRandom")
    })

    it('changesRandomRequestInterval', async () => {
        await expect(eeseeRandom.connect(acc2).changeRandomRequestInterval('1')).to.be.revertedWithCustomError(eeseeRandom, "CallerNotAuthorized")
        await expect(eeseeRandom.connect(acc8).changeRandomRequestInterval(24*60*60 + 1)).to.be.revertedWithCustomError(eeseeRandom, "InvalidRandomRequestInterval")
        const newValue = 3600
        await expect(eeseeRandom.connect(acc8).changeRandomRequestInterval(newValue))
        .to.emit(eeseeRandom, "ChangeRandomRequestInterval")
        .withArgs(12*60*60, newValue)
        assert.equal(newValue, await eeseeRandom.randomRequestInterval(), "randomRequestInterval has changed")
    })

    it('changesCallbackGasLimit', async () => {
        await expect(eeseeRandom.connect(acc2).changeCallbackGasLimit('1')).to.be.revertedWithCustomError(eeseeRandom, "CallerNotAuthorized")
        
        const newValue = 1111111
        await expect(eeseeRandom.connect(acc8).changeCallbackGasLimit(newValue))
        .to.emit(eeseeRandom, "ChangeCallbackGasLimit")
        .withArgs(100000, newValue)
        
        assert.equal(newValue, await eeseeRandom.callbackGasLimit(), "callbackGasLimit has changed")
    })

    it('reverts constructor', async () => {
        const _eeseeRandom = await hre.ethers.getContractFactory("EeseeRandomChainlink");
        await expect(_eeseeRandom.deploy(
            {
                vrfCoordinator: mockVRF.address,
                keyHash: '0x0000000000000000000000000000000000000000000000000000000000000000',
                minimumRequestConfirmations: 1,
                callbackGasLimit: 100000
            },
            zeroAddress
        )).to.be.revertedWithCustomError(eeseeRandom, "InvalidAccessManager")
        await eeseeRandom.deployed()
    })
});
