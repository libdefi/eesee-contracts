const {
    time,
    loadFixture,
  } = require('@nomicfoundation/hardhat-network-helpers');
  const fs = require('fs')
    const path = require('path')
  const { anyValue } = require('@nomicfoundation/hardhat-chai-matchers/withArgs');
  const { expect } = require('chai');
  const { ethers, network } = require('hardhat');
  const assert = require('assert');
  const { StandardMerkleTree } = require('@openzeppelin/merkle-tree');
  const { getContractAddress } = require('@ethersproject/address')
  const { keccak256 } = require('@ethersproject/keccak256')
  const createPermit = require('./utils/createPermit')
  const { BigNumber } = ethers
  const {convertArrayToObject, compareObjects} = require('./utils/compareObjects');
  ((network.name != 'multipleNetworks') ? describe : describe.skip)('Cross Chain', function () {
    let ERC20;
    let NFT;
    let signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector;
    let royaltyEninge;
    let mockAxelarGasService
    let mockAxelarGateway
    let eeseeExpress
    const chainSelectorL2 = 'l2'
    const chainSelectorL1 = 'l1'
    const abi = ethers.utils.defaultAbiCoder
    const zeroBytes32 = '0x0000000000000000000000000000000000000000000000000000000000000000'
    const eAddress= '0xEeeeeEeeeEeEeeEeEeEeeEEEeeeeEeeeeeeeEEeE'
    const zeroAddress = '0x0000000000000000000000000000000000000000'
    const oneAddress = '0x0000000000000000000000000000000000000001'
    let accessManager
    let mockSquid
    let mockSquidMulticall
    let mockUSDC
    let mockNativeSwap
    let NonERC20
    let NonERC20_2
    tokenSymbolUSDC = 'USDC'
    tokenSymbolNonERC20 = 'Non-ERC20'
    tokenSymbolNonERC20_2 = 'Non-ERC20_2'
    this.beforeAll(async() => {   
        [signer, acc2, acc3, acc4, acc5, acc6, acc7, acc8, acc9, feeCollector, royaltyCollector] = await ethers.getSigners()
        ticketBuyers = [acc2,acc3, acc4, acc5, acc6,  acc7]
        const _AssetTransfer = await hre.ethers.getContractFactory('AssetTransfer');
        const assetTransfer = await _AssetTransfer.deploy()
        await assetTransfer.deployed()
        const _MockERC20 = await hre.ethers.getContractFactory('MockERC20');
        const _NFT = await hre.ethers.getContractFactory('EeseeNFT');
        const _royaltyEngine = await hre.ethers.getContractFactory('MockRoyaltyEngine');

        const _eeseeExpress = await hre.ethers.getContractFactory('EeseeExpress')
        const _mockAxelarGasService = await hre.ethers.getContractFactory('MockAxelarGasService')
        const _mockAxelarGateway = await hre.ethers.getContractFactory('MockAxelarGateway')
        const _mockSquid = await hre.ethers.getContractFactory("MockSquid");
        const _mockSquidMulticall = await hre.ethers.getContractFactory("MockSquidMulticall");
        const _mockNativeSwap = await hre.ethers.getContractFactory("MockNativeSwap");
        const _ESE = await hre.ethers.getContractFactory("ESE");
        const _eeseeAccessManager = await hre.ethers.getContractFactory("EeseeAccessManager");
        const NonERC20CompliantToken = await hre.ethers.getContractFactory("NonERC20CompliantToken");
        const NonERC20CompliantToken2 = await hre.ethers.getContractFactory("NonERC20CompliantToken2");
        accessManager =  await _eeseeAccessManager.deploy()
        await accessManager.deployed()

        // PAUSER_ROLE
        await accessManager.grantRole('0x65d7a28e3265b37a6474929f336521b332c1681b933f6cb9f3376673440d862a', acc9.address)

        mockUSDC = await _ESE.deploy([{
            cliff: 0,
            duration: 0,
            TGEMintShare: 10000
        }
        ])
        await mockUSDC.deployed()
        await mockUSDC.addVestingBeneficiaries(0, [{addr: signer.address, amount: '200000000000000000000'}])
        await mockUSDC.initialize(0)

        ERC20 = await _MockERC20.deploy('20000000000000000000000000000')
        await ERC20.deployed()

        NonERC20 = await NonERC20CompliantToken.deploy('20000000000000000000000000000')
        await NonERC20.deployed()
        NonERC20_2 = await NonERC20CompliantToken2.deploy('20000000000000000000000000000')
        await NonERC20_2.deployed()

        royaltyEninge = await _royaltyEngine.deploy();
        await royaltyEninge.deployed()

        NFT = await _NFT.deploy()
        await NFT.initialize({
            name: 'APES',
            symbol:'bayc',
            baseURI: '/',
            revealedURI: '',
            contractURI:'/',
            royaltyReceiver: zeroAddress,
            royaltyFeeNumerator: 0
        },10, signer.address)

        await NFT.deployed()

        mockAxelarGateway = await _mockAxelarGateway.deploy()
        await mockAxelarGateway.deployed()
        mockAxelarGasService = await _mockAxelarGasService.deploy(acc8.address)
        await mockAxelarGasService.deployed()

        mockSquidMulticall = await _mockSquidMulticall.deploy()
        await mockSquidMulticall.deployed()

        mockSquid = await _mockSquid.deploy(mockAxelarGateway.address, mockAxelarGasService.address, mockSquidMulticall.address)
        await mockSquid.deployed()
        
        eeseeExpress = await _eeseeExpress.deploy(
            accessManager.address,
            mockSquid.address,
            zeroAddress
        )
        await eeseeExpress.deployed()

        // Deploy params: (string memory name, string memory symbol, uint8 decimals, uint256 cap, address tokenAddress, uint256 mintLimit)
        let deployTokenParams = abi.encode(
            ['string', 'string', 'uint8', 'uint256', 'address', 'uint256'],
            ['USD Coin', tokenSymbolUSDC, 6, 0, mockUSDC.address, 0]
        )
        await mockAxelarGateway.deployToken(deployTokenParams, zeroBytes32)

        deployTokenParams = abi.encode(
            ['string', 'string', 'uint8', 'uint256', 'address', 'uint256'],
            ['Non ERC20', tokenSymbolNonERC20, 18, 0, NonERC20.address, 0]
        )
        await mockAxelarGateway.deployToken(deployTokenParams, zeroBytes32)

        deployTokenParams = abi.encode(
            ['string', 'string', 'uint8', 'uint256', 'address', 'uint256'],
            ['Non ERC20 2', tokenSymbolNonERC20_2, 18, 0, NonERC20_2.address, 0]
        )
        await mockAxelarGateway.deployToken(deployTokenParams, zeroBytes32)

        mockNativeSwap = await _mockNativeSwap.deploy(mockUSDC.address)
        await mockNativeSwap.deployed()
        await mockUSDC.transfer(mockNativeSwap.address, 100000)
    })

    it('callBridgeCall', async () => {
        const amount = 100
        const value = 100000000

        const transferEncodedData = mockUSDC.interface.encodeFunctionData('transfer', [mockSquid.address, 0])
        const _payload = abi.encode(
            ['address', 'uint256'],
            [mockUSDC.address, 1]
        )

        const call = {
            callType: 1, //FullTokenBalance
            target: mockUSDC.address,
            value: 0,
            callData: transferEncodedData,
            payload: _payload,
        }

        await mockUSDC.approve(eeseeExpress.address, amount)
        await expect(eeseeExpress.callBridgeCall(
            {chain: chainSelectorL2, _address: mockSquid.address},
            tokenSymbolUSDC,
            {
                token: mockUSDC.address,
                amount: amount,
                permit: '0x'
            },

            [call],
            [],
            zeroAddress,
            true,
            {value: value}
        )).to.be.revertedWithCustomError(eeseeExpress, "InvalidRecipient")

        let payload = abi.encode(['tuple(uint8, address, uint256, bytes, bytes)[]', 'address'], [[], acc2.address])
        let payloadHash = keccak256(payload)
        await expect(eeseeExpress.callBridgeCall(
            {chain: chainSelectorL2, _address: mockSquid.address},
            tokenSymbolUSDC,
            {
                token: mockUSDC.address,
                amount: amount,
                permit: '0x'
            },

            [call],
            [],
            acc2.address,
            true,
            {value: value}
        )).to.emit(mockAxelarGateway, 'ContractCallWithToken')
        .withArgs(
            mockSquid.address, 
            chainSelectorL2, 
            mockSquid.address.toLowerCase(),
            payloadHash,
            payload,
            tokenSymbolUSDC,
            amount
        )
        .and.to.emit(mockAxelarGasService, 'NativeGasPaidForExpressCallWithToken')
        .withArgs(
            mockSquid.address,
            chainSelectorL2,
            mockSquid.address.toLowerCase(),
            payloadHash,
            tokenSymbolUSDC,
            amount,
            value,
            signer.address
        )

        // Token is contract's balance, approval already received, with permit
        const correctPermit = await createPermit(signer, eeseeExpress, '40', '999999999999999999999999999999', mockUSDC)
        const params_ = ethers.utils.defaultAbiCoder.encode(
            ["uint256", "uint256", "uint8", "bytes32", "bytes32"],
            ['40', '999999999999999999999999999999', correctPermit.v, correctPermit.r, correctPermit.s]
          );


        await mockUSDC.transfer(eeseeExpress.address, amount)
        await eeseeExpress.callBridgeCall(
            {chain: chainSelectorL2, _address: mockSquid.address},
            tokenSymbolUSDC,
            {
                token: mockUSDC.address,
                amount: amount,
                permit: params_
            },

            [call],
            [],
            acc2.address,
            true,
            {value: value}
        )

        const nativeSwap = mockNativeSwap.interface.encodeFunctionData('nativeSwap', [mockSquid.address])
        const nativeCall = {
            callType: 2, //FullNativeBalance
            target: mockNativeSwap.address,
            value: 0,
            callData: nativeSwap,
            payload: '0x',
        }

        const mockCall = {
            callType: 0,
            target: mockNativeSwap.address,
            value: 0,
            callData: '0x',
            payload: '0x',
        }

        payload = abi.encode(['tuple(uint8, address, uint256, bytes, bytes)[]', 'address'], [[Object.values(mockCall)], acc2.address])
        payloadHash = keccak256(payload)
        await expect(eeseeExpress.callBridgeCall(
            {chain: chainSelectorL2, _address: mockSquid.address},
            tokenSymbolUSDC,
            {
                token: eAddress,
                amount: amount,
                permit: '0x'
            },

            [nativeCall],
            [mockCall],
            acc2.address,
            true,
            {value: value}
        )).to.emit(mockAxelarGateway, 'ContractCallWithToken').withArgs(
            mockSquid.address, 
            chainSelectorL2, 
            mockSquid.address.toLowerCase(),
            payloadHash,
            payload,
            tokenSymbolUSDC,
            amount // This is amount because exchange rate is 1 to 1
        )
        .and.to.emit(mockAxelarGasService, 'NativeGasPaidForExpressCallWithToken')
        .withArgs(
            mockSquid.address,
            chainSelectorL2,
            mockSquid.address.toLowerCase(),
            payloadHash,
            tokenSymbolUSDC,
            amount,
            anyValue,
            signer.address
        )
    })

    it('Allows NonERC20 compliant tokens', async () => {
        const amount = 100
        const value = 100000000

        const transferEncodedData = NonERC20.interface.encodeFunctionData('transfer', [mockSquid.address, 0])
        const _payload = abi.encode(
            ['address', 'uint256'],
            [NonERC20.address, 1]
        )

        const call = {
            callType: 1, //FullTokenBalance
            target: NonERC20.address,
            value: 0,
            callData: transferEncodedData,
            payload: _payload,
        }

        await NonERC20.approve(eeseeExpress.address, amount)
        let payload = abi.encode(['tuple(uint8, address, uint256, bytes, bytes)[]', 'address'], [[], acc2.address])
        let payloadHash = keccak256(payload)
        await expect(eeseeExpress.callBridgeCall(
            {chain: chainSelectorL2, _address: mockSquid.address},
            tokenSymbolNonERC20,
            {
                token: NonERC20.address,
                amount: amount,
                permit: '0x'
            },

            [call],
            [],
            acc2.address,
            true,
            {value: value}
        )).to.emit(mockAxelarGateway, 'ContractCallWithToken')
        .withArgs(
            mockSquid.address, 
            chainSelectorL2, 
            mockSquid.address.toLowerCase(),
            payloadHash,
            payload,
            tokenSymbolNonERC20,
            amount
        )
        .and.to.emit(mockAxelarGasService, 'NativeGasPaidForExpressCallWithToken')
        .withArgs(
            mockSquid.address,
            chainSelectorL2,
            mockSquid.address.toLowerCase(),
            payloadHash,
            tokenSymbolNonERC20,
            amount,
            value,
            signer.address
        )
    })

    it('Allows NonERC20 compliant tokens', async () => {
        const amount = 100
        const value = 100000000

        const transferEncodedData = NonERC20_2.interface.encodeFunctionData('transfer', [mockSquid.address, 0])
        const _payload = abi.encode(
            ['address', 'uint256'],
            [NonERC20_2.address, 1]
        )

        const call = {
            callType: 1, //FullTokenBalance
            target: NonERC20.address,
            value: 0,
            callData: transferEncodedData,
            payload: _payload,
        }

        await NonERC20_2.approve(eeseeExpress.address, amount)
        await expect(eeseeExpress.callBridgeCall(
            {chain: chainSelectorL2, _address: mockSquid.address},
            tokenSymbolNonERC20_2,
            {
                token: NonERC20_2.address,
                amount: amount,
                permit: '0x'
            },

            [call],
            [],
            acc2.address,
            true,
            {value: value}
        )).to.be.revertedWith("SafeERC20: ERC20 operation did not succeed")
    })

    it('executeExpress', async () => {
        const amount = 100

        let _payload = abi.encode(
            ['address'],
            [mockUSDC.address]
        )
        const call1 = {
            callType: 3, //CollectTokenBalance
            target: zeroAddress,
            value: 0,
            callData: '0x',
            payload: _payload,
        }

        const transferEncodedData = mockUSDC.interface.encodeFunctionData('transfer', [acc7.address, 0])
        _payload = abi.encode(
            ['address', 'uint256'],
            [mockUSDC.address, 1]
        )
        const call2 = {
            callType: 1, //FullTokenBalance
            target: mockUSDC.address,
            value: 0,
            callData: transferEncodedData,
            payload: _payload,
        }

        let payload = abi.encode(['tuple(uint8, address, uint256, bytes, bytes)[]', 'address'], [[Object.values(call1), Object.values(call2)], acc2.address])
        let payloadHash = keccak256(payload)
        // Approve call
        const commandId = keccak256(abi.encode(['uint256'],['6']))
        const approveContractCallWithMintParams = abi.encode(
            ['string', 'string', 'address', 'bytes32', 'string', 'uint256', 'bytes32', 'uint256'],
            [chainSelectorL1, mockSquid.address.toLowerCase(), mockSquid.address, payloadHash, tokenSymbolUSDC, amount, commandId, 1]
        )
        await mockAxelarGateway.approveContractCallWithMint(
            approveContractCallWithMintParams,
            commandId
        )

        await expect(mockSquid.executeWithToken(
            commandId,
            chainSelectorL1,
            mockSquid.address.toLowerCase(),
            payload,
            tokenSymbolUSDC,
            amount
        )).to.emit(mockSquid, 'CrossMulticallExecuted')
        .withArgs(payloadHash) 
        assert.equal(await mockUSDC.balanceOf(acc7.address), amount, "not correct")
    })

    it('pausable', async () => {
        await expect(eeseeExpress.pause()).to.be.revertedWithCustomError(eeseeExpress, "CallerNotAuthorized")
        await expect(eeseeExpress.connect(acc9).pause()).to.emit(eeseeExpress, "Paused").withArgs(acc9.address)

        await expect(eeseeExpress.callBridgeCall(
            {chain: "", _address: zeroAddress},
            "",
            {
                token: zeroAddress,
                amount: 0,
                permit: '0x'
            },
            [],
            [],
            zeroAddress,
            true
        )).to.be.revertedWith("Pausable: paused")

        await expect(eeseeExpress.unpause()).to.be.revertedWithCustomError(eeseeExpress, "CallerNotAuthorized")
        await expect(eeseeExpress.connect(acc9).unpause()).to.emit(eeseeExpress, "Unpaused").withArgs(acc9.address)

        await expect(eeseeExpress.callBridgeCall(
            {chain: "", _address: zeroAddress},
            "",
            {
                token: zeroAddress,
                amount: 0,
                permit: '0x'
            },
            [],
            [],
            zeroAddress,
            true
        )).to.be.revertedWithCustomError(eeseeExpress, "InvalidRecipient")
    })

    it('eeseeExpress', async () => {
        const _eeseeExpress = await hre.ethers.getContractFactory('EeseeExpress')
        await expect(_eeseeExpress.deploy(
            accessManager.address,
            zeroAddress,
            zeroAddress
        )).to.be.revertedWithCustomError(_eeseeExpress, "InvalidConstructor")

        await expect(_eeseeExpress.deploy(
            zeroAddress,
            oneAddress,
            oneAddress
        )).to.be.revertedWithCustomError(_eeseeExpress, "InvalidAccessManager")
    })
})
