module.exports={
    //soldiity-coverage doesn't work with GSN, so we skip EeseePaymaster.
    skipFiles: ['test', 'interfaces', 'types', 'periphery/EeseePaymaster.sol', 'random/abstract/GelatoVRFConsumerBase.sol'],
    configureYulOptimizer: true
}