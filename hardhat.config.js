require("@nomicfoundation/hardhat-toolbox");
require('solidity-docgen')
require('dotenv').config()
require("hardhat-contract-sizer");
require('solidity-coverage')
require("./test/utils/changeNetwork.js");
require("hardhat-abi-exporter");

/** @type import('hardhat/config').HardhatUserConfig */
module.exports = {
  etherscan: {
    apiKey: {
      ethereum: process.env.ETHERSCAN,
      mainnet: process.env.ETHERSCAN,
      testnet: process.env.ETHERSCAN,
      goerli: process.env.ETHERSCAN,
      sepolia: process.env.ETHERSCAN,
      polygon:  process.env.POLYGONSCAN,
      polygonMumbai: process.env.POLYGONSCAN,
      blast: process.env.BLASTSCAN,
      blastSepolia: process.env.BLASTSCAN,
      bsc:  process.env.BSCSCAN
    },
    customChains: [
      {
        network: "blastSepolia",
        chainId: 168587773,
        urls: {
          apiURL: "https://api-sepolia.blastscan.io/api",
          browserURL: "https://sepolia.blastscan.io/"
        }
      },
      {
        network: "blast",
        chainId: 81457,
        urls: {
          apiURL: "https://api.blastscan.io/api",
          browserURL: "https://blastscan.io/"
        }
      }
    ]
  },
  contractSizer: {
    runOnCompile: false,
  },
  docgen: { 
    outputDir: 'docs/contracts',
    pages: "files",
    exclude: ["test"],
    templates: 'docgen-templates'
  },
  networks: {
    multipleNetworks: {
      url: "http://127.0.0.1:8545"
    },
    testnet: {
      url: "http://127.0.0.1:8545",
      accounts: [
        "0xac0974bec39a17e36ba4a6b4d238ff944bacb478cbed5efcae784d7bf4f2ff80",
        "0x59c6995e998f97a5a0044966f0945389dc9e86dae88c7a8412f4603b6b78690d",
        "0x5de4111afa1a4b94908f83103eb1f1706367c2e68ca870fc3fb9a804cdab365a",
        "0x7c852118294e51e653712a81e05800f419141751be58f605c371e15141b007a6",
        "0x47e179ec197488593b187f80a00eb0da91f1b9d0b13f8733639f19c30a34926a",
        "0x8b3a350cf5c34c9194ca85829a2df0ec3153be0318b5e2d3348e872092edffba",
        "0x92db14e403b83dfe3df233f83dfa3a0d7096f21ca9b0d6d6b8d88b2b4ec1564e",
        "0x4bbbf85ce3377467afe5d46f804f221813b2bb87f24d81f60f1fcdbf7cbf4356",
        "0xdbda1821b80551c9d65939329250298aa3472ba22feea921c0cf5d620ea67b97",
        "0x2a871d0798f97d79848a013d4936a73bf4cc922c825d33c1cf7073dff6d409c6",
        "0xf214f2b2cd398c806f84e317254e0f0b801d0643303237d97a22a48e01628897",
        "0x701b615bbdfb9de65240bc28bd21bbc0d996645a3dd57e7b12bc2bdf6f192c82",
        "0xa267530f49f8280200edf313ee7af6b827f2a8bce2897751d06a843f644967b1",
        "0x47c99abed3324a2707c28affff1267e45918ec8c3f20b8aa892e8b065d2942dd",
        "0xc526ee95bf44d8fc405a158bb884d9d1238d99f0612e9f33d006bb0789009aaa",
        "0x8166f546bab6da521a8369cab06c5d2b9e46670292d85c875ee9ec20e84ffb61",
        "0xea6c44ac03bff858b476bba40716402b03e41b8e97e276d1baec7c37d42484a0",
        "0x689af8efa8c651a91ad287602527f3af2fe9f6501a7ac4b061667b5a93e037fd",
      ],
      initialBaseFeePerGas: 1,
      baseFeePerGas: 1
    },

    testnet1: {
      url: "http://127.0.0.1:8545",
      initialBaseFeePerGas: 1,
      baseFeePerGas: 1,
      chainId: 100500,
    },
    testnet2: {
      url: "http://127.0.0.1:8546",
      initialBaseFeePerGas: 1,
      baseFeePerGas: 1,
      chainId: 100501,
    },
    goerli: {
      url: "https://goerli.infura.io/v3/9aa3d95b3bc440fa88ea12eaa4456161",
      chainId: 5,
      gasPrice: 10000000000,
      accounts: [process.env.PRIVATE_KEY]
    },
    polygon: {
      url: 'https://polygon-rpc.com/',
      accounts: [process.env.PRIVATE_KEY],
      gasPrice: 250000000000,
      chainId: 137
    },
    sepolia: {
      url: 'https://rpc.ankr.com/eth_sepolia',
      accounts: [process.env.PRIVATE_KEY],
      chainId: 11155111
    },
    mumbai: {
      url: 'https://rpc.ankr.com/polygon_mumbai',
      accounts: [process.env.PRIVATE_KEY],
      chainId: 80001
    },
    bsc: {
      url: 'https://rpc.ankr.com/bsc',
      accounts: [process.env.PRIVATE_KEY],
      chainId: 56
    },
    ethereum: {
      url: process.env.ETHEREUMRPC,
      accounts: [process.env.PRIVATE_KEY],
      chainId: 1
    },
    hardhat: {
      forking: {
        url: process.env.ETHEREUMRPC,
        blockNumber: 17915699
      },
      chainId: 1
    },
    // for mainnet
     "blast": {
      url: "https://rpc.blast.io",
      accounts: [process.env.PRIVATE_KEY]
    },
    // for Sepolia testnet
    "blast-sepolia": {
      url: "https://sepolia.blast.io",
      accounts: [process.env.PRIVATE_KEY],
      gasPrice: 1000000000,
    },
  },
  mocha: {
    timeout: 120000
  },  solidity: {
    compilers: [{
      version: "0.8.21",
      settings: {
        optimizer: {
          enabled: true,
          runs: 100000000
        },
        evmVersion: "shanghai",
      }
    },
    {
      version: "0.8.17",
      settings: {
        viaIR: true,
        optimizer: {
          enabled: true,
          runs: 100000000,
        },
      }
    },
  ],
    overrides: {
      "contracts/marketplace/Eesee.sol": {
        version: "0.8.21",
        settings: {
          optimizer: {
            enabled: true,
            runs: 2500
          },
          evmVersion: "shanghai",
        }
      },
      "contracts/periphery/crosschain/hub/EeseeAssetHubAxelar.sol": {
        version: "0.8.21",
        settings: {
          optimizer: {
            enabled: true,
            runs: 100
          },
          evmVersion: "shanghai",
        }
      },
      "contracts/periphery/crosschain/hub/EeseeAssetHubCCIP.sol": {
        version: "0.8.21",
        settings: {
          optimizer: {
            enabled: true,
            runs: 20000
          },
          evmVersion: "shanghai",
        }
      },
      "contracts/periphery/crosschain/spoke/EeseeAssetSpokeAxelar.sol": {
        version: "0.8.21",
        settings: {
          optimizer: {
            enabled: true,
            runs: 20000
          },
          evmVersion: "shanghai",
        }
      },
      "contracts/test/MockSquid.sol": {
        version: "0.8.21",
        settings: {
          optimizer: {
            enabled: true,
            runs: 100000000,
          },
          viaIR: true,
          evmVersion: "shanghai",
        },
      },
      "contracts/periphery/crosschain/EeseeExpress.sol": {
        version: "0.8.21",
        settings: {
          optimizer: {
            enabled: true,
            runs: 100000000,
          },
          viaIR: true,
          evmVersion: "shanghai",
        },
      },
      "contracts/biconomy/MultichainECDSAValidatorWithBlastPoints.sol": {
        version: "0.8.17",
        settings: {
          optimizer: {
            enabled: true,
            runs: 100000000,
          }
        },
      },
    }
  },
  abiExporter: {
    path: "./abi",
    runOnCompile: true,
    format: "json",
    clear: true,
    flat: true,
    spacing: 4,
    except: [
      "Multicall$",
      "IOwnable$",
      "Ownable$",
      "IERC20$",
      "IERC20Permit$",
      "Pausable$",
      "IPaymaster$",
      "IStakeManager$",
      "BasePaymaster$",
      "BaseSmartAccount$"
    ]
  },
};
