// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
const { network, run, ethers } = require("hardhat");
const { promises: fs } = require('fs')
const { getContractAddress } = require('@ethersproject/address')
const path = require('path')

async function addAddress(key, address) {
    const data = await fs.readFile(path.resolve(__dirname, '../addresses/addresses.json'))
    const json = JSON.parse(data)

    json[key] = address
    await fs.writeFile(path.resolve(__dirname, '../addresses/addresses.json'), JSON.stringify(json))
}

var i = 0
async function verify(contract, constructorArguments, name){
    console.log('...Deploying ' + name)
    await contract.deployTransaction.wait(10);
    console.log('...Verifying on block explorer ' + name)
    try {
        await run("verify:verify", {
            address: contract.address,
            constructorArguments: constructorArguments,
            contract: name
        })
    } catch (error) {
        console.log(error)
    }
    console.log(i)
    i = i + 1
}
async function main() {
    const [signer] = await ethers.getSigners();
    const usdcWrapper = await ethers.getContractFactory("USDCWrapper");

    const CCIPRouter = await ethers.getContractFactory("CCIPRouter");
    const OffRamp = await ethers.getContractFactory("OffRamp");

    let args

    let gateway
    let interchainTokenService
    let gasService
    let ccipRouter
    let trustedForwarder
    let squid
    let ESE
    let recipient
    if(network.name === 'polygon'){
        ccipRouter = '0x849c5ED5a80F5B408Dd4969b78c2C8fdf0565Bfe'
        trustedForwarder = '0xB2b5841DBeF766d4b521221732F9B618fCf34A87'
        squid = '0xce16F69375520ab01377ce7B88f5BA8C48F8D666'
        ESE = '' // TODO
        recipient = '' //TODO: staking and mining
    }else if(network.name === 'mumbai'){
        //const transactionCount = await signer.getTransactionCount()
        //args = [
        //    '0xE3079B4Deb1Af43F913363Cc340b909fCCc041DE', //accessManager
        //    getContractAddress({from: signer.address, nonce: transactionCount + 1})
        //]
        //const offRamp = await OffRamp.deploy(...args)
        //await verify(offRamp, args, "contracts/test/OffRamp.sol:OffRamp")

        //args = [
        //    [], 
        //    [{sourceChainSelector: '16015286601757825753', offRamp:offRamp.address}],
        //    '0x9c3C9283D3e44854697Cd22D3Faa240Cfb032889' //WETH
        //]
        //const _ccipRouter = await CCIPRouter.deploy(...args)
        //await verify(_ccipRouter, args, "contracts/test/CCIPRouter.sol:CCIPRouter")
        //ccipRouter = _ccipRouter.address
        ccipRouter = '0x1035CabC275068e0F4b745A29CEDf38E13aF41b1'
        trustedForwarder = '0xB2b5841DBeF766d4b521221732F9B618fCf34A87'

        squid = '0x481A2AAE41cd34832dDCF5A79404538bb2c02bC8'
        ESE = '0x6e4568Ce1Ca4C3B6E7e8FFFF3f0EB4E53B4e43E5'
        recipient = '0xEa6E311c2365F67218EFdf19C6f24296cdBF0058' //Address for testing

        args = [
            ESE,
            '0x2c852e740B62308c46DD29B982FBb650D063Bd07'
        ]
        const _usdcWrapper = await usdcWrapper.deploy(...args)
        await verify(_usdcWrapper, args, "contracts/test/USDCWrapper.sol:USDCWrapper")
    }else if(network.name === 'blast-sepolia'){
        gateway = //TODO: 
        interchainTokenService = //TODO: 
        gasService = //TODO: 

        trustedForwarder = '0x0000000000000000000000000000000000000000'
        squid = '' //TODO: 
    }else{
        return
    }

    if(ccipRouter != undefined){
        const eeseeAssetHub = await ethers.getContractFactory("EeseeAssetHub");
        args = [
            'ipfs://QmY9Hb9Rwb3Av7MWkNPGNWx7Jb8KC1cpyCD8c2NZi5oBZs/ESE%20Interchain.png',
            "IC-ESE #",
            "Enables the holder to reclaim the underlying asset on the original chain. This NFT serves as a passport to a seamless crosschain experience, bridging the gap between various blockchain ecosystems and the Polygon network.",

            ccipRouter,
            trustedForwarder
        ]
        const _eeseeAssetHub = await eeseeAssetHub.deploy(...args)
        await verify(_eeseeAssetHub, args, "contracts/periphery/crosschain/EeseeAssetHub.sol:EeseeAssetHub")
        await addAddress(network.name + '-eeseeAssetHub', _eeseeAssetHub.address)
    }else{
        const eeseeAssetHubAxelar = await ethers.getContractFactory("EeseeAssetHubAxelar");
        args = [
            'ipfs://QmY9Hb9Rwb3Av7MWkNPGNWx7Jb8KC1cpyCD8c2NZi5oBZs/ESE%20Interchain.png',
            "IC-ESE #",
            "Enables the holder to reclaim the underlying asset on the original chain. This NFT serves as a passport to a seamless crosschain experience, bridging the gap between various blockchain ecosystems and the Blast network.",

            gateway,
            interchainTokenService,
            gasService,

            trustedForwarder
        ]
        const _eeseeAssetHubAxelar = await eeseeAssetHubAxelar.deploy(...args)
        await verify(_eeseeAssetHubAxelar, args, "contracts/periphery/crosschain/EeseeAssetHubAxelar.sol:EeseeAssetHubAxelar")
        await addAddress(network.name + '-eeseeAssetHubAxelar', _eeseeAssetHubAxelar.address)
    }

    if(ESE != undefined){
        const eeseeForwarderOffRamp = await ethers.getContractFactory("EeseeForwarderOffRamp");
        args = [ESE, recipient]
        const _eeseeForwarderOffRamp = await eeseeForwarderOffRamp.deploy(...args)
        await verify(_eeseeForwarderOffRamp, args, "contracts/rewards/crosschain/EeseeForwarderOffRamp.sol:EeseeForwarderOffRamp")
        await addAddress(network.name + '-eeseeForwarderOffRamp', _eeseeForwarderOffRamp.address)
    }

    if(squid != undefined){
        const eeseeExpress = await ethers.getContractFactory("EeseeExpress");
        args = [
            squid,
            trustedForwarder
        ]
        const _eeseeExpress = await eeseeExpress.deploy(...args)
        await verify(_eeseeExpress, args, "contracts/periphery/crosschain/EeseeExpress.sol:EeseeExpress")
    }
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});

