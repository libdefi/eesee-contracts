// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
const { network, run, ethers } = require("hardhat");
const fs = require('fs/promises')
const { getContractAddress } = require('@ethersproject/address')
const path = require('path')

async function readAddress(app) {
    const data = await fs.readFile(path.resolve(__dirname, '../addresses/addresses.json'))
    const json = JSON.parse(data)

    return json[app]
}

var i = 0
async function verify(contract, constructorArguments, name){
    console.log('...Deploying ' + name)
    await contract.deployTransaction.wait(6);
    console.log('...Verifying on block explorer ' + name)
    try {
        await run("verify:verify", {
            address: contract.address,
            constructorArguments: constructorArguments,
            contract: name
        })
    } catch (error) {
        console.log(error)
    }
    console.log(i)
    i = i + 1
}
async function main() {
    const [signer] = await ethers.getSigners();

    const EeseeAssetSpoke = await ethers.getContractFactory("EeseeAssetSpoke");
    const EeseeExpress = await ethers.getContractFactory("EeseeExpress");
    const usdcWrapper = await ethers.getContractFactory("USDCWrapper");

    const CCIPRouter = await ethers.getContractFactory("CCIPRouter");
    const OnRamp = await ethers.getContractFactory("OnRamp");

    const eeseeForwarderOnRamp = await ethers.getContractFactory("EeseeForwarderOnRamp");
    
    let args

    let chainL1
    let chainL2
    let eeseeAssetHub

    let royaltyEngine
    let ccipRouter
    let trustedForwarder
    let squid
    let eeseeForwarderOffRamp
    let ESE
    if(network.name === 'ethereum'){
        chainL1 = '5009297550715157269'
        chainL2 = '4051577828743386545'
        royaltyEngine = '0x0385603ab55642cb4Dd5De3aE9e306809991804f'
        eeseeAssetHub = await readAddress('polygon-eeseeAssetHub')
        eeseeForwarderOffRamp = await readAddress('polygon-eeseeForwarderOffRamp')

        ccipRouter = '0x80226fc0Ee2b096224EeAc085Bb9a8cba1146f7D'
        trustedForwarder = '0xB2b5841DBeF766d4b521221732F9B618fCf34A87'
        squid = '0xce16F69375520ab01377ce7B88f5BA8C48F8D666'
        accessManager = ''//TODO:
        ESE = ''//TODO:
    }else if(network.name === 'goerli'){
        chainL1 = '16015286601757825753'
        chainL2 = '12532609583862916517'
        eeseeAssetHub = await readAddress('mumbai-eeseeAssetHub')
        eeseeForwarderOffRamp = await readAddress('mumbai-eeseeForwarderOffRamp')

        royaltyEngine = '0xEF770dFb6D5620977213f55f99bfd781D04BBE15'

        const transactionCount = await signer.getTransactionCount()
        args = [
            {
                chainSelector: chainL1,
                destChainSelector: chainL2
            }, {
                router: getContractAddress({from: signer.address, nonce: transactionCount + 1}),
                maxDataBytes: '30000',
                maxPerMsgGasLimit: '2000000',
            }
        ]
        const onRamp = await OnRamp.deploy(...args)
        await verify(onRamp, args, "contracts/test/OnRamp.sol:OnRamp")

        args = [
            [{destChainSelector:chainL2, onRamp: onRamp.address}], 
            [],
            '0xb4fbf271143f4fbf7b91a5ded31805e42b2208d6' //WETH
        ]
        const _ccipRouter = await CCIPRouter.deploy(...args)
        await verify(_ccipRouter, args, "contracts/test/CCIPRouter.sol:CCIPRouter")
        ccipRouter = _ccipRouter.address
        
        trustedForwarder = '0xB2b5841DBeF766d4b521221732F9B618fCf34A87'
        accessManager = '0xBFddC8E225b4540cDEA4a6929DfB789757da22aC'
        ESE = '0xa3e0Dfbf8DbD86e039f7CDB37682A776D66dae4b'
        
        //squid = '0x481A2AAE41cd34832dDCF5A79404538bb2c02bC8'
        //args = [
        //    '0xa3e0Dfbf8DbD86e039f7CDB37682A776D66dae4b',
        //    '0x254d06f33bDc5b8ee05b2ea472107E300226659A'
        //]
        //const _usdcWrapper = await usdcWrapper.deploy(...args)
        //await verify(_usdcWrapper, args, "contracts/test/USDCWrapper.sol:USDCWrapper")
    }else{
        return
    }

    args = [
        accessManager,
        chainL1,
        {chainSelector: chainL2, _address: eeseeAssetHub},
        royaltyEngine,
        ccipRouter,
        trustedForwarder
    ]
    const _eeseeAssetSpoke = await EeseeAssetSpoke.deploy(...args)
    await verify(_eeseeAssetSpoke, args, "contracts/periphery/crosschain/EeseeAssetSpoke.sol:EeseeAssetSpoke")

    args = [
        chainL1,
        {offRamp: {chainSelector: chainL2, _address: eeseeForwarderOffRamp}, gasLimit: 200000},
        ESE,
        accessManager,
        ccipRouter
    ]
    const _eeseeForwarderOnRamp = await eeseeForwarderOnRamp.deploy(...args)
    await verify(_eeseeForwarderOnRamp, args, "contracts/rewards/crosschain/EeseeForwarderOnRamp.sol:EeseeForwarderOnRamp")

    if(squid != undefined){
        args = [
            squid,
            trustedForwarder
        ]
        const _eeseeExpress = await EeseeExpress.deploy(...args)
        await verify(_eeseeExpress, args, "contracts/periphery/crosschain/EeseeExpress.sol:EeseeExpress")
    }
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});

