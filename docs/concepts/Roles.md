Eesee has a number of roles in the project:

* **ADMIN_ROLE** has the ability to: 
    * Grant and revoke other roles in [EeseeAccessManager](../contracts/admin/EeseeAccessManager.md). 
    * Change project fees in [Eesee](../contracts/marketplace/Eesee.md) and [EeseeDrops](../contracts/marketplace/EeseeDrops.md). 
    * Change the minimum and the maximum lot durations in [Eesee](../contracts/marketplace/Eesee.md). 
    * Approve and revoke approvals for contracts for use in [EeseePaymaster](../contracts/periphery/EeseePaymaster.md). 
    * Change random request intervals in [EeseeRandomBase](../contracts/random/abstract/EeseeRandomBase.md). 
    * Change Locked staking duration in [EeseeStaking](../contracts/rewards/EeseeStaking.md). 
    * Change staking reward rates in [EeseeStaking](../contracts/rewards/EeseeStaking.md).
    * Update approvedFunctions list in [BESE](../contracts/token/BESE.md).
    * Update onRampImplementation in [EeseeOnRampProxy](../contracts/periphery/crosschain/onRamp/EeseeOnRampProxy.md).
    * Update treasury address in [EeseeSwapBase](../contracts/periphery/abstract/EeseeSwapBase.md).

* **REQUEST_RANDOM_ROLE** can request random at any time in [EeseeRandomBase](../contracts/random/abstract/EeseeRandomBase.md). 

* **SIGNER_ROLE** is used in [EeseePaymaster](../contracts/periphery/EeseePaymaster.md) to sign current ESE token price and a personal user discount.

* **MERKLE_ROOT_UPDATER_ROLE** can add new merkle roots in [EeseeMining](../contracts/rewards/EeseeMining.md) contract.

* **volumeUpdater** (in [EeseeStaking.sol](../contracts/rewards/EeseeStaking.md)) can update lifetime user volume for [EeseeStaking](../contracts/rewards/EeseeStaking.md). Is intended to be a contract with which the users interact with to gain volume on our platform, eg. [Eesee](../contracts/marketplace/Eesee.md) or [EeseeDrops](../contracts/marketplace/EeseeDrops.md).

* **_initializer** (in [ESE.sol](../contracts/token/ESE.md)) - Can initialize [ESE](../contracts/token/ESE.md) contract and set vesting beneficiaries during uninitialized state. Is not used after initialization.

* **minter** (in [EeseeNFTLazyMint.sol](../contracts/NFT/EeseeNFTLazyMint.md)) - Can mint new NFTs for [EeseeNFTLazyMint](../contracts/NFT/EeseeNFTLazyMint.md) contract.

* **minter** (in [EeseeNFTDrop.sol](../contracts/NFT/EeseeNFTDrop.md)) - Can mint new NFTs for [EeseeNFTDrop.sol](../contracts/NFT/EeseeNFTDrop.md) contract.

* **PAUSER_ROLE** - Can pause [EeseeExecuteWithSwapBase.sol](../contracts/periphery/abstract/EeseeExecuteWithSwapBase.md), [EeseeOpenseaRouter.sol](../contracts/periphery/routers/EeseeOpenseaRouter.md), [EeseeRaribleRouter.sol](../contracts/periphery/routers/EeseeRaribleRouter.md), [EeseeAssetSpokeBase.sol](../contracts/periphery/crosschain/abstract/EeseeAssetSpokeBase.md) and [EeseeExpress.sol](../contracts/periphery/crosschain/EeseeExpress.md), [EeseeOnRampProxy](../contracts/periphery/crosschain/onRamp/EeseeOnRampProxy.md).

* **CCIPRouter** - Is Chainlink CCIP router. Can call ccipReceive() function in [EeseeAssetSpokeCCIP.sol](../contracts/periphery/crosschain/spoke/EeseeAssetSpokeCCIP.md) and [EeseeAssetHubCCIP.sol](../contracts/periphery/crosschain/hub/EeseeAssetHubCCIP.md), meaning it has the right to mint new ERC1155s in EeseeAssetHub, or to release tokens locked in EeseeAssetSpoke.
