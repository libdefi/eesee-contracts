This project has been developed with **Solidity** language, using **Hardhat** as a development environment. **JavaScript** is the selecting language for testing and scripting.

In addition, there are a number of third-party contracts that are used in the project:
1. [OpenZeppelin's libraries](https://github.com/OpenZeppelin/openzeppelin-contracts).
2. [OpenGSN's libraries](https://github.com/opengsn/gsn).
3. Azuki's [ERC721A.sol](https://www.erc721a.org/), which was slightly modified.
4. [RoyaltyEngine](https://royaltyregistry.xyz/).
5. [1inch](https://1inch.io/) contracts.
6. [Rarible](https://rarible.com/) contracts.
7. [OpenSea](https://opensea.io/) contracts.
8. [Squid](https://app.squidrouter.com/) contracts.
9. [Thirdweb libraries](https://thirdweb.com/).
10. [Axelar](https://axelar.network/) contracts.
11. [Chainlink VRF](https://docs.chain.link/vrf) contracts.
12. [Chainlink CCIP](https://chain.link/cross-chain) contracts.
13. [Chainlink Automation](https://chain.link/automation) contracts.
14. [Uniswap](https://uniswap.org/) contracts.
15. [Gelato](https://docs.gelato.network/) contracts.

Our contracts are licenced under BUSL 1.1 licence with an expiration date of 2027.01.01, after which it will turn into GNUv2.0 licence, with an exception of EeseePaymaster, EeseeUniswapBase, EeseePeripheryUniswap and EeseeSwapUniswap which are licenced under GPL-3.0-only.