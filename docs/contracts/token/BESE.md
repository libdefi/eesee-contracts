# BESE

_ESE token wrapper that can only be used with eesee's approved functions._


## UserApprovalData

```solidity
struct UserApprovalData {
  uint256 lastApprovalId;
  uint256 availableForUnwrap;
}
```
## Function

```solidity
struct Function {
  address _contract;
  bytes4 functionSelector;
}
```
## ESE

```solidity
contract IERC20 ESE
```

_ESE token contract address._

## approvedFunctions

```solidity
mapping(bytes32 => bool) approvedFunctions
```

_Functions approved for use with this token._

## approvalId

```solidity
uint256 approvalId
```

_Increments every time an approval is revoked. In such case, all current token holders are able to unwrap their tokens back to ERC20._

## userApprovalData

```solidity
mapping(address => struct BESE.UserApprovalData) userApprovalData
```

_User's last {approvalId} and available funds for unwrap._

## GrantApproval

```solidity
event GrantApproval(struct BESE.Function _function)
```

## RevokeApproval

```solidity
event RevokeApproval(struct BESE.Function _function, uint256 newApprovalId)
```

## TransferNotAvailable

```solidity
error TransferNotAvailable()
```

## InvalidContract

```solidity
error InvalidContract(address _contract)
```

## FunctionNotApproved

```solidity
error FunctionNotApproved(struct BESE.Function _function)
```

## InsufficientAvailableBalance

```solidity
error InsufficientAvailableBalance(uint256 available)
```

## ApproveNotAvailable

```solidity
error ApproveNotAvailable()
```

## InvalidESE

```solidity
error InvalidESE()
```

## FunctionAlreadyApproved

```solidity
error FunctionAlreadyApproved(struct BESE.Function _function)
```

## InvalidFunctionsLength

```solidity
error InvalidFunctionsLength()
```

## InvalidFunctionSelector

```solidity
error InvalidFunctionSelector(bytes data)
```

## wrap

```solidity
function wrap(uint256 amount, address to, bytes permit) external
```

_Burns ESE from sender and wraps them to BESE._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| amount | uint256 | - Amount of tokens to wrap. |
| to | address | - Address to mint BESE to. |
| permit | bytes | - Abi-encoded ESE permit data containing approveAmount, deadline, v, r and s. Set to empty bytes to skip permit. |

## unwrap

```solidity
function unwrap(uint256 amount, address to) external
```

_Unwraps BESE tokens back to ESE. Is only callable with the liquidity wrapped prior to the last {approvalId} increment._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| amount | uint256 | - Amount of tokens to unwrap. |
| to | address | - Address to transfer ESE to. |

## callExternal

```solidity
function callExternal(address _contract, bytes data) external payable returns (bool balanceIncreased, uint256 amount, bytes returnData)
```

_Call external function from the approvedFunctions list. Uses BESE from msgSender's balance._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _contract | address | - Contract to call. |
| data | bytes | - Calldata to call {_contract} with. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| balanceIncreased | bool | - Has contract balance increased after this call.          If true, BESE is minted to msgSender, else BESE is burned from msgSender. |
| amount | uint256 | - Amount of BESE minted or burned. |
| returnData | bytes | - Data returned from external call. |

## grantFunctionApprovals

```solidity
function grantFunctionApprovals(struct BESE.Function[] functions) external
```

_Grants approvals to call selected functions in selected contracts using {callExternal} function. Emits {GrantApproval} event for each function approved._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| functions | struct BESE.Function[] | - Contract addresses with function selectors to approve. Note: This function is only callable by ADMIN_ROLE. |

## revokeFunctionApprovals

```solidity
function revokeFunctionApprovals(struct BESE.Function[] functions) external
```

_Revokes granted approvals and increments approvalId. Emits {RevokeApproval} event for each approval revoked._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| functions | struct BESE.Function[] | - Contract addresses with function selectors to revoke approval from. Note: This function is only callable by ADMIN_ROLE. |

## isFunctionApproved

```solidity
function isFunctionApproved(struct BESE.Function _function) external view returns (bool)
```

_Checks if a given function is approved in this contract._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _function | struct BESE.Function | - Function to check. |


# Inherited from EeseeRoleHandler



# Inherited from ERC2771Context

## isTrustedForwarder

```solidity
function isTrustedForwarder(address forwarder) public view virtual returns (bool)
```



# Inherited from ReentrancyGuard



# Inherited from ERC20

## name

```solidity
function name() public view virtual returns (string)
```

_Returns the name of the token._

## symbol

```solidity
function symbol() public view virtual returns (string)
```

_Returns the symbol of the token, usually a shorter version of the
name._

## decimals

```solidity
function decimals() public view virtual returns (uint8)
```

_Returns the number of decimals used to get its user representation.
For example, if `decimals` equals `2`, a balance of `505` tokens should
be displayed to a user as `5.05` (`505 / 10 ** 2`).

Tokens usually opt for a value of 18, imitating the relationship between
Ether and Wei. This is the default value returned by this function, unless
it's overridden.

NOTE: This information is only used for _display_ purposes: it in
no way affects any of the arithmetic of the contract, including
{IERC20-balanceOf} and {IERC20-transfer}._

## totalSupply

```solidity
function totalSupply() public view virtual returns (uint256)
```

_See {IERC20-totalSupply}._

## balanceOf

```solidity
function balanceOf(address account) public view virtual returns (uint256)
```

_See {IERC20-balanceOf}._

## transfer

```solidity
function transfer(address to, uint256 amount) public virtual returns (bool)
```

_See {IERC20-transfer}.

Requirements:

- `to` cannot be the zero address.
- the caller must have a balance of at least `amount`._

## allowance

```solidity
function allowance(address owner, address spender) public view virtual returns (uint256)
```

_See {IERC20-allowance}._

## approve

```solidity
function approve(address spender, uint256 amount) public virtual returns (bool)
```

_See {IERC20-approve}.

NOTE: If `amount` is the maximum `uint256`, the allowance is not updated on
`transferFrom`. This is semantically equivalent to an infinite approval.

Requirements:

- `spender` cannot be the zero address._

## transferFrom

```solidity
function transferFrom(address from, address to, uint256 amount) public virtual returns (bool)
```

_See {IERC20-transferFrom}.

Emits an {Approval} event indicating the updated allowance. This is not
required by the EIP. See the note at the beginning of {ERC20}.

NOTE: Does not update the allowance if the current allowance
is the maximum `uint256`.

Requirements:

- `from` and `to` cannot be the zero address.
- `from` must have a balance of at least `amount`.
- the caller must have allowance for ``from``'s tokens of at least
`amount`._

## increaseAllowance

```solidity
function increaseAllowance(address spender, uint256 addedValue) public virtual returns (bool)
```

_Atomically increases the allowance granted to `spender` by the caller.

This is an alternative to {approve} that can be used as a mitigation for
problems described in {IERC20-approve}.

Emits an {Approval} event indicating the updated allowance.

Requirements:

- `spender` cannot be the zero address._

## decreaseAllowance

```solidity
function decreaseAllowance(address spender, uint256 subtractedValue) public virtual returns (bool)
```

_Atomically decreases the allowance granted to `spender` by the caller.

This is an alternative to {approve} that can be used as a mitigation for
problems described in {IERC20-approve}.

Emits an {Approval} event indicating the updated allowance.

Requirements:

- `spender` cannot be the zero address.
- `spender` must have allowance for the caller of at least
`subtractedValue`._



# Inherited from IERC20


## Transfer

```solidity
event Transfer(address from, address to, uint256 value)
```

_Emitted when `value` tokens are moved from one account (`from`) to
another (`to`).

Note that `value` may be zero._

## Approval

```solidity
event Approval(address owner, address spender, uint256 value)
```

_Emitted when the allowance of a `spender` for an `owner` is set by
a call to {approve}. `value` is the new allowance._


