# EeseeOffchain

_Contract for offchain eesee lots logic._


## SignatureData

```solidity
struct SignatureData {
  uint256 nonce;
  uint256 deadline;
  bytes signature;
}
```
## nonceUsed

```solidity
mapping(uint256 => bool) nonceUsed
```

_Is given nonce used._

## CreateLot

```solidity
event CreateLot(bytes32 ID, struct Asset asset, address sender, bytes data, uint256 nonce)
```

## BuyTickets

```solidity
event BuyTickets(bytes32 ID, struct Asset asset, address sender, address recipient, uint256 nonce)
```

## ClaimAsset

```solidity
event ClaimAsset(bytes32 ID, struct Asset asset, address sender, address recipient, uint256 nonce)
```

## RevokeSignature

```solidity
event RevokeSignature(uint256 nonce)
```

## InvalidRecipient

```solidity
error InvalidRecipient()
```

## ExpiredDeadline

```solidity
error ExpiredDeadline()
```

## NonceUsed

```solidity
error NonceUsed()
```

## InvalidAmount

```solidity
error InvalidAmount()
```

## InvalidSignature

```solidity
error InvalidSignature()
```

## InvalidValue

```solidity
error InvalidValue()
```

## TransferNotSuccessful

```solidity
error TransferNotSuccessful()
```

## InvalidAssetType

```solidity
error InvalidAssetType()
```

## createLot

```solidity
function createLot(struct Asset asset, bytes data, struct EeseeOffchain.SignatureData signatureData) external payable returns (bytes32 ID)
```

_Creates lot with offchain logic. Emits {CreateLot} event._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| asset | struct Asset | - Assets to list. Note: The sender must have them approved for this contract. |
| data | bytes | - Lot data for offchain logic. |
| signatureData | struct EeseeOffchain.SignatureData | - Signature for asset and data signed by SIGNER_ROLE. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| ID | bytes32 | - ID of created lot. |

## buyTickets

```solidity
function buyTickets(bytes32 ID, struct Asset asset, address recipient, struct EeseeOffchain.SignatureData signatureData) external payable
```

_Buys tickets to participate in a lot. Emits {BuyTickets} event._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| ID | bytes32 | - ID of lot to buy tickets in. |
| asset | struct Asset | - Asset to buy tickets with. |
| recipient | address | - Recipient of tickets. |
| signatureData | struct EeseeOffchain.SignatureData | - Signature for ID, asset and recipient signed by SIGNER_ROLE. |

## claimAsset

```solidity
function claimAsset(bytes32 ID, struct Asset asset, address recipient, bytes _call, struct EeseeOffchain.SignatureData signatureData) external payable returns (bytes returnData)
```

_Claims asset from a lot. Emits {ClaimAsset} event._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| ID | bytes32 | - ID of lot to claim asset in. |
| asset | struct Asset | - Asset to claim and send to recipient. |
| recipient | address | - Recipient of asset. |
| _call | bytes | - Additional external call to make. |
| signatureData | struct EeseeOffchain.SignatureData | - Signature for ID, asset, msgSender, recipient and _call signed by SIGNER_ROLE. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| returnData | bytes | - Data returned from external call. |

## revokeSignatures

```solidity
function revokeSignatures(uint256[] nonces) external
```

_Callable by SIGNER_ROLE to revoke signatures. Emits {RevokeSignature} for each signature revoked._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| nonces | uint256[] | - Signature nonces to revoke. |

## getLotId

```solidity
function getLotId(uint256 nonce) public pure returns (bytes32)
```


# Inherited from EeseeRoleHandler



# Inherited from EIP712

## eip712Domain

```solidity
function eip712Domain() public view virtual returns (bytes1 fields, string name, string version, uint256 chainId, address verifyingContract, bytes32 salt, uint256[] extensions)
```

_See {EIP-5267}.

_Available since v4.9.__



# Inherited from IERC5267


## EIP712DomainChanged

```solidity
event EIP712DomainChanged()
```

_MAY be emitted to signal that the domain could have changed._


# Inherited from ERC1155Holder

## onERC1155Received

```solidity
function onERC1155Received(address, address, uint256, uint256, bytes) public virtual returns (bytes4)
```

## onERC1155BatchReceived

```solidity
function onERC1155BatchReceived(address, address, uint256[], uint256[], bytes) public virtual returns (bytes4)
```



# Inherited from ERC1155Receiver

## supportsInterface

```solidity
function supportsInterface(bytes4 interfaceId) public view virtual returns (bool)
```

_See {IERC165-supportsInterface}._



# Inherited from ERC721Holder

## onERC721Received

```solidity
function onERC721Received(address, address, uint256, bytes) public virtual returns (bytes4)
```

_See {IERC721Receiver-onERC721Received}.

Always returns `IERC721Receiver.onERC721Received.selector`._



# Inherited from ERC2771Context

## isTrustedForwarder

```solidity
function isTrustedForwarder(address forwarder) public view virtual returns (bool)
```



