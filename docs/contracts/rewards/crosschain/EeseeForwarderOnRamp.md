# IERC20Burnable


## burn

```solidity
function burn(uint256 amount) external
```

## burnFrom

```solidity
function burnFrom(address account, uint256 amount) external
```


# Inherited from IERC20

## totalSupply

```solidity
function totalSupply() external view returns (uint256)
```

_Returns the amount of tokens in existence._

## balanceOf

```solidity
function balanceOf(address account) external view returns (uint256)
```

_Returns the amount of tokens owned by `account`._

## transfer

```solidity
function transfer(address to, uint256 amount) external returns (bool)
```

_Moves `amount` tokens from the caller's account to `to`.

Returns a boolean value indicating whether the operation succeeded.

Emits a {Transfer} event._

## allowance

```solidity
function allowance(address owner, address spender) external view returns (uint256)
```

_Returns the remaining number of tokens that `spender` will be
allowed to spend on behalf of `owner` through {transferFrom}. This is
zero by default.

This value changes when {approve} or {transferFrom} are called._

## approve

```solidity
function approve(address spender, uint256 amount) external returns (bool)
```

_Sets `amount` as the allowance of `spender` over the caller's tokens.

Returns a boolean value indicating whether the operation succeeded.

IMPORTANT: Beware that changing an allowance with this method brings the risk
that someone may use both the old and the new allowance by unfortunate
transaction ordering. One possible solution to mitigate this race
condition is to first reduce the spender's allowance to 0 and set the
desired value afterwards:
https://github.com/ethereum/EIPs/issues/20#issuecomment-263524729

Emits an {Approval} event._

## transferFrom

```solidity
function transferFrom(address from, address to, uint256 amount) external returns (bool)
```

_Moves `amount` tokens from `from` to `to` using the
allowance mechanism. `amount` is then deducted from the caller's
allowance.

Returns a boolean value indicating whether the operation succeeded.

Emits a {Transfer} event._


## Transfer

```solidity
event Transfer(address from, address to, uint256 value)
```

_Emitted when `value` tokens are moved from one account (`from`) to
another (`to`).

Note that `value` may be zero._

## Approval

```solidity
event Approval(address owner, address spender, uint256 value)
```

_Emitted when the allowance of a `spender` for an `owner` is set by
a call to {approve}. `value` is the new allowance._


# EeseeForwarderOnRamp

_Forwards ESE received from vesting to the specified offRamp._


## DestinationData

```solidity
struct DestinationData {
  struct AddressWithChain offRamp;
  uint256 gasLimit;
}
```
## CCIPRouter

```solidity
contract IRouterClient CCIPRouter
```

_Chainlink CCIP router._

## ESE

```solidity
contract IERC20Burnable ESE
```

_ESE token to forward._

## ADMIN_ROLE

```solidity
bytes32 ADMIN_ROLE
```

_Admin role af defined in {accessManager}._

## chainSelector

```solidity
uint64 chainSelector
```

_Chain selector for the current chain._

## destinationData

```solidity
struct EeseeForwarderOnRamp.DestinationData destinationData
```

_Info for the destination including destination chain, offRamp, gasLimit._

## Forward

```solidity
event Forward(bytes32 messageId, struct EeseeForwarderOnRamp.DestinationData destinationData, uint256 amount, uint256 fee)
```

## UpdateDestinationData

```solidity
event UpdateDestinationData(struct EeseeForwarderOnRamp.DestinationData oldDestination, struct EeseeForwarderOnRamp.DestinationData newDestination)
```

## InvalidConstructor

```solidity
error InvalidConstructor()
```

## InvalidValue

```solidity
error InvalidValue()
```

## InsufficientBalance

```solidity
error InsufficientBalance()
```

## forward

```solidity
function forward() external payable returns (bytes32 messageId, uint256 fee)
```

_Forwards all ESE balance from this contract to {destinationData.offRamp}._

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| messageId | bytes32 | - CCIP message id. |
| fee | uint256 | - Ether paid for the crosschain call. |

## updateDestinationData

```solidity
function updateDestinationData(struct EeseeForwarderOnRamp.DestinationData newDestinationData) external
```

_Updates destination data._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| newDestinationData | struct EeseeForwarderOnRamp.DestinationData | - New destinationData containing destination chain, offRamp, gasLimit. |


# Inherited from EeseePausable

## pause

```solidity
function pause() external
```

_Called by the PAUSER_ROLE to pause, triggers stopped state._

## unpause

```solidity
function unpause() external
```

_Called by the PAUSER_ROLE to unpause, returns to normal state._



# Inherited from Pausable

## paused

```solidity
function paused() public view virtual returns (bool)
```

_Returns true if the contract is paused, and false otherwise._


## Paused

```solidity
event Paused(address account)
```

_Emitted when the pause is triggered by `account`._

## Unpaused

```solidity
event Unpaused(address account)
```

_Emitted when the pause is lifted by `account`._


