# EeseeForwarderOffRamp

_Forwards ESE received from onRamp to the specified recipient._


## ESE

```solidity
contract IERC20 ESE
```

_ESE token to forward._

## recipient

```solidity
address recipient
```

_Address to forward ESE token to._

## Forward

```solidity
event Forward(uint256 amount)
```

## InvalidConstructor

```solidity
error InvalidConstructor()
```

## ccipReceive

```solidity
function ccipReceive(struct Client.Any2EVMMessage) external
```

_Whenever this contract receives CCIP message, transfers all ESE in balance to recipient.
       Note that this function does not have means of access control, meaning anyone can call it._

## supportsInterface

```solidity
function supportsInterface(bytes4 interfaceId) public view returns (bool)
```

_See {IERC165-supportsInterface}._


