# MulticallExternal


## blacklist

```solidity
mapping(address => mapping(bytes4 => bool)) blacklist
```

_Blacklist for functions._

## AddToBlacklist

```solidity
event AddToBlacklist(address target, bytes4 functionSelector)
```

## RemoveFromBlacklist

```solidity
event RemoveFromBlacklist(address target, bytes4 functionSelector)
```

## MulticallFailed

```solidity
event MulticallFailed(bytes err)
```

## MulticallReverted

```solidity
error MulticallReverted(bytes err)
```

## FunctionBlacklisted

```solidity
error FunctionBlacklisted(address target, bytes4 functionSignature)
```

## multicall

```solidity
function multicall(struct Call[] calls) external
```

_Call any contract's function._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| calls | struct Call[] | - Stuct describing the call. |


