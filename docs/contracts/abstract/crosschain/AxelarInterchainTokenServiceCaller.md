# AxelarInterchainTokenServiceCaller


## AXELAR_INTERCHAIN_TOKEN_SERVICE_CALLER_IDENTIFIER

```solidity
bytes32 AXELAR_INTERCHAIN_TOKEN_SERVICE_CALLER_IDENTIFIER
```

## InvalidInterchainTokenService

```solidity
error InvalidInterchainTokenService()
```


# Inherited from ICallerBase


## CrosschainSend

```solidity
event CrosschainSend(bytes32 projectIdentifier, struct AddressWithChain destination, bytes additionalData)
```

## CrosschainReceive

```solidity
event CrosschainReceive(bytes32 projectIdentifier, struct AddressWithChain source, bytes additionalData)
```


# Inherited from InterchainTokenExecutable

## executeWithInterchainToken

```solidity
function executeWithInterchainToken(bytes32 commandId, string sourceChain, bytes sourceAddress, bytes data, bytes32 tokenId, address token, uint256 amount) external virtual returns (bytes32)
```

Executes logic in the context of an interchain token transfer.

_Only callable by the interchain token service._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| commandId | bytes32 | The unique message id. |
| sourceChain | string | The source chain of the token transfer. |
| sourceAddress | bytes | The source address of the token transfer. |
| data | bytes | The data associated with the token transfer. |
| tokenId | bytes32 | The token ID. |
| token | address | The token address. |
| amount | uint256 | The amount of tokens being transferred. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| [0] | bytes32 | bytes32 Hash indicating success of the execution. |



