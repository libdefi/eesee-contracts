# AxelarGatewayBase


## gateway

```solidity
contract IAxelarGateway gateway
```

_Axelar gateway._

## gasService

```solidity
contract IAxelarGasService gasService
```

_Gas service for Axelar._

## InvalidGasService

```solidity
error InvalidGasService()
```

## InvalidGateway

```solidity
error InvalidGateway()
```

## NotApprovedByGateway

```solidity
error NotApprovedByGateway()
```


