# AxelarTokenServiceCaller


## AXELAR_TOKEN_SERVICE_CALLER_IDENTIFIER

```solidity
bytes32 AXELAR_TOKEN_SERVICE_CALLER_IDENTIFIER
```

## executeWithToken

```solidity
function executeWithToken(bytes32 commandId, string sourceChain, string sourceAddress, bytes payload, string tokenSymbol, uint256 amount) external
```


# Inherited from ICallerBase


## CrosschainSend

```solidity
event CrosschainSend(bytes32 projectIdentifier, struct AddressWithChain destination, bytes additionalData)
```

## CrosschainReceive

```solidity
event CrosschainReceive(bytes32 projectIdentifier, struct AddressWithChain source, bytes additionalData)
```


