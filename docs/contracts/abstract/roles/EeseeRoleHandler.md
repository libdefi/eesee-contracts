# EeseeRoleHandler


## accessManager

```solidity
contract IEeseeAccessManager accessManager
```

_Access manager for Eesee contract ecosystem._

## CallerNotAuthorized

```solidity
error CallerNotAuthorized()
```

## InvalidAccessManager

```solidity
error InvalidAccessManager()
```


