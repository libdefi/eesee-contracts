# AddressWithChain

```solidity
struct AddressWithChain {
  bytes chainSelector;
  address _address;
}
```
# AssetHashWithSource

```solidity
struct AssetHashWithSource {
  struct AddressWithChain source;
  bytes32 assetHash;
}
```
