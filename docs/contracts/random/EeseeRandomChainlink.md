# EeseeRandomChainlink


## ChainlinkContructorArgs

_Because of the Stack too deep error, we combine some constructor arguments into a single stuct_

```solidity
struct ChainlinkContructorArgs {
  address vrfCoordinator;
  bytes32 keyHash;
  uint16 minimumRequestConfirmations;
  uint32 callbackGasLimit;
}
```
## subscriptionID

```solidity
uint64 subscriptionID
```

_Chainlink VRF V2 subscription ID._

## callbackGasLimit

```solidity
uint32 callbackGasLimit
```

_Chainlink VRF V2 gas limit to call fulfillRandomWords() with._

## vrfCoordinator

```solidity
contract VRFCoordinatorV2Interface vrfCoordinator
```

_Chainlink VRF V2 coordinator._

## ChangeCallbackGasLimit

```solidity
event ChangeCallbackGasLimit(uint32 previousCallbackGasLimit, uint32 newCallbackGasLimit)
```

## performUpkeep

```solidity
function performUpkeep(bytes) external
```

_Requests random from Chainlink. Emits {RequestRandom} event._

## changeCallbackGasLimit

```solidity
function changeCallbackGasLimit(uint32 _callbackGasLimit) external
```

_Changes callbackGasLimit. Emits {ChangeCallbackGasLimit} event._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _callbackGasLimit | uint32 | - New callbackGasLimit. Note: This function can only be called by ADMIN_ROLE. |

## checkUpkeep

```solidity
function checkUpkeep(bytes) external view returns (bool upkeepNeeded, bytes)
```

_Called by Chainlink Keeper to determine if performUpkeep() needs to be called._


# Inherited from VRFConsumerBaseV2

## rawFulfillRandomWords

```solidity
function rawFulfillRandomWords(uint256 requestId, uint256[] randomWords) external
```



# Inherited from EeseeRandomBase

## requestRandomImmediate

```solidity
function requestRandomImmediate() external
```

_Request random immediately, skipping canRequestRandom() check. Emits {RequestRandom} event.
Note: This function can only be called by REQUEST_RANDOM_ROLE._

## changeRandomRequestInterval

```solidity
function changeRandomRequestInterval(uint32 _randomRequestInterval) external
```

_Changes randomRequestInterval. Emits {ChangeRandomRequestInterval} event._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _randomRequestInterval | uint32 | - New randomRequestInterval. Note: This function can only be called by ADMIN_ROLE. |

## getRandomFromTimestamp

```solidity
function getRandomFromTimestamp(uint256 _timestamp) external view returns (uint256 word, uint256 timestamp)
```

## canRequestRandom

```solidity
function canRequestRandom() public view virtual returns (bool randomNeeded)
```

_Check if random needs to be requested._


## ChangeRandomRequestInterval

```solidity
event ChangeRandomRequestInterval(uint32 previousRandomRequestInterval, uint32 newRandomRequestInterval)
```


# Inherited from EeseeRoleHandler



# Inherited from IEeseeRandom

## random

```solidity
function random(uint256) external view returns (uint256 word, uint256 timestamp)
```

## lastRandomRequestTimestamp

```solidity
function lastRandomRequestTimestamp() external view returns (uint64)
```

## randomRequestInterval

```solidity
function randomRequestInterval() external view returns (uint32)
```

## MAX_RANDOM_REQUEST_INTERVAL

```solidity
function MAX_RANDOM_REQUEST_INTERVAL() external view returns (uint48)
```


## FulfillRandom

```solidity
event FulfillRandom(uint256 random)
```

## RequestRandom

```solidity
event RequestRandom()
```


