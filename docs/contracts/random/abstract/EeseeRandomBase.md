# EeseeRandomBase


## random

```solidity
struct Random[] random
```

_Random words and timestamps received._

## lastRandomRequestTimestamp

```solidity
uint64 lastRandomRequestTimestamp
```

_Last random request timestamp._

## randomRequestInterval

```solidity
uint32 randomRequestInterval
```

_Random request frequency._

## MAX_RANDOM_REQUEST_INTERVAL

```solidity
uint48 MAX_RANDOM_REQUEST_INTERVAL
```

_Max possible {randomRequestInterval}._

## ChangeRandomRequestInterval

```solidity
event ChangeRandomRequestInterval(uint32 previousRandomRequestInterval, uint32 newRandomRequestInterval)
```

## RandomRequestNotNeeded

```solidity
error RandomRequestNotNeeded()
```

## InvalidRandomRequestInterval

```solidity
error InvalidRandomRequestInterval()
```

## CallOnTheSameBlock

```solidity
error CallOnTheSameBlock()
```

## requestRandomImmediate

```solidity
function requestRandomImmediate() external
```

_Request random immediately, skipping canRequestRandom() check. Emits {RequestRandom} event.
Note: This function can only be called by REQUEST_RANDOM_ROLE._

## changeRandomRequestInterval

```solidity
function changeRandomRequestInterval(uint32 _randomRequestInterval) external
```

_Changes randomRequestInterval. Emits {ChangeRandomRequestInterval} event._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _randomRequestInterval | uint32 | - New randomRequestInterval. Note: This function can only be called by ADMIN_ROLE. |

## getRandomFromTimestamp

```solidity
function getRandomFromTimestamp(uint256 _timestamp) external view returns (uint256 word, uint256 timestamp)
```

## canRequestRandom

```solidity
function canRequestRandom() public view virtual returns (bool randomNeeded)
```

_Check if random needs to be requested._


# Inherited from EeseeRoleHandler



# Inherited from IEeseeRandom

## random

```solidity
function random(uint256) external view returns (uint256 word, uint256 timestamp)
```

## lastRandomRequestTimestamp

```solidity
function lastRandomRequestTimestamp() external view returns (uint64)
```

## randomRequestInterval

```solidity
function randomRequestInterval() external view returns (uint32)
```

## MAX_RANDOM_REQUEST_INTERVAL

```solidity
function MAX_RANDOM_REQUEST_INTERVAL() external view returns (uint48)
```


## FulfillRandom

```solidity
event FulfillRandom(uint256 random)
```

## RequestRandom

```solidity
event RequestRandom()
```


