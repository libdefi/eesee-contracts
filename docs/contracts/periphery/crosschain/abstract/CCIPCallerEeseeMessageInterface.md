# CCIPCallerEeseeMessageInterface



# Inherited from EeseeMessageInterface



# Inherited from CCIPCaller

## ccipReceive

```solidity
function ccipReceive(struct Client.Any2EVMMessage message) external virtual
```

Called by the Router to deliver a message.
If this reverts, any token transfers also revert. The message
will move to a FAILED state and become available for manual execution.

_Note ensure you check the msg.sender is the OffRampRouter_

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| message | struct Client.Any2EVMMessage | CCIP Message |

## supportsInterface

```solidity
function supportsInterface(bytes4 interfaceId) public view virtual returns (bool)
```

_See {IERC165-supportsInterface}._



# Inherited from ICallerBase


## CrosschainSend

```solidity
event CrosschainSend(bytes32 projectIdentifier, struct AddressWithChain destination, bytes additionalData)
```

## CrosschainReceive

```solidity
event CrosschainReceive(bytes32 projectIdentifier, struct AddressWithChain source, bytes additionalData)
```


