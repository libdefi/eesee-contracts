# CCIPCaller


## CCIPRouter

```solidity
contract IRouterClient CCIPRouter
```

## CCIPSend

```solidity
event CCIPSend(bytes32 messageId, struct AddressWithChain destination, uint256 fee)
```

## CCIPReceive

```solidity
event CCIPReceive(bytes32 messageId, struct AddressWithChain source)
```

## InvalidRouter

```solidity
error InvalidRouter()
```

## InsufficientValue

```solidity
error InsufficientValue()
```

## ccipReceive

```solidity
function ccipReceive(struct Client.Any2EVMMessage message) external virtual
```

Called by the Router to deliver a message.
If this reverts, any token transfers also revert. The message
will move to a FAILED state and become available for manual execution.

_Note ensure you check the msg.sender is the OffRampRouter_

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| message | struct Client.Any2EVMMessage | CCIP Message |

## supportsInterface

```solidity
function supportsInterface(bytes4 interfaceId) public view virtual returns (bool)
```

_See {IERC165-supportsInterface}._


