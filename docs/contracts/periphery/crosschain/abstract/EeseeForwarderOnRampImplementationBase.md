# EeseeForwarderOnRampImplementationBase

_Forwards ESE tokens in balance to the specified offRamp._


## ESE

```solidity
contract IERC20 ESE
```

_ESE token to forward._

## offRamp

```solidity
struct AddressWithChain offRamp
```

_Info for the destination offRamp contract._

## InvalidOffRamp

```solidity
error InvalidOffRamp()
```

## InvalidESE

```solidity
error InvalidESE()
```

## InsufficientBalance

```solidity
error InsufficientBalance()
```

## forward

```solidity
function forward(bytes data) external payable
```

_Forwards ESE balance from this contract to {offRamp}._

## initialize

```solidity
function initialize(bytes data) public virtual
```

_Initialize onRamp with provided data. Data is abi-encoded tuple(bytes,address) offRamp.
Note: Proxies using this implementation should implement access control for this function._


# Inherited from Initializable


## Initialized

```solidity
event Initialized(uint8 version)
```

_Triggered when the contract has been initialized or reinitialized._


# Inherited from IEeseeForwarderOnRamp

## ESE

```solidity
function ESE() external view returns (contract IERC20)
```

## offRamp

```solidity
function offRamp() external view returns (bytes chainSelector, address _address)
```


## Forward

```solidity
event Forward(struct AddressWithChain destination, uint256 amount)
```


