# AxelarCaller


## gasService

```solidity
contract IAxelarGasService gasService
```

_Gas service for Axelar._

## InvalidGasService

```solidity
error InvalidGasService()
```


# Inherited from AxelarExecutable

## execute

```solidity
function execute(bytes32 commandId, string sourceChain, string sourceAddress, bytes payload) external
```

## executeWithToken

```solidity
function executeWithToken(bytes32 commandId, string sourceChain, string sourceAddress, bytes payload, string tokenSymbol, uint256 amount) external
```



# Inherited from IAxelarExecutable

## gateway

```solidity
function gateway() external view returns (contract IAxelarGateway)
```



# Inherited from AxelarBase


## AxelarSend

```solidity
event AxelarSend(struct AxelarBase.AddressWithChain destination, uint256 gasPaid)
```

## AxelarReceive

```solidity
event AxelarReceive(struct AxelarBase.AddressWithChain source)
```


