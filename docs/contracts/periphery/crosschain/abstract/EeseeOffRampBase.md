# EeseeOffRampBase

_Forwards ESE in balance to the specified recipient._


## ESE

```solidity
contract IERC20 ESE
```

_ESE token to forward._

## recipient

```solidity
address recipient
```

_Address to forward ESE token to._

## InvalidConstructor

```solidity
error InvalidConstructor()
```


# Inherited from IEeseeOffRamp

## ESE

```solidity
function ESE() external view returns (contract IERC20)
```

## recipient

```solidity
function recipient() external view returns (address)
```


## Forward

```solidity
event Forward(uint256 amount)
```


