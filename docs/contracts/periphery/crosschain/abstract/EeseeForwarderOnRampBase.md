# EeseeForwarderOnRampBase

_Forwards ESE tokens in balance to the specified offRamp._


## ESE

```solidity
contract IERC20 ESE
```

_ESE token to forward._

## offRamp

```solidity
struct AddressWithChain offRamp
```

_Info for the destination offRamp contract._

## InvalidConstructor

```solidity
error InvalidConstructor()
```

## InsufficientBalance

```solidity
error InsufficientBalance()
```

## CantReceiveMessages

```solidity
error CantReceiveMessages()
```

## forward

```solidity
function forward() external payable
```

_Forwards ESE balance from this contract to {offRamp}._


# Inherited from IEeseeForwarderOnRamp

## ESE

```solidity
function ESE() external view returns (contract IERC20)
```

## offRamp

```solidity
function offRamp() external view returns (bytes chainSelector, address _address)
```


## Forward

```solidity
event Forward(uint256 amount)
```


