# AxelarInterchainTokenServiceCaller


## InvalidInterchainTokenService

```solidity
error InvalidInterchainTokenService()
```


# Inherited from InterchainTokenExecutable

## executeWithInterchainToken

```solidity
function executeWithInterchainToken(bytes32 commandId, string sourceChain, bytes sourceAddress, bytes data, bytes32 tokenId, address token, uint256 amount) external virtual returns (bytes32)
```

Executes logic in the context of an interchain token transfer.

_Only callable by the interchain token service._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| commandId | bytes32 | The unique message id. |
| sourceChain | string | The source chain of the token transfer. |
| sourceAddress | bytes | The source address of the token transfer. |
| data | bytes | The data associated with the token transfer. |
| tokenId | bytes32 | The token ID. |
| token | address | The token address. |
| amount | uint256 | The amount of tokens being transferred. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| [0] | bytes32 | bytes32 Hash indicating success of the execution. |



# Inherited from AxelarBase


## AxelarSend

```solidity
event AxelarSend(struct AxelarBase.AddressWithChain destination, uint256 gasPaid)
```

## AxelarReceive

```solidity
event AxelarReceive(struct AxelarBase.AddressWithChain source)
```


