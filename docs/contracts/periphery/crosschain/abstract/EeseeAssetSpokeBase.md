# EeseeAssetSpokeBase

_Crosschain contract that allows users to wrap their assets and send them to a different chain._


## eeseeSpoke

```solidity
struct AddressWithChain eeseeSpoke
```

_AddressWithChain struct for the current contract._

## eeseeAssetHub

```solidity
struct AddressWithChain eeseeAssetHub
```

_AddressWithChain struct for EeseeAssetHub contract._

## assetsStorage

```solidity
mapping(bytes32 => struct Asset) assetsStorage
```

_Maps asset hash to the asset itself._

## stuckAmount

```solidity
mapping(address => mapping(bytes32 => uint256)) stuckAmount
```

_Amount of assets stuck for a given account._

## Stuck

```solidity
event Stuck(struct Asset asset, bytes32 assetHash, address recipient, bytes err)
```

## Unstuck

```solidity
event Unstuck(struct Asset asset, bytes32 assetHash, address sender, address recipient)
```

## InvalidConstructor

```solidity
error InvalidConstructor()
```

## InvalidSender

```solidity
error InvalidSender()
```

## InvalidInterface

```solidity
error InvalidInterface()
```

## InvalidTokenID

```solidity
error InvalidTokenID()
```

## InvalidAsset

```solidity
error InvalidAsset()
```

## InvalidRecipient

```solidity
error InvalidRecipient()
```

## OnlySelf

```solidity
error OnlySelf()
```

## NoAssetsStuck

```solidity
error NoAssetsStuck()
```

## TransferNotSuccessful

```solidity
error TransferNotSuccessful()
```

## InvalidAssetsLength

```solidity
error InvalidAssetsLength()
```

## wrap

```solidity
function wrap(struct Asset[] assets, struct Call[] crosschainCalls, address fallbackRecipient, bytes additionalData) external payable returns (uint256 gasPaid)
```

_Wraps specified assets and sends them to {recipient} in the form of ERC1155 tokens._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| assets | struct Asset[] | - Assets to wrap. |
| crosschainCalls | struct Call[] | - Calls to make on destination chain. |
| fallbackRecipient | address | - Address to transfer leftover tokens after crosschainCalls. |
| additionalData | bytes | - Additional information to pass to crosschain protocol. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| gasPaid | uint256 | - Gas paid for the crosschain call. |

## unstuck

```solidity
function unstuck(bytes32 stuckAssetHash, address recipient) external returns (struct Asset asset)
```

_Reclaim assets that might have been stuck after _unwrap. Emits Unstuck event.
Note: Must be called from asset recipient address that had their asset stuck._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| stuckAssetHash | bytes32 | - Hash of the asset that got stuck. |
| recipient | address | - Address to transfer asset to. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| asset | struct Asset | - Asset sent to {recipient}. |

## getTokenIdL2

```solidity
function getTokenIdL2(struct Asset asset) external view returns (uint256)
```

_Allows calculating token ID on L2 chain before wrapping._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| asset | struct Asset | - Asset to calculate token Id for. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| [0] | uint256 | uint256 - Calculated token Id for wrapped asset on L2 chain. |

## getTokenIdL2

```solidity
function getTokenIdL2(bytes32 assetHash) public view returns (uint256)
```

_Allows calculating token ID on L2 chain before wrapping._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| assetHash | bytes32 | - Asset hash to calculate token Id for. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| [0] | uint256 | uint256 - Calculated token Id for wrapped asset on L2 chain. |

## getAssetHash

```solidity
function getAssetHash(struct Asset asset) public pure returns (bytes32)
```

_Calculates asset hash._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| asset | struct Asset | - Asset to calculate asset hash for. |

## _transferAssetTo

```solidity
function _transferAssetTo(bytes32 assetHash, uint256 amount, address to) external returns (struct Asset asset)
```


# Inherited from EeseeMessageInterface



# Inherited from EeseePausable

## pause

```solidity
function pause() external virtual
```

_Called by the PAUSER_ROLE to pause, triggers stopped state._

## unpause

```solidity
function unpause() external virtual
```

_Called by the PAUSER_ROLE to unpause, returns to normal state._



# Inherited from EeseeRoleHandler



# Inherited from Pausable

## paused

```solidity
function paused() public view virtual returns (bool)
```

_Returns true if the contract is paused, and false otherwise._


## Paused

```solidity
event Paused(address account)
```

_Emitted when the pause is triggered by `account`._

## Unpaused

```solidity
event Unpaused(address account)
```

_Emitted when the pause is lifted by `account`._


# Inherited from ERC1155Holder

## onERC1155Received

```solidity
function onERC1155Received(address, address, uint256, uint256, bytes) public virtual returns (bytes4)
```

## onERC1155BatchReceived

```solidity
function onERC1155BatchReceived(address, address, uint256[], uint256[], bytes) public virtual returns (bytes4)
```



# Inherited from ERC1155Receiver

## supportsInterface

```solidity
function supportsInterface(bytes4 interfaceId) public view virtual returns (bool)
```

_See {IERC165-supportsInterface}._



# Inherited from ERC721Holder

## onERC721Received

```solidity
function onERC721Received(address, address, uint256, bytes) public virtual returns (bytes4)
```

_See {IERC721Receiver-onERC721Received}.

Always returns `IERC721Receiver.onERC721Received.selector`._



# Inherited from ERC2771Context

## isTrustedForwarder

```solidity
function isTrustedForwarder(address forwarder) public view virtual returns (bool)
```



# Inherited from IEeseeAssetSpoke

## eeseeAssetHub

```solidity
function eeseeAssetHub() external view returns (bytes chainSelector, address _address)
```

## assetsStorage

```solidity
function assetsStorage(bytes32) external view returns (address token, uint256 tokenID, uint256 amount, enum AssetType assetType, bytes data)
```


## Wrap

```solidity
event Wrap(struct Asset asset, bytes32 assetHash, address sender)
```

## Unwrap

```solidity
event Unwrap(struct Asset asset, bytes32 assetHash, address recipient)
```


