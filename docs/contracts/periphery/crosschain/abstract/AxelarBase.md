# AxelarBase


## AddressWithChain

```solidity
struct AddressWithChain {
  string chainSelector;
  address _address;
}
```
## AssetHashWithSource

```solidity
struct AssetHashWithSource {
  struct AxelarBase.AddressWithChain source;
  bytes32 assetHash;
}
```
## AxelarSend

```solidity
event AxelarSend(struct AxelarBase.AddressWithChain destination, uint256 gasPaid)
```

## AxelarReceive

```solidity
event AxelarReceive(struct AxelarBase.AddressWithChain source)
```

## InvalidToken

```solidity
error InvalidToken()
```

## InvalidAmount

```solidity
error InvalidAmount()
```


