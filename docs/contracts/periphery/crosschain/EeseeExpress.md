# EeseeExpress

_Squid Router extension that adds permit support and the ability to use ERC20 tokens from contract's balance._


## AddressWithChain

```solidity
struct AddressWithChain {
  string chain;
  address _address;
}
```
## TokenData

```solidity
struct TokenData {
  contract IERC20 token;
  uint256 amount;
  bytes permit;
}
```
## squid

```solidity
contract ISquidRouter squid
```

_Contract address for Squid Router._

## InvalidConstructor

```solidity
error InvalidConstructor()
```

## InvalidRecipient

```solidity
error InvalidRecipient()
```

## callBridgeCall

```solidity
function callBridgeCall(struct EeseeExpress.AddressWithChain squidRouter, string tokenToSymbol, struct EeseeExpress.TokenData tokenFrom, struct ISquidMulticall.Call[] sourceCalls, struct ISquidMulticall.Call[] destinationCalls, address refundRecipient, bool isExpress) external payable
```

_Send tokens to another chain with Squid and call multicall on both chains.
        Note: The caller must either own those tokens, or have them transfered to this contract beforehand. 
       !WARNING! Never send any tokens to this contract from an EOA, or by contract in a separate transaction or they will be lost.
       Only send funds to this contract if you spend them by calling callBridgeCall function in the same transaction._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| squidRouter | struct EeseeExpress.AddressWithChain | - Struct with destination chain and the address of SquidRouter on that chain. |
| tokenToSymbol | string | - Token symbol to transfer to {squidRouter.chain}. |
| tokenFrom | struct EeseeExpress.TokenData | - Token to collect from sender or use from this contract's balance with abi-encoded permit containing approveAmount, deadline, v, r and s. Set to empty bytes to skip permit. |
| sourceCalls | struct ISquidMulticall.Call[] | - Calls to make on this chain. |
| destinationCalls | struct ISquidMulticall.Call[] | - Calls to make on {squidRouter.chain}. |
| refundRecipient | address | - In case calls on destination chain fail, this is the address that will receive the tokens instead. |
| isExpress | bool | - Set to true for GMP express calls. |


# Inherited from EeseePausable

## pause

```solidity
function pause() external virtual
```

_Called by the PAUSER_ROLE to pause, triggers stopped state._

## unpause

```solidity
function unpause() external virtual
```

_Called by the PAUSER_ROLE to unpause, returns to normal state._



# Inherited from EeseeRoleHandler



# Inherited from Pausable

## paused

```solidity
function paused() public view virtual returns (bool)
```

_Returns true if the contract is paused, and false otherwise._


## Paused

```solidity
event Paused(address account)
```

_Emitted when the pause is triggered by `account`._

## Unpaused

```solidity
event Unpaused(address account)
```

_Emitted when the pause is lifted by `account`._


# Inherited from ERC2771Context

## isTrustedForwarder

```solidity
function isTrustedForwarder(address forwarder) public view virtual returns (bool)
```



