# EeseeAssetHubAxelar



# Inherited from AxelarCallerEeseeMessageInterface



# Inherited from EeseeAssetHubBase

## unwrap

```solidity
function unwrap(uint256[] tokenIds, uint256[] amounts, address recipient, bytes additionalData) external payable
```

_Unwraps specified tokens to their source chain. 
       Note: The caller must either own those tokens, or have them transfered to this contract beforehand. 
       !WARNING! Never send any tokens to this contract from an EOA, or by contract in a separate transaction or they will be lost.
       Only send funds to this contract if you spend them by calling unwrap function in the same transaction._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| tokenIds | uint256[] | - Token IDs to unwrap. All tokens must have the same {source}. |
| amounts | uint256[] | - Amount of tokens to unwrap. |
| recipient | address | - Address to unwrap and send tokens to. |
| additionalData | bytes | - Additional information to pass to crosschain protocol. |

## uri

```solidity
function uri(uint256 tokenId) public view returns (string)
```

_Builds and returns tokenId's token URI._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| tokenId | uint256 | - Token ID to check. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| [0] | string | string Token URI. |

## getTokenId

```solidity
function getTokenId(struct AssetHashWithSource assetHashWithSource) public pure returns (uint256)
```

_Allows calculating token IDs before wrapping._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| assetHashWithSource | struct AssetHashWithSource | - Asset hash together with it's source. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| [0] | uint256 | uint256 - Calculated token Id for wrapped asset. |

## supportsInterface

```solidity
function supportsInterface(bytes4 interfaceId) public view virtual returns (bool)
```

## _setRoyalty

```solidity
function _setRoyalty(uint256 tokenId, bytes assetData) external
```



# Inherited from AxelarCaller

## execute

```solidity
function execute(bytes32 commandId, string sourceChain, string sourceAddress, bytes payload) external
```



# Inherited from ICallerBase


## CrosschainSend

```solidity
event CrosschainSend(bytes32 projectIdentifier, struct AddressWithChain destination, bytes additionalData)
```

## CrosschainReceive

```solidity
event CrosschainReceive(bytes32 projectIdentifier, struct AddressWithChain source, bytes additionalData)
```


# Inherited from MulticallExternal

## multicall

```solidity
function multicall(struct Call[] calls) external
```

_Call any contract's function._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| calls | struct Call[] | - Stuct describing the call. |


## AddToBlacklist

```solidity
event AddToBlacklist(address target, bytes4 functionSelector)
```

## RemoveFromBlacklist

```solidity
event RemoveFromBlacklist(address target, bytes4 functionSelector)
```

## MulticallFailed

```solidity
event MulticallFailed(bytes err)
```


# Inherited from ERC2771Context

## isTrustedForwarder

```solidity
function isTrustedForwarder(address forwarder) public view virtual returns (bool)
```



# Inherited from ERC1155Holder

## onERC1155Received

```solidity
function onERC1155Received(address, address, uint256, uint256, bytes) public virtual returns (bytes4)
```

## onERC1155BatchReceived

```solidity
function onERC1155BatchReceived(address, address, uint256[], uint256[], bytes) public virtual returns (bytes4)
```



# Inherited from ERC2981

## royaltyInfo

```solidity
function royaltyInfo(uint256 tokenId, uint256 salePrice) public view virtual returns (address, uint256)
```

_Returns how much royalty is owed and to whom, based on a sale price that may be denominated in any unit of
exchange. The royalty amount is denominated and should be paid in that same unit of exchange._



# Inherited from ERC1155

## balanceOf

```solidity
function balanceOf(address account, uint256 id) public view virtual returns (uint256)
```

_See {IERC1155-balanceOf}.

Requirements:

- `account` cannot be the zero address._

## balanceOfBatch

```solidity
function balanceOfBatch(address[] accounts, uint256[] ids) public view virtual returns (uint256[])
```

_See {IERC1155-balanceOfBatch}.

Requirements:

- `accounts` and `ids` must have the same length._

## setApprovalForAll

```solidity
function setApprovalForAll(address operator, bool approved) public virtual
```

_See {IERC1155-setApprovalForAll}._

## isApprovedForAll

```solidity
function isApprovedForAll(address account, address operator) public view virtual returns (bool)
```

_See {IERC1155-isApprovedForAll}._

## safeTransferFrom

```solidity
function safeTransferFrom(address from, address to, uint256 id, uint256 amount, bytes data) public virtual
```

_See {IERC1155-safeTransferFrom}._

## safeBatchTransferFrom

```solidity
function safeBatchTransferFrom(address from, address to, uint256[] ids, uint256[] amounts, bytes data) public virtual
```

_See {IERC1155-safeBatchTransferFrom}._



# Inherited from IERC1155


## TransferSingle

```solidity
event TransferSingle(address operator, address from, address to, uint256 id, uint256 value)
```

_Emitted when `value` tokens of token type `id` are transferred from `from` to `to` by `operator`._

## TransferBatch

```solidity
event TransferBatch(address operator, address from, address to, uint256[] ids, uint256[] values)
```

_Equivalent to multiple {TransferSingle} events, where `operator`, `from` and `to` are the same for all
transfers._

## ApprovalForAll

```solidity
event ApprovalForAll(address account, address operator, bool approved)
```

_Emitted when `account` grants or revokes permission to `operator` to transfer their tokens, according to
`approved`._

## URI

```solidity
event URI(string value, uint256 id)
```

_Emitted when the URI for token type `id` changes to `value`, if it is a non-programmatic URI.

If an {URI} event was emitted for `id`, the standard
https://eips.ethereum.org/EIPS/eip-1155#metadata-extensions[guarantees] that `value` will equal the value
returned by {IERC1155MetadataURI-uri}._


# Inherited from IEeseeAssetHub

## assetHashesWithSources

```solidity
function assetHashesWithSources(uint256) external view returns (struct AddressWithChain source, bytes32 assetHash)
```


## Wrap

```solidity
event Wrap(uint256 tokenId, uint256 amount, struct AssetHashWithSource asset)
```

## Unwrap

```solidity
event Unwrap(uint256 tokenId, uint256 amount, struct AssetHashWithSource asset, address sender, address recipient)
```


