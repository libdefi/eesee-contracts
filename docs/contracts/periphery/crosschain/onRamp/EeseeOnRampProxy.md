# EeseeOnRampProxy


## onRampImplementation

```solidity
address onRampImplementation
```

_onRamp contract implementation._

## UpdateOnRampImplementation

```solidity
event UpdateOnRampImplementation(address oldOnRampImplementation, address newOnRampImplementation, bytes initData)
```

## ChangedOnRampImplementation

```solidity
error ChangedOnRampImplementation()
```

## InvalidOnRampImplementation

```solidity
error InvalidOnRampImplementation()
```

## forward

```solidity
function forward(bytes data) external payable returns (bytes returnData)
```

_Delegates coll to onRampImplementation._

## updateOnRampImplementation

```solidity
function updateOnRampImplementation(address _onRampImplementation, bytes initData) external
```

_Updates onRamp implementation. Note: can only be called by ADMIN_ROLE._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _onRampImplementation | address | - New onRampImplementation. |
| initData | bytes | - Data with which initialize function in implementation is called. |


# Inherited from EeseePausable

## pause

```solidity
function pause() external virtual
```

_Called by the PAUSER_ROLE to pause, triggers stopped state._

## unpause

```solidity
function unpause() external virtual
```

_Called by the PAUSER_ROLE to unpause, returns to normal state._



# Inherited from EeseeRoleHandler



# Inherited from Pausable

## paused

```solidity
function paused() public view virtual returns (bool)
```

_Returns true if the contract is paused, and false otherwise._


## Paused

```solidity
event Paused(address account)
```

_Emitted when the pause is triggered by `account`._

## Unpaused

```solidity
event Unpaused(address account)
```

_Emitted when the pause is lifted by `account`._


