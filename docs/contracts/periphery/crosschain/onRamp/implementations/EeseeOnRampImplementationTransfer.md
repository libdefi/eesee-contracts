# EeseeOnRampImplementationTransfer


## InvalidChainSelector

```solidity
error InvalidChainSelector()
```

## InvalidMsgValue

```solidity
error InvalidMsgValue()
```

## initialize

```solidity
function initialize(bytes data) public
```

_Initialize onRamp with provided data. 
Data is abi-encoded tuple(0x,address) offRamp.
Note: Proxies using this implementation should implement access control for this function._


# Inherited from EeseeOnRampImplementationBase

## forward

```solidity
function forward(bytes data) external payable
```

_Forwards ESE balance from this contract to {offRamp}._



# Inherited from IEeseeOnRampImplementation

## ESE

```solidity
function ESE() external view returns (contract IERC20)
```

## offRamp

```solidity
function offRamp() external view returns (bytes chainSelector, address _address)
```


## Forward

```solidity
event Forward(struct AddressWithChain destination, uint256 amount)
```


