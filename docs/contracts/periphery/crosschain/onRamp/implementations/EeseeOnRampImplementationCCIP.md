# EeseeOnRampImplementationCCIP


## gasLimit

```solidity
uint256 gasLimit
```

_Gas Limit used in CCIP call._

## CantReceiveMessages

```solidity
error CantReceiveMessages()
```

## InvalidGasLimit

```solidity
error InvalidGasLimit()
```

## initialize

```solidity
function initialize(bytes data) public
```

_Initialize onRamp with provided data. 
Data is abi-encoded (uint256 gasLimit, tuple(bytes,address) offRamp).
Note: Proxies using this implementation should implement access control for this function._

## supportsInterface

```solidity
function supportsInterface(bytes4 interfaceId) public view virtual returns (bool)
```

_See {IERC165-supportsInterface}._


# Inherited from CCIPCaller

## ccipReceive

```solidity
function ccipReceive(struct Client.Any2EVMMessage message) external virtual
```

Called by the Router to deliver a message.
If this reverts, any token transfers also revert. The message
will move to a FAILED state and become available for manual execution.

_Note ensure you check the msg.sender is the OffRampRouter_

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| message | struct Client.Any2EVMMessage | CCIP Message |



# Inherited from ICallerBase


## CrosschainSend

```solidity
event CrosschainSend(bytes32 projectIdentifier, struct AddressWithChain destination, bytes additionalData)
```

## CrosschainReceive

```solidity
event CrosschainReceive(bytes32 projectIdentifier, struct AddressWithChain source, bytes additionalData)
```


# Inherited from EeseeOnRampImplementationBase

## forward

```solidity
function forward(bytes data) external payable
```

_Forwards ESE balance from this contract to {offRamp}._



# Inherited from IEeseeOnRampImplementation

## ESE

```solidity
function ESE() external view returns (contract IERC20)
```

## offRamp

```solidity
function offRamp() external view returns (bytes chainSelector, address _address)
```


## Forward

```solidity
event Forward(struct AddressWithChain destination, uint256 amount)
```


