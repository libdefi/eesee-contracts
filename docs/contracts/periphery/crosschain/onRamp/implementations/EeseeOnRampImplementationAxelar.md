# EeseeOnRampImplementationAxelar

Note: WIP, this should be either protected by access control or be using Axelar's on onchain gas estimation, which has not released yet.


## tokenId

```solidity
bytes32 tokenId
```

_Token Id used in Axelar Interchain Token Service._

## CantReceiveMessages

```solidity
error CantReceiveMessages()
```

## InvalidTokenId

```solidity
error InvalidTokenId()
```

## initialize

```solidity
function initialize(bytes data) public
```

_Initialize onRamp with provided data. Data is abi-encoded tuple(bytes,address) offRamp.
Note: Proxies using this implementation should implement access control for this function._


# Inherited from AxelarInterchainTokenServiceCaller



# Inherited from ICallerBase


## CrosschainSend

```solidity
event CrosschainSend(bytes32 projectIdentifier, struct AddressWithChain destination, bytes additionalData)
```

## CrosschainReceive

```solidity
event CrosschainReceive(bytes32 projectIdentifier, struct AddressWithChain source, bytes additionalData)
```


# Inherited from InterchainTokenExecutable

## executeWithInterchainToken

```solidity
function executeWithInterchainToken(bytes32 commandId, string sourceChain, bytes sourceAddress, bytes data, bytes32 tokenId, address token, uint256 amount) external virtual returns (bytes32)
```

Executes logic in the context of an interchain token transfer.

_Only callable by the interchain token service._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| commandId | bytes32 | The unique message id. |
| sourceChain | string | The source chain of the token transfer. |
| sourceAddress | bytes | The source address of the token transfer. |
| data | bytes | The data associated with the token transfer. |
| tokenId | bytes32 | The token ID. |
| token | address | The token address. |
| amount | uint256 | The amount of tokens being transferred. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| [0] | bytes32 | bytes32 Hash indicating success of the execution. |



# Inherited from EeseeOnRampImplementationBase

## forward

```solidity
function forward(bytes data) external payable
```

_Forwards ESE balance from this contract to {offRamp}._



# Inherited from IEeseeOnRampImplementation

## ESE

```solidity
function ESE() external view returns (contract IERC20)
```

## offRamp

```solidity
function offRamp() external view returns (bytes chainSelector, address _address)
```


## Forward

```solidity
event Forward(struct AddressWithChain destination, uint256 amount)
```


