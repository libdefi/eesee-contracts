# EeseeOffRampCCIP


## ccipReceive

```solidity
function ccipReceive(struct Client.Any2EVMMessage) external
```

_Whenever this contract receives CCIP message, transfers all ESE in balance to recipient.
       Note that this function does not have means of access control, meaning anyone can call it._

## supportsInterface

```solidity
function supportsInterface(bytes4 interfaceId) public view returns (bool)
```

_See {IERC165-supportsInterface}._


# Inherited from EeseeOffRampBase



# Inherited from IEeseeOffRamp

## ESE

```solidity
function ESE() external view returns (contract IERC20)
```

## recipient

```solidity
function recipient() external view returns (address)
```


## Forward

```solidity
event Forward(uint256 amount)
```


