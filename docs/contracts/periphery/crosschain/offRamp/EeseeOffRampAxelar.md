# EeseeOffRampAxelar


## EXECUTE_SUCCESS

```solidity
bytes32 EXECUTE_SUCCESS
```

## executeWithInterchainToken

```solidity
function executeWithInterchainToken(bytes32, string, bytes, bytes, bytes32, address, uint256) external returns (bytes32)
```

_Whenever this contract receives Axelar message, transfers all ESE in balance to recipient.
       Note that this function does not have means of access control, meaning anyone can call it._


# Inherited from EeseeOffRampBase



# Inherited from IEeseeOffRamp

## ESE

```solidity
function ESE() external view returns (contract IERC20)
```

## recipient

```solidity
function recipient() external view returns (address)
```


## Forward

```solidity
event Forward(uint256 amount)
```


