# EeseeSplit


## Recipient

```solidity
struct Recipient {
  address payable recipient;
  uint16 bps;
}
```
## royaltyShare

```solidity
uint256 royaltyShare
```

_Share of total sale that goes to royalty. [10000 = 100%]._

## InvalidRoyaltyShare

```solidity
error InvalidRoyaltyShare()
```

## initializeWithRoyaltyShare

```solidity
function initializeWithRoyaltyShare(uint256 _royaltyShare, address _defaultAdmin, string _contractURI, address[] _trustedForwarders, address[] _payees, uint256[] _shares) external
```

## getRecipients

```solidity
function getRecipients() external view returns (struct EeseeSplit.Recipient[] splitRecipients)
```

_RoyaltyEngine requires this function to be present for Multi Recipient ERC2981 Royalties._


# Inherited from Split

## initialize

```solidity
function initialize(address _defaultAdmin, string _contractURI, address[] _trustedForwarders, address[] _payees, uint256[] _shares) external
```

_Performs the job of the constructor.
shares_ are scaled by 10,000 to prevent precision loss when including fees_

## contractType

```solidity
function contractType() external pure returns (bytes32)
```

_Returns the module type of the contract._

## contractVersion

```solidity
function contractVersion() external pure returns (uint8)
```

_Returns the version of the contract._

## release

```solidity
function release(address payable account) public virtual
```

_Triggers a transfer to `account` of the amount of Ether they are owed, according to their percentage of the
total shares and their previous withdrawals._

## release

```solidity
function release(contract IERC20Upgradeable token, address account) public virtual
```

_Triggers a transfer to `account` of the amount of `token` tokens they are owed, according to their
percentage of the total shares and their previous withdrawals. `token` must be the address of an IERC20
contract._

## distribute

```solidity
function distribute() public virtual
```

_Release the owed amount of token to all of the payees._

## distribute

```solidity
function distribute(contract IERC20Upgradeable token) public virtual
```

_Release owed amount of the `token` to all of the payees._

## setContractURI

```solidity
function setContractURI(string _uri) external
```

_Sets contract URI for the contract-level metadata of the contract._



# Inherited from PaymentSplitterUpgradeable

## totalShares

```solidity
function totalShares() public view returns (uint256)
```

_Getter for the total shares held by payees._

## totalReleased

```solidity
function totalReleased() public view returns (uint256)
```

_Getter for the total amount of Ether already released._

## totalReleased

```solidity
function totalReleased(contract IERC20Upgradeable token) public view returns (uint256)
```

_Getter for the total amount of `token` already released. `token` should be the address of an IERC20
contract._

## shares

```solidity
function shares(address account) public view returns (uint256)
```

_Getter for the amount of shares held by an account._

## released

```solidity
function released(address account) public view returns (uint256)
```

_Getter for the amount of Ether already released to a payee._

## released

```solidity
function released(contract IERC20Upgradeable token, address account) public view returns (uint256)
```

_Getter for the amount of `token` tokens already released to a payee. `token` should be the address of an
IERC20 contract._

## payee

```solidity
function payee(uint256 index) public view returns (address)
```

_Getter for the address of the payee number `index`._

## payeeCount

```solidity
function payeeCount() public view returns (uint256)
```

_Get the number of payees_

## releasable

```solidity
function releasable(address account) public view returns (uint256)
```

_Getter for the amount of payee's releasable Ether._

## releasable

```solidity
function releasable(contract IERC20Upgradeable token, address account) public view returns (uint256)
```

_Getter for the amount of payee's releasable `token` tokens. `token` should be the address of an
IERC20 contract._


## PayeeAdded

```solidity
event PayeeAdded(address account, uint256 shares)
```

## PaymentReleased

```solidity
event PaymentReleased(address to, uint256 amount)
```

## ERC20PaymentReleased

```solidity
event ERC20PaymentReleased(contract IERC20Upgradeable token, address to, uint256 amount)
```

## PaymentReceived

```solidity
event PaymentReceived(address from, uint256 amount)
```


# Inherited from AccessControlEnumerableUpgradeable

## supportsInterface

```solidity
function supportsInterface(bytes4 interfaceId) public view virtual returns (bool)
```

_See {IERC165-supportsInterface}._

## getRoleMember

```solidity
function getRoleMember(bytes32 role, uint256 index) public view virtual returns (address)
```

_Returns one of the accounts that have `role`. `index` must be a
value between 0 and {getRoleMemberCount}, non-inclusive.

Role bearers are not sorted in any particular way, and their ordering may
change at any point.

WARNING: When using {getRoleMember} and {getRoleMemberCount}, make sure
you perform all queries on the same block. See the following
https://forum.openzeppelin.com/t/iterating-over-elements-on-enumerableset-in-openzeppelin-contracts/2296[forum post]
for more information._

## getRoleMemberCount

```solidity
function getRoleMemberCount(bytes32 role) public view virtual returns (uint256)
```

_Returns the number of accounts that have `role`. Can be used
together with {getRoleMember} to enumerate all bearers of a role._



# Inherited from AccessControlUpgradeable

## hasRole

```solidity
function hasRole(bytes32 role, address account) public view virtual returns (bool)
```

_Returns `true` if `account` has been granted `role`._

## getRoleAdmin

```solidity
function getRoleAdmin(bytes32 role) public view virtual returns (bytes32)
```

_Returns the admin role that controls `role`. See {grantRole} and
{revokeRole}.

To change a role's admin, use {_setRoleAdmin}._

## grantRole

```solidity
function grantRole(bytes32 role, address account) public virtual
```

_Grants `role` to `account`.

If `account` had not been already granted `role`, emits a {RoleGranted}
event.

Requirements:

- the caller must have ``role``'s admin role.

May emit a {RoleGranted} event._

## revokeRole

```solidity
function revokeRole(bytes32 role, address account) public virtual
```

_Revokes `role` from `account`.

If `account` had been granted `role`, emits a {RoleRevoked} event.

Requirements:

- the caller must have ``role``'s admin role.

May emit a {RoleRevoked} event._

## renounceRole

```solidity
function renounceRole(bytes32 role, address account) public virtual
```

_Revokes `role` from the calling account.

Roles are often managed via {grantRole} and {revokeRole}: this function's
purpose is to provide a mechanism for accounts to lose their privileges
if they are compromised (such as when a trusted device is misplaced).

If the calling account had been revoked `role`, emits a {RoleRevoked}
event.

Requirements:

- the caller must be `account`.

May emit a {RoleRevoked} event._



# Inherited from ERC165Upgradeable



# Inherited from IAccessControlUpgradeable


## RoleAdminChanged

```solidity
event RoleAdminChanged(bytes32 role, bytes32 previousAdminRole, bytes32 newAdminRole)
```

_Emitted when `newAdminRole` is set as ``role``'s admin role, replacing `previousAdminRole`

`DEFAULT_ADMIN_ROLE` is the starting admin for all roles, despite
{RoleAdminChanged} not being emitted signaling this.

_Available since v3.1.__

## RoleGranted

```solidity
event RoleGranted(bytes32 role, address account, address sender)
```

_Emitted when `account` is granted `role`.

`sender` is the account that originated the contract call, an admin role
bearer except when using {AccessControl-_setupRole}._

## RoleRevoked

```solidity
event RoleRevoked(bytes32 role, address account, address sender)
```

_Emitted when `account` is revoked `role`.

`sender` is the account that originated the contract call:
  - if using `revokeRole`, it is the admin role bearer
  - if using `renounceRole`, it is the role bearer (i.e. `account`)_


# Inherited from ERC2771ContextUpgradeable

## isTrustedForwarder

```solidity
function isTrustedForwarder(address forwarder) public view virtual returns (bool)
```



# Inherited from ContextUpgradeable



# Inherited from Multicall

## multicall

```solidity
function multicall(bytes[] data) external virtual returns (bytes[] results)
```

@notice Receives and executes a batch of function calls on this contract.
 @dev Receives and executes a batch of function calls on this contract.

 @param data The bytes data that makes up the batch of function calls to execute.
 @return results The bytes data that makes up the result of the batch of function calls executed.



# Inherited from Initializable


## Initialized

```solidity
event Initialized(uint8 version)
```

_Triggered when the contract has been initialized or reinitialized._


# Inherited from IThirdwebContract

## contractURI

```solidity
function contractURI() external view returns (string)
```

_Returns the metadata URI of the contract._



