# EeseeWinner


## Eesee

```solidity
contract IEesee Eesee
```

_Eesee contract._

## random

```solidity
contract IEeseeRandom random
```

_Contract that provides Eesee with random._

## returnInterval

```solidity
uint32 returnInterval
```

_In case random request fails to get delivered {returnInterval} seconds after the lot was closed, unlock Reclaim functions._

## InvalidEesee

```solidity
error InvalidEesee()
```

## LotNotExists

```solidity
error LotNotExists()
```

## LotNotFulfilled

```solidity
error LotNotFulfilled()
```

## LotExpired

```solidity
error LotExpired()
```

## NoTicketsBought

```solidity
error NoTicketsBought()
```

## getLotWinner

```solidity
function getLotWinner(uint256 ID) external view returns (address winner, bool isAssetWinner)
```

_Get the winner of eesee lot. Return address(0) if no winner found._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| ID | uint256 | - ID of the lot. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| winner | address | - Lot winner. Returns address(0) if no winner chosen. |
| isAssetWinner | bool | - True if winner recieves asset. False if winner receives ESE tokens. |


