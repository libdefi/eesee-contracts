# EeseePeripheryBase


## ExecutionFailed

```solidity
error ExecutionFailed(bytes err)
```

## executeWithSwap

```solidity
function executeWithSwap(bytes swapData, address target, bytes targetCalldata) external payable returns (contract IERC20 srcToken, uint256 srcSpent, uint256 srcDust, contract IERC20 dstToken, uint256 dstAmount, uint256 dstDust, bytes returnData)
```

_Collects tokens from sender, swaps them for dstToken and calls {target} contract with {targetCalldata}._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| swapData | bytes | - Data for swap. |
| target | address | - Target to call with {targetCalldata}. |
| targetCalldata | bytes | - Data to call {target} with. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| srcToken | contract IERC20 | - Source token used in swap. |
| srcSpent | uint256 | - Source token amount spent in swap. |
| srcDust | uint256 | - Source token dust returned to msg.sender. |
| dstToken | contract IERC20 | - Destination token returned from swap. |
| dstAmount | uint256 | - Destination token amount spent or recieved in {target} call. |
| dstDust | uint256 | - Destination token dust returned to msg.sender. |
| returnData | bytes | - Data returned from {target} call. |


# Inherited from EeseeExecuteWithSwapBase



# Inherited from EeseePausable

## pause

```solidity
function pause() external virtual
```

_Called by the PAUSER_ROLE to pause, triggers stopped state._

## unpause

```solidity
function unpause() external virtual
```

_Called by the PAUSER_ROLE to unpause, returns to normal state._



# Inherited from EeseeRoleHandler



# Inherited from Pausable

## paused

```solidity
function paused() public view virtual returns (bool)
```

_Returns true if the contract is paused, and false otherwise._


## Paused

```solidity
event Paused(address account)
```

_Emitted when the pause is triggered by `account`._

## Unpaused

```solidity
event Unpaused(address account)
```

_Emitted when the pause is lifted by `account`._


# Inherited from ERC2771Context

## isTrustedForwarder

```solidity
function isTrustedForwarder(address forwarder) public view virtual returns (bool)
```



