# IBlastPoints


## configurePointsOperator

```solidity
function configurePointsOperator(address operator) external
```


# MultichainECDSAValidatorWithBlastPoints


## blastPointsAddress

```solidity
address blastPointsAddress
```

## isInitializedBlastPointsOperatorForAccount

```solidity
mapping(address => bool) isInitializedBlastPointsOperatorForAccount
```

## BlastPointsOperatorAlreadyInitialized

```solidity
error BlastPointsOperatorAlreadyInitialized()
```

## InvalidBlastPointsAddress

```solidity
error InvalidBlastPointsAddress()
```

## InvalidPointsOperator

```solidity
error InvalidPointsOperator()
```

## ExecTransacionFromModuleFailed

```solidity
error ExecTransacionFromModuleFailed()
```

## InvalidSmartAccount

```solidity
error InvalidSmartAccount()
```

## initializeBlastPointsOperator

```solidity
function initializeBlastPointsOperator(address smartAccountAddress, address pointsOperator) external
```


# Inherited from MultichainECDSAValidator

## validateUserOp

```solidity
function validateUserOp(struct UserOperation userOp, bytes32 userOpHash) external view virtual returns (uint256)
```

_Validates User Operation.
leaf = validUntil + validAfter + userOpHash
If the leaf is the part of the Tree with a root provided, userOp considered
to be authorized by user_

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| userOp | struct UserOperation | user operation to be validated |
| userOpHash | bytes32 | hash of the userOp provided by the EP |



# Inherited from EcdsaOwnershipRegistryModule

## initForSmartAccount

```solidity
function initForSmartAccount(address eoaOwner) external returns (address)
```

_Initializes the module for a Smart Account.
Should be used at a time of first enabling the module for a Smart Account._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| eoaOwner | address | The owner of the Smart Account. Should be EOA! |

## transferOwnership

```solidity
function transferOwnership(address owner) external
```

_Sets/changes an for a Smart Account.
Should be called by Smart Account itself._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| owner | address | The owner of the Smart Account. |

## renounceOwnership

```solidity
function renounceOwnership() external
```

_Renounces ownership
should be called by Smart Account._

## getOwner

```solidity
function getOwner(address smartAccount) external view returns (address)
```

_Returns the owner of the Smart Account. Reverts for Smart Accounts without owners._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| smartAccount | address | Smart Account address. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| [0] | address | owner The owner of the Smart Account. |

## isValidSignature

```solidity
function isValidSignature(bytes32 dataHash, bytes moduleSignature) public view virtual returns (bytes4)
```

_Validates a signature for a message.
To be called from a Smart Account._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| dataHash | bytes32 | Exact hash of the data that was signed. |
| moduleSignature | bytes | Signature to be validated. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| [0] | bytes4 | EIP1271_MAGIC_VALUE if signature is valid, 0xffffffff otherwise. |

## isValidSignatureForAddress

```solidity
function isValidSignatureForAddress(bytes32 dataHash, bytes moduleSignature, address smartAccount) public view virtual returns (bytes4)
```

_Validates a signature for a message signed by address.
Also try dataHash.toEthSignedMessageHash()_

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| dataHash | bytes32 | hash of the data |
| moduleSignature | bytes | Signature to be validated. |
| smartAccount | address | expected signer Smart Account address. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| [0] | bytes4 | EIP1271_MAGIC_VALUE if signature is valid, 0xffffffff otherwise. |


## OwnershipTransferred

```solidity
event OwnershipTransferred(address smartAccount, address oldOwner, address newOwner)
```


