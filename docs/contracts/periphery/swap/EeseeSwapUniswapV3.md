# EeseeSwapUniswapV3



# Inherited from EeseeUniswapV3Base



# Inherited from EeseeSwapBase

## swapTokensForAssets

```solidity
function swapTokensForAssets(struct IEeseeSwap.SwapParams swapParams, address recipient) external payable returns (contract IERC20 srcToken, uint256 srcSpent, struct Asset[] assets)
```

_Swap tokens and receive assets the sender chooses. Emits {ReceiveAsset} event for each of the asset received.
        Note: Any dust left after swap or asset purchases goes to eesee's treasury as fee._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| swapParams | struct IEeseeSwap.SwapParams | - Struct containing swap data and addresses with data to call marketplace routers with. |
| recipient | address | - Address to send rewards to. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| srcToken | contract IERC20 | - srcToken used in swap. |
| srcSpent | uint256 | - srcToken amount used. |
| assets | struct Asset[] | - Assets received. |

## updateTreasury

```solidity
function updateTreasury(address _treasury) external
```

_Update treasury account address leftover tokens are sent to._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| _treasury | address | - New treasury account. |


## CollectToTreasury

```solidity
event CollectToTreasury(contract IERC20 token, uint256 amount, address treasury)
```

## UpdateTreasury

```solidity
event UpdateTreasury(address previousTreasury, address newTreasury)
```


# Inherited from EeseeExecuteWithSwapBase



# Inherited from EeseePausable

## pause

```solidity
function pause() external virtual
```

_Called by the PAUSER_ROLE to pause, triggers stopped state._

## unpause

```solidity
function unpause() external virtual
```

_Called by the PAUSER_ROLE to unpause, returns to normal state._



# Inherited from EeseeRoleHandler



# Inherited from Pausable

## paused

```solidity
function paused() public view virtual returns (bool)
```

_Returns true if the contract is paused, and false otherwise._


## Paused

```solidity
event Paused(address account)
```

_Emitted when the pause is triggered by `account`._

## Unpaused

```solidity
event Unpaused(address account)
```

_Emitted when the pause is lifted by `account`._


# Inherited from ERC2771Context

## isTrustedForwarder

```solidity
function isTrustedForwarder(address forwarder) public view virtual returns (bool)
```



# Inherited from IEeseeSwap


## ReceiveAsset

```solidity
event ReceiveAsset(struct Asset assetBought, contract IERC20 tokenSpent, uint256 spent, address recipient)
```


