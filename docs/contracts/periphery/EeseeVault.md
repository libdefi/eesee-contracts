# EeseeVault

_Contract to store and reclaim stuck eeseeAssetHub tokens._


## assetHub

```solidity
contract IERC1155 assetHub
```

_Asset hub ERC1155s this contract accepts._

## stuck

```solidity
mapping(address => mapping(uint256 => uint256)) stuck
```

_Amount of tokens stuck for a given account and tokenId._

## InvalidConstructor

```solidity
error InvalidConstructor()
```

## InvalidRecipient

```solidity
error InvalidRecipient()
```

## NoTokensStuck

```solidity
error NoTokensStuck()
```

## InvalidTokenSent

```solidity
error InvalidTokenSent()
```

## InvalidTokenIdsLength

```solidity
error InvalidTokenIdsLength()
```

## unstuck

```solidity
function unstuck(uint256[] tokenIds, address recipient) external returns (uint256[] amounts)
```

_Reclaim assetHub ERC1155s that might have been stuck in this contract.
Note: Must be called from an address that had their asset stuck._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| tokenIds | uint256[] | - Tokens ids to claim. |
| recipient | address | - Address to transfer tokens to. |

### Return Values

| Name | Type | Description |
| ---- | ---- | ----------- |
| amounts | uint256[] | - Amounts of each tokenId claimed. |

## onERC1155Received

```solidity
function onERC1155Received(address, address, uint256 tokenId, uint256 amount, bytes data) public returns (bytes4)
```

_Whennever an assetHub ERC1155 gets sent to this contract, write it to storage for later claiming with {unstuck} function._

## onERC1155BatchReceived

```solidity
function onERC1155BatchReceived(address, address, uint256[] tokenIds, uint256[] amounts, bytes data) public returns (bytes4)
```

_Whennever an assetHub ERC1155 gets sent to this contract, write it to storage for later claiming with {unstuck} function._


# Inherited from ERC1155Receiver

## supportsInterface

```solidity
function supportsInterface(bytes4 interfaceId) public view virtual returns (bool)
```

_See {IERC165-supportsInterface}._



# Inherited from ERC2771Context

## isTrustedForwarder

```solidity
function isTrustedForwarder(address forwarder) public view virtual returns (bool)
```



