# SmartAccountFactoryWrapper


## smartAccountFactory

```solidity
contract SmartAccountFactory smartAccountFactory
```

## multichainECDSAValidatorWithBlastPoints

```solidity
contract MultichainECDSAValidatorWithBlastPoints multichainECDSAValidatorWithBlastPoints
```

## InvalidSmartAccountFactory

```solidity
error InvalidSmartAccountFactory()
```

## InvalidModule

```solidity
error InvalidModule()
```

## tryDeployCounterFactualAccount

```solidity
function tryDeployCounterFactualAccount(address eoaOwner, address pointsOperator) external returns (address account)
```

_Deploys and initializes account if it was not deployed yet._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| eoaOwner | address | - Owner to deploy account for. |
| pointsOperator | address | - Blast points operator. |


