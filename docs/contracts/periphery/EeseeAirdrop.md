# EeseeAirdrop


## Recipient

```solidity
struct Recipient {
  address recipient;
  uint256 amount;
}
```
## ESE

```solidity
contract IERC20 ESE
```

_Token to airdrop._

## InvalidESE

```solidity
error InvalidESE()
```

## airdrop

```solidity
function airdrop(struct EeseeAirdrop.Recipient[] recipients) external
```

_Airdrops ESE tokens to recipients._

### Parameters

| Name | Type | Description |
| ---- | ---- | ----------- |
| recipients | struct EeseeAirdrop.Recipient[] | - Recipients' addresses and amounts they receive. Note: This function can only be called by AIRDROP_ROLE. |


# Inherited from EeseeRoleHandler



