# IWETH9User


## WETH9

```solidity
function WETH9() external view returns (address)
```


# EeseeUniswapV3Base


## uniswapRouterV3

```solidity
address uniswapRouterV3
```

_Uniswap router used for token swaps._

## WETH

```solidity
address WETH
```

_WETH token address._

## InvalidRouter

```solidity
error InvalidRouter()
```

## InvalidSelector

```solidity
error InvalidSelector()
```


# Inherited from EeseeExecuteWithSwapBase



# Inherited from EeseePausable

## pause

```solidity
function pause() external virtual
```

_Called by the PAUSER_ROLE to pause, triggers stopped state._

## unpause

```solidity
function unpause() external virtual
```

_Called by the PAUSER_ROLE to unpause, returns to normal state._



# Inherited from EeseeRoleHandler



# Inherited from Pausable

## paused

```solidity
function paused() public view virtual returns (bool)
```

_Returns true if the contract is paused, and false otherwise._


## Paused

```solidity
event Paused(address account)
```

_Emitted when the pause is triggered by `account`._

## Unpaused

```solidity
event Unpaused(address account)
```

_Emitted when the pause is lifted by `account`._


# Inherited from ERC2771Context

## isTrustedForwarder

```solidity
function isTrustedForwarder(address forwarder) public view virtual returns (bool)
```



