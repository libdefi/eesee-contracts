# InvalidToken

```solidity
error InvalidToken()
```

# InvalidAmount

```solidity
error InvalidAmount()
```

# InsufficientValue

```solidity
error InsufficientValue()
```

