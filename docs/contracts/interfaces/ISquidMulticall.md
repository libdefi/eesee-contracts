# ISquidMulticall


## CallType

```solidity
enum CallType {
  Default,
  FullTokenBalance,
  FullNativeBalance,
  CollectTokenBalance
}
```
## Call

```solidity
struct Call {
  enum ISquidMulticall.CallType callType;
  address target;
  uint256 value;
  bytes callData;
  bytes payload;
}
```
## AlreadyRunning

```solidity
error AlreadyRunning()
```

## CallFailed

```solidity
error CallFailed(uint256 callPosition, bytes reason)
```

## run

```solidity
function run(struct ISquidMulticall.Call[] calls) external payable
```


