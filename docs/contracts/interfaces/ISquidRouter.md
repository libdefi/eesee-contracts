# ISquidRouter


## CrossMulticallExecuted

```solidity
event CrossMulticallExecuted(bytes32 payloadHash)
```

## CrossMulticallFailed

```solidity
event CrossMulticallFailed(bytes32 payloadHash, bytes reason, address refundRecipient)
```

## ZeroAddressProvided

```solidity
error ZeroAddressProvided()
```

## ApprovalFailed

```solidity
error ApprovalFailed()
```

## bridgeCall

```solidity
function bridgeCall(string bridgedTokenSymbol, uint256 amount, string destinationChain, string destinationAddress, bytes payload, address gasRefundRecipient, bool enableExpress) external payable
```

## callBridge

```solidity
function callBridge(address token, uint256 amount, struct ISquidMulticall.Call[] calls, string bridgedTokenSymbol, string destinationChain, string destinationAddress) external payable
```

## callBridgeCall

```solidity
function callBridgeCall(address token, uint256 amount, struct ISquidMulticall.Call[] calls, string bridgedTokenSymbol, string destinationChain, string destinationAddress, bytes payload, address gasRefundRecipient, bool enableExpress) external payable
```

## fundAndRunMulticall

```solidity
function fundAndRunMulticall(address token, uint256 amount, struct ISquidMulticall.Call[] calls) external payable
```


