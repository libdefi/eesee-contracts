# IEeseeAssetHub


## assetHashesWithSources

```solidity
function assetHashesWithSources(uint256) external view returns (struct AddressWithChain source, bytes32 assetHash)
```

## Wrap

```solidity
event Wrap(uint256 tokenId, uint256 amount, struct AssetHashWithSource asset)
```

## Unwrap

```solidity
event Unwrap(uint256 tokenId, uint256 amount, struct AssetHashWithSource asset, address sender, address recipient)
```

## unwrap

```solidity
function unwrap(uint256[] tokenIds, uint256[] amounts, address recipient, bytes additionalData) external payable
```

## getTokenId

```solidity
function getTokenId(struct AssetHashWithSource assetHashWithSource) external view returns (uint256)
```


