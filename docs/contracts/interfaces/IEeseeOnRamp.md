# IEeseeOnRamp


## Forward

```solidity
event Forward(struct AddressWithChain destination, uint256 amount)
```

## ESE

```solidity
function ESE() external view returns (contract IERC20)
```

## offRamp

```solidity
function offRamp() external view returns (bytes chainSelector, address _address)
```

## initialize

```solidity
function initialize(bytes data) external
```

## forward

```solidity
function forward(bytes data) external payable
```


