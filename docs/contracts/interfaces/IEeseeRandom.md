# IEeseeRandom


## FulfillRandom

```solidity
event FulfillRandom(uint256 random)
```

## RequestRandom

```solidity
event RequestRandom()
```

## random

```solidity
function random(uint256) external view returns (uint256 word, uint256 timestamp)
```

## lastRandomRequestTimestamp

```solidity
function lastRandomRequestTimestamp() external view returns (uint64)
```

## randomRequestInterval

```solidity
function randomRequestInterval() external view returns (uint32)
```

## MAX_RANDOM_REQUEST_INTERVAL

```solidity
function MAX_RANDOM_REQUEST_INTERVAL() external view returns (uint48)
```

## getRandomFromTimestamp

```solidity
function getRandomFromTimestamp(uint256 _timestamp) external view returns (uint256 word, uint256 timestamp)
```

## canRequestRandom

```solidity
function canRequestRandom() external view returns (bool randomNeeded)
```


