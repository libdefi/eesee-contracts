# IEeseePeriphery


## executeWithSwap

```solidity
function executeWithSwap(bytes swapData, address target, bytes targetCalldata) external payable returns (contract IERC20 srcToken, uint256 srcSpent, uint256 srcDust, contract IERC20 dstToken, uint256 dstAmount, uint256 dstDust, bytes returnData)
```


