# IEeseeSwap


## SwapParams

_Struct used for NFT swap.
{swapData} - Swap data to use.
{marketplaceRoutersData} - Addresses with data to call marketplace routers with._

```solidity
struct SwapParams {
  bytes swapData;
  struct IEeseeSwap.MarketplaceRouterData[] marketplaceRoutersData;
}
```
## MarketplaceRouterData

```solidity
struct MarketplaceRouterData {
  contract IEeseeMarketplaceRouter router;
  bytes data;
}
```
## ReceiveAsset

```solidity
event ReceiveAsset(struct Asset assetBought, contract IERC20 tokenSpent, uint256 spent, address recipient)
```

## swapTokensForAssets

```solidity
function swapTokensForAssets(struct IEeseeSwap.SwapParams swapParams, address recipient) external payable returns (contract IERC20 srcToken, uint256 srcSpent, struct Asset[] assets)
```


