# IEeseeRandomGelato


## FulfillRandomWords

```solidity
event FulfillRandomWords(uint256 randomWord)
```

## ChangeAutomationInterval

```solidity
event ChangeAutomationInterval(uint32 previousAutomationInterval, uint32 newAutomationInterval)
```

## CallerNotAuthorized

```solidity
error CallerNotAuthorized()
```

## InvalidAutomationInterval

```solidity
error InvalidAutomationInterval()
```

## UpkeepNotNeeded

```solidity
error UpkeepNotNeeded()
```

## CallOnTheSameBlock

```solidity
error CallOnTheSameBlock()
```

## InvalidAccessManager

```solidity
error InvalidAccessManager()
```

## InvalidOperator

```solidity
error InvalidOperator()
```

## RETURN_INTERVAL

```solidity
function RETURN_INTERVAL() external view returns (uint48)
```

## accessManager

```solidity
function accessManager() external view returns (contract IEeseeAccessManager)
```

## performUpkeepTimestamp

```solidity
function performUpkeepTimestamp() external view returns (uint64)
```

## automationInterval

```solidity
function automationInterval() external view returns (uint32)
```

## random

```solidity
function random(uint256) external view returns (uint256 word, uint256 timestamp)
```

## getRandomFromTimestamp

```solidity
function getRandomFromTimestamp(uint256 _timestamp) external view returns (uint256 word, uint256 timestamp)
```

## changeAutomationInterval

```solidity
function changeAutomationInterval(uint32 _automationInterval) external
```

## performUpkeep

```solidity
function performUpkeep() external
```


