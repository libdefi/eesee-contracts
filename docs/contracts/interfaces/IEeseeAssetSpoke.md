# IEeseeAssetSpoke


## eeseeAssetHub

```solidity
function eeseeAssetHub() external view returns (bytes chainSelector, address _address)
```

## assetsStorage

```solidity
function assetsStorage(bytes32) external view returns (address token, uint256 tokenID, uint256 amount, enum AssetType assetType, bytes data)
```

## Wrap

```solidity
event Wrap(struct Asset asset, bytes32 assetHash, address sender)
```

## Unwrap

```solidity
event Unwrap(struct Asset asset, bytes32 assetHash, address recipient)
```

## wrap

```solidity
function wrap(struct Asset[] assets, struct Call[] crosschainCalls, address fallbackRecipient, bytes additionalData) external payable returns (uint256 gasPaid)
```

## unstuck

```solidity
function unstuck(bytes32 stuckAssetHash, address recipient) external returns (struct Asset asset)
```

## getTokenIdL2

```solidity
function getTokenIdL2(struct Asset asset) external view returns (uint256)
```

## getTokenIdL2

```solidity
function getTokenIdL2(bytes32 assetHash) external view returns (uint256)
```

## getAssetHash

```solidity
function getAssetHash(struct Asset asset) external pure returns (bytes32)
```


