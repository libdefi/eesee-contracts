## eesee - NFT marketplace

# Smart Contracts

All smart contracts are located in the `./contracts` folder.

## Development

1. Create and fill in `.env` file using `.env.org` example file

2. Run `npm i` to install all packages

3. Compile all contracts before running migrations `npx hardhat compile`

4. Deploy main contracts on the desired chain with `npx hardhat run ./scripts/1_deploy.js --network <network>`

### Crosschain

5a. Deploy EeseeHub.sol by executing `npx hardhat run ./scripts/2_deployCrosschainL2.js --network <network>`.

5б.Deploy EeseeSpoke on any desired chain with `npx hardhat run ./scripts/3_deployCrosschainL1.js --network <network>`

## Testing

All tests are located in the `./test` folder. Mock contracts used in tests can be found in `./contracts/test`.

To run tests run `npm run test` in the console.

To also test GSN capabilities run
1. `npx hardhat node`

2. `npx hardhat test --network testnet`

To check coverage run `npm run coverage`

To test crosschain functionality on multiple networks:
1. Install Ganache globally with `npm install ganache --global`

2. Run the first Ganache instance with `npx ganache --port 8545  --networkId 100500 --chain.chainId 100500 --k shanghai --m "test test test test test test test test test test test junk"`

3. Run the second Ganache instance with `npx ganache --port 8546  --networkId 100501 --chain.chainId 100501 --k shanghai --m "test test test test test test test test test test test junk"`

4. `npx hardhat test --network multipleNetworks`

## Docs

Technical documentation is located at `./docs` and [`here`](https://docs.eesee.io/eeesee-contracts/).
Contracts also contain comments to all functions describing what the function does, parameters and their return values.

## Configuration

The project configuration is found in `./hardhat.config.js` folder.
